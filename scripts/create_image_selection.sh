#!/bin/bash
INPUT_DIR="/media/ImageData/NL/aerial_images/selections/LR/squares_100m/2017_rgb"
DESTINATION_DIR="/media/ImageData/NL/aerial_images/selections/LR/squares_100m/samples/ai_course/"
ORIGINAL_DIR="${DESTINATION_DIR}original/"
RESIZED_DIR="${DESTINATION_DIR}resized/"
QUANTILE_DIR="${DESTINATION_DIR}resized_quantiles/"
QUANTILE_TRAIN_TEST_DIR="${DESTINATION_DIR}quantiles_train_test/"

python create_image_dataset.py -n 3000 -d $INPUT_DIR -o $ORIGINAL_DIR  -s /media/Data/Work/CBS/Projects/deepstat/Data/Vierkant/CBS_VK100_2018_v1.shp
find $ORIGINAL_DIR -iname '*.tiff' -exec sh -c 'convert $0 -verbose -resize 40x40\> $1$(basename "$0")' {} $RESIZED_DIR \;

python create_quantiles.py -d $RESIZED_DIR -o $QUANTILE_DIR -l "${ORIGINAL_DIR}labels.csv" -n 2
python split_train_test.py -i $QUANTILE_DIR -o $QUANTILE_TRAIN_TEST_DIR
