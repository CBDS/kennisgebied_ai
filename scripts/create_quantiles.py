import argparse
import numpy as np
import csv
import shutil
import os
import progressbar
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dataset-directory", required=True,
                    help="The directory containing the images")
parser.add_argument("-l", "--label-file", required=True,
                    help="The file containing the labels")
parser.add_argument("-o", "--output-directory", required=True,
                    help="The directory containing the quantiles")
parser.add_argument("-f", "--field", default="WONING",
                    help="The field along which the dataset should be split in quantiles")
parser.add_argument("-n", "--number-of-quantiles", default=4,
                    type=int, help="The number of quantiles to calculate (default=4)")
parser.add_argument("-i", "--image_id_key", default="C28992R100",
                    help="The record in the shapefile used as id for the image file")
parser.add_argument("-is", "--image_suffix", default="_rgb_2017_lr",
                    help="The suffix for all image filenames")
args = parser.parse_args()

with open(args.label_file) as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=";")
    image_ids = []
    selected_column_values = []
    for row in csv_reader:
        value = row[args.field]
        image_ids.append(row[args.image_id_key])

        selected_column_values.append(float(value))
    quantile_ranges = np.linspace(0, 1, args.number_of_quantiles+1)[1:-1]
    quantiles = np.quantile(selected_column_values, quantile_ranges)
    print(
        f"Dividing image by {args.field} quantiles {quantile_ranges} with values: {quantiles}.")

    widgets = ["Writing record selection: ", progressbar.Percentage(
    ), " ", progressbar.Bar(), " ", progressbar.ETA()]
    pbar = progressbar.ProgressBar(
        maxval=len(image_ids), widgets=widgets).start()

    number_of_files_copied = 0
    selected_column_values = np.array(selected_column_values)
    image_ids = np.array(image_ids)
    for i in range(len(quantiles)+1):
        min_bound = -1.0 * sys.float_info.max
        if i > 0:
            min_bound = quantiles[i-1]
        elif i == len(quantiles) - 1 and len(quantiles) > 1:
            min_bound = quantiles[i]

        max_bound = quantiles[i] \
            if i < len(quantiles) else sys.float_info.max

        quantile_indices = np.where(np.logical_and(
            selected_column_values > min_bound, selected_column_values <= max_bound))

        quantile_images = image_ids[quantile_indices]

        print(
            f"quantile {i}, from {min_bound} - {max_bound} number of indices: {len(quantile_indices[0])}, number of images: {len(quantile_images)}")
        for image_id in quantile_images:
            source_file = os.path.join(args.dataset_directory, "{image_id}{suffix}.tiff".format(
                image_id=image_id, suffix=args.image_suffix))
            destination_dir = os.path.join(args.output_directory, str(i) + "/")
            if not os.path.exists(destination_dir):
                os.makedirs(destination_dir)
            shutil.copy(source_file, destination_dir)
            pbar.update(number_of_files_copied)
            number_of_files_copied += 1
    pbar.finish()
