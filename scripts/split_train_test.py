from sklearn.model_selection import train_test_split
import os
from imutils.paths import list_images
import argparse
import shutil


def copy_images(images, destination_directory):
    os.makedirs(destination_directory)
    last_destination_component = os.path.split(destination_directory)[-1]
    for image_filename in images:
        image_dir = os.path.dirname(image_filename)
        last_path_component = os.path.split(image_dir)[-1]
        dst = destination_directory
        if last_path_component != last_destination_component:
            dst = os.path.join(
                destination_directory, last_path_component)
            if not os.path.exists(dst):
                os.makedirs(dst)

        shutil.copy(image_filename, dst)


parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input-directory", required=True,
                    help="The input directory with the images")
parser.add_argument("-o", "--output-directory", required=True,
                    help="The output directory for the training and test set images")
parser.add_argument("-t", "--test-ratio", default=0.25,
                    help="The ratio of the images used for the test set")
args = parser.parse_args()

original_images = [
    image_filename for image_filename in list_images(args.input_directory)]
training_images, testing_images = train_test_split(
    original_images, test_size=args.test_ratio)

train_directory = os.path.join(args.output_directory, "train")
copy_images(training_images, train_directory)

test_directory = os.path.join(args.output_directory, "test")
copy_images(testing_images, test_directory)
