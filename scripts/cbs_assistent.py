from functools import partial
import nl_core_news_sm  # Dutch language package
from spacy import displacy
import spacy
import pandas as pd
from collections import Counter
import os
import csv
from flask import Flask, request
from flask_cors import CORS


working_dir = os.getcwd().split('\\')[:-1]
working_dir.append('data')
data_dir = '\\'.join(working_dir)

with open("../data/kwb-2018.csv", encoding="cp1252") as file:
    cbs_data = pd.read_csv(file, sep=';')

# filter 'gemeentes' only
cbs_data = cbs_data[cbs_data.recs == "Gemeente"]

cbs_column_description = {}

with open("../data/kwb-2018-toelichting.csv") as file:
    reader = csv.reader(file, delimiter=';')
    cbs_column_description = {row[0]: ' '.join(r
                                               for r in map(str.strip, row[1:])
                                               if len(r) > 0)
                              for row in reader if row[0] in cbs_data.columns}

'''
## Usefull snippits
To help implementing the questions here are some snippits that you can use.

First here is an example for using Spacy to determine the locations. Keep in mind that Spacy might not correctly identify locations depending on the sentence.
'''


language_model = nl_core_news_sm.load()  # ready the language package

'''

### Some ideas on column deduction
Selecting the correct column can be tricky. To assist you can use and build on the following pieces of code.

The concept behind the word probability matrix is as follows.

By placing words on one axis and the columns on the other axis we can create a presence matrix.
If a word X is present in a description belonging to column Y, it gets a 1 in the table at position (X,Y).

After building the presence matrix, we can compute the chances. 
Given a question with a word which occurs 4 times in the presence matrix. 
The chances of any of the columns to be correct is 1 in 4 (25%). 
For this reason we compute a probability matrix by dividing by the total occurences of each word.

'''
word_probabilities = pd.DataFrame((
    pd.Series(
        # convert discriptions to lowercase and split them to obtain words
        Counter(descr.lower().split()),
        name=column  # the name of the series is the column it belongs to
    )
    for column, descr in iter(cbs_column_description.items())
)).fillna(0)  # the matrix will fill empty places with NaN, to simplify we replace this NaN with a 0.

# now sum over the columns and divide by the sum (thus obtaining the probabilities)
word_probabilities = word_probabilities / word_probabilities.sum()


def simple_column_select(word_prob: pd.DataFrame, vraag: str) -> str:
    # convert the question to a one-hot vector
    vraag_vector = pd.Series(Counter(vraag.lower().split()))

    # suggestion: you can compute a distance to known words in the word probability using word-embeddings instead of simple one hot.
    # In this case you could look for each word in word_prob compute the embedding for it, compute the embedding for each word in the question
    # and using a cosine similarity compute the similarity (1 being an exact copy and 0 not matching).
    # you can also implement other similarity measures (# of alterations needed to go from one word to others = Levenshtein-Damereau)

    result = (word_probabilities * vraag_vector).sum(
        axis=1)  # multiply the question vector with the probability matrix and sum acros words

    # point of interest: the result table can have multiple high similar scoring columns.
    # In case you wonder why a column is returned. You can plot the result by uncommenting the next line.
    result.plot.bar(figsize=(20, 2))

    if result.max() < .75:  # we only want results with a given certainty (you can choose to alter this cut-off)
        return 'Sorry, I do not understand the question?'
    else:
        return result.idxmax()  # return one result with the highest score


# here we curry the function
# Select_column is now a function calling "simple_column_select" with as first parameter "word_probabilities".
# Therefore you no longer need to supply this matrix as parameter on calling the function.
select_column = partial(simple_column_select, word_probabilities)


def Base_Question(question: str) -> str:
    return "The answer to your ultimate question, of live, the universe and everything, is,..., 42!"


def Compare_Question(question: str) -> str:
    return "Number 5 is alive! Need input!"


def Cluster_Question(question: str) -> str:
    return "I'm sorry Dave, I'm afraid I can't do that!"


def Any_Question(question: str) -> str:
    from random import randrange
    r = randrange(3)
    if r == 0:
        return (Base_Question(question))
    elif r == 1:
        return (Compare_Question(question))
    elif r == 2:
        return (Cluster_Question(question))
    return "Does not compute!"


app = Flask(__name__)
CORS(app)


@app.route('/ask')
def ask_question():
    question = request.args.get('question', '')
    return Any_Question(question)
