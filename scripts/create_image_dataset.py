import shapefile
import argparse
import random
import os
import csv
import shutil
import progressbar

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dataset_directory",
                    required=True,
                    help="The directory containing the images")
parser.add_argument("-s", "--shapefile",
                    required=True,
                    help="The shapefile to base the selection on")
parser.add_argument("-n", "--number-of-images",
                    default=1000, type=int,
                    help="The number of images to copy (randomly)")
parser.add_argument("-o", "--output-directory",
                    required=True,
                    help="The output directory to write to")
parser.add_argument("-i", "--image_id_key", default="C28992R100",
                    help="The record in the shapefile used as id for the image file")
parser.add_argument("-is", "--image_suffix", default="_rgb_2017_lr",
                    help="The suffix for all image filenames")
parser.add_argument("-f", "--field", default="WONING",
                    help="The field along which the dataset should be split in quantiles")
parser.add_argument("-m", "--ignore-values", action="append",
                    default=["-99997"], help="The values that should be ignored.")
args = parser.parse_args()

with shapefile.Reader(args.shapefile) as shp:
    record_indices = range(shp.numRecords)
    selected_record_indices = random.sample(
        record_indices, args.number_of_images)

    widgets = ["Writing record selection: ", progressbar.Percentage(
    ), " ", progressbar.Bar(), " ", progressbar.ETA()]
    pbar = progressbar.ProgressBar(maxval=len(
        selected_record_indices), widgets=widgets)
    pbar.start()
    with open(os.path.join(args.output_directory, "labels.csv"), "w") as csv_file:
        fieldnames = [field[0] for field in shp.fields[1:]]
        field_index = fieldnames.index(args.field)
        csv_writer = csv.DictWriter(
            csv_file, fieldnames=fieldnames, delimiter=";")
        csv_writer.writeheader()
        number_of_images_written = 0
        for record_index, record in enumerate(shp.iterRecords()):
            value = record[field_index]
            if value in args.ignore_values:
                continue

            if record_index not in selected_record_indices:
                continue
            record_with_keys = {key: value for key,
                                value in zip(fieldnames, record)}
            csv_writer.writerow(record_with_keys)

            image_id = record_with_keys[args.image_id_key]
            source_file = os.path.join(args.dataset_directory, "{image_id}{suffix}.tiff".format(
                image_id=image_id, suffix=args.image_suffix))
            shutil.copy(source_file, args.output_directory)
            pbar.update(number_of_images_written)
            number_of_images_written += 1
    pbar.finish()
