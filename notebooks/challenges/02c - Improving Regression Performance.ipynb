{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Improving Regression Performance\n",
    "\n",
    "In the previous notebook, we have seen how to train a regression model to predict house prices using the `LinearRegression` class from the `sklearn.linear_model` module. We saw that the performance of such a `LinearRegression` model is not very good. In this notebook, you are going to try and improve the performance of the model by using more complex models. Using more complex models is one way of improving the performance of a model. Later in the course, you will learn about other techniques that can be used to improve the performance of a model. We begin again by loading the preprocessed data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "try:\n",
    "    import google.colab\n",
    "    IN_COLAB = True\n",
    "except:\n",
    "    IN_COLAB = False\n",
    "\n",
    "if IN_COLAB:\n",
    "    os.environ['DATA_DIRECTORY'] = '/content'\n",
    "else:\n",
    "    os.environ['DATA_DIRECTORY'] = '../../data'\n",
    "\n",
    "data_directory = os.getenv('DATA_DIRECTORY')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "if [ ! -f \"$DATA_DIRECTORY/course-data.zip\" ]; then\n",
    "    wget \"https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip\" -O \"$DATA_DIRECTORY/course-data.zip\"\n",
    "    unzip \"$DATA_DIRECTORY/course-data.zip\" -d \"$DATA_DIRECTORY\"\n",
    "fi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd \n",
    "\n",
    "house_prices_df = pd.read_csv(os.path.join(data_directory, \"house-prices/train_preprocessed.csv\"), sep= \",\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get the Labels\n",
    "\n",
    "Again we get the labels from the preprocessed data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "y_true = house_prices_df[\"SalePrice\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Remove the label column\n",
    "\n",
    "After getting the labels, we remove the label column from the preprocessed data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "house_train_df = house_prices_df.copy().drop(columns = [\"SalePrice\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Split the data into training and testing data\n",
    "\n",
    "And we split the data again into training and testing data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "train_df, test_df, y_train, y_test = train_test_split(house_train_df, y_true, test_size=0.2, random_state=42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training more complex models\n",
    "\n",
    "We are going to train more complex models to predict house prices. Below you see a Python dictionary with some commonly available regression models in the `sklearn` library. You are going to train these models and evaluate their performance.\n",
    "Each of these models has the same `fit` and `predict` methods as the `LinearRegression` model that we used in the previous notebook. Remember, to fit the model, we used the following code in the previous notebook:\n",
    "\n",
    "```python\n",
    "\n",
    "from sklearn.linear_model import LinearRegression\n",
    "\n",
    "model = LinearRegression()\n",
    "model.fit(train_df, y_train)\n",
    "\n",
    "```\n",
    "\n",
    "This passes the training data `train_df` and the training labels (the true labels or ground truth) `y_train` to the `fit` method of the model. The model then learns the relationship between the training data and the training labels. Each of the models below can be trained in the same way. \n",
    "\n",
    "To make predictions and evaluate these predictions, we used the following code:\n",
    "\n",
    "```python\n",
    "\n",
    "from sklearn.metrics import root_mean_squared_error\n",
    "\n",
    "y_pred = model.predict(test_df)\n",
    "root_mean_squared_error(y_test, y_pred)\n",
    "\n",
    "```\n",
    "\n",
    "Each of the models below can also be evalauted in the same way as you trained the `LinearRegression` model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.svm import SVR\n",
    "from sklearn.linear_model import Ridge\n",
    "from sklearn.linear_model import ElasticNet\n",
    "from sklearn.linear_model import SGDRegressor\n",
    "from sklearn.linear_model import BayesianRidge\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.ensemble import RandomForestRegressor\n",
    "\n",
    "\n",
    "models = {\n",
    "    'SVR':SVR(),\n",
    "    'Ridge':Ridge(),\n",
    "    'ElasticNet':ElasticNet(),\n",
    "    'SGDRegressor':SGDRegressor(),\n",
    "    'BayesianRidge':BayesianRidge(),\n",
    "    'LinearRegression':LinearRegression(),\n",
    "    'RandomForestRegressor':RandomForestRegressor()\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "Train each of the models in the dictionary below and evaluate the performance of each model. Use the `root_mean_squared_error` function from the `sklearn.metrics` module to evaluate the performance of each model. The `root_mean_squared_error` function takes the true labels and the predicted labels as arguments and returns the root mean squared error of the predictions.\n",
    "\n",
    "**Question:** Which model performs the best? \n",
    "\n",
    "\n",
    "**Tip**: You can use a for loop to iterate over the models in the dictionary and train and evaluate each model. You can store these results in a dataframe together with the model name.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c7dcda1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add your code and new cells here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "After training and evaluating the models, you can compare the performance of each model. You can see which model performs the best and which model performs the worst. You can also see how much better the best model performs compared to the `LinearRegression` model that you trained in the previous notebook. You also saw that `sklearn` provides a unified way of training and evaluating models. This makes it easy to train and evaluate different models and compare their performance."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "ai_course",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
