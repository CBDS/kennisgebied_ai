from nbformat.v4 import new_code_cell, new_markdown_cell, new_notebook
from typing import List
import nbformat
import argparse
import os
import tqdm


def read_notebook(notebook_path):
    with open(notebook_path, 'r') as f:
        return nbformat.read(f, as_version=4)


def write_notebook(notebook, notebook_path):
    with open(notebook_path, 'w') as f:
        nbformat.write(notebook, f)


def create_new_challenge_cell():
    code_content = "# Add your code and new cells here"
    return new_code_cell(code_content)


def filter_solution_cells(notebook, solution_start_marker, solution_end_marker):
    filtered_cells = []
    add_cell = True
    for cell in notebook.cells:
        if solution_start_marker in cell.source:
            add_cell = False
        elif solution_end_marker in cell.source:
            filtered_cells.append(create_new_challenge_cell())
            add_cell = True
        elif add_cell:
            filtered_cells.append(cell)
    notebook.cells = filtered_cells
    return notebook


def write_challenge_notebook(input_filename, output_directory, solution_start_marker, solution_end_marker):
    original_notebook = read_notebook(input_filename)
    challenges_notebook = filter_solution_cells(
        original_notebook, solution_start_marker, solution_end_marker)

    challenge_filename = os.path.join(
        output_directory, os.path.basename(input_filename))
    write_notebook(challenges_notebook, challenge_filename)


def get_solution_files(input_directory: str) -> List[str]:
    return [os.path.join(input_directory, filename)
            for filename in os.listdir(input_directory)
            if filename.endswith(".ipynb")]


parser = argparse.ArgumentParser(
    description='Generate challenges from a notebook')
parser.add_argument("-i", "--input-directory", type=str,
                    default="./solutions", help='Directory with all the solution notebooks')
parser.add_argument("-o", "--output-directory", type=str,
                    default="./challenges", help='Path to the output notebook')
parser.add_argument("-s", "--solution-start-marker", type=str,
                    default=None, help='Start marker for the solution')
parser.add_argument("-e", "--solution-end-marker", type=str,
                    default=None, help='End marker for the solution')
args = parser.parse_args()

if not os.path.exists(args.output_directory):
    os.makedirs(args.output_directory)

SOLUTION_START_MARKER = "#begin solution" if not args.solution_start_marker else args.solution_start_marker
SOLUTION_END_MARKER = "#end solution" if not args.solution_end_marker else args.solution_end_marker

for solution_file in tqdm.tqdm(get_solution_files(args.input_directory)):
    write_challenge_notebook(solution_file, args.output_directory,
                             SOLUTION_START_MARKER, SOLUTION_END_MARKER)
