# Improving Regression Performance

In the previous notebook, we have seen how to train a regression model to predict house prices using the `LinearRegression` class from the `sklearn.linear_model` module. We saw that the performance of such a `LinearRegression` model is not very good. In this notebook, you are going to try and improve the performance of the model by using more complex models. Using more complex models is one way of improving the performance of a model. Later in the course, you will learn about other techniques that can be used to improve the performance of a model. We begin again by loading the preprocessed data.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

house_prices_df = pd.read_csv(os.path.join(data_directory, "house-prices/train_preprocessed.csv"), sep= ",")
```

## Get the Labels

Again we get the labels from the preprocessed data.


```python
y_true = house_prices_df["SalePrice"]
```

## Remove the label column

After getting the labels, we remove the label column from the preprocessed data.


```python
house_train_df = house_prices_df.copy().drop(columns = ["SalePrice"])
```

## Split the data into training and testing data

And we split the data again into training and testing data.


```python
from sklearn.model_selection import train_test_split

train_df, test_df, y_train, y_test = train_test_split(house_train_df, y_true, test_size=0.2, random_state=42)
```

## Training more complex models

We are going to train more complex models to predict house prices. Below you see a Python dictionary with some commonly available regression models in the `sklearn` library. You are going to train these models and evaluate their performance.
Each of these models has the same `fit` and `predict` methods as the `LinearRegression` model that we used in the previous notebook. Remember, to fit the model, we used the following code in the previous notebook:

```python

from sklearn.linear_model import LinearRegression

model = LinearRegression()
model.fit(train_df, y_train)

```

This passes the training data `train_df` and the training labels (the true labels or ground truth) `y_train` to the `fit` method of the model. The model then learns the relationship between the training data and the training labels. Each of the models below can be trained in the same way. 

To make predictions and evaluate these predictions, we used the following code:

```python

from sklearn.metrics import root_mean_squared_error

y_pred = model.predict(test_df)
root_mean_squared_error(y_test, y_pred)

```

Each of the models below can also be evalauted in the same way as you trained the `LinearRegression` model.


```python
from sklearn.svm import SVR
from sklearn.linear_model import Ridge
from sklearn.linear_model import ElasticNet
from sklearn.linear_model import SGDRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor


models = {
    'SVR':SVR(),
    'Ridge':Ridge(),
    'ElasticNet':ElasticNet(),
    'SGDRegressor':SGDRegressor(),
    'BayesianRidge':BayesianRidge(),
    'LinearRegression':LinearRegression(),
    'RandomForestRegressor':RandomForestRegressor()
}

```

## Exercise

Train each of the models in the dictionary below and evaluate the performance of each model. Use the `root_mean_squared_error` function from the `sklearn.metrics` module to evaluate the performance of each model. The `root_mean_squared_error` function takes the true labels and the predicted labels as arguments and returns the root mean squared error of the predictions.

**Question:** Which model performs the best? 


**Tip**: You can use a for loop to iterate over the models in the dictionary and train and evaluate each model. You can store these results in a dataframe together with the model name.



```python
#begin solution
```


```python
## TODO: remove this for the copy of this notebook in the 
from sklearn.metrics import root_mean_squared_error

#taking results from the models
model_results = []
model_names = []

# training the model with function
for name, model in models.items():
    fitted_model = model.fit(train_df, y_train)
    y_pred = fitted_model.predict(test_df)
    score = root_mean_squared_error(y_test, y_pred)
    model_results.append(score)
    model_names.append(name)
    
    #creating dataframe
    df_results = pd.DataFrame([model_names,model_results])
    df_results = df_results.transpose()
    df_results = df_results.rename(columns={0:'Model',1:'RMSE'}).sort_values(by='RMSE',ascending=False)
    
print(df_results)
```

                       Model                   RMSE
    5       LinearRegression  170105783239899.28125
    0                    SVR           88626.246927
    2             ElasticNet           36260.716493
    3           SGDRegressor           31004.383924
    4          BayesianRidge           30526.296518
    6  RandomForestRegressor           29607.535407
    1                  Ridge           29237.199264



```python
#end solution
```

## Conclusion

After training and evaluating the models, you can compare the performance of each model. You can see which model performs the best and which model performs the worst. You can also see how much better the best model performs compared to the `LinearRegression` model that you trained in the previous notebook. You also saw that `sklearn` provides a unified way of training and evaluating models. This makes it easy to train and evaluate different models and compare their performance.
