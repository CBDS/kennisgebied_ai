# Training a DecisionTree algorithm on Real (CBS) data

Now we have learned about the Decision Tree algorithm, it is time to apply it to a real dataset. In this notebook, you will use a CBS dataset with statistics about municipalities and try to predict the degree of urbanization by using the features provided. As always we start by reading the dataset. 

## Read reference data: description of columns in data

The CBS dataset comes with an extra file describing the columns in the dataset. Let's read this file first.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd
 
kwb_description = pd.read_csv(os.path.join(data_directory, "kwb-2018-toelichting.csv"), delimiter=';', names=["column_name", "description", "comments"], index_col=0)
kwb_description.drop(columns=["comments"], inplace=True)
kwb_description.head()  

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
  </tbody>
</table>
</div>



We can see that for every column in the dataset, there is a description of the column. This will help us understand the dataset better.
So if for example we want to know what the column `a_hh` means, we can look it up in this file using the following code:


```python
kwb_description.loc['a_hh']
```




    description    Huishoudens totaal
    Name: a_hh, dtype: object



After reading the metadata, we can read the dataset itself. The dataset is stored in a CSV file, so we can use the `read_csv` function from the `pandas` library to read the dataset. We will also print the first few rows of the dataset to get an idea of what the dataset looks like.


```python
kwb_data = pd.read_csv(os.path.join(data_directory, "kwb-2018.csv"), delimiter=';', encoding='latin1')
kwb_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>a_bst_b</th>
      <th>a_bst_nb</th>
      <th>g_pau_hh</th>
      <th>g_pau_km</th>
      <th>a_m2w</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NL00</td>
      <td>0</td>
      <td>Nederland</td>
      <td>Nederland</td>
      <td>Land</td>
      <td>NL00</td>
      <td>.</td>
      <td>17181084</td>
      <td>8527041</td>
      <td>8654043</td>
      <td>...</td>
      <td>6649495</td>
      <td>1723750</td>
      <td>1,1</td>
      <td>249</td>
      <td>661640</td>
      <td>4154338</td>
      <td>3367109</td>
      <td>787228</td>
      <td>2</td>
      <td>1978</td>
    </tr>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>4650</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>2</th>
      <td>WK000300</td>
      <td>300</td>
      <td>Wijk 00</td>
      <td>Appingedam</td>
      <td>Wijk</td>
      <td>WK000300</td>
      <td>1</td>
      <td>11800</td>
      <td>5750</td>
      <td>6050</td>
      <td>...</td>
      <td>4645</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>3</th>
      <td>BU00030000</td>
      <td>30000</td>
      <td>Appingedam-Centrum</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030000</td>
      <td>1</td>
      <td>2355</td>
      <td>1120</td>
      <td>1235</td>
      <td>...</td>
      <td>925</td>
      <td>150</td>
      <td>0,8</td>
      <td>1275</td>
      <td>95</td>
      <td>90</td>
      <td>84</td>
      <td>5</td>
      <td>3</td>
      <td>1195</td>
    </tr>
    <tr>
      <th>4</th>
      <td>BU00030001</td>
      <td>30001</td>
      <td>Appingedam-West</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030001</td>
      <td>1</td>
      <td>3030</td>
      <td>1505</td>
      <td>1525</td>
      <td>...</td>
      <td>1305</td>
      <td>240</td>
      <td>1,2</td>
      <td>977</td>
      <td>155</td>
      <td>163</td>
      <td>158</td>
      <td>5</td>
      <td>4</td>
      <td>896</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 56 columns</p>
</div>



## Selecting the data we want to use

The dataset contains several types of regions, but we are only interested in the municipalities. We first have to find the rows that correspond to municipalities. Let's look at the metadata to find the column containing the region type.


```python
kwb_description
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>a_wat_ha</th>
      <td>Oppervlakte water</td>
    </tr>
    <tr>
      <th>pst_mvp</th>
      <td>Meest voorkomende postcode</td>
    </tr>
    <tr>
      <th>pst_dekp</th>
      <td>Dekkingspercentage</td>
    </tr>
    <tr>
      <th>ste_mvs</th>
      <td>Mate van stedelijkheid</td>
    </tr>
    <tr>
      <th>ste_oad</th>
      <td>Omgevingsadressendichtheid</td>
    </tr>
  </tbody>
</table>
<p>109 rows × 1 columns</p>
</div>



We can see here that the `recs` column contains the region type:


```python
kwb_description.loc['recs']
```




    description    Soort regio
    Name: recs, dtype: object



To see what values the `recs` column can have, we can use the `unique` function from the `pandas` library. This function returns all unique values in a column.


```python
kwb_data['recs'].unique()
```




    array(['Land', 'Gemeente', 'Wijk', 'Buurt'], dtype=object)



We can filter the dataset to only include the municipalities by selecting the rows where the `recs` column is equal to `Gemeente`. 


```python
data_for_selected_region_type = kwb_data[kwb_data.recs == 'Gemeente']
data_for_selected_region_type.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>a_bst_b</th>
      <th>a_bst_nb</th>
      <th>g_pau_hh</th>
      <th>g_pau_km</th>
      <th>a_m2w</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>4650</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>9</th>
      <td>GM0005</td>
      <td>5</td>
      <td>Bedum</td>
      <td>Bedum</td>
      <td>Gemeente</td>
      <td>GM0005</td>
      <td>.</td>
      <td>10475</td>
      <td>5235</td>
      <td>5240</td>
      <td>...</td>
      <td>4045</td>
      <td>965</td>
      <td>1,1</td>
      <td>112</td>
      <td>515</td>
      <td>4496</td>
      <td>4454</td>
      <td>41</td>
      <td>4</td>
      <td>685</td>
    </tr>
    <tr>
      <th>19</th>
      <td>GM0009</td>
      <td>9</td>
      <td>Ten Boer</td>
      <td>Ten Boer</td>
      <td>Gemeente</td>
      <td>GM0009</td>
      <td>.</td>
      <td>7292</td>
      <td>3616</td>
      <td>3676</td>
      <td>...</td>
      <td>3025</td>
      <td>710</td>
      <td>1,3</td>
      <td>82</td>
      <td>445</td>
      <td>4573</td>
      <td>4531</td>
      <td>42</td>
      <td>5</td>
      <td>372</td>
    </tr>
    <tr>
      <th>34</th>
      <td>GM0010</td>
      <td>10</td>
      <td>Delfzijl</td>
      <td>Delfzijl</td>
      <td>Gemeente</td>
      <td>GM0010</td>
      <td>.</td>
      <td>24864</td>
      <td>12475</td>
      <td>12389</td>
      <td>...</td>
      <td>10220</td>
      <td>2280</td>
      <td>1,1</td>
      <td>94</td>
      <td>1285</td>
      <td>22750</td>
      <td>13307</td>
      <td>9443</td>
      <td>4</td>
      <td>677</td>
    </tr>
    <tr>
      <th>64</th>
      <td>GM0014</td>
      <td>14</td>
      <td>Groningen</td>
      <td>Groningen</td>
      <td>Gemeente</td>
      <td>GM0014</td>
      <td>.</td>
      <td>202810</td>
      <td>101299</td>
      <td>101511</td>
      <td>...</td>
      <td>53560</td>
      <td>19295</td>
      <td>0,6</td>
      <td>768</td>
      <td>5255</td>
      <td>10150</td>
      <td>9492</td>
      <td>658</td>
      <td>1</td>
      <td>3456</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 56 columns</p>
</div>



Finally, we can also check how many municipalities are in the dataset.


```python
len(data_for_selected_region_type)
```




    380



The filtered dataset contains 380 municipalities. The data is from 2018, and indeed that number is correct. 

## Get the Labels

Next, it is time to get our features and target variable. The target variable is the degree of urbanization, which is stored in the `ste_mvs` column. The features can be the other columns in the dataset. We can retrieve the target variable by using the following code:


```python
y_true = data_for_selected_region_type[['ste_mvs']]
```

Now the variable `y_true` contains the target variable. In this case, the target variable is the degree of urbanization. CBS classifies the degree of urbanization into five classes. Based on the environmental address density, each neighborhood, district, or municipality has been assigned an urbanization class. The following classification has been used:

1: very strongly urbanized >= 2,500 addresses per km²

2: strongly urbanized 1,500 - 2,500 addresses per km²

3: moderately urbanized 1,000 - 1,500 addresses per km²

4: weakly urbanized 500 - 1,000 addresses per km²

5: non-urbanized < 500 addresses per km²

Let's see how many municipalities are in each class.


```python
target_name = ['ste_mvs']
target_categories = ['very strongly urbanized', 'strongly urbanized', 'moderately urbanized', 'weakly urbanized', 'non-urbanized']
y_true[target_name].value_counts().sort_index()
```




    ste_mvs
    1           19
    2           74
    3           78
    4          135
    5           74
    Name: count, dtype: int64



Most municipalities are classified as weakly urbanized.

## Selecting some features

The dataset contains a lot of characteristics per municipality. Not all of these characteristics are useful for predicting the degree of urbanization. We can select a subset of the features that we think are most relevant. For example, we can select the area of the municipality in hectares and the number of households as features:


```python
feature_names = ['a_opp_ha','a_hh']
X = data_for_selected_region_type[feature_names]
X.head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a_opp_ha</th>
      <th>a_hh</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>2458</td>
      <td>5540</td>
    </tr>
    <tr>
      <th>9</th>
      <td>4496</td>
      <td>4391</td>
    </tr>
    <tr>
      <th>19</th>
      <td>4573</td>
      <td>2980</td>
    </tr>
    <tr>
      <th>34</th>
      <td>22750</td>
      <td>11617</td>
    </tr>
    <tr>
      <th>64</th>
      <td>10150</td>
      <td>122541</td>
    </tr>
  </tbody>
</table>
</div>



## Split the Data into training and testing data

Now that we have our features and target variable, we can split the data into training and testing data. We will use 80% of the data for training and 20% for testing. We can use the `train_test_split` function from the `sklearn` library to split the data.


```python
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y_true, random_state=0)
```

## Train the Model

Once we have split the data we can train the Decision Tree model. We can use the `DecisionTreeClassifier` class from the `sklearn` library to create a Decision Tree model. We can then use the `fit` method to train the model on the training data.


```python
from sklearn.tree import DecisionTreeClassifier

decision_tree = DecisionTreeClassifier(max_depth = 10, min_samples_leaf = 1)
decision_tree.fit(X_train, y_train)
```




<style>#sk-container-id-1 {
  /* Definition of color scheme common for light and dark mode */
  --sklearn-color-text: black;
  --sklearn-color-line: gray;
  /* Definition of color scheme for unfitted estimators */
  --sklearn-color-unfitted-level-0: #fff5e6;
  --sklearn-color-unfitted-level-1: #f6e4d2;
  --sklearn-color-unfitted-level-2: #ffe0b3;
  --sklearn-color-unfitted-level-3: chocolate;
  /* Definition of color scheme for fitted estimators */
  --sklearn-color-fitted-level-0: #f0f8ff;
  --sklearn-color-fitted-level-1: #d4ebff;
  --sklearn-color-fitted-level-2: #b3dbfd;
  --sklearn-color-fitted-level-3: cornflowerblue;

  /* Specific color for light theme */
  --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, white)));
  --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-icon: #696969;

  @media (prefers-color-scheme: dark) {
    /* Redefinition of color scheme for dark theme */
    --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, #111)));
    --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-icon: #878787;
  }
}

#sk-container-id-1 {
  color: var(--sklearn-color-text);
}

#sk-container-id-1 pre {
  padding: 0;
}

#sk-container-id-1 input.sk-hidden--visually {
  border: 0;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}

#sk-container-id-1 div.sk-dashed-wrapped {
  border: 1px dashed var(--sklearn-color-line);
  margin: 0 0.4em 0.5em 0.4em;
  box-sizing: border-box;
  padding-bottom: 0.4em;
  background-color: var(--sklearn-color-background);
}

#sk-container-id-1 div.sk-container {
  /* jupyter's `normalize.less` sets `[hidden] { display: none; }`
     but bootstrap.min.css set `[hidden] { display: none !important; }`
     so we also need the `!important` here to be able to override the
     default hidden behavior on the sphinx rendered scikit-learn.org.
     See: https://github.com/scikit-learn/scikit-learn/issues/21755 */
  display: inline-block !important;
  position: relative;
}

#sk-container-id-1 div.sk-text-repr-fallback {
  display: none;
}

div.sk-parallel-item,
div.sk-serial,
div.sk-item {
  /* draw centered vertical line to link estimators */
  background-image: linear-gradient(var(--sklearn-color-text-on-default-background), var(--sklearn-color-text-on-default-background));
  background-size: 2px 100%;
  background-repeat: no-repeat;
  background-position: center center;
}

/* Parallel-specific style estimator block */

#sk-container-id-1 div.sk-parallel-item::after {
  content: "";
  width: 100%;
  border-bottom: 2px solid var(--sklearn-color-text-on-default-background);
  flex-grow: 1;
}

#sk-container-id-1 div.sk-parallel {
  display: flex;
  align-items: stretch;
  justify-content: center;
  background-color: var(--sklearn-color-background);
  position: relative;
}

#sk-container-id-1 div.sk-parallel-item {
  display: flex;
  flex-direction: column;
}

#sk-container-id-1 div.sk-parallel-item:first-child::after {
  align-self: flex-end;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:last-child::after {
  align-self: flex-start;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:only-child::after {
  width: 0;
}

/* Serial-specific style estimator block */

#sk-container-id-1 div.sk-serial {
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--sklearn-color-background);
  padding-right: 1em;
  padding-left: 1em;
}


/* Toggleable style: style used for estimator/Pipeline/ColumnTransformer box that is
clickable and can be expanded/collapsed.
- Pipeline and ColumnTransformer use this feature and define the default style
- Estimators will overwrite some part of the style using the `sk-estimator` class
*/

/* Pipeline and ColumnTransformer style (default) */

#sk-container-id-1 div.sk-toggleable {
  /* Default theme specific background. It is overwritten whether we have a
  specific estimator or a Pipeline/ColumnTransformer */
  background-color: var(--sklearn-color-background);
}

/* Toggleable label */
#sk-container-id-1 label.sk-toggleable__label {
  cursor: pointer;
  display: block;
  width: 100%;
  margin-bottom: 0;
  padding: 0.5em;
  box-sizing: border-box;
  text-align: center;
}

#sk-container-id-1 label.sk-toggleable__label-arrow:before {
  /* Arrow on the left of the label */
  content: "▸";
  float: left;
  margin-right: 0.25em;
  color: var(--sklearn-color-icon);
}

#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {
  color: var(--sklearn-color-text);
}

/* Toggleable content - dropdown */

#sk-container-id-1 div.sk-toggleable__content {
  max-height: 0;
  max-width: 0;
  overflow: hidden;
  text-align: left;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content pre {
  margin: 0.2em;
  border-radius: 0.25em;
  color: var(--sklearn-color-text);
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted pre {
  /* unfitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {
  /* Expand drop-down */
  max-height: 200px;
  max-width: 100%;
  overflow: auto;
}

#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {
  content: "▾";
}

/* Pipeline/ColumnTransformer-specific style */

#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-label.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator-specific style */

/* Colorize estimator box */
#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

#sk-container-id-1 div.sk-label label.sk-toggleable__label,
#sk-container-id-1 div.sk-label label {
  /* The background is the default theme color */
  color: var(--sklearn-color-text-on-default-background);
}

/* On hover, darken the color of the background */
#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

/* Label box, darken color on hover, fitted */
#sk-container-id-1 div.sk-label.fitted:hover label.sk-toggleable__label.fitted {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator label */

#sk-container-id-1 div.sk-label label {
  font-family: monospace;
  font-weight: bold;
  display: inline-block;
  line-height: 1.2em;
}

#sk-container-id-1 div.sk-label-container {
  text-align: center;
}

/* Estimator-specific */
#sk-container-id-1 div.sk-estimator {
  font-family: monospace;
  border: 1px dotted var(--sklearn-color-border-box);
  border-radius: 0.25em;
  box-sizing: border-box;
  margin-bottom: 0.5em;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-estimator.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

/* on hover */
#sk-container-id-1 div.sk-estimator:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Specification for estimator info (e.g. "i" and "?") */

/* Common style for "i" and "?" */

.sk-estimator-doc-link,
a:link.sk-estimator-doc-link,
a:visited.sk-estimator-doc-link {
  float: right;
  font-size: smaller;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1em;
  height: 1em;
  width: 1em;
  text-decoration: none !important;
  margin-left: 1ex;
  /* unfitted */
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
  color: var(--sklearn-color-unfitted-level-1);
}

.sk-estimator-doc-link.fitted,
a:link.sk-estimator-doc-link.fitted,
a:visited.sk-estimator-doc-link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
div.sk-estimator:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover,
div.sk-label-container:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

div.sk-estimator.fitted:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover,
div.sk-label-container:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

/* Span, style for the box shown on hovering the info icon */
.sk-estimator-doc-link span {
  display: none;
  z-index: 9999;
  position: relative;
  font-weight: normal;
  right: .2ex;
  padding: .5ex;
  margin: .5ex;
  width: min-content;
  min-width: 20ex;
  max-width: 50ex;
  color: var(--sklearn-color-text);
  box-shadow: 2pt 2pt 4pt #999;
  /* unfitted */
  background: var(--sklearn-color-unfitted-level-0);
  border: .5pt solid var(--sklearn-color-unfitted-level-3);
}

.sk-estimator-doc-link.fitted span {
  /* fitted */
  background: var(--sklearn-color-fitted-level-0);
  border: var(--sklearn-color-fitted-level-3);
}

.sk-estimator-doc-link:hover span {
  display: block;
}

/* "?"-specific style due to the `<a>` HTML tag */

#sk-container-id-1 a.estimator_doc_link {
  float: right;
  font-size: 1rem;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1rem;
  height: 1rem;
  width: 1rem;
  text-decoration: none;
  /* unfitted */
  color: var(--sklearn-color-unfitted-level-1);
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
}

#sk-container-id-1 a.estimator_doc_link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
#sk-container-id-1 a.estimator_doc_link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

#sk-container-id-1 a.estimator_doc_link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
}
</style><div id="sk-container-id-1" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>DecisionTreeClassifier(max_depth=10)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator fitted sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-1" type="checkbox" checked><label for="sk-estimator-id-1" class="sk-toggleable__label fitted sk-toggleable__label-arrow fitted">&nbsp;&nbsp;DecisionTreeClassifier<a class="sk-estimator-doc-link fitted" rel="noreferrer" target="_blank" href="https://scikit-learn.org/1.5/modules/generated/sklearn.tree.DecisionTreeClassifier.html">?<span>Documentation for DecisionTreeClassifier</span></a><span class="sk-estimator-doc-link fitted">i<span>Fitted</span></span></label><div class="sk-toggleable__content fitted"><pre>DecisionTreeClassifier(max_depth=10)</pre></div> </div></div></div></div>



## Visualize the model

After training the model, we can visualize the Decision Tree. Decision Trees are easy to interpret and visualize. However, as you will see, the Decision Tree for this dataset is quite large and therefore may be more difficult to interpret. Still, the visualization is more intuitive than a visualization of the Multi-Layer Perceptron that we saw in previous notebooks.


```python
from sklearn.tree import export_graphviz
import graphviz


dot_data = export_graphviz(decision_tree, 
                            out_file=None,
                            feature_names=feature_names,
                            class_names=target_categories,
                            filled=True, rounded=True,
                            special_characters=True)

graph = graphviz.Source(dot_data)
graph

graph.render(os.path.join(data_directory, 'decision_tree'), view=True) # for printing to pdf
```




    '../decision_tree.pdf'



## Evaluate the Model

To evaluate the model, we will use the `classification_report` function from the `sklearn.metrics` module. As we saw, this function calculates various metrics that are useful for evaluating the performance of a classification model. We evaluate the model on the test data.


```python
from sklearn.metrics import classification_report

y_pred = decision_tree.predict(X_test)

print(classification_report(y_test, y_pred, target_names=target_categories))
```

                             precision    recall  f1-score   support
    
    very strongly urbanized       1.00      0.33      0.50         3
         strongly urbanized       0.64      0.78      0.70        18
       moderately urbanized       0.26      0.29      0.28        17
           weakly urbanized       0.66      0.49      0.56        39
              non-urbanized       0.46      0.61      0.52        18
    
                   accuracy                           0.53        95
                  macro avg       0.60      0.50      0.51        95
               weighted avg       0.56      0.53      0.53        95
    


In this example, we see that there are more than two labels. As opposed to the Titanic dataset, we now have a multi-class classification problem. The classification report shows the precision, recall, and F1-score for each class. The support column shows the number of samples for each class. To know if the model is performing well, and outperforming a random guess, we can use the `DummyClassifier` class from the `sklearn` library (see the documentation [here](https://scikit-learn.org/dev/modules/generated/sklearn.dummy.DummyClassifier.html)). This class provides several strategies for making predictions. Here, we are using the `stratified` strategy; this strategy generates random predictions by respecting the training set's class distribution. By comparing the predictions of our Decision Tree model with the performance of the `DummyClassifier`, we can test whether the model has learned to predict anything.


```python
from sklearn.dummy import DummyClassifier

dummy = DummyClassifier(strategy='stratified')
dummy.fit(X_train, y_train)

y_pred = dummy.predict(X_test)
print(classification_report(y_test, y_pred, target_names=target_categories))
```

                             precision    recall  f1-score   support
    
    very strongly urbanized       0.00      0.00      0.00         3
         strongly urbanized       0.14      0.17      0.15        18
       moderately urbanized       0.13      0.18      0.15        17
           weakly urbanized       0.37      0.28      0.32        39
              non-urbanized       0.17      0.17      0.17        18
    
                   accuracy                           0.21        95
                  macro avg       0.16      0.16      0.16        95
               weighted avg       0.23      0.21      0.22        95
    


We see that our `DecisionTree` model outperforms the `DummyClassifier` model. The Decision Tree model has a higher precision, recall, and F1-score for all classes. This means that the Decision Tree model has learned to predict the degree of urbanization better than a random guess. However, we can see the model can still be improved. We leave it as an exercise for you to try to improve the model by selecting different features or tuning the hyperparameters of the Decision Tree model.

## Example: Creating Derived Features

One way to improve the model is to create new features from the existing features. For example, we can calculate the population density by dividing the number of inhabitants by the area of the municipality. We can then use this new feature as input for the Decision Tree model. Let's see how we can do this:


```python
# a_opp_ha is totale oppervlakte in hele hectaren.
# een km2 is 100 hectaren
# data: inwoners, huishoudens
# 1: zeer sterk stedelijk >= 2 500 adressen per km²
# 2: sterk stedelijk 1 500 - 2 500 adressen per km²
# 3: matig stedelijk 1 000 - 1 500 adressen per km²
# 4: weinig stedelijk 500 - 1 000 adressen per km²
# 5: niet stedelijk < 500 adressen per km²

data_for_selected_region_type['a_hh_p_km2'] = data_for_selected_region_type['a_hh'] / (data_for_selected_region_type['a_opp_ha']/100)
data_for_selected_region_type[['a_hh_p_km2','a_hh','a_opp_ha']].head(5)
```

    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_27891/3336679801.py:10: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      data_for_selected_region_type['a_hh_p_km2'] = data_for_selected_region_type['a_hh'] / (data_for_selected_region_type['a_opp_ha']/100)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a_hh_p_km2</th>
      <th>a_hh</th>
      <th>a_opp_ha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>225.386493</td>
      <td>5540</td>
      <td>2458</td>
    </tr>
    <tr>
      <th>9</th>
      <td>97.664591</td>
      <td>4391</td>
      <td>4496</td>
    </tr>
    <tr>
      <th>19</th>
      <td>65.165099</td>
      <td>2980</td>
      <td>4573</td>
    </tr>
    <tr>
      <th>34</th>
      <td>51.063736</td>
      <td>11617</td>
      <td>22750</td>
    </tr>
    <tr>
      <th>64</th>
      <td>1207.300493</td>
      <td>122541</td>
      <td>10150</td>
    </tr>
  </tbody>
</table>
</div>




```python
from sklearn.model_selection import train_test_split

feature_names = ['a_hh_p_km2']
X = data_for_selected_region_type[feature_names]
X_train, X_test, y_train, y_test = train_test_split(X, y_true, random_state=0)
X.shape, X_train.shape, X_test.shape, y_train.shape, y_test.shape
```




    ((380, 1), (285, 1), (95, 1), (285, 1), (95, 1))




```python
# all in one cell
feature_names = ['a_hh_p_km2']

decision_tree = DecisionTreeClassifier(max_depth = 4, min_samples_leaf = 50)
decision_tree.fit(X_train, y_train)

y_pred = decision_tree.predict(X_test)
print(classification_report(y_test, y_pred, target_names=target_categories))
```

                             precision    recall  f1-score   support
    
    very strongly urbanized       0.00      0.00      0.00         3
         strongly urbanized       0.67      0.56      0.61        18
       moderately urbanized       0.43      0.71      0.53        17
           weakly urbanized       0.74      0.74      0.74        39
              non-urbanized       0.77      0.56      0.65        18
    
                   accuracy                           0.64        95
                  macro avg       0.52      0.51      0.51        95
               weighted avg       0.65      0.64      0.64        95
    


    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/sklearn/metrics/_classification.py:1531: UndefinedMetricWarning: Precision is ill-defined and being set to 0.0 in labels with no predicted samples. Use `zero_division` parameter to control this behavior.
      _warn_prf(average, modifier, f"{metric.capitalize()} is", len(result))
    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/sklearn/metrics/_classification.py:1531: UndefinedMetricWarning: Precision is ill-defined and being set to 0.0 in labels with no predicted samples. Use `zero_division` parameter to control this behavior.
      _warn_prf(average, modifier, f"{metric.capitalize()} is", len(result))
    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/sklearn/metrics/_classification.py:1531: UndefinedMetricWarning: Precision is ill-defined and being set to 0.0 in labels with no predicted samples. Use `zero_division` parameter to control this behavior.
      _warn_prf(average, modifier, f"{metric.capitalize()} is", len(result))



```python
dot_data = export_graphviz(decision_tree, out_file=None,
                      feature_names=feature_names,
                      class_names=target_categories,
                      filled=True, rounded=True,
                      special_characters=True)

graph = graphviz.Source(dot_data)
graph
```




    
![svg](05b%20-%20Training%20a%20Decision%20Tree%20on%20CBS%20data_files/05b%20-%20Training%20a%20Decision%20Tree%20on%20CBS%20data_41_0.svg)
    



Now try to add more features to the model and see if you can improve the performance of the model.


```python
#begin solution
```


```python
# # data: inwoners, huishoudens
# # 1: zeer sterk stedelijk >= 2 500 adressen per km²
# # 2: sterk stedelijk 1 500 - 2 500 adressen per km²
# # 3: matig stedelijk 1 000 - 1 500 adressen per km²
# # 4: weinig stedelijk 500 - 1 000 adressen per km²
# # 5: niet stedelijk < 500 adressen per km²

# data_for_selected_region_type[data_for_selected_region_type['a_hh_p_km2']<500][['gm_naam','recs','a_woning','a_hh','a_opp_ha','a_hh_p_km2','ste_mvs']]
```


```python
# data_for_selected_region_type[data_for_selected_region_type['ste_mvs']=='3'][['gm_naam','recs','a_woning','a_hh','a_lan_ha','a_opp_ha','a_hh_p_km2','ste_mvs']]
```


```python
#end solution
```
