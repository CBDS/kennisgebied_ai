# LLM encoders in Practice

With text an often encountered issue is finding similar text.
In this notebook, you will use encoders to solve this issue for you.

First lets get the data which you can use by default.
You can use other sources if you prefer.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```

With the data downloaded, we first need the package to load the transformers.


```python
!pip install sentence-transformers
```

    Collecting sentence-transformers
      Downloading sentence-transformers-2.2.2.tar.gz (85 kB)
    [?25l     [90m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━[0m [32m0.0/86.0 kB[0m [31m?[0m eta [36m-:--:--[0m[2K     [91m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━[0m[90m╺[0m[90m━[0m [32m81.9/86.0 kB[0m [31m2.6 MB/s[0m eta [36m0:00:01[0m[2K     [90m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━[0m [32m86.0/86.0 kB[0m [31m2.2 MB/s[0m eta [36m0:00:00[0m
    [?25h  Preparing metadata (setup.py) ... [?25l[?25hdone
    Requirement already satisfied: transformers<5.0.0,>=4.6.0 in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (4.35.2)
    Requirement already satisfied: tqdm in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (4.66.1)
    Requirement already satisfied: torch>=1.6.0 in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (2.1.0+cu118)
    Requirement already satisfied: torchvision in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (0.16.0+cu118)
    Requirement already satisfied: numpy in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (1.23.5)
    Requirement already satisfied: scikit-learn in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (1.2.2)
    Requirement already satisfied: scipy in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (1.11.3)
    Requirement already satisfied: nltk in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (3.8.1)
    Collecting sentencepiece (from sentence-transformers)
      Downloading sentencepiece-0.1.99-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl (1.3 MB)
    [2K     [90m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━[0m [32m1.3/1.3 MB[0m [31m23.2 MB/s[0m eta [36m0:00:00[0m
    [?25hRequirement already satisfied: huggingface-hub>=0.4.0 in /usr/local/lib/python3.10/dist-packages (from sentence-transformers) (0.19.4)
    Requirement already satisfied: filelock in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (3.13.1)
    Requirement already satisfied: fsspec>=2023.5.0 in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (2023.6.0)
    Requirement already satisfied: requests in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (2.31.0)
    Requirement already satisfied: pyyaml>=5.1 in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (6.0.1)
    Requirement already satisfied: typing-extensions>=3.7.4.3 in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (4.5.0)
    Requirement already satisfied: packaging>=20.9 in /usr/local/lib/python3.10/dist-packages (from huggingface-hub>=0.4.0->sentence-transformers) (23.2)
    Requirement already satisfied: sympy in /usr/local/lib/python3.10/dist-packages (from torch>=1.6.0->sentence-transformers) (1.12)
    Requirement already satisfied: networkx in /usr/local/lib/python3.10/dist-packages (from torch>=1.6.0->sentence-transformers) (3.2.1)
    Requirement already satisfied: jinja2 in /usr/local/lib/python3.10/dist-packages (from torch>=1.6.0->sentence-transformers) (3.1.2)
    Requirement already satisfied: triton==2.1.0 in /usr/local/lib/python3.10/dist-packages (from torch>=1.6.0->sentence-transformers) (2.1.0)
    Requirement already satisfied: regex!=2019.12.17 in /usr/local/lib/python3.10/dist-packages (from transformers<5.0.0,>=4.6.0->sentence-transformers) (2023.6.3)
    Requirement already satisfied: tokenizers<0.19,>=0.14 in /usr/local/lib/python3.10/dist-packages (from transformers<5.0.0,>=4.6.0->sentence-transformers) (0.15.0)
    Requirement already satisfied: safetensors>=0.3.1 in /usr/local/lib/python3.10/dist-packages (from transformers<5.0.0,>=4.6.0->sentence-transformers) (0.4.0)
    Requirement already satisfied: click in /usr/local/lib/python3.10/dist-packages (from nltk->sentence-transformers) (8.1.7)
    Requirement already satisfied: joblib in /usr/local/lib/python3.10/dist-packages (from nltk->sentence-transformers) (1.3.2)
    Requirement already satisfied: threadpoolctl>=2.0.0 in /usr/local/lib/python3.10/dist-packages (from scikit-learn->sentence-transformers) (3.2.0)
    Requirement already satisfied: pillow!=8.3.*,>=5.3.0 in /usr/local/lib/python3.10/dist-packages (from torchvision->sentence-transformers) (9.4.0)
    Requirement already satisfied: MarkupSafe>=2.0 in /usr/local/lib/python3.10/dist-packages (from jinja2->torch>=1.6.0->sentence-transformers) (2.1.3)
    Requirement already satisfied: charset-normalizer<4,>=2 in /usr/local/lib/python3.10/dist-packages (from requests->huggingface-hub>=0.4.0->sentence-transformers) (3.3.2)
    Requirement already satisfied: idna<4,>=2.5 in /usr/local/lib/python3.10/dist-packages (from requests->huggingface-hub>=0.4.0->sentence-transformers) (3.4)
    Requirement already satisfied: urllib3<3,>=1.21.1 in /usr/local/lib/python3.10/dist-packages (from requests->huggingface-hub>=0.4.0->sentence-transformers) (2.0.7)
    Requirement already satisfied: certifi>=2017.4.17 in /usr/local/lib/python3.10/dist-packages (from requests->huggingface-hub>=0.4.0->sentence-transformers) (2023.7.22)
    Requirement already satisfied: mpmath>=0.19 in /usr/local/lib/python3.10/dist-packages (from sympy->torch>=1.6.0->sentence-transformers) (1.3.0)
    Building wheels for collected packages: sentence-transformers
      Building wheel for sentence-transformers (setup.py) ... [?25l[?25hdone
      Created wheel for sentence-transformers: filename=sentence_transformers-2.2.2-py3-none-any.whl size=125923 sha256=254e0447012ad7eaca7b1a18377f631811d0bd77b97aeadd80a604fc561a8977
      Stored in directory: /root/.cache/pip/wheels/62/f2/10/1e606fd5f02395388f74e7462910fe851042f97238cbbd902f
    Successfully built sentence-transformers
    Installing collected packages: sentencepiece, sentence-transformers
    Successfully installed sentence-transformers-2.2.2 sentencepiece-0.1.99



```python
from typing import Collection, Callable

import numpy as np
from functools import cache, partial

from sentence_transformers import SentenceTransformer


# this implementation caches all data sent to it. Thus reducing calculation time drastically
@cache
def encode_sentence(text:str,  model: SentenceTransformer)-> np.ndarray:
    """
    Encodes a sentence into a vector.
    The results are cached to ensure a better performance for multiple tests.
    :param text: The text to be converted into an array.
    :return: The vector array for the sentence
    """
    return model.encode(text)


def cosine_sym(vector_a:np.ndarray, vector_b: np.ndarray) -> float:
    """
    Computes the similarity between two vectors.
    :param vector_a: First vector
    :param vector_b: Second vector
    :return: a value between [-1, 1] with 1 being a high similarity and -1 as dissimilarity
    """
    # in case something went wrong, and either a or b is None, the value is set at -1
    if vector_a is None or vector_b is None:
        return -1
    answer = -1.
    # in case a vector consists of only 0's the cosine sym cannot be computed, and as such this safeguard is put in place
    if round(sum(vector_a) * sum(vector_b), 2) != 0.00:
        answer = float(np.dot(vector_a, vector_b) / (np.linalg.norm(vector_a) * np.linalg.norm(vector_b)))
    return answer


def compare_sentence(text_a: str, text_b: str, encode:Callable[[str], np.ndarray]) -> float:
    """
    Compares 2 sentences using an encoding scheme given to this function.
    """
    return cosine_sym(
        vector_a = encode(text_a),
        vector_b = encode(text_b)
    )



def classify_text(text:str, corpus:Collection[str], compare_text: Callable[[str,str], float]) -> str:
    """
    Function which scans 1 or more of the file from the Data
    directory and returns the best fitting text.
    :param text: The text to compare against the available sentences
    :param corpus: The sentences available for comparisson
    :param compare_text: Function to compare two sentences
    :return: the best fitting sentence from the given text
    """
    # create a tuple of the similarity and the sentence from the corpus
    sentence_similarities = ((compare_text(text, sentence), sentence) for sentence in iter(corpus))
    # retrieve the highest similarity
    best_hit = max(sentence_similarities)
    # return the text of the best tuple
    return best_hit[1]

```

## Load an Encoder

In the following part we download the LaBSE encoder.
After downloading and setting up we bind it into the other functions.
This enables calling the functions and testing their output (if you'd want to).
There is also a simple example of how it can be called in the classify call.


```python
# First retrieve the model from huggingface
LabseModel = SentenceTransformer("sentence-transformers/LaBSE")

# Now create a function on labse for encoding, comparing and classification*
LabseEncode = partial(encode_sentence, model=LabseModel)
LabseCompare = partial(compare_sentence, encode=LabseEncode)
LabseClassify = partial(classify_text, compare_text=LabseCompare)

# * The partial call alters a function by pre-filling a variable.
# For instance a function F(a,b) can be simplified using partial by writing:
# G = partial(F, b=1)
# Now every time you call G with an "a" it is the equivalent of calling F(a, 1) as the value of b is set to 1 in the partial call.
```


```python
# You can change sentences to what youd like.
LabseClassify("Dit is een simpele zin.",
                ["Deze zin is eenvoudig.",
                 "This is a simple sentence.",
                 "De kat krabt de krullen van de trap."])
```




    'This is a simple sentence.'



# Challenge
Now given the example above and having loaded the data.
The goal is to load the text into a format for the classify function.
And setup a function which allows you to find the best fitting sentence in a book given your input.


For this challenge you can also alter the encoder.
LaBSE (the example) is a model with good multi-language capabilities.
However, in the domain of English, there are better models.
You can find them at:
https://huggingface.co/models?pipeline_tag=sentence-similarity&sort=trending



```python

```
