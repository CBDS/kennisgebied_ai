# Python Basics
In this course on Machine Learning, some lesser-known functions and options from Python are used.
These are chosen due to either their performance or brevity.
This notebook shows and explains these operations.
Other packages and functions are explained during the course.

## Generators & Comprehensions
Comprehensions and Generators are very fast and memory efficient under Python.
The main downside is that they can be tricky to wrap your head around at first.
That is why this notebook might be a bit on the slow side.

First off we have generators.
A generator is a pattern where you define a series that can be iterated over once.
For instance:


```python
generator_a = (i for i in range(5))
```

This generator_a can be iterated over once.


```python
for value in generator_a:
    print(value)
print("End of generator")

print("Testing for additional data in generator.")
is_value = False
for value in generator_a:
    print(value)
    is_value = True
if is_value:
    print("Yikes, there was data")
else:
    print("As expected, nothing to see") 
```

    0
    1
    2
    3
    4
    End of generator
    Testing for additional data in generator.
    As expected, nothing to see


As you can see in the example a generator follows the pattern:
( \<value> \<iterable of values> )

From this pattern there are 3 components which can be altered: The brackets, The value, and the Iterable.
### Brackets
First lets go into the brackets.
It is possible to switch the ( ) with [ ].
The result is what is referred to as a List Comprehension, it produces a list.


```python
[i for i in range(5)]
```




    [0, 1, 2, 3, 4]



Another option is to use { }, this can be used to create a set or a dictionary.


```python
set_of_values = {i for i in range(5)}
print(set_of_values, type(set_of_values))

dictionary_of_value = {i: i*2 for i in range(5)}
print(dictionary_of_value, type(dictionary_of_value))
```

    {0, 1, 2, 3, 4} <class 'set'>
    {0: 0, 1: 2, 2: 4, 3: 6, 4: 8} <class 'dict'>


Each of these Comprehensions is a shorthand for having a generator and casting it to a type of collection.
In the main course you will encounter the different comprehensions.

### \< Value >
The value can also be a function call which is performed multiple times:


```python
# this "-> int " is how a return type is defined. Python does not require it, but it can make code easier to understand
def f() -> int: 
    return 42

[f() for i in range(5)]
```




    [42, 42, 42, 42, 42]



And it can also be used to pass the variable you are moving over to be passed to it.


```python
# Just as in the previous example there is a type definition in this function call
# it is the "v: int" , this defines v as having to be an integer. Again this is not required, but sometimes makes it easier to understand.
def f2(v: int) -> int:
    return v*v
[f2(i) for i in range(5)]
```




    [0, 1, 4, 9, 16]



The value statement can be any expression.

### \<iterable of values>
The iterable is where things might get more complicated.
In the examples so far, we only looked at a single for loop.
Consider the following example:


```python
print([(i,j) for i in range(5) for j in range(3)])
```

    [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2), (4, 0), (4, 1), (4, 2)]


The code above could be written in a normal format as follows:


```python
answer = list()
for i in range(5):
    for j in range(3):
        answer.append( (i,j) )
print(answer)
```

    [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2), (3, 0), (3, 1), (3, 2), (4, 0), (4, 1), (4, 2)]


Next to chaining multiple for statements it is also possible to remove or select (filter) values:


```python
print(
    [(i,j)
     for i in range(5)
     for j in range(3)
     if i!=j
    ]
     )
```

    [(0, 1), (0, 2), (1, 0), (1, 2), (2, 0), (2, 1), (3, 0), (3, 1), (3, 2), (4, 0), (4, 1), (4, 2)]


As you see, this conditional limits what is returned in the final list.

## Lambda
Lambda expressions, also known as nameless functions can be a usefull tool for datascience under Python.
They allow for a quick definition of what has to happen without the need to declare a function.
For instance:


```python
square = lambda v: v**2

square(10)
```




    100



Some IDE's will note that these are not desired, and give a warning on them.
The main reason for their use is that they are limited in scope and easy in terms of quick operations.
Meaning, you only use them for code which could be written in 1 line which would normally clog up the readability of the code.

Example:


```python
def foo():
    # with lambda
    bar = lambda a,b: min(a-b, b-a)*max(a-b, b-a)
    print( [bar(i,j) for i in range(10) for j in range(3)] )

    print('-'*10)
    # without lambda
    print( [min(i-j, j-i)*max(i-j, j-i) for i in range(10) for j in range(3)] )

foo()

try:
    bar(10,8)
except NameError as e:
    print("Geen idee:",e)
```

    [0, -1, -4, -1, 0, -1, -4, -1, 0, -9, -4, -1, -16, -9, -4, -25, -16, -9, -36, -25, -16, -49, -36, -25, -64, -49, -36, -81, -64, -49]
    ----------
    [0, -1, -4, -1, 0, -1, -4, -1, 0, -9, -4, -1, -16, -9, -4, -25, -16, -9, -36, -25, -16, -49, -36, -25, -64, -49, -36, -81, -64, -49]
    Geen idee: name 'bar' is not defined


## Map & Filter
Last in this refresh course is the use of map.
Map is a special type of operation under Python.
In most of the examples above, the code is eager.
It performs the computation in the spot that defines the step.
With map we push that execution point to when its output is requested.
In this regard it is similar to a generator.

map follows the following pattern:
map( \<function>, \<iterable> )

The function needs to accept the objects from the iterable.

Example:


```python
# running this will take a while if it is eager, but it is instant due to its lazy execution
a = (i for i in range(10**100))
square = lambda v: v**2

# here we square all values of a, that is if we start to iterate over them
a_squared = map(square, a)
```

The above code only defines what has to happen, not when.
We can filter the results we are interested in using filter:


```python
range_of_interest = lambda x: 200 < x < 300

a_of_interest = filter(range_of_interest, a_squared)
```

filter has an identical format to the map function in that it has to accept the items from the iterable.
Due to the efficiency of the generator, map and filter functions, the code will not clog the memory with useless results.
However, its size (10^100 values) is enough to bottleneck the CPU. 
If you try to make a list of all answers, it will take quite a while.

To show that this works, we'll just select the first 3 values:


```python
# every iterable has a next operation, resulting in the next item on the list
[next(a_of_interest) for i in range(3)]
```




    [225, 256, 289]



All the explained operations are native to Python and do not require any import.
Some are more widely used during the course when compared to others.
If there are questions regarding these operations please contact your teacher.
