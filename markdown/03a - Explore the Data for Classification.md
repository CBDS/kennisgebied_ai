# Exploring the Data for classification

In this notebook, we will look at another dataset from [Kaggle](https://www.kaggle.com/competitions/titanic/overview). The dataset is called the Titanic dataset and our goal is to predict whether a passenger survived or not. Instead of predicting a continuous value, we are now going to predict a binary value: survived or not. While this is also a supervised learning problem, it is not a regression problem but a classification problem; we are trying to predict a limited number of classes. Before we start building models, first, we will explore the data to understand the features and the target variable. We start off by loading the data:


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd

titanic_df = pd.read_csv(os.path.join(data_directory,'titanic/train.csv'))
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>3</td>
      <td>Braund, Mr. Owen Harris</td>
      <td>male</td>
      <td>22.0</td>
      <td>1</td>
      <td>0</td>
      <td>A/5 21171</td>
      <td>7.2500</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>Cumings, Mrs. John Bradley (Florence Briggs Th...</td>
      <td>female</td>
      <td>38.0</td>
      <td>1</td>
      <td>0</td>
      <td>PC 17599</td>
      <td>71.2833</td>
      <td>C85</td>
      <td>C</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>1</td>
      <td>3</td>
      <td>Heikkinen, Miss. Laina</td>
      <td>female</td>
      <td>26.0</td>
      <td>0</td>
      <td>0</td>
      <td>STON/O2. 3101282</td>
      <td>7.9250</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>1</td>
      <td>1</td>
      <td>Futrelle, Mrs. Jacques Heath (Lily May Peel)</td>
      <td>female</td>
      <td>35.0</td>
      <td>1</td>
      <td>0</td>
      <td>113803</td>
      <td>53.1000</td>
      <td>C123</td>
      <td>S</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>0</td>
      <td>3</td>
      <td>Allen, Mr. William Henry</td>
      <td>male</td>
      <td>35.0</td>
      <td>0</td>
      <td>0</td>
      <td>373450</td>
      <td>8.0500</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
  </tbody>
</table>
</div>



As we can see, the dataset has 12 columns. The columns are as follows:

| Column Name | Description | Values |
|-------------|-------------|--------|
| PassengerId | A unique identifier for the passenger | 
| Survived | Whether the passenger survived or not | 0 = No, 1 = Yes | 
| Pclass | The class of the ticket the passenger bought | 1 = 1st class, 2 = 2nd class, 3 = 3rd class | 
| Name | The name of the passenger | 
| Sex  | The gender of the passenger | male, female | 
| Age  | The age of the passenger in years | 
| SibSp | The number of siblings or spouses the passenger had on board |
| Parch | The number of parents or children the passenger had on board |
| Ticket | The ticket number | 
| Fare | The fare the passenger paid | 
| Cabin | The cabin number | 
| Embarked | The port the passenger embarked from | (C = Cherbourg, Q = Queenstown, S = Southampton) | 

Using this information, we can start exploring the data. Let's start by looking at the number of rows in the dataset:


```python
len(titanic_df)
```




    891



There are 891 rows in the dataset, this means that there are 891 passengers in the dataset.

## Missing Values

Let's first look if there are any missing values in the dataset. Let's look at the number of missing values in each column:


```python
titanic_df.isnull().sum().to_frame()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>PassengerId</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Survived</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Pclass</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Name</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Sex</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Age</th>
      <td>177</td>
    </tr>
    <tr>
      <th>SibSp</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Parch</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Ticket</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Fare</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Cabin</th>
      <td>687</td>
    </tr>
    <tr>
      <th>Embarked</th>
      <td>2</td>
    </tr>
  </tbody>
</table>
</div>



We can see that the `Age`, `Cabin`, and `Embarked` columns have missing values. We will have to handle these missing values later. Next, let's look at a summary of the dataset. To do this, we will use the `describe` method of the DataFrame. This method gives us a summary of the numerical columns in the dataset:


```python
titanic_df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>714.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>446.000000</td>
      <td>0.383838</td>
      <td>2.308642</td>
      <td>29.699118</td>
      <td>0.523008</td>
      <td>0.381594</td>
      <td>32.204208</td>
    </tr>
    <tr>
      <th>std</th>
      <td>257.353842</td>
      <td>0.486592</td>
      <td>0.836071</td>
      <td>14.526497</td>
      <td>1.102743</td>
      <td>0.806057</td>
      <td>49.693429</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.420000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>223.500000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>20.125000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>7.910400</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>446.000000</td>
      <td>0.000000</td>
      <td>3.000000</td>
      <td>28.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>14.454200</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>668.500000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>38.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>31.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>891.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>80.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>512.329200</td>
    </tr>
  </tbody>
</table>
</div>



The table above shows the summary statistics per column of the dataset. We can see that the `Age` column has missing values, the row count is `714` and we should have `891`. We will need to handle these missing values before we can build a model. We will do this later. We can also see the mean values of each column. We see that the `Survived` column has a mean of `0.383838`. This means that only `38.4%` of the passengers survived. This is important to know as it will help us understand the distribution of the target variable. We can confirm this count by calculating the percentage ourselves:


```python
percentage_survived = titanic_df['Survived'].mean() * 100
percentage_survived
```




    38.38383838383838



Only 38% of the passengers survived. This is important to know because if we were to predict that all passengers did not survive, we would be correct 62% of the time. This is called the baseline accuracy. We will use this as a reference point to evaluate our models. If our model is not better than the baseline accuracy, then it is not a good model. 

## Distribution of the categorical features


In the table above, we saw that four variables (including the target variable) are categorical: `Survived`, `Pclass`, `Sex`, and `Embarked`. Let's look at the distribution of these variables. 
We will start by looking at the distribution of the `Pclass` column:


```python
import matplotlib.pyplot as plt
import seaborn as sns

plt.figure(figsize=(6, 4))

sns.countplot(x='Pclass', data=titanic_df)

plt.title('Pclass Distribution')

plt.show()
```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_13_0.png)
    


We can see that the majority of the passengers were in the 3rd class, followed by the 1st class and then the 2nd class. Next, let's look at the distribution of the `Sex` column:


```python
plt.figure(figsize=(6, 4))

sns.countplot(x='Sex', data=titanic_df)

plt.title('Gender Distribution')

plt.show()
```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_15_0.png)
    


We can see that there were more male passengers than female passengers. This is something we will need to keep in mind when building the model. 
Next, let's look at the distribution of the `Embarked` column:


```python
plt.figure(figsize=(6, 4))

sns.countplot(x='Embarked', data=titanic_df)

plt.title('Embarked Distribution')

plt.show()
```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_17_0.png)
    


Most of the passengers embarked from Southampton, followed by Cherbourg and then Queenstown. Next, we will look at the distribution of the target variable, `Survived`:


```python
plt.figure(figsize=(6, 4))

sns.countplot(x='Survived', data=titanic_df)

plt.title('Target Variable Distribution (Survived)')

plt.show()
```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_19_0.png)
    


We can see that the majority of the passengers did not survive. This is important to know because the model might be biased towards predicting that passengers did not survive. 
We will need to keep this in mind when building the model.

## Distributions of the numerical features

There are two numerical columns in the dataset: `Age` and `Fare`. Let's look at the distribution of these columns. We will start by looking at the distribution of the `Age` column:


```python


plt.figure(figsize=(10, 5))

sns.histplot(titanic_df['Age'], bins=30, kde=True)

plt.title('Age Distribution')

plt.show()
```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_21_0.png)
    


We can see that the majority of the passengers were between 20 and 30 years old. Next, let's look at the distribution of the `Fare` column:


```python
plt.figure(figsize=(10, 5))

sns.histplot(titanic_df['Fare'], bins=30, kde=True)

plt.title('Fare Distribution')

plt.show()


```


    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_23_0.png)
    


## Distributions of the target variable in relation to the categorical features

Next, we will look at the distribution of the target variable with respect to the other features. First, we will look at the distribution of the target variable with respect to the `Sex` feature.


```python
titanic_df.groupby('Sex')['Survived'].mean() * 100
```




    Sex
    female    74.203822
    male      18.890815
    Name: Survived, dtype: float64



We can see the percentage of women who survived is much higher than the percentage of men who survived. This is also an important feature to consider when building our model. Next, we will look at the distribution of the target variable with respect to the `Pclass` feature.


```python
titanic_df.groupby('Pclass')['Survived'].mean() * 100
```




    Pclass
    1    62.962963
    2    47.282609
    3    24.236253
    Name: Survived, dtype: float64



These results show that people in a higher class, were more likely to survive. This is also an important feature to consider when building our model. Next, we will look at the distribution of the target variable with respect to the `Embarked` feature.


```python
titanic_df.groupby('Embarked')['Survived'].mean() * 100
```




    Embarked
    C    55.357143
    Q    38.961039
    S    33.695652
    Name: Survived, dtype: float64



The place where people embarked from also seems to have an effect on the survival rate. This is also an important feature to consider when building our model. Next, we will look at the distribution of the target variable with respect to the `SibSp` feature.

## Pearson's Correlation Matrix for correlation between numerical features and the target variable

In the previous section, we looked at the distribution of the target variable with respect to the other features. we saw already that the `Sex`, `Pclass`, and `Embarked` features have an effect on the survival rate. To further understand the relationship between the features and the target variable, we will look at the correlation between the numerical features and the target variable. First, we will look at the Pearson's correlation matrix for the numerical features. In the next section, we will look at the $\chi^2$ test for the categorical features.
 
The Pearson's correlation matrix quantifies the linear relationship between the numerical features. The correlation coefficient ranges from -1 to 1:

- 1: perfect positive linear relationship (as the feature increases, the target variable also increases proportionally)
- -1: perfect negative linear relationship (as the feature increases, the target variable decreases proportionally)
- 0: no linear relationship

This matrix helps identify which features are strongly or weakly correlated with the target variable. We can use this information to select the most important features for our model.
Let's start by looking at the correlation between the `Age`, `Fare` and `Survived` columns:


```python
# Plot pearson correlation matrix
numerical_columns = ['Age',  'Fare', 'Survived']


plt.figure(figsize=(10, 6))

sns.heatmap(titanic_df[numerical_columns].corr(), annot=True, cmap='coolwarm')

plt.title('Pearson Correlation Matrix')
```




    Text(0.5, 1.0, 'Pearson Correlation Matrix')




    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_32_1.png)
    


From the Pearson's correlation matrix above, we can see that the `Fare` column has the higher correlation with the `Survived` column than the `Age` column. This could mean that the `Fare` column is a better predictor of survival than the `Age` column. We will keep this in mind when building the model. 

Note that also the `Fare` column only has a weak correlation with the `Age` column. This means that the two columns are not strongly correlated. This is important to know because if two columns are strongly correlated, we only need to keep one of them in the model.

## Chi-squared test for correlation between categorical variables and target variable

The $\chi^2$ test is used to determine whether there is a significant association between two categorical variables. The test is based on the difference between the expected and observed frequencies of a feature variable with the target variable. The $\chi^2$ test is used to test the null hypothesis that the two variables are independent. If the p-value is less than a certain threshold (usually 0.05), we reject the null hypothesis and conclude that the two variables are dependent. To perform the $\chi^2$ test, we will first need to encode the categorical variables as numerical variables. To do this, we will use the `get_dummies` function of the pandas library. This function creates a new column for each unique value in the categorical column. The new columns are binary columns, where 1 indicates the presence of the value and 0 indicates the absence of the value. Let's start by encoding the categorical columns:


```python
categorical_columns = ['Pclass', 'Sex', 'Embarked', 'SibSp', 'Parch']
titanic_dummies_df = pd.get_dummies(titanic_df, columns=categorical_columns)
titanic_dummies_df.head()

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Name</th>
      <th>Age</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Pclass_1</th>
      <th>Pclass_2</th>
      <th>Pclass_3</th>
      <th>...</th>
      <th>SibSp_4</th>
      <th>SibSp_5</th>
      <th>SibSp_8</th>
      <th>Parch_0</th>
      <th>Parch_1</th>
      <th>Parch_2</th>
      <th>Parch_3</th>
      <th>Parch_4</th>
      <th>Parch_5</th>
      <th>Parch_6</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>Braund, Mr. Owen Harris</td>
      <td>22.0</td>
      <td>A/5 21171</td>
      <td>7.2500</td>
      <td>NaN</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>...</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>1</td>
      <td>Cumings, Mrs. John Bradley (Florence Briggs Th...</td>
      <td>38.0</td>
      <td>PC 17599</td>
      <td>71.2833</td>
      <td>C85</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>...</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>1</td>
      <td>Heikkinen, Miss. Laina</td>
      <td>26.0</td>
      <td>STON/O2. 3101282</td>
      <td>7.9250</td>
      <td>NaN</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>...</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>1</td>
      <td>Futrelle, Mrs. Jacques Heath (Lily May Peel)</td>
      <td>35.0</td>
      <td>113803</td>
      <td>53.1000</td>
      <td>C123</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>...</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>0</td>
      <td>Allen, Mr. William Henry</td>
      <td>35.0</td>
      <td>373450</td>
      <td>8.0500</td>
      <td>NaN</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>...</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>True</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 29 columns</p>
</div>



After encoding, we need to select the encoded columns. Each of the encoded columns starts with the name of the original column followed by an underscore and the value. For example, the `Sex_female` column is the encoded column for the `Sex` column with the value `female`. The code below selects all the encoded columns by checking which of the columns start with the name of the original columns:


```python
# Chi-square test
encoded_columns = [column 
                   for column in titanic_dummies_df.columns
                   for category in categorical_columns
                   if column.startswith(category)]
encoded_columns
```




    ['Pclass_1',
     'Pclass_2',
     'Pclass_3',
     'Sex_female',
     'Sex_male',
     'Embarked_C',
     'Embarked_Q',
     'Embarked_S',
     'SibSp_0',
     'SibSp_1',
     'SibSp_2',
     'SibSp_3',
     'SibSp_4',
     'SibSp_5',
     'SibSp_8',
     'Parch_0',
     'Parch_1',
     'Parch_2',
     'Parch_3',
     'Parch_4',
     'Parch_5',
     'Parch_6']



After we have selected the encoded columns, we can perform the $\chi^2$ test. We will use the `chi2` function of the `sklearn.feature_selection` module. This function returns the $\chi^2$ statistic and the p-value. 


```python
from sklearn.feature_selection import chi2

chi2_values, p_values = chi2(titanic_dummies_df[encoded_columns], titanic_df['Survived'])
chi2_values, p_values
```




    (array([5.51751510e+01, 6.16076687e+00, 4.15530709e+01, 1.70348127e+02,
            9.27024470e+01, 2.04644013e+01, 1.08467891e-02, 5.98483982e+00,
            3.79935346e+00, 2.04294991e+01, 7.66193147e-01, 1.21182053e+00,
            3.58951682e+00, 3.11475410e+00, 4.36065574e+00, 4.62827086e+00,
            1.39161500e+01, 4.56427955e+00, 9.87834340e-01, 2.49180328e+00,
            7.14495255e-01, 6.22950820e-01]),
     array([1.10253810e-13, 1.30614569e-02, 1.14714147e-10, 6.21058490e-39,
            6.07783826e-22, 6.07507131e-06, 9.17051963e-01, 1.44293530e-02,
            5.12723773e-02, 6.18687479e-06, 3.81397315e-01, 2.70971869e-01,
            5.81451466e-02, 7.75861891e-02, 3.67781458e-02, 3.14492437e-02,
            1.91148980e-04, 3.26454024e-02, 3.20272257e-01, 1.14440534e-01,
            3.97955523e-01, 4.29952852e-01]))



To check on which variable the target variable depends the most, we will sort the p-values in ascending order. The smaller the p-value, the more likely the target variable depends on the feature variable. For variables with a p-value less than 0.05, we can reject the null hypothesis and conclude that the two variables are dependent. Let's look at the p-values of the $\chi^2$ test:


```python
# Plot chi2 values

plt.figure(figsize=(10, 6))

chi2_values_df = pd.DataFrame({"chi2": chi2_values, "p_value": p_values}, index=encoded_columns)

sorted_chi2_values = chi2_values_df.sort_values(by="p_value", ascending=True)
sorted_chi2_values['significant'] = sorted_chi2_values["p_value"] < 0.05
sorted_chi2_values

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chi2</th>
      <th>p_value</th>
      <th>significant</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Sex_female</th>
      <td>170.348127</td>
      <td>6.210585e-39</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Sex_male</th>
      <td>92.702447</td>
      <td>6.077838e-22</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Pclass_1</th>
      <td>55.175151</td>
      <td>1.102538e-13</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Pclass_3</th>
      <td>41.553071</td>
      <td>1.147141e-10</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Embarked_C</th>
      <td>20.464401</td>
      <td>6.075071e-06</td>
      <td>True</td>
    </tr>
    <tr>
      <th>SibSp_1</th>
      <td>20.429499</td>
      <td>6.186875e-06</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Parch_1</th>
      <td>13.916150</td>
      <td>1.911490e-04</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Pclass_2</th>
      <td>6.160767</td>
      <td>1.306146e-02</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Embarked_S</th>
      <td>5.984840</td>
      <td>1.442935e-02</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Parch_0</th>
      <td>4.628271</td>
      <td>3.144924e-02</td>
      <td>True</td>
    </tr>
    <tr>
      <th>Parch_2</th>
      <td>4.564280</td>
      <td>3.264540e-02</td>
      <td>True</td>
    </tr>
    <tr>
      <th>SibSp_8</th>
      <td>4.360656</td>
      <td>3.677815e-02</td>
      <td>True</td>
    </tr>
    <tr>
      <th>SibSp_0</th>
      <td>3.799353</td>
      <td>5.127238e-02</td>
      <td>False</td>
    </tr>
    <tr>
      <th>SibSp_4</th>
      <td>3.589517</td>
      <td>5.814515e-02</td>
      <td>False</td>
    </tr>
    <tr>
      <th>SibSp_5</th>
      <td>3.114754</td>
      <td>7.758619e-02</td>
      <td>False</td>
    </tr>
    <tr>
      <th>Parch_4</th>
      <td>2.491803</td>
      <td>1.144405e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>SibSp_3</th>
      <td>1.211821</td>
      <td>2.709719e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>Parch_3</th>
      <td>0.987834</td>
      <td>3.202723e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>SibSp_2</th>
      <td>0.766193</td>
      <td>3.813973e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>Parch_5</th>
      <td>0.714495</td>
      <td>3.979555e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>Parch_6</th>
      <td>0.622951</td>
      <td>4.299529e-01</td>
      <td>False</td>
    </tr>
    <tr>
      <th>Embarked_Q</th>
      <td>0.010847</td>
      <td>9.170520e-01</td>
      <td>False</td>
    </tr>
  </tbody>
</table>
</div>




    <Figure size 1000x600 with 0 Axes>


We can also plot the p-values to visualize the results:


```python
sorted_chi2_values[["chi2", "p_value"]].plot.barh()
```




    <Axes: >




    
![png](03a%20-%20Explore%20the%20Data%20for%20Classification_files/03a%20-%20Explore%20the%20Data%20for%20Classification_42_1.png)
    


From the table and figure above, we can see that the target variable is dependent on the following variables: `Sex_female`, `Sex_male`, `Pclass_1`, `Pclass2`, `Pclass3`, `Embarked_C`,  `Embarked_S`, `SibSp1`, `SibSp8`, `Parch_0`, `Parch_1`, and `Parch_2`. This means that these variables are important for predicting the target variable. We will keep this in mind when building the model.

# Conclusion

In this notebook, we explored the Titanic dataset. We looked at:

- the distribution of the features separately, 
- at the distributions between the features and the target variable. 
- at the Pearson's correlation between the features and the target variable. 
- at the $\chi^2$ test for the categorical features.
 
From these analyses we found that:
- the `Sex`, `Pclass`, `Embarked`, `SibSp`, and `Parch` features may have an effect on the survival rate. 
- that the `Fare` column has a higher correlation with the `Survived` column than the `Age` column. This means that the `Fare` column could be a better predictor of survival than the `Age` column. 

 By doing a preliminary exploration of the data, we can already get a good idea of the data and which variables will be important in building a good model. When building the model, we will need to keep these findings in mind. We will also need to handle the missing values in the dataset before we can build the model. We will do this in the next notebook.
