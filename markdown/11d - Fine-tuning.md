# Fine-tuning Models

In this notebook, we will use an existing ResNet50 model to classify images. [ResNet50](https://arxiv.org/abs/1512.03385v1) is a well-known and widely used model for image classification. A version of this model pre-trained on millions of images of the [ImageNet](https://www.image-net.org/) database can be downloaded from the several places. We are going to use the well-known [HuggingFace](https://huggingface.co/) model repository.

The idea of fine-tuning is as follows: you download weights trained on another large dataset, in this case ImageNet, and you use those weights as a starting point for your training. Training the model with these existing weights allows training on much smaller datasets and in a lot less time. The idea is that the model can use the knowledge it learned when it was trained on the large dataset and benefit from that. This will work in almost all cases, even if your dataset is not so similar to the one the model originally was trained on. In contrast, the closer the dataset will be to the large dataset, the easier the training. We will show this in this notebook too.


```python
!pip install datasets evaluate "transformers[torch]"
!pip install accelerate -U
```

    Requirement already satisfied: datasets in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (2.14.6)
    Requirement already satisfied: evaluate in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (0.4.1)
    Requirement already satisfied: transformers[torch] in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (4.48.2)
    Requirement already satisfied: numpy>=1.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (1.26.1)
    Requirement already satisfied: pyarrow>=8.0.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (14.0.0)
    Requirement already satisfied: dill<0.3.8,>=0.3.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.3.7)
    Requirement already satisfied: pandas in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (2.1.2)
    Requirement already satisfied: requests>=2.19.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (2.31.0)
    Requirement already satisfied: tqdm>=4.62.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (4.66.1)
    Requirement already satisfied: xxhash in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (3.4.1)
    Requirement already satisfied: multiprocess in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.70.15)
    Requirement already satisfied: fsspec<=2023.10.0,>=2023.1.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from fsspec[http]<=2023.10.0,>=2023.1.0->datasets) (2023.10.0)
    Requirement already satisfied: aiohttp in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (3.8.6)
    Requirement already satisfied: huggingface-hub<1.0.0,>=0.14.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.28.1)
    Requirement already satisfied: packaging in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (23.2)
    Requirement already satisfied: pyyaml>=5.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (6.0.1)
    Requirement already satisfied: responses<0.19 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from evaluate) (0.18.0)
    Requirement already satisfied: filelock in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (3.13.1)
    Requirement already satisfied: regex!=2019.12.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (2023.10.3)
    Requirement already satisfied: tokenizers<0.22,>=0.21 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (0.21.0)
    Requirement already satisfied: safetensors>=0.4.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (0.5.2)
    Requirement already satisfied: torch>=2.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (2.1.0)
    Requirement already satisfied: accelerate>=0.26.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from transformers[torch]) (1.3.0)
    Requirement already satisfied: psutil in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate>=0.26.0->transformers[torch]) (5.9.6)
    Requirement already satisfied: attrs>=17.3.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (23.1.0)
    Requirement already satisfied: charset-normalizer<4.0,>=2.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (3.3.2)
    Requirement already satisfied: multidict<7.0,>=4.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (6.0.4)
    Requirement already satisfied: async-timeout<5.0,>=4.0.0a3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (4.0.3)
    Requirement already satisfied: yarl<2.0,>=1.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.9.2)
    Requirement already satisfied: frozenlist>=1.1.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.4.0)
    Requirement already satisfied: aiosignal>=1.1.2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.3.1)
    Requirement already satisfied: typing-extensions>=3.7.4.3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub<1.0.0,>=0.14.0->datasets) (4.8.0)
    Requirement already satisfied: idna<4,>=2.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (3.4)
    Requirement already satisfied: urllib3<3,>=1.21.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (2.0.7)
    Requirement already satisfied: certifi>=2017.4.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (2023.7.22)
    Requirement already satisfied: sympy in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0->transformers[torch]) (1.12)
    Requirement already satisfied: networkx in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0->transformers[torch]) (3.2.1)
    Requirement already satisfied: jinja2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0->transformers[torch]) (3.1.2)
    Requirement already satisfied: python-dateutil>=2.8.2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2.8.2)
    Requirement already satisfied: pytz>=2020.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2023.3.post1)
    Requirement already satisfied: tzdata>=2022.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2023.3)
    Requirement already satisfied: six>=1.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from python-dateutil>=2.8.2->pandas->datasets) (1.16.0)
    Requirement already satisfied: MarkupSafe>=2.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from jinja2->torch>=2.0->transformers[torch]) (2.1.3)
    Requirement already satisfied: mpmath>=0.19 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from sympy->torch>=2.0->transformers[torch]) (1.3.0)
    Requirement already satisfied: accelerate in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (1.3.0)
    Collecting accelerate
      Downloading accelerate-1.4.0-py3-none-any.whl.metadata (19 kB)
    Requirement already satisfied: numpy<3.0.0,>=1.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (1.26.1)
    Requirement already satisfied: packaging>=20.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (23.2)
    Requirement already satisfied: psutil in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (5.9.6)
    Requirement already satisfied: pyyaml in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (6.0.1)
    Requirement already satisfied: torch>=2.0.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (2.1.0)
    Requirement already satisfied: huggingface-hub>=0.21.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (0.28.1)
    Requirement already satisfied: safetensors>=0.4.3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from accelerate) (0.5.2)
    Requirement already satisfied: filelock in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub>=0.21.0->accelerate) (3.13.1)
    Requirement already satisfied: fsspec>=2023.5.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub>=0.21.0->accelerate) (2023.10.0)
    Requirement already satisfied: requests in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub>=0.21.0->accelerate) (2.31.0)
    Requirement already satisfied: tqdm>=4.42.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub>=0.21.0->accelerate) (4.66.1)
    Requirement already satisfied: typing-extensions>=3.7.4.3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub>=0.21.0->accelerate) (4.8.0)
    Requirement already satisfied: sympy in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0.0->accelerate) (1.12)
    Requirement already satisfied: networkx in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0.0->accelerate) (3.2.1)
    Requirement already satisfied: jinja2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from torch>=2.0.0->accelerate) (3.1.2)
    Requirement already satisfied: MarkupSafe>=2.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from jinja2->torch>=2.0.0->accelerate) (2.1.3)
    Requirement already satisfied: charset-normalizer<4,>=2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests->huggingface-hub>=0.21.0->accelerate) (3.3.2)
    Requirement already satisfied: idna<4,>=2.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests->huggingface-hub>=0.21.0->accelerate) (3.4)
    Requirement already satisfied: urllib3<3,>=1.21.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests->huggingface-hub>=0.21.0->accelerate) (2.0.7)
    Requirement already satisfied: certifi>=2017.4.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests->huggingface-hub>=0.21.0->accelerate) (2023.7.22)
    Requirement already satisfied: mpmath>=0.19 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from sympy->torch>=2.0.0->accelerate) (1.3.0)
    Downloading accelerate-1.4.0-py3-none-any.whl (342 kB)
    Installing collected packages: accelerate
      Attempting uninstall: accelerate
        Found existing installation: accelerate 1.3.0
        Uninstalling accelerate-1.3.0:
          Successfully uninstalled accelerate-1.3.0
    Successfully installed accelerate-1.4.0



```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/DeepStat-WP5-dataset.zip" ]; then
    wget "https://zenodo.org/records/7551799/files/DeepStat-WP5-dataset.zip?download=1" -O "$DATA_DIRECTORY/DeepStat-WP5-dataset.zip"
fi
```


```bash
%%bash
if [ ! -d "$DATA_DIRECTORY/DeepStat" ]; then
    mkdir -p $DATA_DIRECTORY/DeepStat
    unzip "$DATA_DIRECTORY/DeepStat-WP5-dataset.zip" -d "$DATA_DIRECTORY/DeepStat"
fi
```

First, let's start by defining some convenience methods to extract features from a CNN model.


```python
# do feature extraction from these images using a resnet50 model from huggingface
import torch
import torch.nn as nn
from transformers import AutoImageProcessor, ResNetModel

def get_features_from_model(processor, model, images):
    inputs = processor(images, return_tensors="pt").to(model.device)

    # Extract features
    with torch.no_grad():
        features = model(**inputs).pooler_output.squeeze()
        return features

def get_resnet_features(images, device):
    # Load the model
    processor = AutoImageProcessor.from_pretrained("microsoft/resnet-50")
    model = ResNetModel.from_pretrained("microsoft/resnet-50")
    model.to(device)
    return get_features_from_model(processor, model, images)

def get_device():
    if torch.cuda.is_available(): 
        return torch.device('cuda') 
    if torch.backends.mps.is_available(): 
        return torch.device('mps')
    return torch.device('cpu')

device = get_device()
```


```python
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt


def tsne_plot(features, labels):
    # Convert features tensor to numpy array
    features_np = features.cpu().numpy()

    # Apply t-SNE
    tsne = TSNE(n_components=2, random_state=0)
    features_2d = tsne.fit_transform(features_np)

    # Create a scatter plot
    plt.figure(figsize=(6, 6))
    scatter = plt.scatter(features_2d[:, 0], features_2d[:, 1], c=labels, cmap='coolwarm')

    # Add a color bar
    plt.colorbar(scatter)

    plt.show()

```

# Cats vs. Dogs

First, we will see how a ResNet50 trained on ImageNet, will perform on a dataset that contains images that are similar to the images in ImageNet. We will download a dataset from [HuggingFace](https://huggingface.co) that contains images of Cats and Dogs. We want to use the model to classify whether an image contains a Cat or a Dog. Within the large collection of images in ImageNet, there are already thousands of images of cats and dogs, but also thousands in other categories. A model trained on ImageNet already learned to distinguish between Cats and Dogs. Therefore, the task won't need any fine-tuning. First, we will download the dataset:


```python
from datasets import load_dataset

dataset = load_dataset("cats_vs_dogs", split="train")
print(dataset)
```

    Dataset({
        features: ['image', 'labels'],
        num_rows: 23410
    })


    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/datasets/table.py:1421: FutureWarning: promote has been superseded by mode='default'.
      table = cls._concat_blocks(blocks, axis=0)



```python
from itertools import islice
import numpy as np

def memory_efficient_sampler(dataset, sample_size, width_threshold=300, batch_size=100):
    """
    Yields indices of selected wide images in memory-efficient batches
    """
    selected = 0
    
    # Create shuffled indices
    all_indices = np.random.permutation(len(dataset))
    print("all indices", all_indices)
    
    # Process dataset in batches
    for i in range(0, len(all_indices), batch_size):
        batch_indices = all_indices[i:min(i + batch_size, len(all_indices))]
        batch = dataset.select(batch_indices)
        
        # Find wide images in current batch
        selected_batch_indices = [idx for idx, img in enumerate(batch['image']) 
                        if img.size[0] > width_threshold ]
        wide_indices = [batch_indices[idx] for idx in selected_batch_indices]

        # Randomly select from current batch
        batch_selection = np.random.choice(
            wide_indices, 
            size=min(len(wide_indices), sample_size - selected),
            replace=False
        ).tolist()
        
        # Update tracking
        selected += len(batch_selection)
        
        # Yield selected indices
        yield from batch_selection
        
        if selected >= sample_size:
            break

```


```python
selected_indices = list(memory_efficient_sampler(dataset, sample_size=700))
dataset = dataset.select(selected_indices)

X = dataset['image']
y_true = dataset['labels']
```


```python
import pandas as pd

pd.Series(y_true).value_counts()
```

And visualize some of them. Each time you run this cell a random picture from the dataset should be displayed:


```python
import random

plt.imshow(X[random.randint(0, len(X)-1)])

```




    <matplotlib.image.AxesImage at 0x3a5218750>




    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_14_1.png)
    


One way deep-learning algorithms differ from traditional machine learning models, is that they also learn feature extraction on the data. In contrast, in traditional machine learning, the data scientist need to look for suitable features themselves. Schematically, this looks like this ![Traditional machine learning vs. deep-learning](./traditional_vs_deep_learning.png)


Good features separate the classes you want to label the best. If we look at a deep-learning model in more detail, we see that often a lot of the layers in the model are aimed at feature extraction. After those layers often a small classifier performs the classification between the classes. This can be a simple as a logistic regression. This image shows the structure of a lot of image classification models:


<img src="./applsci-10-03359-g001.png" alt="Feature extraction and classification in deep learning" style="width: 50%;"/>

<!-- [Source](https://www.mdpi.com/2076-3417/10/10/3359) -->

The feature layers of the model take the image and map the image into a certain combination of numbers called a feature vector. Basically, the model tries to put the image in a certain location in a highly dimensional space. The location is described by the combination of numbers given by the feature vector. When we take only the first layers of the model responsible for this feature extraction, we can extract these image "locations" and try to visualize them.


```python
cats_features = get_resnet_features(X, device)
print(cats_features.shape)
```

    Using a slow image processor as `use_fast` is unset and a slow processor was saved with this model. `use_fast=True` will be the default behavior in v4.48, even if the model was saved with a slow processor. This will result in minor differences in outputs. You'll still be able to use a slow processor with `use_fast=False`.


    torch.Size([500, 2048])


Now we have all the locations for all of the images in the dataset, we can visualize them. Of course, it is difficult to visualize a high dimensional space. Instead, we perform a dimension reduction technique called `TSNE` that maps the locations in the high dimensional feature space to a 2D space that we can visualize:


```python
# Assuming binary_classes is a list of your binary classes
tsne_plot(cats_features, y_true)
```


    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_18_0.png)
    


In the picture above, we can clearly see that the two classes, Cats and Dogs, are clearly separated. There's a blue cluster and a red cluster of points. This indicates that the ResNet 50 model that we downloaded can easily separate these two classes of images. This is no surprise as there are thousands of images containing cats and dogs in the ImageNet dataset it was trained on. We can see that there are some blue points within the red cluster, and also some red points in the blue cluster. These are images that are harder to classify, maybe cats that look like dogs, dogs that look like cats, or images with both cats and dogs.

In general, the model looks like it does a pretty good job though. This can also be seen when we train a simple logistic regression model on the feature vectors we extracted:


```python
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

# Split the data into train and test sets
train_features_cats, test_features_cats, train_labels_cats, test_labels_cats = train_test_split(cats_features.cpu(), y_true, test_size=0.2, random_state=42)

lr_model = LogisticRegression()
lr_model.fit(train_features_cats, train_labels_cats)

# Make predictions
predictions = lr_model.predict(test_features_cats)

# Print classification report
print(classification_report(test_labels_cats, predictions))
```

                  precision    recall  f1-score   support
    
               0       1.00      0.98      0.99        49
               1       0.98      1.00      0.99        51
    
        accuracy                           0.99       100
       macro avg       0.99      0.99      0.99       100
    weighted avg       0.99      0.99      0.99       100
    


From the classification report, we can see that the model has very high performance. Indeed, the ResNet 50 model can easily learn to distinguish between the two classes, based on what it learned when it was trained on ImageNet.

# Detecting Solar Panels

Cats and dogs were easy for our model. What would happen if we try to train the model to classify something it hasn't seen before. Instead of natural pictures that are present in the ImageNet database, we are now presenting the model with Earth Observation photographs, which are not part of ImageNet. We will try to distinguish whether there are solar panels or not in the picture.


```python
import pandas as pd

solar_data_directory = os.path.join(data_directory, "DeepStat/selected/")
annotations_df = pd.read_csv(os.path.join(solar_data_directory, "all_annotations.csv"), sep=";")
annotations_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>dataset_id</th>
      <th>region</th>
      <th>year</th>
      <th>image_quality</th>
      <th>image_pixel_resolution</th>
      <th>image_uuid</th>
      <th>filename</th>
      <th>defaultAnnotation</th>
      <th>annotation</th>
      <th>annotation_created_date</th>
      <th>annotation_updated_date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>00009ae2-f851-4fd9-8a2a-2ac517df9b95</td>
      <td>/Heerlen_2018_HR/full/00009ae2-f851-4fd9-8a2a-...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-15 14:58:34.371</td>
      <td>2019-08-15 14:58:34.371</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>0001a16c-6798-4b20-b0ff-21745d2bf177</td>
      <td>/Heerlen_2018_HR/full/0001a16c-6798-4b20-b0ff-...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 11:38:47.622</td>
      <td>2019-08-03 11:38:47.622</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>00026530-22b0-4124-8a57-4f65e83ea82a</td>
      <td>/Heerlen_2018_HR/full/00026530-22b0-4124-8a57-...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 10:44:24.868</td>
      <td>2019-08-03 10:44:24.868</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>001381b4-9b66-4443-8f84-7275d30cac0e</td>
      <td>/Heerlen_2018_HR/full/001381b4-9b66-4443-8f84-...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-15 15:20:17.869</td>
      <td>2019-08-15 15:20:17.869</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>0023bf05-a166-4049-8148-f4409966b052</td>
      <td>/Heerlen_2018_HR/full/0023bf05-a166-4049-8148-...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 10:27:44.940</td>
      <td>2019-08-03 10:27:44.940</td>
    </tr>
  </tbody>
</table>
</div>




```python
annotations_df["filename"] = annotations_df["filename"].apply(lambda original_filename: os.path.join(solar_data_directory, original_filename[1:]))
annotations_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>dataset_id</th>
      <th>region</th>
      <th>year</th>
      <th>image_quality</th>
      <th>image_pixel_resolution</th>
      <th>image_uuid</th>
      <th>filename</th>
      <th>defaultAnnotation</th>
      <th>annotation</th>
      <th>annotation_created_date</th>
      <th>annotation_updated_date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>00009ae2-f851-4fd9-8a2a-2ac517df9b95</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-15 14:58:34.371</td>
      <td>2019-08-15 14:58:34.371</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>0001a16c-6798-4b20-b0ff-21745d2bf177</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 11:38:47.622</td>
      <td>2019-08-03 11:38:47.622</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>00026530-22b0-4124-8a57-4f65e83ea82a</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 10:44:24.868</td>
      <td>2019-08-03 10:44:24.868</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>001381b4-9b66-4443-8f84-7275d30cac0e</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-15 15:20:17.869</td>
      <td>2019-08-15 15:20:17.869</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>0023bf05-a166-4049-8148-f4409966b052</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-03 10:27:44.940</td>
      <td>2019-08-03 10:27:44.940</td>
    </tr>
  </tbody>
</table>
</div>



Let's explore some images with a negative label, i.e. the image does not contain any solar panels. Every time you run the cell below another image should be displayed.


```python
import matplotlib.pyplot as plt
from matplotlib.pyplot import imread

negative_images = annotations_df[annotations_df.annotation == 0].reset_index(drop=True)
plt.imshow(imread(negative_images["filename"].iloc[random.randint(0, len(negative_images) - 1)]))
```




    <matplotlib.image.AxesImage at 0x39d2e8750>




    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_26_1.png)
    



```python
positive_images = annotations_df[annotations_df.annotation == 1].reset_index(drop=True)
plt.imshow(imread(positive_images["filename"].iloc[random.randint(0, len(positive_images) - 1)]))
```




    <matplotlib.image.AxesImage at 0x39d300910>




    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_27_1.png)
    


The solar panel dataset is quite large. Extracting features for all the images will take a lot of time. Let's take a sample of 1000 images instead.


```python
sample_df = annotations_df.sample(500)
sample_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>user_id</th>
      <th>dataset_id</th>
      <th>region</th>
      <th>year</th>
      <th>image_quality</th>
      <th>image_pixel_resolution</th>
      <th>image_uuid</th>
      <th>filename</th>
      <th>defaultAnnotation</th>
      <th>annotation</th>
      <th>annotation_created_date</th>
      <th>annotation_updated_date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>150313</th>
      <td>15</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>b1c265f5-88e7-4a38-a378-482348b4dcd4</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>1</td>
      <td>1</td>
      <td>2019-08-13 14:25:45.195</td>
      <td>2019-08-13 14:25:45.195</td>
    </tr>
    <tr>
      <th>150478</th>
      <td>15</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>b38c42f2-91d8-4b04-8774-47b008e42ad4</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-28 15:03:26.013</td>
      <td>2019-08-28 15:03:26.013</td>
    </tr>
    <tr>
      <th>15176</th>
      <td>1</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>e8d358f1-2600-4e54-aa97-0835f2a69d7d</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-02 18:02:02.104</td>
      <td>2019-08-02 18:02:02.104</td>
    </tr>
    <tr>
      <th>45890</th>
      <td>2</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>a613ddc6-6b8c-433b-9886-0b6e6e715a76</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-02 08:44:50.512</td>
      <td>2019-08-02 08:44:50.512</td>
    </tr>
    <tr>
      <th>37860</th>
      <td>2</td>
      <td>1</td>
      <td>Heerlen</td>
      <td>2018</td>
      <td>HR</td>
      <td>200x200</td>
      <td>4d67fb90-91e0-462b-bc45-c45bf885cb7a</td>
      <td>../../data/DeepStat/selected/Heerlen_2018_HR/f...</td>
      <td>0</td>
      <td>0</td>
      <td>2019-08-02 08:57:02.395</td>
      <td>2019-08-02 08:57:02.395</td>
    </tr>
  </tbody>
</table>
</div>



We will load these images into memory now.


```python
# Load the images
from PIL import Image

images = [Image.open(filename).convert('RGB') for filename in sample_df["filename"]]
solar_features = get_resnet_features(images, device)
print(solar_features.shape)
```

    torch.Size([500, 2048])


And visualize these features, just like we did with the Cats and Dogs dataset.


```python
solar_labels = sample_df["annotation"].tolist()
tsne_plot(solar_features, solar_labels)
```


    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_33_0.png)
    



```python
# Split the data into train and test sets
train_solar_features, test_solar_features, train_solar_labels, test_solar_labels = train_test_split(solar_features.cpu(), solar_labels, test_size=0.2, random_state=42)

lr_model = LogisticRegression()
lr_model.fit(train_solar_features, train_solar_labels)

# Make predictions
solar_predictions = lr_model.predict(test_solar_features)

# Print classification report
print(classification_report(test_solar_labels, solar_predictions))
```

                  precision    recall  f1-score   support
    
               0       0.85      0.95      0.90        78
               1       0.69      0.41      0.51        22
    
        accuracy                           0.83       100
       macro avg       0.77      0.68      0.71       100
    weighted avg       0.82      0.83      0.81       100
    


Here we see that the model has a lot more trouble identifying the positive cases, i.e. the photographs with a solar panel. To improve performance, we need to fine-tune the model to our solar panel dataset.

# Fine-tuning the model

To fine tune the model, we need to train the model further on the dataset containing the Earth Observation images and their labels. We use the original ResNet 50 as a starting point and train the model a couple of more iterations. After fine tuning the model should be able to separate the "solar panel" and "no solar panel" classes a lot better.

To fine-tune the model, we need to choose a performance metric used when training. In this case, we are using the widely used F1 metric.


```python
import numpy as np
import evaluate

metric = evaluate.load("f1")
```

    Using the latest cached version of the module from /Users/timdejong/.cache/huggingface/modules/evaluate_modules/metrics/evaluate-metric--f1/0ca73f6cf92ef5a268320c697f7b940d1030f8471714bffdb6856c641b818974 (last modified on Sun Nov 12 09:15:55 2023) since it couldn't be found locally at evaluate-metric--f1, or remotely on the Hugging Face Hub.



```python
def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels, average="weighted")
```

Before fine-tuning, we need to prepare the dataset so it can be used by the Hugging Face package. We also select as sub-sample of 10000 images. Otherwise training will take too long.


```python
from datasets import Dataset, DatasetInfo, Image, Features, ClassLabel,ImageClassification

solar_df = annotations_df[['filename', 'annotation']].rename(columns={'filename': 'image','annotation': 'label'})
solar_df = solar_df.sample(5000).reset_index(drop=True)

features = Features({"image": Image(), "label": ClassLabel(num_classes=2, names=["negative", "positive"])})
dataset_info = DatasetInfo(supervised_keys=("image", "label"), features=features, task_templates=[ImageClassification(image_column="image", label_column="label")])

dataset = Dataset.from_pandas(solar_df, info=dataset_info).cast_column("image", Image())
```

Before the model can use the images, we need to pre-process them. We will also add some random changes to the images. This often leads to models that can predict better on unseen images, and is called `data augmentation`.


```python

from torchvision.transforms import RandomResizedCrop, RandomHorizontalFlip, RandomVerticalFlip, RandomRotation, Compose, Normalize, ToTensor

processor = AutoImageProcessor.from_pretrained("microsoft/resnet-50")

normalize = Normalize(mean=processor.image_mean, std=processor.image_std)
size = (
    processor.size["shortest_edge"]
    if "shortest_edge" in processor.size
    else (processor.size["height"], processor.size["width"])
)

# Add random distortions to the images to improve training
_transforms = Compose([RandomResizedCrop(size),
    RandomHorizontalFlip(p=0.4),
    RandomVerticalFlip(p=0.2),
    RandomRotation(degrees=4),
    ToTensor(), normalize])

def transform(examples):
    examples["pixel_values"] = [_transforms(img.convert("RGB")) for img in examples["image"]]
    del examples["image"]
    return examples

#dataset.set_transform(transform)
dataset = dataset.with_transform(transform)
```

After preparing the data, we split it in a training set and a test set. Here we use 80% of the sample of 10000 images as training set, and 20% as test set.


```python
# Split the data into train and test sets
dataset = dataset.train_test_split(test_size=0.2, stratify_by_column="label", seed=42)
dataset
```




    DatasetDict({
        train: Dataset({
            features: ['image', 'label'],
            num_rows: 4000
        })
        test: Dataset({
            features: ['image', 'label'],
            num_rows: 1000
        })
    })



After we created a training set and a test set, we need to set up the training. Here we want to fine-tune the model in 15 epochs and a batch size of 64. Deep-learning models are trained incrementally. That means that we offer them a small sample, or mini-batch, of training data, train them, and then continue with the next sample/mini-batch. In this case, we offer the ResNet model 64 images per iteration. We continue this process until we ran through all the training data. This is called an epoch. In total, we train the model for 15 epochs. That means we run through the complete training set 15 times.

Next to the `number of epochs` and the `batch size`, there are also other settings we can use to influence model training. Another important one is the `learning rate`. These settings are called `hyper-parameters`. You will find that you will often use most time to choose hyper-parameters that work well. Luckily, the Hugging Face package provides us with an automatic way of optimize the `hyper-parameters`.


```python
from transformers import TrainingArguments

batch_size = 64
number_of_epochs = 15

training_args = TrainingArguments(
    output_dir="test_trainer",
    remove_unused_columns=False,
    evaluation_strategy="epoch",
    save_strategy="epoch",
    learning_rate=1e-3,
    per_device_train_batch_size=batch_size,
    gradient_accumulation_steps=4,
    per_device_eval_batch_size=batch_size,
    num_train_epochs=number_of_epochs,
    warmup_ratio=0.1,
    logging_steps=10,
    load_best_model_at_end=True,
    push_to_hub=False
)
```

    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/transformers/training_args.py:1575: FutureWarning: `evaluation_strategy` is deprecated and will be removed in version 4.46 of 🤗 Transformers. Use `eval_strategy` instead
      warnings.warn(


Check if we are using a GPU for training.


```python
training_args.device
```




    device(type='mps')



Last, we prepare the model. A standard ResNet model is trained on an ImageNet dataset that has 1000 different classes. Here, we are only trying to predict two classes: the presence or the absence of solar panels. Therefore, we have to slightly change the classifier of the image. After that, we prepare the HuggingFace `Trainer` class that will perform the fine-tuning for us.


```python
from transformers import Trainer, DefaultDataCollator, ResNetForImageClassification
import torch.nn as nn


# Setup model fine-tuning
model = ResNetForImageClassification.from_pretrained("microsoft/resnet-50")
# Normally ResNet classifies 1000 classes, now we need only 2
model.num_labels = 2
# We also need to adapt the classifier to classify only 2 classes
model.classifier = nn.Sequential(
    nn.Flatten(),
    nn.Linear(2048, 2)
)

data_collator = DefaultDataCollator(return_tensors="pt")
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=dataset["train"],
    eval_dataset=dataset["test"],
    data_collator=data_collator,
    tokenizer=processor,
    compute_metrics=compute_metrics
)
```

    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_55868/1734265718.py:16: FutureWarning: `tokenizer` is deprecated and will be removed in version 5.0.0 for `Trainer.__init__`. Use `processing_class` instead.
      trainer = Trainer(


Now it is time to fine-tune the model. We use the solar panel dataset to update the weights of the ResNet model for a further 10 epochs. After training, the model should be optimized further to recognized the presence or absence of solar panels. Training will take a bit of time. The more epochs and the bigger the dataset, the longer the training will take.


```python
# Fine-tune
trainer.train()
```



    <div>

      <progress value='225' max='225' style='width:300px; height:20px; vertical-align: middle;'></progress>
      [225/225 12:05, Epoch 14/15]
    </div>
    <table border="1" class="dataframe">
  <thead>
 <tr style="text-align: left;">
      <th>Epoch</th>
      <th>Training Loss</th>
      <th>Validation Loss</th>
      <th>F1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>0.619400</td>
      <td>0.410381</td>
      <td>0.740298</td>
    </tr>
    <tr>
      <td>2</td>
      <td>0.389100</td>
      <td>0.374856</td>
      <td>0.740298</td>
    </tr>
    <tr>
      <td>3</td>
      <td>0.344400</td>
      <td>0.298877</td>
      <td>0.886394</td>
    </tr>
    <tr>
      <td>4</td>
      <td>0.280000</td>
      <td>0.267563</td>
      <td>0.897643</td>
    </tr>
    <tr>
      <td>5</td>
      <td>0.224600</td>
      <td>0.227635</td>
      <td>0.917910</td>
    </tr>
    <tr>
      <td>6</td>
      <td>0.247000</td>
      <td>0.237246</td>
      <td>0.911847</td>
    </tr>
    <tr>
      <td>7</td>
      <td>0.216300</td>
      <td>0.237963</td>
      <td>0.907154</td>
    </tr>
    <tr>
      <td>8</td>
      <td>0.207200</td>
      <td>0.218025</td>
      <td>0.925483</td>
    </tr>
    <tr>
      <td>9</td>
      <td>0.215200</td>
      <td>0.208645</td>
      <td>0.918709</td>
    </tr>
    <tr>
      <td>10</td>
      <td>0.212200</td>
      <td>0.238706</td>
      <td>0.909597</td>
    </tr>
    <tr>
      <td>11</td>
      <td>0.213800</td>
      <td>0.221628</td>
      <td>0.920848</td>
    </tr>
    <tr>
      <td>12</td>
      <td>0.196600</td>
      <td>0.230713</td>
      <td>0.921063</td>
    </tr>
    <tr>
      <td>13</td>
      <td>0.193700</td>
      <td>0.212522</td>
      <td>0.918010</td>
    </tr>
    <tr>
      <td>14</td>
      <td>0.181000</td>
      <td>0.222954</td>
      <td>0.914302</td>
    </tr>
  </tbody>
</table><p>





    TrainOutput(global_step=225, training_loss=0.26178008132510716, metrics={'train_runtime': 737.2942, 'train_samples_per_second': 81.379, 'train_steps_per_second': 0.305, 'total_flos': 1.194618855236567e+18, 'train_loss': 0.26178008132510716, 'epoch': 14.063492063492063})




```python
import pandas as pd

loss_df = pd.DataFrame(trainer.state.log_history)
loss_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>loss</th>
      <th>grad_norm</th>
      <th>learning_rate</th>
      <th>epoch</th>
      <th>step</th>
      <th>eval_loss</th>
      <th>eval_f1</th>
      <th>eval_runtime</th>
      <th>eval_samples_per_second</th>
      <th>eval_steps_per_second</th>
      <th>train_runtime</th>
      <th>train_samples_per_second</th>
      <th>train_steps_per_second</th>
      <th>total_flos</th>
      <th>train_loss</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.6194</td>
      <td>1.845302</td>
      <td>0.000435</td>
      <td>0.634921</td>
      <td>10</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.000000</td>
      <td>16</td>
      <td>0.410381</td>
      <td>0.740298</td>
      <td>5.6848</td>
      <td>175.909</td>
      <td>2.815</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.4498</td>
      <td>1.436900</td>
      <td>0.000870</td>
      <td>1.253968</td>
      <td>20</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.3891</td>
      <td>0.928942</td>
      <td>0.000965</td>
      <td>1.888889</td>
      <td>30</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2.000000</td>
      <td>32</td>
      <td>0.374856</td>
      <td>0.740298</td>
      <td>5.3778</td>
      <td>185.949</td>
      <td>2.975</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Get loss values and steps from training logs
logs = trainer.state.log_history
train_loss = [(log['epoch'], log['loss']) for log in logs if 'loss' in log]
val_loss = [(log['epoch'], log['eval_loss']) for log in logs if 'eval_loss' in log]

train_epochs, losses = zip(*train_loss)
val_epochs, val_losses = zip(*val_loss)
plt.figure()

plt.plot(train_epochs, losses, label="loss")
plt.plot(val_epochs, val_losses, color="y", label="val_loss")

plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend(loc="best")

```




    <matplotlib.legend.Legend at 0x39d3ab210>




    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_55_1.png)
    


After training, we first evaluate the fine-tuned model on our test set. We let it predict a class for each image in the test set.


```python
# Evaluate
solar_predictions_fine_tuned = trainer.predict(dataset["test"])
```





Let's see what the label distribution in the test set looks like. We see the majority of the images do not have a solar panel (label 0).


```python
solar_fine_tune_labels = dataset["test"].to_pandas().label.values
pd.Series(solar_fine_tune_labels).value_counts()
```




    0    821
    1    179
    Name: count, dtype: int64



We can compare this to the label distribution of our predictions. We see that the model predicts slightly different amounts of images with a solar panel (label 1) and without a solar panel (label 0)


```python
solar_fine_pred = solar_predictions_fine_tuned.predictions.argmax(axis=-1)
pd.Series(solar_fine_pred).value_counts()
```




    0    877
    1    123
    Name: count, dtype: int64



To see how well the model predicts solar panels, we use the `sklearn classification_report` function. We can compare this classification report with the one that we provided above, when we trained a `LogisticRegression` classifier on the features extracted by a standard ResNet model.


```python
from sklearn.metrics import classification_report

print(classification_report(solar_fine_tune_labels, solar_fine_pred))
```

                  precision    recall  f1-score   support
    
               0       0.92      0.98      0.95       821
               1       0.87      0.60      0.71       179
    
        accuracy                           0.91      1000
       macro avg       0.89      0.79      0.83      1000
    weighted avg       0.91      0.91      0.91      1000
    


Compare this to the classification report from above, and we see that in general the fine-tuned model performs better. However, the test set we were using before contains different and a lot less pictures than the one we are using above.


```python
print(classification_report(test_solar_labels, solar_predictions))
```

                  precision    recall  f1-score   support
    
               0       0.85      0.95      0.90        78
               1       0.69      0.41      0.51        22
    
        accuracy                           0.83       100
       macro avg       0.77      0.68      0.71       100
    weighted avg       0.82      0.83      0.81       100
    


If we use the same images, we see a different story. First preprocess the images:


```python
inputs = processor(images, return_tensors="pt")
```

Then extract the features from our fine-tuned model:


```python
with torch.no_grad():
    fine_tuned_model = trainer.model.to("cpu")
    fine_tuned_features = fine_tuned_model.resnet(**inputs).pooler_output.squeeze()
```


```python
fine_tuned_features
```




    tensor([[0.0000, 0.0087, 0.0000,  ..., 0.0000, 0.5546, 0.0000],
            [0.2895, 0.1245, 0.0089,  ..., 0.0368, 0.0107, 0.0000],
            [0.2414, 0.0000, 0.0145,  ..., 0.0000, 0.0492, 0.0000],
            ...,
            [0.2821, 0.0712, 0.0319,  ..., 0.0055, 0.0675, 0.0000],
            [0.1160, 0.2354, 0.0173,  ..., 0.1559, 0.0661, 0.3714],
            [0.0908, 0.0252, 0.1303,  ..., 0.1240, 0.0010, 0.0000]])



So that we can visualize them with a TSNE.


```python
tsne_plot(fine_tuned_features, solar_labels)
```


    
![png](11d%20-%20Fine-tuning_files/11d%20-%20Fine-tuning_72_0.png)
    


Here it becomes clear that the model has learned to separate the solar panel/no solar panel classes a lot better. If we train a logistic regression on these features, we also see that it performs a lot better than the logistic regression we trained on the original (not fine-tuned) ResNet features. This shows that fine-tuning makes a model learn new features, so that it can perform new tasks, i.e recognize previously unseen image classes, a lot more effectively. In addition, fine-tuning is a lot faster than training a model from scratch.


```python
# Split the data into train and test sets
train_fine_tuned_features, test_fine_tuned_features, train_fine_tuned_labels, test_fine_tuned_labels = train_test_split(fine_tuned_features, solar_labels, test_size=0.2, random_state=42)

lr_model = LogisticRegression()
lr_model.fit(train_fine_tuned_features, train_fine_tuned_labels)

# Make predictions
fine_tuned_predictions = lr_model.predict(test_fine_tuned_features)

# Print classification report
print(classification_report(test_fine_tuned_labels, fine_tuned_predictions))
```

                  precision    recall  f1-score   support
    
               0       0.92      0.99      0.95        78
               1       0.94      0.68      0.79        22
    
        accuracy                           0.92       100
       macro avg       0.93      0.83      0.87       100
    weighted avg       0.92      0.92      0.92       100
    

