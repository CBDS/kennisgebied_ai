# Unsupervised Learning: DB-Scan Clustering

In the previous notebook, you learned about K-Means clustering, which is a centroid-based clustering algorithm. In this notebook, you will learn about `DBSCAN` clustering, which is a density-based clustering algorithm. 

The `DBSCAN` algorithm is based on the idea that a cluster is a dense area surrounded by a sparse area. The algorithm works by defining clusters as continuous regions of high density. Here are the main concepts of the `DBSCAN` algorithm:

- **Core Points**: A point is a core point if it has at least a minimum number of other points ($minPts$) within a distance $\epsilon$.
- **Directly Reachable**: A point $P$ is directly reachable from another point $Q$ if $P$ is within $\epsilon$ distance from $Q$ and $Q$ is a core point.
- **Reachable Points**: A point is a reachable point if it is reachable from a core point and has fewer than $minPts$ within $\epsilon$.
- **Noise Points**: All other points that are neither core points nor reachable points are noise points.
- **Reachability**: A point $Q$ is reachable from another point P if there is a path of points $P_1, P_2, \ldots , P_n$, with $P_1 = P$ and $P_n = Q$, such that each Pi+1 is directly reachable from Pi.


The `DBSCAN` algorithm works as follows:

1. Randomly select a point P.
2. Retrieve all points directly reachable from P.
3. If P is a core point, a cluster is formed.
4. If P is a reachable point, no more points (than the core point) are reachable from P, and `DBSCAN` moves to the next point.
5. Repeat the process until all points have been processed.

For a graphical representation of the `DBSCAN` algorithm see the course slides. For more information see the [DBSCAN Wikipedia page](https://en.wikipedia.org/wiki/DBSCAN).

In this notebook, you will apply the DBSCAN algorithm to the shop customer dataset we saw in the previous notebook. Luckily, `sklearn` makes it easy to apply the `DBSCAN` algorithm. Let's get started!



```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd

customers_df = pd.read_csv(os.path.join(data_directory, 'customers-dataset/Customers.csv'))
customers_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CustomerID</th>
      <th>Gender</th>
      <th>Age</th>
      <th>Annual Income ($)</th>
      <th>Spending Score (1-100)</th>
      <th>Profession</th>
      <th>Work Experience</th>
      <th>Family Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>Male</td>
      <td>19</td>
      <td>15000</td>
      <td>39</td>
      <td>Healthcare</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>Male</td>
      <td>21</td>
      <td>35000</td>
      <td>81</td>
      <td>Engineer</td>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>Female</td>
      <td>20</td>
      <td>86000</td>
      <td>6</td>
      <td>Engineer</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>Female</td>
      <td>23</td>
      <td>59000</td>
      <td>77</td>
      <td>Lawyer</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>Female</td>
      <td>31</td>
      <td>38000</td>
      <td>40</td>
      <td>Entertainment</td>
      <td>2</td>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>



## Preprocessing

Before doing any clustering, you need to preprocess the data. We perform the same preprocessing steps as in the previous notebook. First, we drop the `CustomerID` column:


```python
customers_df.drop(columns=['CustomerID'], inplace=True)
```

Then we impute the missing values in the `Profession` column:


```python
customers_df["Profession"].fillna('mode', inplace=True)
```

Next, we encode the categorical columns using the `LabelEncoder` class from `sklearn`:


```python
from sklearn.preprocessing import LabelEncoder

categorical_columns = ["Gender", "Profession"]

for categorical_column in categorical_columns:
    encoder = LabelEncoder()
    customers_df[categorical_column] = encoder.fit_transform(customers_df[categorical_column])

customers_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age</th>
      <th>Annual Income ($)</th>
      <th>Spending Score (1-100)</th>
      <th>Profession</th>
      <th>Work Experience</th>
      <th>Family Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>19</td>
      <td>15000</td>
      <td>39</td>
      <td>5</td>
      <td>1</td>
      <td>4</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>21</td>
      <td>35000</td>
      <td>81</td>
      <td>2</td>
      <td>3</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0</td>
      <td>20</td>
      <td>86000</td>
      <td>6</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0</td>
      <td>23</td>
      <td>59000</td>
      <td>77</td>
      <td>7</td>
      <td>0</td>
      <td>2</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0</td>
      <td>31</td>
      <td>38000</td>
      <td>40</td>
      <td>3</td>
      <td>2</td>
      <td>6</td>
    </tr>
  </tbody>
</table>
</div>



Finally, we scale the data using the `StandardScaler` class from `sklearn`:


```python
from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()

customers_df[customers_df.columns] = scaler.fit_transform(customers_df)
customers_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Gender</th>
      <th>Age</th>
      <th>Annual Income ($)</th>
      <th>Spending Score (1-100)</th>
      <th>Profession</th>
      <th>Work Experience</th>
      <th>Family Size</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.207064</td>
      <td>-1.054089</td>
      <td>-2.093501</td>
      <td>-0.428339</td>
      <td>0.778356</td>
      <td>-0.791207</td>
      <td>0.117497</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.207064</td>
      <td>-0.983723</td>
      <td>-1.656133</td>
      <td>1.075546</td>
      <td>-0.354347</td>
      <td>-0.281162</td>
      <td>-0.390051</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.828457</td>
      <td>-1.018906</td>
      <td>-0.540845</td>
      <td>-1.609962</td>
      <td>-0.354347</td>
      <td>-0.791207</td>
      <td>-1.405148</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-0.828457</td>
      <td>-0.913356</td>
      <td>-1.131292</td>
      <td>0.932319</td>
      <td>1.533491</td>
      <td>-1.046230</td>
      <td>-0.897599</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-0.828457</td>
      <td>-0.631891</td>
      <td>-1.590528</td>
      <td>-0.392532</td>
      <td>0.023220</td>
      <td>-0.536185</td>
      <td>1.132594</td>
    </tr>
  </tbody>
</table>
</div>



## Clustering

Now that you have preprocessed the data, you can apply the `DBSCAN` algorithm to the data. For more information, see the documentation on the  [sklearn DBSCAN](https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html) class.


```python
#begin solution
```


```python
#TODO can we optimize this further?
from sklearn.cluster import DBSCAN

dbscan = DBSCAN(eps=0.7, min_samples=5)
dbscan.fit(customers_df)

customers_df['Cluster'] = dbscan.labels_
```

### Evaluating the DBSCAN algorithm


```python
from sklearn.metrics import silhouette_score, calinski_harabasz_score, davies_bouldin_score

dbscan_evaluation = pd.DataFrame({
    'silhouette_score': silhouette_score(customers_df, dbscan.labels_),
    'calinski_harabasz_score': calinski_harabasz_score(customers_df, dbscan.labels_)
}, index=[0])

dbscan_evaluation
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>silhouette_score</th>
      <th>calinski_harabasz_score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.072401</td>
      <td>9.015241</td>
    </tr>
  </tbody>
</table>
</div>



### Visualizing the clusters

Let's visualize the clusters created by the `DBSCAN` algorithm. Again, we will use a `PCA` to reduce the dimensionality of the data to two dimensions. We will then plot the clusters in the `PCA` dimensions.


```python
from sklearn.decomposition import PCA
import seaborn as sns

pca = PCA(n_components=2)
customers_df_pca = pca.fit_transform(customers_df[customers_df.columns[:-1]])

sns.scatterplot(x=customers_df_pca[:, 0], y=customers_df_pca[:, 1], hue=customers_df["Cluster"], palette='viridis')
```




    <Axes: >




    
![png](08b%20-%20Unsupervised%20Learning_dbscan_files/08b%20-%20Unsupervised%20Learning_dbscan_18_1.png)
    


To make the clusters more apparent, we filter out the noise points. We then plot the clusters in different colors.


```python
customers_clusters_df = customers_df[customers_df['Cluster'] != -1]

pca = PCA(n_components=2)
customers_clusters_pca = pca.fit_transform(customers_clusters_df[customers_clusters_df.columns[:-1]])

sns.scatterplot(x=customers_clusters_pca[:, 0], y=customers_clusters_pca[:, 1], hue=customers_clusters_df["Cluster"], palette='viridis')
```




    <Axes: >




    
![png](08b%20-%20Unsupervised%20Learning_dbscan_files/08b%20-%20Unsupervised%20Learning_dbscan_20_1.png)
    


As we can see, the `DBSCAN` algorithm also does not perform well on this dataset. We see it has created three clusters, but the clusters are not very clear. In addition, a large majority of the points are classified as noise points.


```python
#end solution
```


# Conclusion

In this notebook, you learned about the `DBSCAN` clustering algorithm. You applied the `DBSCAN` algorithm to the shop customer dataset and visualized the dataset. You've learned that it is difficult to compare the performance of the `DBSCAN` algorithm with the `K-Means` algorithm because `DBSCAN` works very differently. The `DBSCAN` algorithm is a density-based clustering algorithm that is useful when the clusters are not spherical or when the clusters have different densities. Measures like the `silhouette score` and the `calinsk-harabasz` are not that useful for evaluating the performance of the `DBSCAN` algorithm, as these scores are based on the distance between the points and the centroids and therefore prefer spherical clusters. When comparing the `DBSCAN` algorithm with the `K-Means` algorithm, these measures would almost always favor the `K-means` algorithm (more information [here](https://www.reddit.com/r/datascience/comments/yzir4v/comment/ix81sb0/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button) and [here](https://github.com/christopherjenness/DBCV)). 

In this lies the difficulty of evaluating the performance of cluster algorithms. As we often do not have a ground-truth value, as was the case with supervised learning, the evaluation of cluster algorithms is often subjective. The best way to evaluate the performance of a clustering algorithm is to use domain knowledge and visualize the clusters. 
