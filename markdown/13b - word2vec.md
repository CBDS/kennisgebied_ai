# Word2Vec

## Unsupervised Learning [Word2Vec](https://radimrehurek.com/gensim/models/word2vec.html) [gensim]

State-of-the-art algorithm (altough it is already some years of age)

Given a text, it moves over all words using a viewing range. Within this range it looks at all words and tries to predict the current word.
To do so, it places all words as points in an X dimensional space. From this space the model generates noise answers and amidst them the current word. The model then uses binary classification to select which word is the real one and updates its model accordingly.

Unlike other models, word2vec is not used for its prediction capabilities. After having trained a model, we inspect and use the internal vectors of the model instead of just using it as a blackbox.


### Advantages of Word2Vec
------------------------------------------------------------
- Dense vector representations of words
- Easy to use for machine learning
- Results can be used for simple vector calculus

### Disadvantages of Word2Vec
------------------------------------------------------------
- Limited to known words
- Difficult to understand the vectors
- Learnings based on corpus, faulty corpus = faulty results
   - Unlike faulty data on (traditional) supervised algorithms, it can be very difficult to detect faulty data here.

### The parameters of a Perceptron include
------------------------------------------------------------
- Vector size of the model
- Dictionary of known words




```python
import gensim.downloader as api
import pandas as pd

# the twitter model is smaller and more up to date than the googlenews model.
# if you whish to use the Google model, you'll need to alter the RAM for the VM (to around 6 GB)
# for this course the twitter model is sufficient

# the Glove model type is a further adjustment to the original word2vec algorithm for more information you
# can visit the site: https://nlp.stanford.edu/projects/glove/

model = api.load("glove-twitter-100")
```

    [==================================================] 100.0% 387.1/387.1MB downloaded


The trained model can come to some interesting results. Not all results are readable and some are relatable. Keep in mind, the data used to train the model from GoogleNews might be a bit dated.


```python
pd.DataFrame(model.similar_by_word('virus'), columns=['Word', 'Match'])
```





  <div id="df-1c93d790-469f-4481-8854-a7e8a7b11cda" class="colab-df-container">
    <div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Word</th>
      <th>Match</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>malware</td>
      <td>0.687942</td>
    </tr>
    <tr>
      <th>1</th>
      <td>hepatitis</td>
      <td>0.647192</td>
    </tr>
    <tr>
      <th>2</th>
      <td>influenza</td>
      <td>0.630611</td>
    </tr>
    <tr>
      <th>3</th>
      <td>flu</td>
      <td>0.609586</td>
    </tr>
    <tr>
      <th>4</th>
      <td>anti</td>
      <td>0.608975</td>
    </tr>
    <tr>
      <th>5</th>
      <td>hiv</td>
      <td>0.602588</td>
    </tr>
    <tr>
      <th>6</th>
      <td>hacker</td>
      <td>0.596336</td>
    </tr>
    <tr>
      <th>7</th>
      <td>tumor</td>
      <td>0.583012</td>
    </tr>
    <tr>
      <th>8</th>
      <td>tsunami</td>
      <td>0.579735</td>
    </tr>
    <tr>
      <th>9</th>
      <td>malaria</td>
      <td>0.579490</td>
    </tr>
  </tbody>
</table>
</div>
    <div class="colab-df-buttons">

  <div class="colab-df-container">
    <button class="colab-df-convert" onclick="convertToInteractive('df-1c93d790-469f-4481-8854-a7e8a7b11cda')"
            title="Convert this dataframe to an interactive table."
            style="display:none;">

  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960">
    <path d="M120-120v-720h720v720H120Zm60-500h600v-160H180v160Zm220 220h160v-160H400v160Zm0 220h160v-160H400v160ZM180-400h160v-160H180v160Zm440 0h160v-160H620v160ZM180-180h160v-160H180v160Zm440 0h160v-160H620v160Z"/>
  </svg>
    </button>

  <style>
    .colab-df-container {
      display:flex;
      gap: 12px;
    }

    .colab-df-convert {
      background-color: #E8F0FE;
      border: none;
      border-radius: 50%;
      cursor: pointer;
      display: none;
      fill: #1967D2;
      height: 32px;
      padding: 0 0 0 0;
      width: 32px;
    }

    .colab-df-convert:hover {
      background-color: #E2EBFA;
      box-shadow: 0px 1px 2px rgba(60, 64, 67, 0.3), 0px 1px 3px 1px rgba(60, 64, 67, 0.15);
      fill: #174EA6;
    }

    .colab-df-buttons div {
      margin-bottom: 4px;
    }

    [theme=dark] .colab-df-convert {
      background-color: #3B4455;
      fill: #D2E3FC;
    }

    [theme=dark] .colab-df-convert:hover {
      background-color: #434B5C;
      box-shadow: 0px 1px 3px 1px rgba(0, 0, 0, 0.15);
      filter: drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.3));
      fill: #FFFFFF;
    }
  </style>

    <script>
      const buttonEl =
        document.querySelector('#df-1c93d790-469f-4481-8854-a7e8a7b11cda button.colab-df-convert');
      buttonEl.style.display =
        google.colab.kernel.accessAllowed ? 'block' : 'none';

      async function convertToInteractive(key) {
        const element = document.querySelector('#df-1c93d790-469f-4481-8854-a7e8a7b11cda');
        const dataTable =
          await google.colab.kernel.invokeFunction('convertToInteractive',
                                                    [key], {});
        if (!dataTable) return;

        const docLinkHtml = 'Like what you see? Visit the ' +
          '<a target="_blank" href=https://colab.research.google.com/notebooks/data_table.ipynb>data table notebook</a>'
          + ' to learn more about interactive tables.';
        element.innerHTML = '';
        dataTable['output_type'] = 'display_data';
        await google.colab.output.renderOutput(dataTable, element);
        const docLink = document.createElement('div');
        docLink.innerHTML = docLinkHtml;
        element.appendChild(docLink);
      }
    </script>
  </div>


<div id="df-91f882db-8370-41e4-8cd2-dfa138d127ac">
  <button class="colab-df-quickchart" onclick="quickchart('df-91f882db-8370-41e4-8cd2-dfa138d127ac')"
            title="Suggest charts"
            style="display:none;">

<svg xmlns="http://www.w3.org/2000/svg" height="24px"viewBox="0 0 24 24"
     width="24px">
    <g>
        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
    </g>
</svg>
  </button>

<style>
  .colab-df-quickchart {
      --bg-color: #E8F0FE;
      --fill-color: #1967D2;
      --hover-bg-color: #E2EBFA;
      --hover-fill-color: #174EA6;
      --disabled-fill-color: #AAA;
      --disabled-bg-color: #DDD;
  }

  [theme=dark] .colab-df-quickchart {
      --bg-color: #3B4455;
      --fill-color: #D2E3FC;
      --hover-bg-color: #434B5C;
      --hover-fill-color: #FFFFFF;
      --disabled-bg-color: #3B4455;
      --disabled-fill-color: #666;
  }

  .colab-df-quickchart {
    background-color: var(--bg-color);
    border: none;
    border-radius: 50%;
    cursor: pointer;
    display: none;
    fill: var(--fill-color);
    height: 32px;
    padding: 0;
    width: 32px;
  }

  .colab-df-quickchart:hover {
    background-color: var(--hover-bg-color);
    box-shadow: 0 1px 2px rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15);
    fill: var(--button-hover-fill-color);
  }

  .colab-df-quickchart-complete:disabled,
  .colab-df-quickchart-complete:disabled:hover {
    background-color: var(--disabled-bg-color);
    fill: var(--disabled-fill-color);
    box-shadow: none;
  }

  .colab-df-spinner {
    border: 2px solid var(--fill-color);
    border-color: transparent;
    border-bottom-color: var(--fill-color);
    animation:
      spin 1s steps(1) infinite;
  }

  @keyframes spin {
    0% {
      border-color: transparent;
      border-bottom-color: var(--fill-color);
      border-left-color: var(--fill-color);
    }
    20% {
      border-color: transparent;
      border-left-color: var(--fill-color);
      border-top-color: var(--fill-color);
    }
    30% {
      border-color: transparent;
      border-left-color: var(--fill-color);
      border-top-color: var(--fill-color);
      border-right-color: var(--fill-color);
    }
    40% {
      border-color: transparent;
      border-right-color: var(--fill-color);
      border-top-color: var(--fill-color);
    }
    60% {
      border-color: transparent;
      border-right-color: var(--fill-color);
    }
    80% {
      border-color: transparent;
      border-right-color: var(--fill-color);
      border-bottom-color: var(--fill-color);
    }
    90% {
      border-color: transparent;
      border-bottom-color: var(--fill-color);
    }
  }
</style>

  <script>
    async function quickchart(key) {
      const quickchartButtonEl =
        document.querySelector('#' + key + ' button');
      quickchartButtonEl.disabled = true;  // To prevent multiple clicks.
      quickchartButtonEl.classList.add('colab-df-spinner');
      try {
        const charts = await google.colab.kernel.invokeFunction(
            'suggestCharts', [key], {});
      } catch (error) {
        console.error('Error during call to suggestCharts:', error);
      }
      quickchartButtonEl.classList.remove('colab-df-spinner');
      quickchartButtonEl.classList.add('colab-df-quickchart-complete');
    }
    (() => {
      let quickchartButtonEl =
        document.querySelector('#df-91f882db-8370-41e4-8cd2-dfa138d127ac button');
      quickchartButtonEl.style.display =
        google.colab.kernel.accessAllowed ? 'block' : 'none';
    })();
  </script>
</div>
    </div>
  </div>





```python
pd.DataFrame(model.similar_by_word('trump'), columns=['Word', 'Match'])
```





  <div id="df-946205bd-23bb-489e-9d2e-8bb7181a925a" class="colab-df-container">
    <div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Word</th>
      <th>Match</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>donald</td>
      <td>0.712049</td>
    </tr>
    <tr>
      <th>1</th>
      <td>rogers</td>
      <td>0.636376</td>
    </tr>
    <tr>
      <th>2</th>
      <td>buffett</td>
      <td>0.632475</td>
    </tr>
    <tr>
      <th>3</th>
      <td>birther</td>
      <td>0.621992</td>
    </tr>
    <tr>
      <th>4</th>
      <td>bloomberg</td>
      <td>0.609225</td>
    </tr>
    <tr>
      <th>5</th>
      <td>zuckerberg</td>
      <td>0.590997</td>
    </tr>
    <tr>
      <th>6</th>
      <td>warren</td>
      <td>0.588790</td>
    </tr>
    <tr>
      <th>7</th>
      <td>christie</td>
      <td>0.578354</td>
    </tr>
    <tr>
      <th>8</th>
      <td>biden</td>
      <td>0.575188</td>
    </tr>
    <tr>
      <th>9</th>
      <td>rockefeller</td>
      <td>0.571660</td>
    </tr>
  </tbody>
</table>
</div>
    <div class="colab-df-buttons">

  <div class="colab-df-container">
    <button class="colab-df-convert" onclick="convertToInteractive('df-946205bd-23bb-489e-9d2e-8bb7181a925a')"
            title="Convert this dataframe to an interactive table."
            style="display:none;">

  <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960">
    <path d="M120-120v-720h720v720H120Zm60-500h600v-160H180v160Zm220 220h160v-160H400v160Zm0 220h160v-160H400v160ZM180-400h160v-160H180v160Zm440 0h160v-160H620v160ZM180-180h160v-160H180v160Zm440 0h160v-160H620v160Z"/>
  </svg>
    </button>

  <style>
    .colab-df-container {
      display:flex;
      gap: 12px;
    }

    .colab-df-convert {
      background-color: #E8F0FE;
      border: none;
      border-radius: 50%;
      cursor: pointer;
      display: none;
      fill: #1967D2;
      height: 32px;
      padding: 0 0 0 0;
      width: 32px;
    }

    .colab-df-convert:hover {
      background-color: #E2EBFA;
      box-shadow: 0px 1px 2px rgba(60, 64, 67, 0.3), 0px 1px 3px 1px rgba(60, 64, 67, 0.15);
      fill: #174EA6;
    }

    .colab-df-buttons div {
      margin-bottom: 4px;
    }

    [theme=dark] .colab-df-convert {
      background-color: #3B4455;
      fill: #D2E3FC;
    }

    [theme=dark] .colab-df-convert:hover {
      background-color: #434B5C;
      box-shadow: 0px 1px 3px 1px rgba(0, 0, 0, 0.15);
      filter: drop-shadow(0px 1px 2px rgba(0, 0, 0, 0.3));
      fill: #FFFFFF;
    }
  </style>

    <script>
      const buttonEl =
        document.querySelector('#df-946205bd-23bb-489e-9d2e-8bb7181a925a button.colab-df-convert');
      buttonEl.style.display =
        google.colab.kernel.accessAllowed ? 'block' : 'none';

      async function convertToInteractive(key) {
        const element = document.querySelector('#df-946205bd-23bb-489e-9d2e-8bb7181a925a');
        const dataTable =
          await google.colab.kernel.invokeFunction('convertToInteractive',
                                                    [key], {});
        if (!dataTable) return;

        const docLinkHtml = 'Like what you see? Visit the ' +
          '<a target="_blank" href=https://colab.research.google.com/notebooks/data_table.ipynb>data table notebook</a>'
          + ' to learn more about interactive tables.';
        element.innerHTML = '';
        dataTable['output_type'] = 'display_data';
        await google.colab.output.renderOutput(dataTable, element);
        const docLink = document.createElement('div');
        docLink.innerHTML = docLinkHtml;
        element.appendChild(docLink);
      }
    </script>
  </div>


<div id="df-15fe33b6-7393-4f19-bd42-c6445f40c7b8">
  <button class="colab-df-quickchart" onclick="quickchart('df-15fe33b6-7393-4f19-bd42-c6445f40c7b8')"
            title="Suggest charts"
            style="display:none;">

<svg xmlns="http://www.w3.org/2000/svg" height="24px"viewBox="0 0 24 24"
     width="24px">
    <g>
        <path d="M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z"/>
    </g>
</svg>
  </button>

<style>
  .colab-df-quickchart {
      --bg-color: #E8F0FE;
      --fill-color: #1967D2;
      --hover-bg-color: #E2EBFA;
      --hover-fill-color: #174EA6;
      --disabled-fill-color: #AAA;
      --disabled-bg-color: #DDD;
  }

  [theme=dark] .colab-df-quickchart {
      --bg-color: #3B4455;
      --fill-color: #D2E3FC;
      --hover-bg-color: #434B5C;
      --hover-fill-color: #FFFFFF;
      --disabled-bg-color: #3B4455;
      --disabled-fill-color: #666;
  }

  .colab-df-quickchart {
    background-color: var(--bg-color);
    border: none;
    border-radius: 50%;
    cursor: pointer;
    display: none;
    fill: var(--fill-color);
    height: 32px;
    padding: 0;
    width: 32px;
  }

  .colab-df-quickchart:hover {
    background-color: var(--hover-bg-color);
    box-shadow: 0 1px 2px rgba(60, 64, 67, 0.3), 0 1px 3px 1px rgba(60, 64, 67, 0.15);
    fill: var(--button-hover-fill-color);
  }

  .colab-df-quickchart-complete:disabled,
  .colab-df-quickchart-complete:disabled:hover {
    background-color: var(--disabled-bg-color);
    fill: var(--disabled-fill-color);
    box-shadow: none;
  }

  .colab-df-spinner {
    border: 2px solid var(--fill-color);
    border-color: transparent;
    border-bottom-color: var(--fill-color);
    animation:
      spin 1s steps(1) infinite;
  }

  @keyframes spin {
    0% {
      border-color: transparent;
      border-bottom-color: var(--fill-color);
      border-left-color: var(--fill-color);
    }
    20% {
      border-color: transparent;
      border-left-color: var(--fill-color);
      border-top-color: var(--fill-color);
    }
    30% {
      border-color: transparent;
      border-left-color: var(--fill-color);
      border-top-color: var(--fill-color);
      border-right-color: var(--fill-color);
    }
    40% {
      border-color: transparent;
      border-right-color: var(--fill-color);
      border-top-color: var(--fill-color);
    }
    60% {
      border-color: transparent;
      border-right-color: var(--fill-color);
    }
    80% {
      border-color: transparent;
      border-right-color: var(--fill-color);
      border-bottom-color: var(--fill-color);
    }
    90% {
      border-color: transparent;
      border-bottom-color: var(--fill-color);
    }
  }
</style>

  <script>
    async function quickchart(key) {
      const quickchartButtonEl =
        document.querySelector('#' + key + ' button');
      quickchartButtonEl.disabled = true;  // To prevent multiple clicks.
      quickchartButtonEl.classList.add('colab-df-spinner');
      try {
        const charts = await google.colab.kernel.invokeFunction(
            'suggestCharts', [key], {});
      } catch (error) {
        console.error('Error during call to suggestCharts:', error);
      }
      quickchartButtonEl.classList.remove('colab-df-spinner');
      quickchartButtonEl.classList.add('colab-df-quickchart-complete');
    }
    (() => {
      let quickchartButtonEl =
        document.querySelector('#df-15fe33b6-7393-4f19-bd42-c6445f40c7b8 button');
      quickchartButtonEl.style.display =
        google.colab.kernel.accessAllowed ? 'block' : 'none';
    })();
  </script>
</div>
    </div>
  </div>




The famous example of king - man + woman = ?


```python
result = model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)
print(f'king - man + woman = {str(result[0])}')
```

    king - man + woman = ('queen', 0.7052316069602966)


Influencing the media can have some effects on the outcome. Interpreting the results (even when they appear plausible) can be dangerous.


```python
result = model.most_similar(positive=['dishonest', 'obama'], negative=['honest'], topn=1)
print(f'obama - honest + dishonest = {str(result[0])}')
```

    obama - honest + dishonest = ('romney', 0.643196165561676)



```python
result = model.most_similar(positive=['funny', 'german'], negative=['serious'], topn=1)
print(f'German - serious + funny =', result)
```

    German - serious + funny = [('spanish', 0.666408360004425)]



```python

```
