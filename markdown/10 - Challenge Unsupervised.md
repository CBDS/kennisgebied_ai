# Unsupervised Challenge

Based on CBS data, can you discover a cluster of cities with many students? Do the city names in your cluster make sense (i.e., Amsterdam, Rotterdam, Eindhoven, Tilburg, Maastricht, Breda, Utrecht, Enschede, Groningen, Delft, and more)? Bonus question, within your cluster of 'student cities', can you discover which type of schools, universities are present (e.g., technical ones, social studies?)

The following cities are examples of student cities:
- Amsterdam
- Breda
- Delft
- Den Bosch
- Den Haag
- Eindhoven
- Enschede
- Groningen
- Leeuwarden
- Leiden 
- Maastricht
- Nijmegen
- Rotterdam
- Tilburg
- Utrecht
- Wageningen

Of course, there are more cities with a lot of students, but these are the most well-known ones.

## Read reference data: description of columns in data

The CBS dataset comes with an extra file describing the columns in the dataset. Let's read this file first.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd
 
kwb_description = pd.read_csv(os.path.join(data_directory, "kwb-2018-toelichting.csv"), delimiter=';', names=["column_name", "description", "comments"], index_col=0)
kwb_description.drop(columns=["comments"], inplace=True)
kwb_description.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
  </tbody>
</table>
</div>



We can see that for every column in the dataset, there is a description of the column. This will help us understand the dataset better. So if for example we want to know what the column `a_pau` means, we can look it up in this file using the following code:


```python
kwb_description.loc['a_pau']  # use these descriptions to try and determine the parameter being asked.
```




    description    Personenauto's totaal
    Name: a_pau, dtype: object



After reading the metadata, we can read the dataset itself. The dataset is stored in a CSV file, so we can use the `read_csv` function from the `pandas` library to read the dataset. We will also print the first few rows of the dataset to get an idea of what the dataset looks like.


```python
kwb_data = pd.read_csv(os.path.join(data_directory, "kwb-2018.csv"), delimiter=';', encoding="latin1")
kwb_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>a_bst_b</th>
      <th>a_bst_nb</th>
      <th>g_pau_hh</th>
      <th>g_pau_km</th>
      <th>a_m2w</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NL00</td>
      <td>0</td>
      <td>Nederland</td>
      <td>Nederland</td>
      <td>Land</td>
      <td>NL00</td>
      <td>.</td>
      <td>17181084</td>
      <td>8527041</td>
      <td>8654043</td>
      <td>...</td>
      <td>6649495</td>
      <td>1723750</td>
      <td>1,1</td>
      <td>249</td>
      <td>661640</td>
      <td>4154338</td>
      <td>3367109</td>
      <td>787228</td>
      <td>2</td>
      <td>1978</td>
    </tr>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>4650</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>2</th>
      <td>WK000300</td>
      <td>300</td>
      <td>Wijk 00</td>
      <td>Appingedam</td>
      <td>Wijk</td>
      <td>WK000300</td>
      <td>1</td>
      <td>11800</td>
      <td>5750</td>
      <td>6050</td>
      <td>...</td>
      <td>4645</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>3</th>
      <td>BU00030000</td>
      <td>30000</td>
      <td>Appingedam-Centrum</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030000</td>
      <td>1</td>
      <td>2355</td>
      <td>1120</td>
      <td>1235</td>
      <td>...</td>
      <td>925</td>
      <td>150</td>
      <td>0,8</td>
      <td>1275</td>
      <td>95</td>
      <td>90</td>
      <td>84</td>
      <td>5</td>
      <td>3</td>
      <td>1195</td>
    </tr>
    <tr>
      <th>4</th>
      <td>BU00030001</td>
      <td>30001</td>
      <td>Appingedam-West</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030001</td>
      <td>1</td>
      <td>3030</td>
      <td>1505</td>
      <td>1525</td>
      <td>...</td>
      <td>1305</td>
      <td>240</td>
      <td>1,2</td>
      <td>977</td>
      <td>155</td>
      <td>163</td>
      <td>158</td>
      <td>5</td>
      <td>4</td>
      <td>896</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 56 columns</p>
</div>



## Selecting the data we want to use

The dataset contains several types of regions, but we are only interested in the municipalities. We first have to find the rows that correspond to municipalities. Let's look at the metadata to find the column containing the region type.


```python
kwb_description
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>a_wat_ha</th>
      <td>Oppervlakte water</td>
    </tr>
    <tr>
      <th>pst_mvp</th>
      <td>Meest voorkomende postcode</td>
    </tr>
    <tr>
      <th>pst_dekp</th>
      <td>Dekkingspercentage</td>
    </tr>
    <tr>
      <th>ste_mvs</th>
      <td>Mate van stedelijkheid</td>
    </tr>
    <tr>
      <th>ste_oad</th>
      <td>Omgevingsadressendichtheid</td>
    </tr>
  </tbody>
</table>
<p>109 rows × 1 columns</p>
</div>



We can see here that the `recs` column contains the region type:


```python
kwb_description.loc['recs']
```




    description    Soort regio
    Name: recs, dtype: object



To see what values the `recs` column can have, we can use the `unique` function from the `pandas` library. This function returns all unique values in a column.


```python
kwb_data['recs'].unique()
```




    array(['Land', 'Gemeente', 'Wijk', 'Buurt'], dtype=object)




```python
selected_region_type = 'Gemeente'
```

We can filter the dataset to only include the municipalities by selecting the rows where the `recs` column is equal to `Gemeente`. 


```python
data_for_selected_region_type = kwb_data[kwb_data.recs == selected_region_type].copy()
data_for_selected_region_type.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>a_bst_b</th>
      <th>a_bst_nb</th>
      <th>g_pau_hh</th>
      <th>g_pau_km</th>
      <th>a_m2w</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>4650</td>
      <td>790</td>
      <td>1</td>
      <td>229</td>
      <td>550</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>9</th>
      <td>GM0005</td>
      <td>5</td>
      <td>Bedum</td>
      <td>Bedum</td>
      <td>Gemeente</td>
      <td>GM0005</td>
      <td>.</td>
      <td>10475</td>
      <td>5235</td>
      <td>5240</td>
      <td>...</td>
      <td>4045</td>
      <td>965</td>
      <td>1,1</td>
      <td>112</td>
      <td>515</td>
      <td>4496</td>
      <td>4454</td>
      <td>41</td>
      <td>4</td>
      <td>685</td>
    </tr>
    <tr>
      <th>19</th>
      <td>GM0009</td>
      <td>9</td>
      <td>Ten Boer</td>
      <td>Ten Boer</td>
      <td>Gemeente</td>
      <td>GM0009</td>
      <td>.</td>
      <td>7292</td>
      <td>3616</td>
      <td>3676</td>
      <td>...</td>
      <td>3025</td>
      <td>710</td>
      <td>1,3</td>
      <td>82</td>
      <td>445</td>
      <td>4573</td>
      <td>4531</td>
      <td>42</td>
      <td>5</td>
      <td>372</td>
    </tr>
    <tr>
      <th>34</th>
      <td>GM0010</td>
      <td>10</td>
      <td>Delfzijl</td>
      <td>Delfzijl</td>
      <td>Gemeente</td>
      <td>GM0010</td>
      <td>.</td>
      <td>24864</td>
      <td>12475</td>
      <td>12389</td>
      <td>...</td>
      <td>10220</td>
      <td>2280</td>
      <td>1,1</td>
      <td>94</td>
      <td>1285</td>
      <td>22750</td>
      <td>13307</td>
      <td>9443</td>
      <td>4</td>
      <td>677</td>
    </tr>
    <tr>
      <th>64</th>
      <td>GM0014</td>
      <td>14</td>
      <td>Groningen</td>
      <td>Groningen</td>
      <td>Gemeente</td>
      <td>GM0014</td>
      <td>.</td>
      <td>202810</td>
      <td>101299</td>
      <td>101511</td>
      <td>...</td>
      <td>53560</td>
      <td>19295</td>
      <td>0,6</td>
      <td>768</td>
      <td>5255</td>
      <td>10150</td>
      <td>9492</td>
      <td>658</td>
      <td>1</td>
      <td>3456</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 56 columns</p>
</div>



Next, we can check how many municipalities are in the dataset.


```python
len(data_for_selected_region_type)
```




    380



The filtered dataset contains 380 municipalities. The data is from 2018, and indeed that number is correct. 

## Creating derived variables: An example

The dataset contains several columns that we can use to create derived variables. For example, we can use the percentage of rental houses to derive a new variable that instead gives the fraction of houses rented (between 0 and 1). We do this by dividing the percentage of rental houses by 100:



```python
# data cleaning
data_for_selected_region_type['p_huurw_f'] = pd.to_numeric(data_for_selected_region_type['p_huurw'], downcast="float", errors="coerce") / 100
data_for_selected_region_type['p_huurw_f'].head()
```




    1     0.51
    9     0.31
    19    0.23
    34    0.38
    64    0.61
    Name: p_huurw_f, dtype: float32



To solve the challenge, you will need to create new derived variables that help you answer the question.

## Design features that allows you to answer the question, find the right clusters using some clustering algorithm

Based on CBS data, can you discover a cluster of cities with many students? Do the city names in your cluster make sense? Bonus question, within your cluster of 'student cities', can you discover which type of schools, universities are present (e.g., technical ones, social studies?)


```python
#begin solution
```


```python
# feature engineering
data_for_selected_region_type['perc_ongehuwd'] = data_for_selected_region_type['a_ongeh'] / (data_for_selected_region_type['a_inw'])
data_for_selected_region_type['perc_vrouwen'] = data_for_selected_region_type['a_vrouw'] / (data_for_selected_region_type['a_inw'])
data_for_selected_region_type['perc_15_24'] = data_for_selected_region_type['a_15_24'] / (data_for_selected_region_type['a_inw'])
data_for_selected_region_type['perc_hh_z_k'] = data_for_selected_region_type['a_hh_z_k'] / (data_for_selected_region_type['a_hh'])
data_for_selected_region_type.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
      <th>p_huurw_f</th>
      <th>perc_ongehuwd</th>
      <th>perc_vrouwen</th>
      <th>perc_15_24</th>
      <th>perc_hh_z_k</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>3</td>
      <td>1051</td>
      <td>0.51</td>
      <td>0.424795</td>
      <td>0.512668</td>
      <td>0.101856</td>
      <td>0.320397</td>
    </tr>
    <tr>
      <th>9</th>
      <td>GM0005</td>
      <td>5</td>
      <td>Bedum</td>
      <td>Bedum</td>
      <td>Gemeente</td>
      <td>GM0005</td>
      <td>.</td>
      <td>10475</td>
      <td>5235</td>
      <td>5240</td>
      <td>...</td>
      <td>4496</td>
      <td>4454</td>
      <td>41</td>
      <td>4</td>
      <td>685</td>
      <td>0.31</td>
      <td>0.437709</td>
      <td>0.500239</td>
      <td>0.113126</td>
      <td>0.347529</td>
    </tr>
    <tr>
      <th>19</th>
      <td>GM0009</td>
      <td>9</td>
      <td>Ten Boer</td>
      <td>Ten Boer</td>
      <td>Gemeente</td>
      <td>GM0009</td>
      <td>.</td>
      <td>7292</td>
      <td>3616</td>
      <td>3676</td>
      <td>...</td>
      <td>4573</td>
      <td>4531</td>
      <td>42</td>
      <td>5</td>
      <td>372</td>
      <td>0.23</td>
      <td>0.443637</td>
      <td>0.504114</td>
      <td>0.116292</td>
      <td>0.329866</td>
    </tr>
    <tr>
      <th>34</th>
      <td>GM0010</td>
      <td>10</td>
      <td>Delfzijl</td>
      <td>Delfzijl</td>
      <td>Gemeente</td>
      <td>GM0010</td>
      <td>.</td>
      <td>24864</td>
      <td>12475</td>
      <td>12389</td>
      <td>...</td>
      <td>22750</td>
      <td>13307</td>
      <td>9443</td>
      <td>4</td>
      <td>677</td>
      <td>0.38</td>
      <td>0.414776</td>
      <td>0.498271</td>
      <td>0.105253</td>
      <td>0.326504</td>
    </tr>
    <tr>
      <th>64</th>
      <td>GM0014</td>
      <td>14</td>
      <td>Groningen</td>
      <td>Groningen</td>
      <td>Gemeente</td>
      <td>GM0014</td>
      <td>.</td>
      <td>202810</td>
      <td>101299</td>
      <td>101511</td>
      <td>...</td>
      <td>10150</td>
      <td>9492</td>
      <td>658</td>
      <td>1</td>
      <td>3456</td>
      <td>0.61</td>
      <td>0.678399</td>
      <td>0.500523</td>
      <td>0.235062</td>
      <td>0.207996</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 61 columns</p>
</div>



After deriving the variables, select only those variables from the dataset:


```python
unsupervised_data = data_for_selected_region_type[['perc_15_24','perc_ongehuwd','perc_hh_z_k','p_huurw_f','perc_vrouwen']]
unsupervised_data.shape
```




    (380, 5)



# Clustering the Data

Now that we have selected the data we want to use, we can start clustering the data. We will use the KMeans algorithm to cluster the data. We use the Elbow method to determine the optimal number of clusters.


```python
from sklearn.cluster import KMeans

def evaluate_kmeans(data: pd.DataFrame, 
                    min_clusters: int = 1,
                    max_clusters: int = 10, 
                    evaluation_function = lambda data, kmeans: kmeans.inertia_):
    scores = []

    for k in range(min_clusters, max_clusters+1):
        kmeans = KMeans(n_clusters=k, random_state=0)
        kmeans.fit(data)
        score = evaluation_function(data, kmeans)
        scores.append(score)
    return scores 
```


```python
import seaborn as sns

inertia = evaluate_kmeans(unsupervised_data, max_clusters=len(unsupervised_data))

sns.lineplot(inertia)
```




    <Axes: >




    
![png](10%20-%20Challenge%20Unsupervised_files/10%20-%20Challenge%20Unsupervised_28_1.png)
    



```python
best_k = 5

kmeans = KMeans(n_clusters=best_k, random_state=0)
kmeans.fit(unsupervised_data)

# predictions for all gemeentes
df_predictions = pd.DataFrame({"cluster" : kmeans.labels_})

# we now want to make a dataset with cluster (the predictions), we first copy the unsupervised dataset
unsupervised_data_w_cluster = unsupervised_data

# assign cluster to dataset
unsupervised_data_w_cluster = unsupervised_data_w_cluster.assign(cluster = df_predictions.values)

# assign the name of gemeente to dataset
unsupervised_data_w_cluster = unsupervised_data_w_cluster.assign(gm_naam = data_for_selected_region_type['gm_naam'].values)

unsupervised_data_w_cluster.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>perc_15_24</th>
      <th>perc_ongehuwd</th>
      <th>perc_hh_z_k</th>
      <th>p_huurw_f</th>
      <th>perc_vrouwen</th>
      <th>cluster</th>
      <th>gm_naam</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>0.101856</td>
      <td>0.424795</td>
      <td>0.320397</td>
      <td>0.51</td>
      <td>0.512668</td>
      <td>1</td>
      <td>Appingedam</td>
    </tr>
    <tr>
      <th>9</th>
      <td>0.113126</td>
      <td>0.437709</td>
      <td>0.347529</td>
      <td>0.31</td>
      <td>0.500239</td>
      <td>4</td>
      <td>Bedum</td>
    </tr>
    <tr>
      <th>19</th>
      <td>0.116292</td>
      <td>0.443637</td>
      <td>0.329866</td>
      <td>0.23</td>
      <td>0.504114</td>
      <td>0</td>
      <td>Ten Boer</td>
    </tr>
    <tr>
      <th>34</th>
      <td>0.105253</td>
      <td>0.414776</td>
      <td>0.326504</td>
      <td>0.38</td>
      <td>0.498271</td>
      <td>2</td>
      <td>Delfzijl</td>
    </tr>
    <tr>
      <th>64</th>
      <td>0.235062</td>
      <td>0.678399</td>
      <td>0.207996</td>
      <td>0.61</td>
      <td>0.500523</td>
      <td>3</td>
      <td>Groningen</td>
    </tr>
  </tbody>
</table>
</div>




```python
kwb_description.loc['perc_15_24']
```




    description     % jonge mensen
    perc_15_24      % jonge mensen
    perc_vrouwen    % jonge mensen
    Name: perc_15_24, dtype: object




```python
import matplotlib.pyplot as plt
import matplotlib.cm as cm

feature_one = 'perc_15_24'
feature_two = 'perc_vrouwen'

plt.scatter(unsupervised_data_w_cluster[feature_one], unsupervised_data_w_cluster[feature_two], c=unsupervised_data_w_cluster['cluster'], cmap=cm.jet)
plt.colorbar()
plt.xlabel('% jonge mensen')
plt.ylabel('% vrouwen')

# comment out for not showing labels
for i, txt in enumerate(unsupervised_data_w_cluster['gm_naam']):
    x_co = unsupervised_data_w_cluster.iloc[i][feature_one]
    y_co = unsupervised_data_w_cluster.iloc[i][feature_two]
    plt.annotate(txt, (x_co, y_co))
```


    
![png](10%20-%20Challenge%20Unsupervised_files/10%20-%20Challenge%20Unsupervised_31_0.png)
    


One cluster in the figure above contains a lot of the student cities: Maastricht, Wageningen, Nijmegen, Utrecht, Groningen, Delft. Let's check which cities are in this cluster.


```python
cluster_nr = unsupervised_data_w_cluster[unsupervised_data_w_cluster['gm_naam']=="Maastricht"]['cluster'].values[0]
unsupervised_data_w_cluster[unsupervised_data_w_cluster['cluster']==cluster_nr][['gm_naam','cluster']]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gm_naam</th>
      <th>cluster</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>64</th>
      <td>Groningen</td>
      <td>3</td>
    </tr>
    <tr>
      <th>2648</th>
      <td>Arnhem</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3561</th>
      <td>Nijmegen</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3835</th>
      <td>Wageningen</td>
      <td>3</td>
    </tr>
    <tr>
      <th>4683</th>
      <td>Utrecht</td>
      <td>3</td>
    </tr>
    <tr>
      <th>5156</th>
      <td>Amsterdam</td>
      <td>3</td>
    </tr>
    <tr>
      <th>5907</th>
      <td>Diemen</td>
      <td>3</td>
    </tr>
    <tr>
      <th>7447</th>
      <td>Delft</td>
      <td>3</td>
    </tr>
    <tr>
      <th>7805</th>
      <td>'s-Gravenhage</td>
      <td>3</td>
    </tr>
    <tr>
      <th>8143</th>
      <td>Leiden</td>
      <td>3</td>
    </tr>
    <tr>
      <th>8510</th>
      <td>Rotterdam</td>
      <td>3</td>
    </tr>
    <tr>
      <th>10131</th>
      <td>Eindhoven</td>
      <td>3</td>
    </tr>
    <tr>
      <th>11921</th>
      <td>Maastricht</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



All of these cities are student cities. We can see that the clustering algorithm has successfully identified a cluster of cities with many students. 


## Dimensionality Reduction: PCA


```python
from sklearn.decomposition import PCA
from sklearn import decomposition

pca = PCA()
pca.fit(unsupervised_data)

# the covariance matrix
print(pca.components_)

plt.plot(list(range(1, len(pca.explained_variance_)+1)) , pca.explained_variance_)
```

    [[ 0.07316871  0.32016737 -0.31361648  0.89050677  0.02795703]
     [ 0.26256787  0.74348393 -0.42865954 -0.43829592 -0.04937051]
     [ 0.51159693  0.30038939  0.74359437  0.12075019 -0.28376328]
     [ 0.78053196 -0.49907184 -0.36635211 -0.01102668 -0.08580031]
     [ 0.233972    0.07360716  0.17535462 -0.01386299  0.95336125]]





    [<matplotlib.lines.Line2D at 0x3499a8a50>]




    
![png](10%20-%20Challenge%20Unsupervised_files/10%20-%20Challenge%20Unsupervised_36_2.png)
    



```python
pca.n_components = 2
dataset_reduced = pca.fit_transform(unsupervised_data)
dataset_reduced
```




    array([[ 1.41355216e-01, -8.98809540e-02],
           [-4.06432371e-02, -6.77550696e-04],
           [-1.04106374e-01,  4.70048289e-02],
           [ 2.03125795e-02, -4.13659706e-02],
           [ 3.56259570e-01,  1.38597291e-01],
           [-8.29481051e-02,  6.80713136e-02],
           [-5.55127307e-02,  2.56532163e-02],
           [-1.13951921e-04, -1.31572659e-02],
           [-1.79785978e-02, -2.06252129e-02],
           [-6.57216177e-02,  4.05347798e-02],
           [ 6.36416513e-02,  8.66119388e-02],
           [ 2.37172266e-02, -5.61419139e-02],
           [-1.24610107e-02, -1.71684408e-02],
           [ 6.13980825e-04,  5.48213973e-02],
           [-3.43897797e-02,  7.13198375e-03],
           [-8.23842409e-02,  4.51710374e-02],
           [ 1.18261519e-02,  1.02490106e-02],
           [ 9.93394596e-03,  8.46538679e-03],
           [-5.11308343e-02,  1.85614541e-02],
           [ 8.88578072e-02, -3.75503229e-02],
           [ 5.81381559e-02, -8.63586974e-03],
           [-3.24945559e-02,  3.58391453e-02],
           [ 1.70008246e-01,  4.18761234e-02],
           [-1.78148790e-02, -2.39096431e-02],
           [-5.94641687e-02,  2.68346582e-02],
           [ 7.69136314e-02, -6.44154034e-02],
           [ 5.19298469e-02,  1.08702665e-03],
           [ 2.01736432e-02,  5.86959724e-02],
           [ 2.32326035e-01, -1.84441083e-02],
           [-5.15070082e-04, -3.23628893e-02],
           [ 7.70929800e-02,  3.77187063e-03],
           [-5.11054783e-02, -2.41579324e-02],
           [ 1.81352178e-02, -2.23252595e-02],
           [ 3.41597856e-02, -1.22889968e-02],
           [ 6.79784808e-02, -1.40253684e-02],
           [ 1.11999322e-01, -1.60024701e-02],
           [-3.19622351e-02, -8.29622632e-03],
           [-8.44376861e-02,  2.28496645e-02],
           [ 1.14828603e-01,  1.44984484e-02],
           [ 1.86393910e-01,  2.80873022e-02],
           [-5.84930290e-02,  6.51442695e-03],
           [-4.43796548e-02,  3.45873318e-02],
           [-6.84952047e-02,  7.26459460e-03],
           [ 9.48471353e-02, -2.98485073e-03],
           [ 2.36063364e-02,  1.06732057e-02],
           [-4.97245024e-02, -1.87650263e-02],
           [ 6.97042091e-04,  1.44712155e-02],
           [ 2.40027610e-02, -1.11258478e-02],
           [-5.91300480e-02,  5.23907837e-03],
           [-2.93510829e-02,  1.10804367e-03],
           [-8.99822371e-02,  9.63761615e-02],
           [-8.46066367e-02,  6.45886072e-02],
           [-6.58975032e-02,  1.59344581e-01],
           [-8.72901300e-02,  1.40641867e-02],
           [ 1.55868283e-01,  3.19030790e-02],
           [-5.61595143e-02,  1.30660454e-02],
           [ 5.71713772e-02, -1.12984761e-02],
           [ 2.58912911e-01,  3.66877929e-02],
           [-2.01323024e-02,  3.56721771e-02],
           [-3.82106084e-02,  3.03702595e-03],
           [-6.52006645e-03, -6.55909084e-02],
           [-9.64522448e-02, -9.09214214e-04],
           [ 4.69331405e-02,  1.14490961e-02],
           [ 1.09840042e-01, -8.69891652e-02],
           [ 6.68673911e-02, -1.25973211e-02],
           [ 5.78325958e-03,  3.28927165e-03],
           [-1.12287942e-02, -1.30121447e-02],
           [-3.16331329e-04,  3.02862954e-02],
           [-2.32921049e-02, -1.55059044e-02],
           [-1.94396315e-02, -5.07930143e-02],
           [ 1.83191101e-02, -2.18899771e-02],
           [-5.36434328e-02,  1.55295891e-02],
           [ 6.01084592e-02, -8.85906965e-03],
           [-2.33661784e-02, -3.77000137e-02],
           [-8.37700005e-02, -4.46041433e-02],
           [-6.89522681e-02,  5.43968407e-03],
           [-8.10787557e-02, -3.72714817e-02],
           [-5.03070466e-02,  2.12488109e-02],
           [-3.19981534e-02,  9.71384587e-03],
           [ 2.79587225e-01,  8.35632851e-02],
           [-5.45054505e-02, -1.07040599e-02],
           [-3.61166614e-02, -1.84386051e-02],
           [ 7.75333473e-03, -4.43088599e-02],
           [ 7.48222213e-02, -4.58726629e-02],
           [-2.48654781e-01,  7.41009475e-03],
           [-6.26830758e-02,  3.94527785e-02],
           [ 1.09794370e-01, -1.86852346e-02],
           [-5.13458313e-02, -1.77273031e-02],
           [ 2.94391281e-01,  9.05719484e-02],
           [ 5.84336145e-02, -5.28452705e-02],
           [-5.34796271e-02,  1.57483681e-02],
           [-2.53802215e-02,  3.36732198e-03],
           [-1.68965498e-02,  2.09552340e-02],
           [ 1.84750893e-02, -4.29167611e-02],
           [ 1.01211932e-01, -1.47567049e-02],
           [-1.23027140e-02, -1.65260428e-02],
           [ 2.25571025e-02, -7.81213575e-03],
           [-5.21310997e-02,  2.06012818e-02],
           [ 9.66691843e-02,  4.20962423e-02],
           [ 3.36519763e-02,  4.28458919e-03],
           [ 6.42402060e-02, -3.83909464e-03],
           [-7.37072622e-02,  3.88150213e-02],
           [-8.37503768e-02,  3.76143264e-02],
           [-4.43040789e-02,  3.00033834e-02],
           [ 1.70317902e-02,  4.08128485e-02],
           [-3.86810851e-02, -2.52201474e-03],
           [-5.33952126e-02,  6.26798688e-02],
           [-6.76887759e-02,  5.59799453e-02],
           [-7.03236046e-02,  4.68070360e-02],
           [ 6.50305692e-03, -2.10932342e-02],
           [ 1.54746429e-02, -7.81797768e-03],
           [ 2.74863347e-01,  1.19101026e-01],
           [ 7.52785136e-02, -2.11270545e-02],
           [-4.00183473e-02,  1.96791127e-02],
           [-3.19304749e-02, -7.61938461e-03],
           [ 4.43584854e-02,  2.68725583e-02],
           [ 1.46436012e-01,  6.99248998e-03],
           [ 6.90174774e-02, -1.00822160e-02],
           [ 1.34362551e-02,  3.35692991e-02],
           [ 8.85266533e-02,  2.61527130e-02],
           [ 1.83717533e-01,  8.28303450e-03],
           [ 4.17876608e-01,  4.49935428e-02],
           [-8.10382865e-02,  5.10909527e-02],
           [-5.66371131e-02, -1.92152129e-02],
           [ 1.32624507e-01,  1.16951620e-03],
           [-4.75673502e-02, -2.10797129e-02],
           [-5.75252281e-02,  2.31926416e-02],
           [-8.64960378e-02,  3.61781142e-02],
           [ 2.64919071e-01,  4.82488507e-02],
           [-1.09982192e-01,  5.62349811e-02],
           [ 3.70971079e-02, -8.03142840e-03],
           [ 1.58804120e-01,  5.44792900e-02],
           [-2.75630231e-02,  4.74271488e-02],
           [ 1.12084538e-02,  4.98080238e-02],
           [ 6.06293843e-02, -2.94130905e-02],
           [-5.37101209e-03,  9.83199869e-03],
           [-2.05290368e-02,  5.75301051e-02],
           [-7.35883065e-02,  1.49082623e-02],
           [ 1.05954573e-01, -5.14603742e-02],
           [ 1.41463500e-01,  1.91263360e-02],
           [ 7.69093794e-02,  2.57283437e-02],
           [ 6.73392215e-02, -3.97641465e-02],
           [-8.64445796e-04,  2.62109818e-02],
           [-7.55448368e-02,  4.03732844e-02],
           [-5.29219143e-02, -1.88984833e-02],
           [-2.87859615e-02,  2.22628111e-02],
           [-7.96948195e-03,  1.34268137e-02],
           [-8.75357093e-02,  6.15724155e-02],
           [ 9.90479746e-02,  2.42759212e-02],
           [ 7.67653863e-02,  6.81149146e-04],
           [-3.24808074e-02,  1.24315268e-02],
           [-4.73393481e-02, -1.78913088e-02],
           [-6.34893024e-02,  6.22438741e-02],
           [ 8.02397265e-02,  8.34991635e-03],
           [ 1.00359947e-01, -1.73105942e-02],
           [ 1.05596694e-01, -7.46890130e-03],
           [ 1.17407455e-01, -3.26554804e-02],
           [ 1.32748787e-01,  2.08600450e-03],
           [ 4.29390618e-02, -2.02672735e-02],
           [ 3.28045064e-02,  8.23962187e-03],
           [-2.21618783e-02,  3.00272613e-02],
           [-9.06969635e-02,  3.48789151e-02],
           [-2.24216710e-02, -3.64908256e-02],
           [ 1.37925390e-01, -1.11804585e-02],
           [ 3.30961804e-01,  5.78663344e-02],
           [ 1.01520787e-01,  5.85765181e-03],
           [ 1.67364222e-01, -3.84139632e-02],
           [ 1.17810381e-01, -3.58130796e-03],
           [ 2.65672298e-01,  4.37407888e-02],
           [ 2.42507887e-04, -1.24123565e-02],
           [-7.32791528e-03, -3.11690265e-02],
           [-3.66567123e-02,  2.20820710e-02],
           [-4.83574503e-02,  4.83981941e-03],
           [ 6.86134218e-03, -9.04415828e-04],
           [ 1.50924970e-02,  1.30667409e-02],
           [ 3.98764008e-02, -4.73178977e-02],
           [ 9.97136874e-02, -3.93207833e-02],
           [ 2.65378272e-01,  7.84631254e-02],
           [ 1.20349046e-02,  6.31768955e-03],
           [ 1.13994352e-02, -1.52281048e-02],
           [ 1.06610534e-01, -6.87770094e-02],
           [-5.98509229e-02,  2.95948037e-02],
           [ 1.12490093e-02,  2.91130919e-02],
           [ 3.36175802e-02,  1.07253594e-02],
           [-3.26799922e-02,  4.75861717e-02],
           [-1.90336246e-02, -9.56110778e-03],
           [-6.07469442e-02, -4.39218149e-02],
           [-9.39618316e-02, -1.79975069e-02],
           [-3.59567974e-02,  4.91690581e-02],
           [-5.77801066e-03, -2.74373980e-02],
           [ 8.31877150e-02, -7.30556838e-02],
           [ 3.34351460e-01,  7.39062096e-03],
           [ 1.69376133e-01, -3.96292112e-02],
           [ 1.87381843e-01,  1.75814301e-04],
           [ 8.12530183e-02, -3.15727964e-02],
           [-7.99558552e-02, -3.76807736e-02],
           [-2.35719719e-02,  2.56506563e-02],
           [-9.69658201e-02, -5.00599790e-02],
           [-5.34859778e-02, -4.85337564e-02],
           [ 2.37155294e-02, -9.86860858e-03],
           [ 1.84813953e-01, -5.15088200e-02],
           [ 2.17333782e-02,  2.00754999e-03],
           [ 3.02992509e-02, -3.85729776e-02],
           [ 8.78916659e-02, -4.83847199e-02],
           [-5.32483274e-03,  4.32426001e-02],
           [ 1.14174852e-01, -1.17673701e-02],
           [-7.47040073e-02,  4.34081412e-02],
           [ 8.33686715e-02, -5.16815912e-02],
           [-7.85378605e-02,  1.09944787e-02],
           [ 3.75312127e-02, -3.05763619e-02],
           [-6.63050798e-02, -6.42939460e-03],
           [-9.53266671e-02, -2.42774422e-02],
           [-1.10288904e-01,  1.09214303e-02],
           [ 5.98859755e-02, -9.10932514e-03],
           [-1.04591607e-01,  6.09437138e-03],
           [-2.89918789e-02,  3.99427005e-02],
           [-6.62324205e-02,  1.28236312e-02],
           [-5.08998051e-02, -2.87738939e-02],
           [-4.57462449e-02,  6.82116386e-03],
           [-1.58201356e-01, -1.30627196e-02],
           [ 9.65266271e-02, -2.66732869e-02],
           [-3.78592996e-02, -1.75062147e-02],
           [-1.41703953e-03,  6.73405330e-03],
           [-7.62442675e-02,  1.50172421e-02],
           [-6.36723080e-02,  3.67680574e-02],
           [-5.05486689e-02,  2.25251705e-02],
           [-6.29594070e-02, -2.66958153e-02],
           [ 6.87469237e-02, -5.49397892e-02],
           [-5.89366258e-02,  2.62597980e-02],
           [-9.38219812e-02,  7.46448204e-02],
           [-4.32086843e-02, -1.96403624e-03],
           [ 3.21306447e-02, -1.45837321e-02],
           [ 1.38417779e-01,  4.31361616e-02],
           [-1.96043555e-02, -2.68998239e-03],
           [ 1.13884985e-02, -3.31064117e-02],
           [-4.14631488e-02, -5.92801770e-03],
           [-9.92288106e-02,  1.75141754e-02],
           [ 2.28187174e-01,  2.31508435e-02],
           [ 1.66582241e-02, -2.44182134e-02],
           [ 2.27383245e-02, -3.40416630e-02],
           [ 5.33560780e-04, -2.96949252e-02],
           [-6.61804196e-02, -1.00213272e-02],
           [-3.58913707e-02,  8.41429059e-03],
           [-1.07727621e-01,  2.62512241e-02],
           [ 1.20566531e-01, -1.89428178e-02],
           [ 1.42741629e-01,  5.65477822e-03],
           [-3.04375089e-02, -5.33568639e-03],
           [-1.16668108e-01,  2.61093238e-02],
           [ 1.05040845e-02, -3.38401159e-02],
           [-9.15747819e-02,  1.22409503e-02],
           [-1.12604848e-01, -1.28368909e-02],
           [-1.31067803e-01,  4.23289534e-02],
           [-3.87879918e-02, -1.02018340e-03],
           [ 4.61461279e-02, -5.36541154e-02],
           [ 1.96348893e-02,  3.45301118e-03],
           [-4.30792897e-02, -7.57972105e-02],
           [-1.10434738e-01,  3.45450531e-02],
           [-6.50024208e-02,  1.80437187e-02],
           [-8.37145278e-02,  1.99630982e-02],
           [-2.83546146e-02,  2.02295551e-02],
           [-3.88246295e-02, -1.78131173e-03],
           [ 1.82355932e-01,  4.24168876e-02],
           [ 2.08737955e-02,  6.95760446e-03],
           [ 2.08914544e-02, -5.91626799e-02],
           [-2.83812428e-02, -2.84913040e-03],
           [ 3.85530754e-02,  1.62840511e-02],
           [-9.62828090e-02,  1.57039198e-02],
           [ 3.65573250e-02, -2.98838181e-02],
           [-2.53186084e-02, -1.60099429e-02],
           [-8.15446345e-02, -3.95119442e-02],
           [-2.86411042e-02, -2.49276644e-02],
           [-7.82082107e-02,  1.46169095e-02],
           [-5.44464811e-03, -1.44786649e-03],
           [-9.28479553e-02, -2.06362354e-02],
           [-3.69910772e-03, -6.72584911e-02],
           [-7.19774152e-02, -4.49177788e-02],
           [-3.72575505e-02, -4.05344150e-02],
           [-6.17608816e-02, -2.03733520e-02],
           [ 2.72149172e-02, -5.57393545e-02],
           [-8.26981309e-03, -2.38963033e-03],
           [ 1.72516117e-01, -5.46879049e-02],
           [ 9.38369877e-02, -9.95558152e-02],
           [ 2.74493460e-01,  5.76383959e-03],
           [-1.01023876e-01, -3.49298721e-02],
           [-1.06351872e-01, -1.94062464e-02],
           [-6.56691068e-02, -2.61383403e-02],
           [-9.42836875e-02, -4.51027593e-02],
           [ 1.19478561e-01, -2.30522760e-02],
           [-1.11361352e-01, -3.29284611e-02],
           [-2.59115892e-02, -8.76104049e-02],
           [-9.90287175e-02, -5.41435547e-02],
           [ 1.64858468e-01, -1.31199655e-01],
           [ 6.34414375e-02, -2.22541021e-02],
           [ 1.30800534e-02, -4.93826396e-03],
           [-1.09799082e-01, -5.24516105e-02],
           [-8.16740043e-03, -7.34945343e-03],
           [ 8.62724937e-03, -6.66175003e-02],
           [ 5.83368856e-02,  3.42271641e-02],
           [-9.92913436e-02,  3.85574172e-02],
           [-3.26998131e-02, -9.00260496e-03],
           [ 1.13235669e-02,  9.75267198e-03],
           [ 5.32916261e-03, -5.50649930e-03],
           [-7.72246707e-02,  4.16453392e-02],
           [-9.57923601e-02,  6.65446006e-02],
           [-4.79110894e-02,  6.61899101e-02],
           [-1.50893497e-01,  2.43733991e-02],
           [-1.14467402e-01, -1.40714871e-02],
           [ 2.87764361e-02, -2.15259939e-02],
           [-1.93069680e-02,  1.83784987e-02],
           [-4.74745020e-03, -6.43267097e-02],
           [-1.27324619e-01,  2.61299807e-02],
           [-5.30838906e-02, -3.51144757e-03],
           [-4.26422420e-02, -8.59286542e-03],
           [-1.31641993e-01,  4.06649104e-02],
           [-1.16132058e-01, -2.17454676e-02],
           [ 2.94854795e-02, -2.27655993e-02],
           [-4.13824712e-02, -7.32887101e-02],
           [-1.22255991e-01, -2.78276818e-02],
           [-9.73279120e-02, -4.41662727e-02],
           [ 1.04335103e-02, -1.04644627e-02],
           [-8.18449507e-02,  5.10407032e-02],
           [-1.23790236e-01,  1.10211874e-02],
           [-3.16524356e-02, -7.73300949e-02],
           [-3.80649975e-02, -2.96073832e-03],
           [-8.20086089e-02, -1.64572370e-02],
           [-1.42909454e-02,  3.34205715e-03],
           [-1.23713870e-01, -3.34659512e-02],
           [-1.12090029e-01,  4.05526666e-02],
           [-7.52968115e-02,  1.70074431e-02],
           [-1.07860130e-01, -1.54581252e-03],
           [ 3.56326090e-03, -2.59069813e-02],
           [-4.93024996e-03, -4.20782064e-02],
           [-8.31113750e-02, -2.17769731e-02],
           [-4.32945797e-02, -5.65246153e-02],
           [-8.08911014e-02, -1.19579525e-02],
           [-6.03715860e-02,  2.65275359e-02],
           [-6.49170973e-02,  3.81579044e-02],
           [-1.28907280e-01,  6.05476639e-03],
           [-1.37250196e-01,  2.99761180e-02],
           [-1.00175408e-01,  3.71041900e-02],
           [-7.64297841e-02, -7.31754296e-02],
           [-1.14441949e-01,  2.51351958e-02],
           [-5.25422959e-02, -2.33051412e-02],
           [-2.46318652e-02,  6.16262624e-03],
           [-4.65431672e-02, -1.53028622e-02],
           [ 1.73843390e-02,  1.78691035e-03],
           [-3.72612323e-02,  2.20788936e-02],
           [ 2.14866357e-03, -9.41754816e-03],
           [-3.48320667e-02, -9.56249968e-03],
           [-9.32461787e-02,  4.97311834e-02],
           [-1.30671703e-03,  2.95814218e-02],
           [-6.25379709e-02,  4.23689845e-02],
           [-4.12557870e-02, -2.33732942e-02],
           [-8.29300509e-02, -1.54713630e-02],
           [ 5.51456295e-02, -4.01152822e-02],
           [-8.82492427e-03, -4.03083973e-03],
           [ 6.35593952e-03, -2.12695658e-02],
           [-4.69690216e-03, -1.29684208e-03],
           [-7.11459361e-02,  7.22520285e-03],
           [ 1.01483068e-02, -4.03108599e-02],
           [-4.86630035e-02,  2.53726881e-02],
           [ 1.61478591e-03,  1.35385194e-02],
           [-6.05677970e-02,  2.42704182e-02],
           [-1.43791294e-01, -5.75729833e-03],
           [ 5.03947829e-03,  1.55263785e-02],
           [-4.93442433e-02,  4.38991083e-03],
           [ 9.72200264e-02, -6.49420422e-03],
           [-1.05564357e-02, -5.62430070e-02],
           [-1.47434047e-02,  6.61957832e-02],
           [-1.32931077e-03,  9.97481752e-03],
           [ 8.01597114e-02, -3.43570473e-02],
           [-2.86592676e-03, -1.64070720e-02],
           [-3.73198763e-02,  5.38888253e-03],
           [ 3.52286925e-02,  4.67418998e-02],
           [ 3.38196015e-02, -3.37037740e-02],
           [-5.88843928e-02,  2.73055337e-02],
           [-2.24307821e-02,  1.17787925e-02],
           [-8.51730165e-02, -4.70630092e-02],
           [ 2.93640831e-02, -1.99736616e-02],
           [-2.32755788e-02, -3.41666494e-02]])




```python
k = 5
kmeans = KMeans(n_clusters=k).fit(dataset_reduced)
```


```python
# predictions for all gemeentes
df_predictions_reduced = pd.DataFrame({"cluster" : kmeans.predict(dataset_reduced).tolist()})

# we now want to make a dataset with cluster (the predictions), we first copy the unsupervised dataset
data_reduced_w_cluster =  pd.DataFrame(dataset_reduced)

# assign cluster to dataset
data_reduced_w_cluster = data_reduced_w_cluster.assign(cluster = df_predictions_reduced.values)

# assign the name of gemeente to dataset
data_reduced_w_cluster = data_reduced_w_cluster.assign(gm_naam = data_for_selected_region_type['gm_naam'].values)
data_reduced_w_cluster.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>cluster</th>
      <th>gm_naam</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.141355</td>
      <td>-0.089881</td>
      <td>3</td>
      <td>Appingedam</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-0.040643</td>
      <td>-0.000678</td>
      <td>2</td>
      <td>Bedum</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.104106</td>
      <td>0.047005</td>
      <td>4</td>
      <td>Ten Boer</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.020313</td>
      <td>-0.041366</td>
      <td>1</td>
      <td>Delfzijl</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.356260</td>
      <td>0.138597</td>
      <td>0</td>
      <td>Groningen</td>
    </tr>
  </tbody>
</table>
</div>




```python
cluster_nr = data_reduced_w_cluster[data_reduced_w_cluster['gm_naam']=="Maastricht"]['cluster'].values[0]
data_reduced_w_cluster[data_reduced_w_cluster['cluster']==cluster_nr][['gm_naam','cluster']]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gm_naam</th>
      <th>cluster</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4</th>
      <td>Groningen</td>
      <td>0</td>
    </tr>
    <tr>
      <th>28</th>
      <td>Vlieland</td>
      <td>0</td>
    </tr>
    <tr>
      <th>57</th>
      <td>Arnhem</td>
      <td>0</td>
    </tr>
    <tr>
      <th>79</th>
      <td>Nijmegen</td>
      <td>0</td>
    </tr>
    <tr>
      <th>88</th>
      <td>Wageningen</td>
      <td>0</td>
    </tr>
    <tr>
      <th>111</th>
      <td>Utrecht</td>
      <td>0</td>
    </tr>
    <tr>
      <th>121</th>
      <td>Amsterdam</td>
      <td>0</td>
    </tr>
    <tr>
      <th>128</th>
      <td>Diemen</td>
      <td>0</td>
    </tr>
    <tr>
      <th>164</th>
      <td>Delft</td>
      <td>0</td>
    </tr>
    <tr>
      <th>168</th>
      <td>'s-Gravenhage</td>
      <td>0</td>
    </tr>
    <tr>
      <th>177</th>
      <td>Leiden</td>
      <td>0</td>
    </tr>
    <tr>
      <th>191</th>
      <td>Rotterdam</td>
      <td>0</td>
    </tr>
    <tr>
      <th>237</th>
      <td>Eindhoven</td>
      <td>0</td>
    </tr>
    <tr>
      <th>282</th>
      <td>Maastricht</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
import matplotlib.colors as cm

kwb_description[0] = 'pca1'

kwb_description[1] = 'pca2'

feature_one = 0
feature_two = 1

plt.scatter(data_reduced_w_cluster[feature_one], data_reduced_w_cluster[feature_two], c=data_reduced_w_cluster['cluster'], cmap=cm.jet)
plt.colorbar()
plt.xlabel(kwb_description[feature_one])
plt.ylabel(kwb_description[feature_two])

# comment out for not showing labels
for i, txt in enumerate(data_reduced_w_cluster['gm_naam']):
    x_co = data_reduced_w_cluster.iloc[i][feature_one]
    y_co = data_reduced_w_cluster.iloc[i][feature_two]
    plt.annotate(txt, (x_co, y_co))
```


    
![png](10%20-%20Challenge%20Unsupervised_files/10%20-%20Challenge%20Unsupervised_41_0.png)
    



```python
#end solution
```
