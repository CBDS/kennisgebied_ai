# Deep Learning Challenge

In the previous notebook, you saw how to train and fine-tune convolutional neural networks on a toy dataset; the cats and dogs dataset. In this challenge, you will instead use a dataset with aerial images used by the CBS.

## The assignment

Below we provide the code to train a network to classify the number of dwellings in a certain part of an aerial image. Can you outperform the network below on our grid statistics problem? What will you do? Tweak the parameters of the network? Change the network structure? Or will you provide the algorithm with more annotated data? Remember the big four??


1) Dataset: in principle all types of data can be processed (e.g., numerical, text, images, sound and even videos)

2) Loss function: a function that indicates how well our model solves a given problem (e.g., categorical or binary crossentropy, etc.)

3) Optimizer: the process of searching for parameters that minimize loss functions (e.g, stochastic gradient descent, RMSProp, Adam, etc.)

4) Network structure: the depth and design of the layers!

To improve network performance, you need to change aspects of the big four. You can change the dataset to include more pictures for example, you can use a different optimizer, or you can change the structure of your neural network. Below we will describe the training of a convolutional neural network on an image dataset in terms of the big four. In each step, we will show you how to carry out that step, but we will also give you ideas how to change aspects of or tune that step to get better network performance. Let's get started!

## 1. The Dataset

First we will load our dataset from disk. We prepared the dataset already for you by cutting up the aerial picture in a grid. Every image represents an area of 100x100m, or 40x40 pixels. We also already labeled the data and split it up in a training and a test set. We labeled the data by taking the amount of houses in every picture and splitting them into two quantiles (number of houses smaller and larger than the median). In short, each image thus has a label 0 or 1. The images are also split into a training set (75%) present in the train directory and a test set (25%) in the test directory. Both directories have 2 subdirectories, each of which contain the images belonging to a certain quantile.

To load the pictures we are using a PyTorch Dataset and DataLoaders, handy tools to load the images and their labels from disk. First let's download the dataset and extract it to the right directories. 


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi

```


```bash
%%bash
if [ ! -d "$DATA_DIRECTORY/DeepStat-Dwellings" ]; then
    mkdir -p $DATA_DIRECTORY/DeepStat-Dwellings
    unzip "$DATA_DIRECTORY/DeepStat-Dwellings.zip" -d "$DATA_DIRECTORY/DeepStat-Dwellings"
fi
```


```python
data_directory = os.path.join(data_directory, 'DeepStat-Dwellings')
```


```python
import torch

def get_device():
    if torch.cuda.is_available(): 
        return torch.device('cuda') 
    if torch.backends.mps.is_available(): 
        return torch.device('mps')
    return torch.device('cpu')
device = get_device()
```

To load the images and their labels from disk, we need to create a custom dataset class. We will subclass the `torch.utils.data.Dataset` class to create a custom dataset. This class is a PyTorch class that allows you to load and manipulate data in a custom way. We will create a custom dataset that loads the images and their labels from disk. 


```python
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from PIL import Image
import os
import numpy as np

class DeepStatDataset(Dataset):
    def __init__(self, root_dir, transform=None):
        self.__root_dir = root_dir
        self.transform = transform
        self.__classes = [directory for directory in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir, directory))]
        self.class_to_idx = {cls: idx for idx, cls in enumerate(self.classes)}
        
        self.__images = []
        self.__labels = []
        
        # Load data from directories
        for class_name in self.classes:
            class_dir = os.path.join(root_dir, class_name)
            for img_name in os.listdir(class_dir):
                self.__images.append(os.path.join(class_dir, img_name))
                self.__labels.append(self.class_to_idx[class_name])

    @property
    def root_dir(self):
        return self.__root_dir
    
    @property
    def classes(self):
        return self.__classes

    @property
    def number_of_classes(self):
        return len(self.classes)
    
    @property
    def images(self):
        return self.__images
    
    @property
    def labels(self):
        return self.__labels

    def __len__(self):
        return len(self.images)

    def __getitem__(self, idx):
        img_path = self.images[idx]
        image = Image.open(img_path)
        label = self.labels[idx]
        
        if self.transform:
            image = self.transform(image)
            
        return image, label

```

Before we can use the images, they need to be preprocessed. PyTorch provides standard preprocessing transforms that can be used to preprocess the images. We will use the following transforms:


```python
from torchvision import transforms

transform = transforms.Compose([
    transforms.Resize((40, 40)),
    transforms.Lambda(lambda img: img.convert('RGB') if img.mode == 'RGBA' else img),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                         std=[0.229, 0.224, 0.225])
])

```

To load the data from disk, we need to use the newly create `DeepStatDataset` class, together with a PyTorch `DataLoader` that can read the data in batches. We will use a batch size of 64. This means that the DataLoader will return 64 images and their labels at a time. Being able to load the data in batches is very useful, because it allows us to train the network on a subset of the data at a time. This is much more memory efficient than loading all the data at once.


```python
# Create datasets
train_dataset = DeepStatDataset(
    root_dir=os.path.join(data_directory, "DeepStat/train"),
    transform=transform
)

test_dataset = DeepStatDataset(
    root_dir=os.path.join(data_directory, "DeepStat/test"),
    transform=transform
)

# Create data loaders
batch_size = 64
train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(test_dataset, batch_size=batch_size)

len(train_dataset.images), len(test_dataset.images)
```




    (2250, 750)



We can see that we have 3000 images in total. 2250 images in the training set, and 750 images in the test set. We see that both the training set and the test set have 2 classes each, corresponding to the quartiles. Because we are using quartiles to label the images, each label should have almost the same amount of images assigned to it.


```python
import os

num_categories = train_dataset.number_of_classes
available_categories = np.linspace(0, num_categories-1, num_categories)

images_per_category = {int(category) : [image for image in os.listdir(os.path.join(data_directory, f"DeepStat/train/{int(category)}"))] for category in available_categories}

for category, images in images_per_category.items():
    print(f"Number of images in category {category}: {len(images)}")

```

    Number of images in category 0: 1341
    Number of images in category 1: 909


As we can see the number of images per quantile are different. To balance the influence each category has when training the network, we therefore use class_weights for each class. We can calculate the class weights as follows. You can see that the class weights for the last category is big bigger than the one for the first. The last category contains less images and is therefore given a bigger weight during training; each separate image has a bit more influence on the end result.


```python
from sklearn.utils import class_weight
import torch

y_train = np.array([category for category, images in images_per_category.items() for _ in images ])

class_weights = class_weight.compute_class_weight('balanced',
                                                 classes=available_categories,
                                                 y=y_train)
class_weights_tensor = torch.tensor(class_weights, dtype=torch.float32)
class_weights_tensor

```




    tensor([0.8389, 1.2376])



### Tips to improve network performance

To improve network performance by means of the dataset, often more data means better performance. In this case, however, you already have a dataset and no way to add more examples. Another way to add more examples, without creating more images, is using data augmentation. In our case we specifically need image data augmentation, which means that we create more examples based on the images we already have. We can do that by rotating, scaling, mirroring images, changing image color, etc. PyTorch offers data augmentation in the from of transforms. These are similar to the transforms we saw above to preprocess the images, but there are specific transforms to add random augmentations. The available transforms can be found [here](https://pytorch.org/vision/stable/transforms.html).  

Try to experiment with some of the random transforms and see what it does with network performance. 

**Note:** You only use image data augmentation during training, when you would like to have as many examples as possible!

## 2. The Optimizer

The optimizer trains our network. It changes the network weights, and minimizes the loss given by the loss function. A lot of different optimizers exists, the most basic one called stochastic gradient descent or SGD. All other optimizers are specializations of SGD that update the weights in a different manner, using specific rules. While SGD allows you to configure learning almost completely to your own wishes, other optimizers tune hyperparameters more automatically and are often more simple to tune. Each optimizer has a list of hyperparameters that influence training behavior. To see a list of available optimizers and their hyperparameters, visit the PyTorch documentation [here]([https://](https://pytorch.org/docs/stable/optim.html)). 

### Tips to improve network performance

While stochastic gradient descent gives you a lot of flexibility, it also gives you a lot of hyperparameters that you need to tune manually. To start a bit more simple, try one of the other optimizers that automatically tune some of the hyperparameters. A very popular optimizer is the [Adam](https://pytorch.org/docs/stable/generated/torch.optim.Adam.html#torch.optim.Adam) optimizer, which is a good optimizer to start with.

## 3. The loss function

The loss function calculates a loss on the basis of the difference between the actual label, and the output of the network. The loss function therefore highly depends on the type of labels you have. In our case, we are training a network for classification and the loss function depends on the number of categories you have. For a binary classification problem, with two classes, often `BCELoss` is used. For a classification problem with more than two classes, like we are having here, we often use `CrossEntropyLoss`. Because changing the loss function depends on your problem, not every loss function works with every dataset. Therefore, before changing the loss function, check which ones are applicable for our problem. To see a list of available loss functions, visit the PyTorch documentation [here](https://pytorch.org/docs/stable/nn.html#loss-functions).

## 4. The Network structure

The last thing we can change is the structure of the convolutional neural network. We can change the network structures in a lot of different ways, as long as we keep the Input layer and the last Dense layer, the softmax layer, in place. Almost, everything else in between can be changed. For example, we can change the number of layers a network has, but we can also change the type of layers, or the order in which certain types of layers follow each other. We can also change the activation functions or use regularization layers like BatchNormalization, or Dropout. Such layers influence the network weights during training to try and make them more robust to changes in the output. To see a list of available layers, activation functions, regularizers, etc., visit the PyTorch documentation [here](https://pytorch.org/docs/stable/nn.html).


### Tips to improve network performance

A first tip: while changing network structure keep in mind the sizes of the subsequent layers: if the last layer size just before the softmax layer are to big, it is difficult for the network to learn, because it has a lot of information it needs to digest. Conversely, it the last layer is to small, it does not have enough information to learn from.

Another tip is not trying to reinvent the wheel; it is often difficult to come up with entirely new network structures, so look at proven network structures and see if you can borrow from them. See for example below a convolutional block from a well-known network called `VGG16`. Neural networks are often build from such blocks, which can easily be reused and adapted to a specific problem. Keep in mind the first tip, adding more or less blocks influences the sizes in the last layers before the softmax layer.

**Note:** While you can change a lot of layers, we will always need `Conv2d` layers to learn the spatial structures in the image. We will also need some pooling layers, or strides to gradually bring the image sizes back to some higher level features that can be classified into our categories.


```python
class DeepStatNet(nn.Module):
    def __init__(self, num_classes=2):
        super(DeepStatNet, self).__init__()
        
        self.features = nn.Sequential(
            nn.Conv2d(3, 32, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=3),
        )
        
        # Calculate size after convolutions
        self.flat_features = 32 * 13 * 13
        
        self.classifier = nn.Sequential(
            nn.Linear(self.flat_features, num_classes),
        )
        
    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, self.flat_features)
        x = self.classifier(x)
        return x
```


```python
import torch.optim as optim


def vgg16_block_conv2(x, layer_depth):
    x = nn.Conv2d(x, layer_depth, kernel_size=3, padding=1)
    x = nn.ReLU()(x)
    x = nn.Conv2d(x, layer_depth, kernel_size=3, padding=1)
    x = nn.ReLU()(x)
    x = nn.MaxPool2d(kernel_size=2, stride=2)
    return x

model = DeepStatNet().to(device)
criterion = nn.CrossEntropyLoss(weight=class_weights_tensor).to(device)
optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)
```


```python
#begin solution
```


```python
class DeepStatNet(nn.Module):
    def __init__(self, num_classes=2):
        super(DeepStatNet, self).__init__()
        
        self.features = nn.Sequential(
            nn.Conv2d(3, 16, kernel_size=3, padding='same'),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2),
            #https://stackoverflow.com/questions/54423078/how-are-the-output-size-of-maxpooling2d-conv2d-upsampling2d-layers-calculated
            # Wnew = (W - F + 2*P)/S + 1 = (40 - 3 + 2*0)/2 + 1 = 19
            # Hnew = (H - F + 2*P)/S + 1 = (40 - 3 + 2*0)/2 + 1 = 19

            nn.Conv2d(16, 16, kernel_size=3, padding='same'),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=3, stride=2),
            # Wnew = (W - F + 2*P)/S + 1 = (19 - 3 + 2*0)/2 + 1 = 9
            # Hnew = (H - F + 2*P)/S + 1 = (19 - 3 + 2*0)/2 + 1 = 9
        )
        # Calculate size after convolutions
        self.flat_features = 16 * 9 * 9
        
        self.classifier = nn.Sequential(
            nn.Flatten(),
            nn.Linear(self.flat_features, 2)            
        )
        
    def forward(self, x):
        x = self.features(x)
        x = self.classifier(x)
        return x
```


```python
import torch.optim as optim


def vgg16_block_conv2(x, layer_depth):
    x = nn.Conv2d(x, layer_depth, kernel_size=3, padding=1)
    x = nn.ReLU()(x)
    x = nn.Conv2d(x, layer_depth, kernel_size=3, padding=1)
    x = nn.ReLU()(x)
    x = nn.MaxPool2d(kernel_size=2, stride=2)
    return x

model = DeepStatNet().to(device)
criterion = nn.CrossEntropyLoss(weight=class_weights_tensor).to(device)
optimizer = optim.Adam(model.parameters(), lr=0.01, weight_decay=1e-5)
```


```python
#end solution
```

When we have our dataset, optimizer, loss function, and convolutional neural network, we can start training. The first thing we can change during training is the batch_size. The batch_size is the sample size of the training set that is given to the network at once for training. The smaller the batch_size, the smaller the sample, and the bigger the chance of having a sample that does not reflect the target population. The larger the batch_size, the less updates you have, and the longer training can take. Also the chances of running out of memory increase. Finding the right batch_size is therefore finding the right balance between a small and a larger sample. Typical batch_sizes range from 32, 64, to 128. For efficiency, they are almost always a power of 2.

**Note:** the batch_size was defined in the start of this notebook, because the `DataLoader` also needs it to determine how many images should be loaded at a time.


```python
def train_model(model, train_loader, test_loader, criterion, optimizer, num_epochs=10):
    train_losses = []
    train_accs = []
    val_losses = []
    val_accs = []
    
    for epoch in range(num_epochs):
        model.train()
        running_loss = 0.0
        correct = 0
        total = 0
        
        for inputs, labels in train_loader:
            inputs, labels = inputs.to(device), labels.to(device)
            
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            
            running_loss += loss.item()
            _, predicted = outputs.max(1)
            total += labels.size(0)
            correct += predicted.eq(labels).sum().item()
            
        epoch_loss = running_loss / len(train_loader)
        epoch_acc = 100. * correct / total
        train_losses.append(epoch_loss)
        train_accs.append(epoch_acc)
        
        # Validation
        model.eval()
        val_loss = 0.0
        correct = 0
        total = 0
        
        with torch.no_grad():
            for inputs, labels in test_loader:
                inputs, labels = inputs.to(device), labels.to(device)
                outputs = model(inputs)
                loss = criterion(outputs, labels)
                
                val_loss += loss.item()
                _, predicted = outputs.max(1)
                total += labels.size(0)
                correct += predicted.eq(labels).sum().item()
        
        val_loss = val_loss / len(test_loader)
        val_acc = 100. * correct / total
        val_losses.append(val_loss)
        val_accs.append(val_acc)
        
        print(f'Epoch {epoch+1}/{num_epochs}:')
        print(f'Train Loss: {epoch_loss:.4f}, Train Acc: {epoch_acc:.2f}%')
        print(f'Val Loss: {val_loss:.4f}, Val Acc: {val_acc:.2f}%')
        
    return train_losses, train_accs, val_losses, val_accs           
```

Another thing we can change during training is the number of epochs the training is run. The number of epochs determines the number of iterations the model is trained. More epochs means longer training, and sometimes results in a better model performance. Other times, however, model performance stagnates after a certain amount of epochs. It then does not make much sense to continue training. For these scenarios, it is handy to look at so-called [early stopping](https://stackoverflow.com/questions/71998978/early-stopping-in-pytorch), that, among others, can stop training after the model performance has stagnated for a couple of epochs.


```python
# Train the model
epochs = 10
history = train_model(model, train_loader, test_loader, criterion, optimizer, num_epochs=epochs)
```

    Epoch 1/10:
    Train Loss: 1.7186, Train Acc: 55.82%
    Val Loss: 1.0803, Val Acc: 45.33%
    Epoch 2/10:
    Train Loss: 0.6904, Train Acc: 65.60%
    Val Loss: 0.5811, Val Acc: 68.53%
    Epoch 3/10:
    Train Loss: 0.5615, Train Acc: 71.56%
    Val Loss: 0.7223, Val Acc: 63.73%
    Epoch 4/10:
    Train Loss: 0.5992, Train Acc: 70.40%
    Val Loss: 0.5760, Val Acc: 70.53%
    Epoch 5/10:
    Train Loss: 0.5217, Train Acc: 73.51%
    Val Loss: 0.5279, Val Acc: 74.27%
    Epoch 6/10:
    Train Loss: 0.5317, Train Acc: 73.47%
    Val Loss: 0.5257, Val Acc: 74.27%
    Epoch 7/10:
    Train Loss: 0.4985, Train Acc: 75.47%
    Val Loss: 0.5155, Val Acc: 73.20%
    Epoch 8/10:
    Train Loss: 0.4688, Train Acc: 77.33%
    Val Loss: 0.6185, Val Acc: 71.07%
    Epoch 9/10:
    Train Loss: 0.4705, Train Acc: 77.82%
    Val Loss: 0.4863, Val Acc: 76.00%
    Epoch 10/10:
    Train Loss: 0.4544, Train Acc: 78.44%
    Val Loss: 0.6067, Val Acc: 67.87%


## Evaluating the training process

To see if the training process went right, it is important to observe the development of the loss function on the training set and the validation set. In addition, we also need to observe the development of the metric we would like to optimize, in this case the accuracy. If training went well, the loss should decrease and the accuracy should increase. Moreover, the loss on the training set should have a similar development as the loss on the validation set. To check for overfitting or underfitting, we plot the loss and the accuracy for both the training set and the validation set. To see how to use learning curves to diagnose machine learning model performance, read the following [article](https://machinelearningmastery.com/learning-curves-for-diagnosing-machine-learning-model-performance/).


```python
import matplotlib.pyplot as plt

def plot_training_history(history):
    train_losses, train_accs, val_losses, val_accs = history
    epochs = range(1, len(train_losses) + 1)
    
    plt.figure(figsize=(12, 4))
    
    plt.subplot(1, 2, 1)
    plt.plot(epochs, train_losses, label='Training Loss')
    plt.plot(epochs, val_losses, label='Validation Loss')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()
    
    plt.subplot(1, 2, 2)
    plt.plot(epochs, train_accs, label='Training Accuracy')
    plt.plot(epochs, val_accs, label='Validation Accuracy')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy (%)')
    plt.legend()
    
    plt.tight_layout()
    plt.show()

plot_training_history(history)
```


    
![png](12%20-%20Challenge%20Deep%20Learning_files/12%20-%20Challenge%20Deep%20Learning_31_0.png)
    


## Evaluating model performance

Last, the code below evaluates how well the model performs on the test set. For that, we first have to retrieve all the ground-truth labels for the test set: the true labels that should have been predicted. After that, we can compare the true labels to the predictions and see whether the model has predicted the label correctly.


```python
# Get true labels
y_true = []
for _, labels in test_loader:
    y_true.extend(labels.numpy())

y_true = np.array(y_true)
```

To evaluate machine learning model performance a lot of different metrics exist. Here we are using the sklearn `classification_report` that prints some of the most widely used metrics for each category that we are training on. For a more exhaustive list of metrics see [here](https://en.wikipedia.org/wiki/Precision_and_recall) or the [sklearn documentation](https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics).

**Note:** in the example presented here, we have a fairly unbalanced dataset, that means that the number of images per category is different. In this case, we normally shouldn't use metrics like accuracy, but use metrics like the f1-score. In the case that we have a more imbalanced dataset, we need balanced metrics that give good estimates of network performance when there are a different number of images per class. For some other examples of performance metrics used for imbalanced datasets, see [here](https://imbalanced-learn.org/stable/metrics.html).


```python
from sklearn.metrics import classification_report

# Get predictions from the model
model.eval()
y_pred = []

with torch.no_grad():
    for inputs in test_loader:
        inputs = inputs[0].to(device)  # Only inputs are needed for prediction
        outputs = model(inputs)
        y_pred.extend(outputs.cpu().numpy())

y_pred = np.array(y_pred)

# Print classification report
print(classification_report(y_true, y_pred.argmax(axis=1), target_names=['Class 0', 'Class 1']))
```

                  precision    recall  f1-score   support
    
         Class 0       0.80      0.58      0.67       427
         Class 1       0.59      0.81      0.68       323
    
        accuracy                           0.68       750
       macro avg       0.70      0.69      0.68       750
    weighted avg       0.71      0.68      0.68       750
    


## Visualizing the Predictions

Next to evaluating model performance, it is also always a good idea to visualize network predictions, along with their true labels. In this way, we can identify cases that go well, but we can also identify cases that go wrong.
As seen below, sometimes a model prediction is completely off. But in other cases, it may be difficult for a model to predict the correct label. Especially, since a human observer would predict a similar label.

You can explore the model predictions for each category below, by changing the `true label` in the drop down box. Then the images for that true label with their predictions will be plotted. By changing the `page_index` you can change the page of images that are plotted.


```python
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from ipywidgets import interact, widgets
import math

images_per_page = 16

def page(images, page_index, items_per_page):
    return [images[i:i+items_per_page] for i in range(0, len(images), items_per_page)][page_index]

def filter_images(test_images, test_labels, predictions_model, filter_label):
    all_images = zip(test_images, test_labels, predictions_model)
    return [(image, label, prediction) for image, label, prediction in all_images if label == filter_label]

def plot_images(test_images, test_labels, predictions_model, filter_label, page_index, images_per_page):
    fig = plt.figure(figsize=(12., 12.))
    grid = ImageGrid(fig, 111,
                 nrows_ncols=(4, 4),
                 axes_pad=0.3,
                 )

    images_for_label = filter_images(test_images, test_labels, predictions_model, filter_label)
    images_for_page = page(images_for_label, page_index, images_per_page)
    for ax, (image, true_label, prediction) in zip(grid, images_for_page):
        ax.imshow(image)  #.permute(1, 2, 0))  # Convert from (C, H, W) to (H, W, C)
        predicted_label = prediction.argmax()
        label_color = 'g' if true_label == predicted_label else 'r'
        ax.set_title(f"true: {true_label}, predicted: {predicted_label}", backgroundcolor=label_color)

    plt.show()

def plot_images_for_labels(true_label, page_index):
    test_labels = test_dataset.labels
    test_images = [np.array(Image.open(img_path))  for img_path in test_dataset.images]
    
    plot_images(test_images=test_images, test_labels=test_labels, predictions_model=y_pred, filter_label=true_label, page_index=page_index, images_per_page=16)

true_label_widget = widgets.Dropdown(options=available_categories)
page_widget = widgets.Dropdown()

def update(*args):
    test_images = []
    test_labels = []
    for images, labels in test_loader:
        test_images.extend(images)
        test_labels.extend(labels.numpy())
    
    images_for_label = filter_images(test_images, test_labels, y_pred, true_label_widget.value)
    number_of_pages = math.ceil(len(images_for_label) / images_per_page)
    page_widget.options = range(number_of_pages)
    page_widget.value = 0

true_label_widget.observe(update)

interact(plot_images_for_labels, true_label=true_label_widget, page_index=page_widget);
```


    interactive(children=(Dropdown(description='true_label', options=(0.0, 1.0), value=0.0), Dropdown(description=…

