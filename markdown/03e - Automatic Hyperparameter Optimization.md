# Automatic Hyperparameter Optimization

In the previous notebook, you tried to find the best hyperparameters for the model manually. You've seen that this can be quite some work and that it's not guaranteed that you'll find the best hyperparameters. In this notebook, you'll learn how to automate this process using different techniques. We will consider the following techniques:

- Grid Search, which is the simplest method to find the best hyperparameters.
- Random Search, which is a more efficient method than Grid Search.

We will use the classification dataset as from the previous notebook, but first we will start again by loading the data and preparing it for training.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

titanic_df = pd.read_csv(os.path.join(data_directory, 'titanic/train_preprocessed.csv'))
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Fare</th>
      <th>Survived</th>
      <th>Pclass_1</th>
      <th>Pclass_2</th>
      <th>Pclass_3</th>
      <th>SibSp_0</th>
      <th>SibSp_1</th>
      <th>SibSp_2</th>
      <th>SibSp_3</th>
      <th>...</th>
      <th>Parch_4</th>
      <th>Parch_5</th>
      <th>Parch_6</th>
      <th>Sex_female</th>
      <th>Sex_male</th>
      <th>Embarked_C</th>
      <th>Embarked_Q</th>
      <th>Embarked_S</th>
      <th>Cabin_False</th>
      <th>Cabin_True</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.565736</td>
      <td>-0.502445</td>
      <td>0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.663861</td>
      <td>0.786845</td>
      <td>1</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.258337</td>
      <td>-0.488854</td>
      <td>1</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.433312</td>
      <td>0.420730</td>
      <td>1</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.433312</td>
      <td>-0.486337</td>
      <td>0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 27 columns</p>
</div>



# Get the Labels 

After the loading the data, we first get the labels of the data. The labels are the target values that we want to predict. In this case, the labels show whether the passenger survived or not. We store the labels in a variable called `y_true`. This variable contains the "ground truth", or the true value, values that we want to predict. By comparing the predicted values of the model to the true values, we can evaluate the performance of the model.


```python
y_true = titanic_df['Survived']
y_true
```




    0      0
    1      1
    2      1
    3      1
    4      0
          ..
    886    0
    887    1
    888    0
    889    1
    890    0
    Name: Survived, Length: 891, dtype: int64



## Remove the Label Column

The second step is to remove the label column from the training data. The label column is the column that we want to predict and therefore cannot be used as a feature. If we do not remove the label column, the model will be able to perfectly predict the label, it could be as easy as returning the label column value. However, in this case, the model will not be able to generalize to new data. 


```python
titanic_df = titanic_df.drop('Survived', axis=1)
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Fare</th>
      <th>Pclass_1</th>
      <th>Pclass_2</th>
      <th>Pclass_3</th>
      <th>SibSp_0</th>
      <th>SibSp_1</th>
      <th>SibSp_2</th>
      <th>SibSp_3</th>
      <th>SibSp_4</th>
      <th>...</th>
      <th>Parch_4</th>
      <th>Parch_5</th>
      <th>Parch_6</th>
      <th>Sex_female</th>
      <th>Sex_male</th>
      <th>Embarked_C</th>
      <th>Embarked_Q</th>
      <th>Embarked_S</th>
      <th>Cabin_False</th>
      <th>Cabin_True</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.565736</td>
      <td>-0.502445</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.663861</td>
      <td>0.786845</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.258337</td>
      <td>-0.488854</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.433312</td>
      <td>0.420730</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.433312</td>
      <td>-0.486337</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 26 columns</p>
</div>



## Split the Data into training and testing data

To evaluate the performance of the model, we split the data into a training set and a testing set. The training set is used to train the model and the testing set is used to evaluate the performance of the model. To split the data, we use the `train_test_split` function from the `sklearn.model_selection` module. We use 80% of the data for training and 20% of the data for testing. We do this by setting the `test_size` parameter to 0.2. We can also set the `random_state` parameter to a fixed value to ensure that the data is split in the same way every time we run the code.


```python
from sklearn.model_selection import train_test_split

train_df, test_df, y_train, y_test = train_test_split(titanic_df, y_true, test_size=0.2, random_state=42)
```


```python
train_df.shape, test_df.shape, y_train.shape, y_test.shape
```




    ((712, 26), (179, 26), (712,), (179,))



## Train the Model

Before we start optimizing the hyperparameters, we first train the model with the default hyperparameters. We use the `LogisticRegression` class from the `sklearn.linear_model` module to create a logistic regression model. We then call the `fit` method on the model to train it on the training data. Finally, we use the `predict` method to make predictions on the test data and evaluate the performance of the model.


```python
from sklearn.linear_model import LogisticRegression

model = LogisticRegression()
model.fit(train_df, y_train)
```




<style>#sk-container-id-1 {
  /* Definition of color scheme common for light and dark mode */
  --sklearn-color-text: black;
  --sklearn-color-line: gray;
  /* Definition of color scheme for unfitted estimators */
  --sklearn-color-unfitted-level-0: #fff5e6;
  --sklearn-color-unfitted-level-1: #f6e4d2;
  --sklearn-color-unfitted-level-2: #ffe0b3;
  --sklearn-color-unfitted-level-3: chocolate;
  /* Definition of color scheme for fitted estimators */
  --sklearn-color-fitted-level-0: #f0f8ff;
  --sklearn-color-fitted-level-1: #d4ebff;
  --sklearn-color-fitted-level-2: #b3dbfd;
  --sklearn-color-fitted-level-3: cornflowerblue;

  /* Specific color for light theme */
  --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, white)));
  --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-icon: #696969;

  @media (prefers-color-scheme: dark) {
    /* Redefinition of color scheme for dark theme */
    --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, #111)));
    --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-icon: #878787;
  }
}

#sk-container-id-1 {
  color: var(--sklearn-color-text);
}

#sk-container-id-1 pre {
  padding: 0;
}

#sk-container-id-1 input.sk-hidden--visually {
  border: 0;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}

#sk-container-id-1 div.sk-dashed-wrapped {
  border: 1px dashed var(--sklearn-color-line);
  margin: 0 0.4em 0.5em 0.4em;
  box-sizing: border-box;
  padding-bottom: 0.4em;
  background-color: var(--sklearn-color-background);
}

#sk-container-id-1 div.sk-container {
  /* jupyter's `normalize.less` sets `[hidden] { display: none; }`
     but bootstrap.min.css set `[hidden] { display: none !important; }`
     so we also need the `!important` here to be able to override the
     default hidden behavior on the sphinx rendered scikit-learn.org.
     See: https://github.com/scikit-learn/scikit-learn/issues/21755 */
  display: inline-block !important;
  position: relative;
}

#sk-container-id-1 div.sk-text-repr-fallback {
  display: none;
}

div.sk-parallel-item,
div.sk-serial,
div.sk-item {
  /* draw centered vertical line to link estimators */
  background-image: linear-gradient(var(--sklearn-color-text-on-default-background), var(--sklearn-color-text-on-default-background));
  background-size: 2px 100%;
  background-repeat: no-repeat;
  background-position: center center;
}

/* Parallel-specific style estimator block */

#sk-container-id-1 div.sk-parallel-item::after {
  content: "";
  width: 100%;
  border-bottom: 2px solid var(--sklearn-color-text-on-default-background);
  flex-grow: 1;
}

#sk-container-id-1 div.sk-parallel {
  display: flex;
  align-items: stretch;
  justify-content: center;
  background-color: var(--sklearn-color-background);
  position: relative;
}

#sk-container-id-1 div.sk-parallel-item {
  display: flex;
  flex-direction: column;
}

#sk-container-id-1 div.sk-parallel-item:first-child::after {
  align-self: flex-end;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:last-child::after {
  align-self: flex-start;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:only-child::after {
  width: 0;
}

/* Serial-specific style estimator block */

#sk-container-id-1 div.sk-serial {
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--sklearn-color-background);
  padding-right: 1em;
  padding-left: 1em;
}


/* Toggleable style: style used for estimator/Pipeline/ColumnTransformer box that is
clickable and can be expanded/collapsed.
- Pipeline and ColumnTransformer use this feature and define the default style
- Estimators will overwrite some part of the style using the `sk-estimator` class
*/

/* Pipeline and ColumnTransformer style (default) */

#sk-container-id-1 div.sk-toggleable {
  /* Default theme specific background. It is overwritten whether we have a
  specific estimator or a Pipeline/ColumnTransformer */
  background-color: var(--sklearn-color-background);
}

/* Toggleable label */
#sk-container-id-1 label.sk-toggleable__label {
  cursor: pointer;
  display: block;
  width: 100%;
  margin-bottom: 0;
  padding: 0.5em;
  box-sizing: border-box;
  text-align: center;
}

#sk-container-id-1 label.sk-toggleable__label-arrow:before {
  /* Arrow on the left of the label */
  content: "▸";
  float: left;
  margin-right: 0.25em;
  color: var(--sklearn-color-icon);
}

#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {
  color: var(--sklearn-color-text);
}

/* Toggleable content - dropdown */

#sk-container-id-1 div.sk-toggleable__content {
  max-height: 0;
  max-width: 0;
  overflow: hidden;
  text-align: left;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content pre {
  margin: 0.2em;
  border-radius: 0.25em;
  color: var(--sklearn-color-text);
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted pre {
  /* unfitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {
  /* Expand drop-down */
  max-height: 200px;
  max-width: 100%;
  overflow: auto;
}

#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {
  content: "▾";
}

/* Pipeline/ColumnTransformer-specific style */

#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-label.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator-specific style */

/* Colorize estimator box */
#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

#sk-container-id-1 div.sk-label label.sk-toggleable__label,
#sk-container-id-1 div.sk-label label {
  /* The background is the default theme color */
  color: var(--sklearn-color-text-on-default-background);
}

/* On hover, darken the color of the background */
#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

/* Label box, darken color on hover, fitted */
#sk-container-id-1 div.sk-label.fitted:hover label.sk-toggleable__label.fitted {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator label */

#sk-container-id-1 div.sk-label label {
  font-family: monospace;
  font-weight: bold;
  display: inline-block;
  line-height: 1.2em;
}

#sk-container-id-1 div.sk-label-container {
  text-align: center;
}

/* Estimator-specific */
#sk-container-id-1 div.sk-estimator {
  font-family: monospace;
  border: 1px dotted var(--sklearn-color-border-box);
  border-radius: 0.25em;
  box-sizing: border-box;
  margin-bottom: 0.5em;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-estimator.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

/* on hover */
#sk-container-id-1 div.sk-estimator:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Specification for estimator info (e.g. "i" and "?") */

/* Common style for "i" and "?" */

.sk-estimator-doc-link,
a:link.sk-estimator-doc-link,
a:visited.sk-estimator-doc-link {
  float: right;
  font-size: smaller;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1em;
  height: 1em;
  width: 1em;
  text-decoration: none !important;
  margin-left: 1ex;
  /* unfitted */
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
  color: var(--sklearn-color-unfitted-level-1);
}

.sk-estimator-doc-link.fitted,
a:link.sk-estimator-doc-link.fitted,
a:visited.sk-estimator-doc-link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
div.sk-estimator:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover,
div.sk-label-container:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

div.sk-estimator.fitted:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover,
div.sk-label-container:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

/* Span, style for the box shown on hovering the info icon */
.sk-estimator-doc-link span {
  display: none;
  z-index: 9999;
  position: relative;
  font-weight: normal;
  right: .2ex;
  padding: .5ex;
  margin: .5ex;
  width: min-content;
  min-width: 20ex;
  max-width: 50ex;
  color: var(--sklearn-color-text);
  box-shadow: 2pt 2pt 4pt #999;
  /* unfitted */
  background: var(--sklearn-color-unfitted-level-0);
  border: .5pt solid var(--sklearn-color-unfitted-level-3);
}

.sk-estimator-doc-link.fitted span {
  /* fitted */
  background: var(--sklearn-color-fitted-level-0);
  border: var(--sklearn-color-fitted-level-3);
}

.sk-estimator-doc-link:hover span {
  display: block;
}

/* "?"-specific style due to the `<a>` HTML tag */

#sk-container-id-1 a.estimator_doc_link {
  float: right;
  font-size: 1rem;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1rem;
  height: 1rem;
  width: 1rem;
  text-decoration: none;
  /* unfitted */
  color: var(--sklearn-color-unfitted-level-1);
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
}

#sk-container-id-1 a.estimator_doc_link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
#sk-container-id-1 a.estimator_doc_link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

#sk-container-id-1 a.estimator_doc_link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
}
</style><div id="sk-container-id-1" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>LogisticRegression()</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator fitted sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-1" type="checkbox" checked><label for="sk-estimator-id-1" class="sk-toggleable__label fitted sk-toggleable__label-arrow fitted">&nbsp;&nbsp;LogisticRegression<a class="sk-estimator-doc-link fitted" rel="noreferrer" target="_blank" href="https://scikit-learn.org/1.5/modules/generated/sklearn.linear_model.LogisticRegression.html">?<span>Documentation for LogisticRegression</span></a><span class="sk-estimator-doc-link fitted">i<span>Fitted</span></span></label><div class="sk-toggleable__content fitted"><pre>LogisticRegression()</pre></div> </div></div></div></div>



## Evaluate the Model

To evaluate the model, we will use the `classification_report` function from the `sklearn.metrics` module. As we saw, this function calculates various metrics that are useful for evaluating the performance of a classification model. We will use the `classification_report` now to evaluate the initial performance of the model, without having fine-tuned the hyperparameters.


```python
from sklearn.metrics import classification_report

y_pred = model.predict(test_df)

print(classification_report(y_test, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.82      0.85      0.83       105
               1       0.77      0.73      0.75        74
    
        accuracy                           0.80       179
       macro avg       0.79      0.79      0.79       179
    weighted avg       0.80      0.80      0.80       179
    


# Searching for the best hyperparameter combinations

Hyperparameters are parameters that are set before training the algorithm. In the previous notebook, we have seen already several examples of hyperparameters. It can for example be the penalty function, the number of iterations, or the solver we use for the `LogisticRegression`. The combination of the hyperparameters can influence the performance of your machine learning algorithm greatly. Some hyperparameters may be more important than others and the importance of hyperparameters can even be dependent on the data set. In this section, we are going to discuss two methods that help you tune your hyperparameters automatically:

* Grid search
* Random search

Before describing both methods in more detail and showing the how to implement both types of searches in Python, the following picture illustrates the differences between the two methods intuitively:

![Visual Representation of Grid Search compared to Random Search](../../images/grid_vs_random_search.png)

In general, it is advised to use random search for hyperparameter optimization as it usually delivers better or similar results faster. See for more information: http://www.jmlr.org/papers/v13/bergstra12a.html To show this we will search for the best hyperparameters for the `LogisticRegression`, defined above, with both Grid Search and Random Search.


## Grid Search

In Grid Search, we try every combination of a preset list of values of the hyper-parameters and evaluate the model for each combination. The pattern followed here is similar to the grid, where all the values are placed in the form of a matrix. Each set of parameters is taken into consideration and the accuracy is noted. Once all the combinations are evaluated, the model with the set of parameters which give the top accuracy is considered to be the best.

![Visual Representation of Grid Search](../../images/grid_search.png)

One of the major drawbacks of grid search is that when it comes to dimensionality, it suffers when the number of hyperparameters grows exponentially. With as few as four parameters this problem can become impractical, because the number of evaluations required for this strategy increases exponentially with each additional parameter, due to the curse of dimensionality.


```python
# Utility function to report best scores
def report(results) -> pd.DataFrame:
    return pd.DataFrame(results).sort_values(by='rank_test_score')
           
```


```python
import time
import warnings
from sklearn.model_selection import GridSearchCV

warnings.filterwarnings('ignore')

t0 = time.time()
# use a full grid over all parameters
param_grid = {"penalty": ["l1", "l2", None],
              "fit_intercept": [True, False],
              "C": [1, 0.1, 0.01, 0.001],
              "max_iter": [100, 200, 300],
              "solver": ["saga"]}


grid_search_model = LogisticRegression()
# run grid search
grid_search = GridSearchCV(grid_search_model, param_grid=param_grid, cv=5, refit=True)
grid_search.fit(train_df, y_train)

print(f"GridSearchCV took {time.time() - t0} seconds")
print(grid_search.best_params_)
```

    GridSearchCV took 3.83418607711792 seconds
    {'C': 1, 'fit_intercept': True, 'max_iter': 100, 'penalty': 'l2', 'solver': 'saga'}



```python
report(grid_search.cv_results_)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mean_fit_time</th>
      <th>std_fit_time</th>
      <th>mean_score_time</th>
      <th>std_score_time</th>
      <th>param_C</th>
      <th>param_fit_intercept</th>
      <th>param_max_iter</th>
      <th>param_penalty</th>
      <th>param_solver</th>
      <th>params</th>
      <th>split0_test_score</th>
      <th>split1_test_score</th>
      <th>split2_test_score</th>
      <th>split3_test_score</th>
      <th>split4_test_score</th>
      <th>mean_test_score</th>
      <th>std_test_score</th>
      <th>rank_test_score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>10</th>
      <td>0.008555</td>
      <td>0.000061</td>
      <td>0.000523</td>
      <td>0.000060</td>
      <td>1.000</td>
      <td>False</td>
      <td>100</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 1, 'fit_intercept': False, 'max_iter': 1...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.011727</td>
      <td>0.002375</td>
      <td>0.001059</td>
      <td>0.001140</td>
      <td>1.000</td>
      <td>True</td>
      <td>100</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 1, 'fit_intercept': True, 'max_iter': 10...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.016415</td>
      <td>0.000580</td>
      <td>0.000533</td>
      <td>0.000087</td>
      <td>1.000</td>
      <td>True</td>
      <td>200</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 1, 'fit_intercept': True, 'max_iter': 20...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0.020607</td>
      <td>0.003288</td>
      <td>0.000527</td>
      <td>0.000069</td>
      <td>1.000</td>
      <td>True</td>
      <td>300</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 1, 'fit_intercept': True, 'max_iter': 30...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>16</th>
      <td>0.014931</td>
      <td>0.000251</td>
      <td>0.000535</td>
      <td>0.000066</td>
      <td>1.000</td>
      <td>False</td>
      <td>300</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 1, 'fit_intercept': False, 'max_iter': 3...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>66</th>
      <td>0.000777</td>
      <td>0.000103</td>
      <td>0.000441</td>
      <td>0.000024</td>
      <td>0.001</td>
      <td>False</td>
      <td>200</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.001, 'fit_intercept': False, 'max_iter...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>61</td>
    </tr>
    <tr>
      <th>45</th>
      <td>0.002164</td>
      <td>0.000195</td>
      <td>0.000528</td>
      <td>0.000122</td>
      <td>0.010</td>
      <td>False</td>
      <td>100</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.01, 'fit_intercept': False, 'max_iter'...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>61</td>
    </tr>
    <tr>
      <th>42</th>
      <td>0.001167</td>
      <td>0.000117</td>
      <td>0.000442</td>
      <td>0.000040</td>
      <td>0.010</td>
      <td>True</td>
      <td>300</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.01, 'fit_intercept': True, 'max_iter':...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>61</td>
    </tr>
    <tr>
      <th>69</th>
      <td>0.000759</td>
      <td>0.000104</td>
      <td>0.000428</td>
      <td>0.000014</td>
      <td>0.001</td>
      <td>False</td>
      <td>300</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.001, 'fit_intercept': False, 'max_iter...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>61</td>
    </tr>
    <tr>
      <th>39</th>
      <td>0.001169</td>
      <td>0.000088</td>
      <td>0.000450</td>
      <td>0.000038</td>
      <td>0.010</td>
      <td>True</td>
      <td>200</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.01, 'fit_intercept': True, 'max_iter':...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>61</td>
    </tr>
  </tbody>
</table>
<p>72 rows × 18 columns</p>
</div>




```python
y_pred_gs = grid_search.predict(test_df)

print(classification_report(y_test, y_pred_gs))
```

                  precision    recall  f1-score   support
    
               0       0.82      0.85      0.83       105
               1       0.77      0.73      0.75        74
    
        accuracy                           0.80       179
       macro avg       0.79      0.79      0.79       179
    weighted avg       0.80      0.80      0.80       179
    


## Random Search

Random search is a technique where random combinations of the hyperparameters are used to find the best solution for the built model. It tries random combinations of a range of values. To optimise with random search, the function is evaluated at some number of random configurations in the parameter space.

![Visual Representation of Random search](../../images/random_search.png)

The chances of finding the optimal parameter are comparatively higher in random search. Random search works best for lower dimensional data since the time taken to find the right set is less with less number of iterations. Random search is the best parameter search technique when there are less number of dimensions. In the paper Random Search for Hyper-Parameter Optimization by Bergstra and Bengio (http://www.jmlr.org/papers/v13/bergstra12a.html), the authors show empirically and theoretically that random search is more efficient for parameter optimization than grid search.


```python
from sklearn.model_selection import RandomizedSearchCV
import scipy.stats as stats

warnings.filterwarnings('ignore')

# use a full grid over all parameters
t0 = time.time()

param_dist = {"penalty": ["l1", "l2", None],
              "fit_intercept": [True, False],
              "C": stats.loguniform(0.001, 1), 
              "max_iter": stats.randint(100, 300),
              "solver": ["saga"]}


# run randomized search
random_search_model = LogisticRegression()
random_search = RandomizedSearchCV(random_search_model, param_distributions=param_dist,
                                   n_iter=50, cv=5, refit=True)

random_search.fit(train_df, y_train)


print(f"RandomizedSearchCV took {time.time() - t0} seconds")
print(random_search.best_params_)
```

    RandomizedSearchCV took 2.6676292419433594 seconds
    {'C': 0.6976351483830793, 'fit_intercept': False, 'max_iter': 279, 'penalty': 'l2', 'solver': 'saga'}



```python
report(random_search.cv_results_)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mean_fit_time</th>
      <th>std_fit_time</th>
      <th>mean_score_time</th>
      <th>std_score_time</th>
      <th>param_C</th>
      <th>param_fit_intercept</th>
      <th>param_max_iter</th>
      <th>param_penalty</th>
      <th>param_solver</th>
      <th>params</th>
      <th>split0_test_score</th>
      <th>split1_test_score</th>
      <th>split2_test_score</th>
      <th>split3_test_score</th>
      <th>split4_test_score</th>
      <th>mean_test_score</th>
      <th>std_test_score</th>
      <th>rank_test_score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>16</th>
      <td>0.013008</td>
      <td>0.000272</td>
      <td>0.000621</td>
      <td>0.000078</td>
      <td>0.801768</td>
      <td>False</td>
      <td>273</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.8017681579033236, 'fit_intercept': Fal...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>26</th>
      <td>0.013487</td>
      <td>0.002313</td>
      <td>0.000550</td>
      <td>0.000088</td>
      <td>0.553061</td>
      <td>True</td>
      <td>192</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.5530608476908904, 'fit_intercept': Tru...</td>
      <td>0.818182</td>
      <td>0.825175</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>48</th>
      <td>0.012184</td>
      <td>0.000229</td>
      <td>0.000549</td>
      <td>0.000066</td>
      <td>0.768543</td>
      <td>False</td>
      <td>166</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.7685426689809047, 'fit_intercept': Fal...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.011519</td>
      <td>0.000397</td>
      <td>0.000556</td>
      <td>0.000078</td>
      <td>0.697635</td>
      <td>False</td>
      <td>279</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.6976351483830793, 'fit_intercept': Fal...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808953</td>
      <td>0.021370</td>
      <td>1</td>
    </tr>
    <tr>
      <th>42</th>
      <td>0.005026</td>
      <td>0.000140</td>
      <td>0.000472</td>
      <td>0.000034</td>
      <td>0.206180</td>
      <td>False</td>
      <td>294</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.206180281160254, 'fit_intercept': Fals...</td>
      <td>0.825175</td>
      <td>0.825175</td>
      <td>0.795775</td>
      <td>0.767606</td>
      <td>0.830986</td>
      <td>0.808943</td>
      <td>0.024062</td>
      <td>5</td>
    </tr>
    <tr>
      <th>0</th>
      <td>0.011380</td>
      <td>0.002090</td>
      <td>0.000538</td>
      <td>0.000058</td>
      <td>0.426246</td>
      <td>True</td>
      <td>175</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.4262460031103187, 'fit_intercept': Tru...</td>
      <td>0.825175</td>
      <td>0.825175</td>
      <td>0.802817</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.808943</td>
      <td>0.022353</td>
      <td>5</td>
    </tr>
    <tr>
      <th>38</th>
      <td>0.006466</td>
      <td>0.001346</td>
      <td>0.000473</td>
      <td>0.000034</td>
      <td>0.199553</td>
      <td>True</td>
      <td>136</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.19955346815031622, 'fit_intercept': Tr...</td>
      <td>0.825175</td>
      <td>0.832168</td>
      <td>0.795775</td>
      <td>0.760563</td>
      <td>0.830986</td>
      <td>0.808933</td>
      <td>0.027578</td>
      <td>7</td>
    </tr>
    <tr>
      <th>30</th>
      <td>0.010261</td>
      <td>0.000160</td>
      <td>0.000586</td>
      <td>0.000069</td>
      <td>0.574528</td>
      <td>False</td>
      <td>119</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.5745275842867984, 'fit_intercept': Fal...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.807554</td>
      <td>0.020473</td>
      <td>8</td>
    </tr>
    <tr>
      <th>12</th>
      <td>0.008855</td>
      <td>0.000038</td>
      <td>0.000484</td>
      <td>0.000039</td>
      <td>0.910937</td>
      <td>False</td>
      <td>102</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.9109366492822957, 'fit_intercept': Fal...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.807554</td>
      <td>0.020473</td>
      <td>8</td>
    </tr>
    <tr>
      <th>41</th>
      <td>0.009466</td>
      <td>0.000066</td>
      <td>0.000458</td>
      <td>0.000051</td>
      <td>0.320045</td>
      <td>True</td>
      <td>110</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.3200449711096388, 'fit_intercept': Tru...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.807554</td>
      <td>0.020473</td>
      <td>8</td>
    </tr>
    <tr>
      <th>25</th>
      <td>0.008299</td>
      <td>0.000183</td>
      <td>0.000504</td>
      <td>0.000119</td>
      <td>0.423659</td>
      <td>False</td>
      <td>253</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.42365882231601387, 'fit_intercept': Fa...</td>
      <td>0.825175</td>
      <td>0.818182</td>
      <td>0.802817</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.807545</td>
      <td>0.021496</td>
      <td>11</td>
    </tr>
    <tr>
      <th>29</th>
      <td>0.013329</td>
      <td>0.000143</td>
      <td>0.000613</td>
      <td>0.000095</td>
      <td>0.503140</td>
      <td>False</td>
      <td>157</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.5031396323863767, 'fit_intercept': Fal...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.816901</td>
      <td>0.806146</td>
      <td>0.019517</td>
      <td>12</td>
    </tr>
    <tr>
      <th>39</th>
      <td>0.011575</td>
      <td>0.000047</td>
      <td>0.000474</td>
      <td>0.000034</td>
      <td>0.001885</td>
      <td>True</td>
      <td>136</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.0018851595322854063, 'fit_intercept': ...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.816901</td>
      <td>0.806146</td>
      <td>0.019517</td>
      <td>12</td>
    </tr>
    <tr>
      <th>28</th>
      <td>0.024540</td>
      <td>0.000270</td>
      <td>0.000631</td>
      <td>0.000059</td>
      <td>0.897692</td>
      <td>True</td>
      <td>294</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.8976917480316512, 'fit_intercept': Tru...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>27</th>
      <td>0.013234</td>
      <td>0.002833</td>
      <td>0.000469</td>
      <td>0.000029</td>
      <td>0.533690</td>
      <td>True</td>
      <td>141</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.5336903191571345, 'fit_intercept': Tru...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.788732</td>
      <td>0.774648</td>
      <td>0.823944</td>
      <td>0.804738</td>
      <td>0.019452</td>
      <td>14</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.024823</td>
      <td>0.000185</td>
      <td>0.000559</td>
      <td>0.000107</td>
      <td>0.016558</td>
      <td>True</td>
      <td>298</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.01655805437407447, 'fit_intercept': Tr...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>36</th>
      <td>0.023100</td>
      <td>0.000314</td>
      <td>0.000577</td>
      <td>0.000097</td>
      <td>0.029075</td>
      <td>True</td>
      <td>277</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.029075117921564135, 'fit_intercept': T...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>18</th>
      <td>0.015933</td>
      <td>0.000255</td>
      <td>0.000434</td>
      <td>0.000022</td>
      <td>0.083934</td>
      <td>True</td>
      <td>186</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.08393376868694793, 'fit_intercept': Tr...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>6</th>
      <td>0.021615</td>
      <td>0.000181</td>
      <td>0.000586</td>
      <td>0.000087</td>
      <td>0.001496</td>
      <td>True</td>
      <td>255</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.0014959373679879371, 'fit_intercept': ...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>10</th>
      <td>0.014470</td>
      <td>0.000156</td>
      <td>0.000550</td>
      <td>0.000036</td>
      <td>0.844318</td>
      <td>False</td>
      <td>170</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.8443175035745726, 'fit_intercept': Fal...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>43</th>
      <td>0.022440</td>
      <td>0.000214</td>
      <td>0.000492</td>
      <td>0.000064</td>
      <td>0.001698</td>
      <td>True</td>
      <td>273</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.0016975010083516197, 'fit_intercept': ...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.809859</td>
      <td>0.804738</td>
      <td>0.018935</td>
      <td>14</td>
    </tr>
    <tr>
      <th>17</th>
      <td>0.019276</td>
      <td>0.006580</td>
      <td>0.000605</td>
      <td>0.000104</td>
      <td>0.645580</td>
      <td>True</td>
      <td>271</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.6455796629539278, 'fit_intercept': Tru...</td>
      <td>0.811189</td>
      <td>0.818182</td>
      <td>0.788732</td>
      <td>0.774648</td>
      <td>0.823944</td>
      <td>0.803339</td>
      <td>0.018671</td>
      <td>22</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0.020813</td>
      <td>0.000239</td>
      <td>0.000599</td>
      <td>0.000075</td>
      <td>0.457312</td>
      <td>True</td>
      <td>249</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.4573119847998048, 'fit_intercept': Tru...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>21</th>
      <td>0.016705</td>
      <td>0.000072</td>
      <td>0.000492</td>
      <td>0.000095</td>
      <td>0.003095</td>
      <td>True</td>
      <td>196</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.003094991388582011, 'fit_intercept': T...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>37</th>
      <td>0.021180</td>
      <td>0.000854</td>
      <td>0.000695</td>
      <td>0.000130</td>
      <td>0.001450</td>
      <td>True</td>
      <td>238</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.0014498486103555535, 'fit_intercept': ...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>31</th>
      <td>0.016406</td>
      <td>0.000098</td>
      <td>0.000542</td>
      <td>0.000038</td>
      <td>0.013516</td>
      <td>True</td>
      <td>198</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.013516473323642518, 'fit_intercept': T...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>13</th>
      <td>0.019255</td>
      <td>0.000192</td>
      <td>0.000541</td>
      <td>0.000115</td>
      <td>0.010057</td>
      <td>False</td>
      <td>229</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.01005736643009879, 'fit_intercept': Fa...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>47</th>
      <td>0.021072</td>
      <td>0.000096</td>
      <td>0.000522</td>
      <td>0.000048</td>
      <td>0.003316</td>
      <td>False</td>
      <td>256</td>
      <td>None</td>
      <td>saga</td>
      <td>{'C': 0.0033155010555730464, 'fit_intercept': ...</td>
      <td>0.818182</td>
      <td>0.818182</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.809859</td>
      <td>0.803329</td>
      <td>0.021704</td>
      <td>23</td>
    </tr>
    <tr>
      <th>45</th>
      <td>0.003395</td>
      <td>0.000723</td>
      <td>0.000431</td>
      <td>0.000017</td>
      <td>0.080305</td>
      <td>True</td>
      <td>278</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.08030491981470017, 'fit_intercept': Tr...</td>
      <td>0.811189</td>
      <td>0.832168</td>
      <td>0.795775</td>
      <td>0.760563</td>
      <td>0.816901</td>
      <td>0.803319</td>
      <td>0.024346</td>
      <td>29</td>
    </tr>
    <tr>
      <th>20</th>
      <td>0.003167</td>
      <td>0.000174</td>
      <td>0.000490</td>
      <td>0.000036</td>
      <td>0.081325</td>
      <td>False</td>
      <td>270</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.08132471136542734, 'fit_intercept': Fa...</td>
      <td>0.811189</td>
      <td>0.832168</td>
      <td>0.795775</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.801911</td>
      <td>0.026853</td>
      <td>30</td>
    </tr>
    <tr>
      <th>32</th>
      <td>0.002413</td>
      <td>0.000070</td>
      <td>0.000451</td>
      <td>0.000046</td>
      <td>0.055422</td>
      <td>False</td>
      <td>242</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.05542174988012715, 'fit_intercept': Fa...</td>
      <td>0.818182</td>
      <td>0.832168</td>
      <td>0.781690</td>
      <td>0.774648</td>
      <td>0.781690</td>
      <td>0.797676</td>
      <td>0.023029</td>
      <td>31</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.005824</td>
      <td>0.001274</td>
      <td>0.000505</td>
      <td>0.000054</td>
      <td>0.210050</td>
      <td>True</td>
      <td>224</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.21005012375891627, 'fit_intercept': Tr...</td>
      <td>0.797203</td>
      <td>0.825175</td>
      <td>0.767606</td>
      <td>0.753521</td>
      <td>0.838028</td>
      <td>0.796307</td>
      <td>0.032339</td>
      <td>32</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0.017571</td>
      <td>0.006575</td>
      <td>0.000636</td>
      <td>0.000137</td>
      <td>0.349132</td>
      <td>False</td>
      <td>254</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.3491319477291526, 'fit_intercept': Fal...</td>
      <td>0.804196</td>
      <td>0.832168</td>
      <td>0.760563</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.793470</td>
      <td>0.031114</td>
      <td>33</td>
    </tr>
    <tr>
      <th>24</th>
      <td>0.003077</td>
      <td>0.000611</td>
      <td>0.000474</td>
      <td>0.000060</td>
      <td>0.064144</td>
      <td>True</td>
      <td>190</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.06414442115093001, 'fit_intercept': Tr...</td>
      <td>0.811189</td>
      <td>0.825175</td>
      <td>0.781690</td>
      <td>0.767606</td>
      <td>0.774648</td>
      <td>0.792061</td>
      <td>0.022232</td>
      <td>34</td>
    </tr>
    <tr>
      <th>49</th>
      <td>0.002033</td>
      <td>0.000200</td>
      <td>0.000470</td>
      <td>0.000041</td>
      <td>0.039797</td>
      <td>False</td>
      <td>277</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.039796880506070914, 'fit_intercept': F...</td>
      <td>0.811189</td>
      <td>0.839161</td>
      <td>0.788732</td>
      <td>0.739437</td>
      <td>0.781690</td>
      <td>0.792042</td>
      <td>0.033082</td>
      <td>35</td>
    </tr>
    <tr>
      <th>34</th>
      <td>0.003328</td>
      <td>0.000275</td>
      <td>0.000447</td>
      <td>0.000039</td>
      <td>0.077988</td>
      <td>True</td>
      <td>264</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.07798755396524847, 'fit_intercept': Tr...</td>
      <td>0.797203</td>
      <td>0.811189</td>
      <td>0.767606</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.789284</td>
      <td>0.024715</td>
      <td>36</td>
    </tr>
    <tr>
      <th>40</th>
      <td>0.002726</td>
      <td>0.000533</td>
      <td>0.000439</td>
      <td>0.000009</td>
      <td>0.028579</td>
      <td>False</td>
      <td>270</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.02857909196417304, 'fit_intercept': Fa...</td>
      <td>0.797203</td>
      <td>0.811189</td>
      <td>0.767606</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.789284</td>
      <td>0.024715</td>
      <td>36</td>
    </tr>
    <tr>
      <th>46</th>
      <td>0.002884</td>
      <td>0.000508</td>
      <td>0.000448</td>
      <td>0.000031</td>
      <td>0.028876</td>
      <td>False</td>
      <td>152</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.028876108970968997, 'fit_intercept': F...</td>
      <td>0.797203</td>
      <td>0.811189</td>
      <td>0.767606</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.789284</td>
      <td>0.024715</td>
      <td>36</td>
    </tr>
    <tr>
      <th>33</th>
      <td>0.002605</td>
      <td>0.000196</td>
      <td>0.000485</td>
      <td>0.000056</td>
      <td>0.046850</td>
      <td>True</td>
      <td>239</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.04685036238670131, 'fit_intercept': Tr...</td>
      <td>0.797203</td>
      <td>0.811189</td>
      <td>0.767606</td>
      <td>0.753521</td>
      <td>0.816901</td>
      <td>0.789284</td>
      <td>0.024715</td>
      <td>36</td>
    </tr>
    <tr>
      <th>23</th>
      <td>0.002696</td>
      <td>0.000187</td>
      <td>0.000500</td>
      <td>0.000041</td>
      <td>0.023354</td>
      <td>True</td>
      <td>255</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.023353857871821797, 'fit_intercept': T...</td>
      <td>0.790210</td>
      <td>0.839161</td>
      <td>0.788732</td>
      <td>0.746479</td>
      <td>0.774648</td>
      <td>0.787846</td>
      <td>0.030083</td>
      <td>40</td>
    </tr>
    <tr>
      <th>22</th>
      <td>0.001745</td>
      <td>0.000076</td>
      <td>0.000479</td>
      <td>0.000087</td>
      <td>0.002235</td>
      <td>False</td>
      <td>103</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.002234783318824092, 'fit_intercept': F...</td>
      <td>0.685315</td>
      <td>0.755245</td>
      <td>0.725352</td>
      <td>0.718310</td>
      <td>0.725352</td>
      <td>0.721915</td>
      <td>0.022303</td>
      <td>41</td>
    </tr>
    <tr>
      <th>19</th>
      <td>0.001854</td>
      <td>0.000145</td>
      <td>0.000502</td>
      <td>0.000055</td>
      <td>0.001048</td>
      <td>False</td>
      <td>170</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.0010482052434876408, 'fit_intercept': ...</td>
      <td>0.685315</td>
      <td>0.720280</td>
      <td>0.718310</td>
      <td>0.704225</td>
      <td>0.704225</td>
      <td>0.706471</td>
      <td>0.012558</td>
      <td>42</td>
    </tr>
    <tr>
      <th>11</th>
      <td>0.001772</td>
      <td>0.000157</td>
      <td>0.000435</td>
      <td>0.000025</td>
      <td>0.001952</td>
      <td>True</td>
      <td>147</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.0019522016152409023, 'fit_intercept': ...</td>
      <td>0.643357</td>
      <td>0.650350</td>
      <td>0.661972</td>
      <td>0.640845</td>
      <td>0.619718</td>
      <td>0.643248</td>
      <td>0.013860</td>
      <td>43</td>
    </tr>
    <tr>
      <th>8</th>
      <td>0.001811</td>
      <td>0.000144</td>
      <td>0.000511</td>
      <td>0.000146</td>
      <td>0.001546</td>
      <td>True</td>
      <td>100</td>
      <td>l2</td>
      <td>saga</td>
      <td>{'C': 0.0015464109017871133, 'fit_intercept': ...</td>
      <td>0.636364</td>
      <td>0.629371</td>
      <td>0.647887</td>
      <td>0.633803</td>
      <td>0.626761</td>
      <td>0.634837</td>
      <td>0.007332</td>
      <td>44</td>
    </tr>
    <tr>
      <th>35</th>
      <td>0.000741</td>
      <td>0.000082</td>
      <td>0.000424</td>
      <td>0.000013</td>
      <td>0.001466</td>
      <td>True</td>
      <td>148</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.001465605100061169, 'fit_intercept': T...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
    <tr>
      <th>15</th>
      <td>0.001164</td>
      <td>0.000089</td>
      <td>0.000515</td>
      <td>0.000122</td>
      <td>0.008710</td>
      <td>True</td>
      <td>298</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.008710205256467794, 'fit_intercept': T...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
    <tr>
      <th>14</th>
      <td>0.002152</td>
      <td>0.000240</td>
      <td>0.000446</td>
      <td>0.000036</td>
      <td>0.009128</td>
      <td>False</td>
      <td>109</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.009128149290042766, 'fit_intercept': F...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
    <tr>
      <th>9</th>
      <td>0.002274</td>
      <td>0.001056</td>
      <td>0.000488</td>
      <td>0.000137</td>
      <td>0.013510</td>
      <td>True</td>
      <td>172</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.013510152298272737, 'fit_intercept': T...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
    <tr>
      <th>44</th>
      <td>0.000798</td>
      <td>0.000111</td>
      <td>0.000433</td>
      <td>0.000008</td>
      <td>0.001935</td>
      <td>False</td>
      <td>137</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.0019347453197567906, 'fit_intercept': ...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.001087</td>
      <td>0.000169</td>
      <td>0.000435</td>
      <td>0.000021</td>
      <td>0.008590</td>
      <td>True</td>
      <td>103</td>
      <td>l1</td>
      <td>saga</td>
      <td>{'C': 0.008590424503567408, 'fit_intercept': T...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>45</td>
    </tr>
  </tbody>
</table>
</div>




```python
y_pred_rs = random_search.predict(test_df)
print(classification_report(y_test, y_pred_rs))
```

                  precision    recall  f1-score   support
    
               0       0.82      0.85      0.83       105
               1       0.77      0.73      0.75        74
    
        accuracy                           0.80       179
       macro avg       0.79      0.79      0.79       179
    weighted avg       0.80      0.80      0.80       179
    


# Conclusion

In this notebook, you have learned how to automatically optimize the hyperparameters of a machine learning model using Grid Search and Random Search. You have seen that Random Search is more efficient than Grid Search and that it can find better (or similar performing) hyperparameters in less time. Both techniques, however, can save you a lot of time compared to manually tuning the hyperparameters.
