[Titanic feature engineering](https://www.kaggle.com/code/gunesevitan/titanic-advanced-feature-engineering-tutorial)

[More feature engineering](https://www.kaggle.com/arthurtok/introduction-to-ensembling-stacking-in-python)

[More feature engineering](https://www.kaggle.com/code/nicapotato/titanic-feature-engineering)

[House Prices engineering](https://medium.com/swlh/explained-kaggle-housing-prices-feature-engineering-and-ridge-regression-88934ad37edc)


# Titanic Dataset: Feature Engineering

In the previous notebooks, we already saw two ways of improving classification performance on the Titanic dataset:

- we could use more complex models, such as Multi-layer Perceptrons, Random Forests or eXtreme Gradient Boosting.
- we could use hyperparameter optimization to tune the model's parameters.

In this notebook, we will see a third way of improving classification performance: feature engineering. Feature engineering is the process of using domain knowledge derive new features from the existing data. By adding domain knowledge, we can create new features that can help the model to learn patterns that it would not be able to learn otherwise. In this notebook, we will see how to create new features from the Titanic dataset and how to use them to improve the classification performance. We start again by loading raw data.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

titanic_df = pd.read_csv(os.path.join(data_directory, 'titanic/train.csv'))
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>3</td>
      <td>Braund, Mr. Owen Harris</td>
      <td>male</td>
      <td>22.0</td>
      <td>1</td>
      <td>0</td>
      <td>A/5 21171</td>
      <td>7.2500</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>Cumings, Mrs. John Bradley (Florence Briggs Th...</td>
      <td>female</td>
      <td>38.0</td>
      <td>1</td>
      <td>0</td>
      <td>PC 17599</td>
      <td>71.2833</td>
      <td>C85</td>
      <td>C</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>1</td>
      <td>3</td>
      <td>Heikkinen, Miss. Laina</td>
      <td>female</td>
      <td>26.0</td>
      <td>0</td>
      <td>0</td>
      <td>STON/O2. 3101282</td>
      <td>7.9250</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>1</td>
      <td>1</td>
      <td>Futrelle, Mrs. Jacques Heath (Lily May Peel)</td>
      <td>female</td>
      <td>35.0</td>
      <td>1</td>
      <td>0</td>
      <td>113803</td>
      <td>53.1000</td>
      <td>C123</td>
      <td>S</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>0</td>
      <td>3</td>
      <td>Allen, Mr. William Henry</td>
      <td>male</td>
      <td>35.0</td>
      <td>0</td>
      <td>0</td>
      <td>373450</td>
      <td>8.0500</td>
      <td>NaN</td>
      <td>S</td>
    </tr>
  </tbody>
</table>
</div>



## Get Labels

After the loading the data, we first get the labels of the data. The labels are the target values that we want to predict. In this case, the labels show whether the passenger survived or not. We store the labels in a variable called `y_true`. This variable contains the "ground truth", or the true value, values that we want to predict. By comparing the predicted values of the model to the true values, we can evaluate the performance of the model.


```python
y_true = titanic_df['Survived']
y_true
```




    0      0
    1      1
    2      1
    3      1
    4      0
          ..
    886    0
    887    1
    888    0
    889    1
    890    0
    Name: Survived, Length: 891, dtype: int64



## Baseline performance

We start by evaluating the performance of the model on the data we preprocessed before. This gives us a baseline that we can improve using feature engineering. We use the same preprocessing steps as before, but we do not use any feature engineering. We use a Random Forest Classifier, and evaluate its performance.


```python
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

titanic_preprocessed_df = pd.read_csv(os.path.join(data_directory, 'titanic/train_preprocessed.csv'))
y_preprocessed_true = titanic_preprocessed_df['Survived']
titanic_preprocessed_df = titanic_preprocessed_df.drop('Survived', axis=1)

rf_model = RandomForestClassifier(n_estimators=300, random_state=42)

X_train_base, X_test_base, y_train_base, y_test_base = train_test_split(titanic_preprocessed_df, y_preprocessed_true, test_size=0.2, random_state=42)

rf_model.fit(X_train_base, y_train_base)

y_pred_base = rf_model.predict(X_test_base)
print(classification_report(y_test_base, y_pred_base))
```

                  precision    recall  f1-score   support
    
               0       0.82      0.84      0.83       105
               1       0.76      0.74      0.75        74
    
        accuracy                           0.80       179
       macro avg       0.79      0.79      0.79       179
    weighted avg       0.80      0.80      0.80       179
    


# Missing values: imputation

In the previous notebook, we saw that the Titanic dataset contains missing values:


```python
titanic_df.isnull().sum().to_frame()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>PassengerId</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Survived</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Pclass</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Name</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Sex</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Age</th>
      <td>177</td>
    </tr>
    <tr>
      <th>SibSp</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Parch</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Ticket</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Fare</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Cabin</th>
      <td>687</td>
    </tr>
    <tr>
      <th>Embarked</th>
      <td>2</td>
    </tr>
  </tbody>
</table>
</div>



In the previous notebook, we used the SimpleImputer class from scikit to impute the `Age` variable with its `median` value. However, this does not take into account that several groups of passengers, may have different median ages. Let's look at which variables the `Age` variable has a high correlation with. These variables, we may need to take into account when imputing the `Age` variable.


```python
titanic_all_corr = titanic_df.corr(numeric_only=True).abs().unstack().sort_values(kind="quicksort", ascending=False).reset_index()
titanic_all_corr.rename(columns={"level_0": "Feature 1", "level_1": "Feature 2", 0: 'Correlation Coefficient'}, inplace=True)
titanic_all_corr[titanic_all_corr['Feature 1'] == 'Age']
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Feature 1</th>
      <th>Feature 2</th>
      <th>Correlation Coefficient</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>Age</td>
      <td>Age</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>12</th>
      <td>Age</td>
      <td>Pclass</td>
      <td>0.369226</td>
    </tr>
    <tr>
      <th>16</th>
      <td>Age</td>
      <td>SibSp</td>
      <td>0.308247</td>
    </tr>
    <tr>
      <th>21</th>
      <td>Age</td>
      <td>Parch</td>
      <td>0.189119</td>
    </tr>
    <tr>
      <th>26</th>
      <td>Age</td>
      <td>Fare</td>
      <td>0.096067</td>
    </tr>
    <tr>
      <th>32</th>
      <td>Age</td>
      <td>Survived</td>
      <td>0.077221</td>
    </tr>
    <tr>
      <th>36</th>
      <td>Age</td>
      <td>PassengerId</td>
      <td>0.036847</td>
    </tr>
  </tbody>
</table>
</div>



We see that `Age` variable has a high correlation with the `Pclass` and `SibSp` variables. This means that the `Age` variable may have different median values for different classes of passengers and passengers with a different number of siblings. Let's look at the median age for each class of passengers.


```python
age_by_class = titanic_df.groupby('Pclass')['Age'].mean()
age_by_class.plot(kind='bar', title='Average Age by Class')
```




    <Axes: title={'center': 'Average Age by Class'}, xlabel='Pclass'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_13_1.png)
    


This shows that the `Age` variable has different median values for different classes of passengers. The passengers in first class have a much higher median age than the passengers in second and third class. If we further use the `Sex` variable, we can see that the median ages per class and gender are also different.


```python
age_by_gender = titanic_df[["Age", "Pclass", "Sex"]].groupby(['Pclass','Sex']).median()
age_by_gender.plot(kind='bar', title='Median Age by Gender')
```




    <Axes: title={'center': 'Median Age by Gender'}, xlabel='Pclass,Sex'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_15_1.png)
    


We can use these more specific groups based on social class and gender to impute the age values:


```python
titanic_df["Age"] = titanic_df.groupby(['Sex', 'Pclass'])['Age'].apply(lambda x: x.fillna(x.median())).reset_index(level=[0,1], drop=True)
```

And check if there are any missing values left:


```python
titanic_df.isnull().sum().to_frame()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>PassengerId</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Survived</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Pclass</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Name</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Sex</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Age</th>
      <td>0</td>
    </tr>
    <tr>
      <th>SibSp</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Parch</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Ticket</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Fare</th>
      <td>0</td>
    </tr>
    <tr>
      <th>Cabin</th>
      <td>687</td>
    </tr>
    <tr>
      <th>Embarked</th>
      <td>2</td>
    </tr>
  </tbody>
</table>
</div>



## Missing values in Embarked

When we look at the passengers with missing values for the `Embarked` variable, we see there are two passengers with a missing value:


```python
titanic_df[titanic_df['Embarked'].isnull()]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>61</th>
      <td>62</td>
      <td>1</td>
      <td>1</td>
      <td>Icard, Miss. Amelie</td>
      <td>female</td>
      <td>38.0</td>
      <td>0</td>
      <td>0</td>
      <td>113572</td>
      <td>80.0</td>
      <td>B28</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>829</th>
      <td>830</td>
      <td>1</td>
      <td>1</td>
      <td>Stone, Mrs. George Nelson (Martha Evelyn)</td>
      <td>female</td>
      <td>62.0</td>
      <td>0</td>
      <td>0</td>
      <td>113572</td>
      <td>80.0</td>
      <td>B28</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



We see that both passengers have the same ticket id. This means that they probably have been traveling together. When we Google the name of the passengers, we find the following [information](https://www.encyclopedia-titanica.org/titanic-survivor/martha-evelyn-stone.html).
We find the following information:

> Mrs Stone boarded the Titanic in Southampton on 10 April 1912 and was travelling in first class with her maid Amelie Icard. She occupied cabin B-28

So both Mrs Stone and her maid Amelie Icard embarked in Southampton. We can impute the missing values with the value `S`.


```python
# Filling the missing values in Embarked with S
titanic_df['Embarked'] = titanic_df['Embarked'].fillna('S')
```

# Missing values in Cabin

The `Cabin` variable has a lot of missing values. Earlier, we encoded the `Cabin` value as a boolean value that indicated whether the cabin is known or not. However, we can also use the known cabins to derive new features. For example, we can use the first letter of the cabin to derive the deck of the cabin. Let's do this now, we will use the `M` value to encode the missing values:


```python
titanic_df['Deck'] = titanic_df['Cabin'].apply(lambda s: s[0] if pd.notnull(s) else 'M')
titanic_df.value_counts('Deck')
```




    Deck
    M    687
    C     59
    B     47
    D     33
    E     32
    A     15
    F     13
    G      4
    T      1
    Name: count, dtype: int64



We can see that 687 passengers have a missing value for the `Cabin` and thus also for the `Deck` variable. However, for the passengers that do have deck information, we can use that information to say something about the survival rates of the passengers on that deck. Some decks were closer to the stairs and therefore have a higher survival rate. Decks were mostly separated per passenger class, but some were used by multiple passenger classes. 

![Titanic deck plan](./Titanic_side_plan.webp)

The deck plan looked like this:

- On the Boat Deck there were 6 rooms labeled as T, U, W, X, Y, Z but only the T cabin is present in the dataset
- A, B and C decks were only for 1st class passengers
- D and E decks were for all classes
- F and G decks were for both 2nd and 3rd class passengers
- From going A to G, distance to the staircase increases which might be a factor of survival

Let's look at the survival rates of the passengers on each deck:


```python
titanic_df.groupby('Deck')['Survived'].mean().plot(kind='bar', title='Survival Rate by Deck')
```




    <Axes: title={'center': 'Survival Rate by Deck'}, xlabel='Deck'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_27_1.png)
    



```python
titanic_df[titanic_df['Deck'] == 'T']
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
      <th>Deck</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>339</th>
      <td>340</td>
      <td>0</td>
      <td>1</td>
      <td>Blackwell, Mr. Stephen Weart</td>
      <td>male</td>
      <td>45.0</td>
      <td>0</td>
      <td>0</td>
      <td>113784</td>
      <td>35.5</td>
      <td>T</td>
      <td>S</td>
      <td>T</td>
    </tr>
  </tbody>
</table>
</div>




```python
total_per_deck_class = titanic_df.groupby(['Deck','Pclass'])['Pclass'].count()
total_by_deck = titanic_df['Deck'].value_counts()

class_percentage_per_deck = (total_per_deck_class / total_by_deck).unstack()
class_percentage_per_deck.plot(kind='bar', stacked=True, title='Class Percentage by Deck')
```




    <Axes: title={'center': 'Class Percentage by Deck'}, xlabel='Deck'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_29_1.png)
    



```python
class_percentage_per_deck
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Pclass</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
    </tr>
    <tr>
      <th>Deck</th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A</th>
      <td>1.000000</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>B</th>
      <td>1.000000</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>C</th>
      <td>1.000000</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>D</th>
      <td>0.878788</td>
      <td>0.121212</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>E</th>
      <td>0.781250</td>
      <td>0.125000</td>
      <td>0.093750</td>
    </tr>
    <tr>
      <th>F</th>
      <td>NaN</td>
      <td>0.615385</td>
      <td>0.384615</td>
    </tr>
    <tr>
      <th>G</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>1.000000</td>
    </tr>
    <tr>
      <th>M</th>
      <td>0.058224</td>
      <td>0.244541</td>
      <td>0.697234</td>
    </tr>
    <tr>
      <th>T</th>
      <td>1.000000</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



In the plot and table above, we can see that on the A, B, C, and T decks there are only first class passengers. The D deck has passengers from the 1st and 2nd class. The E deck of all classes. The F deck has passengers from the 2nd and 3rd class. The G deck has only 3rd class passengers. To see whether the deck the passengers were in had an effect on their survival rate, we can plot the survival rates of the passengers on each deck:


```python
survival_rate_per_deck = titanic_df.groupby('Deck')['Survived'].value_counts(normalize=True).unstack()
survival_rate_per_deck

```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Survived</th>
      <th>0</th>
      <th>1</th>
    </tr>
    <tr>
      <th>Deck</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A</th>
      <td>0.533333</td>
      <td>0.466667</td>
    </tr>
    <tr>
      <th>B</th>
      <td>0.255319</td>
      <td>0.744681</td>
    </tr>
    <tr>
      <th>C</th>
      <td>0.406780</td>
      <td>0.593220</td>
    </tr>
    <tr>
      <th>D</th>
      <td>0.242424</td>
      <td>0.757576</td>
    </tr>
    <tr>
      <th>E</th>
      <td>0.250000</td>
      <td>0.750000</td>
    </tr>
    <tr>
      <th>F</th>
      <td>0.384615</td>
      <td>0.615385</td>
    </tr>
    <tr>
      <th>G</th>
      <td>0.500000</td>
      <td>0.500000</td>
    </tr>
    <tr>
      <th>M</th>
      <td>0.700146</td>
      <td>0.299854</td>
    </tr>
    <tr>
      <th>T</th>
      <td>1.000000</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
survival_rate_per_deck.plot(kind='bar', stacked=True, title='Survival Rate by Deck')
```




    <Axes: title={'center': 'Survival Rate by Deck'}, xlabel='Deck'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_33_1.png)
    


We can see that the deck the passengers were on had an effect on their survival rate. Passengers on the B, C, D, and E decks had a higher survival rate than passengers on the other decks. Passengers on the M deck had the lowest survival rate (the T cabin was on the boat deck and only had one passenger). Moreover passengers on the D and the E deck had similar survival rates. We can use the information about the survival rate per deck and the class distribution per deck to derive combined features:

- We will group the decks A, B, C, and T together, because they only have first class passengers.
- We will group the decks D and E together, because they have a similar class distribution and survival rate.
- We will group the decks F and G together, because they have a similar survival rate.
- We won't change the M deck, because it has a very different survival rate than the other decks.

Let's create these new features: 


```python
titanic_df['Deck'] = titanic_df['Deck'].replace(['A', 'B', 'C', 'T'], 'ABC')
titanic_df['Deck'] = titanic_df['Deck'].replace(['D', 'E'], 'DE')
titanic_df['Deck'] = titanic_df['Deck'].replace(['F', 'G'], 'FG')

titanic_df['Deck'].value_counts()
```




    Deck
    M      687
    ABC    122
    DE      65
    FG      17
    Name: count, dtype: int64



## Categorical Features

### Encoding the categorical features

We can encode the categorical features in two ways:

- We can use one-hot encoding to encode the categorical features. This means that we create a new binary variable for each category of the categorical variable. For example, the `Sex` variable has two categories: `male` and `female`. We can encode this variable using one-hot encoding by creating two new variables: `Sex_male` and `Sex_female`. `Sex_male` variables will have a value of 1 if the passenger is male and 0 otherwise. 
- We can use label encoding to encode the categorical features. This means that we assign a unique integer value to each category of the categorical variable. For example, the `Embarked` variable has three categories: `S`, `C`, and `Q`. We can encode this variable using label encoding by assigning the value 0 to `S`, 1 to `C`, and 2 to `Q`.

In this notebook, we will use label encoding for the categorical features:


```python
from sklearn.preprocessing import LabelEncoder

categorical_features = ["Sex", "Embarked", "Deck"]

for feature in categorical_features:
    label_encoder = LabelEncoder()
    titanic_df[feature] = label_encoder.fit_transform(titanic_df[feature])

titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Name</th>
      <th>Sex</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Ticket</th>
      <th>Fare</th>
      <th>Cabin</th>
      <th>Embarked</th>
      <th>Deck</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>0</td>
      <td>3</td>
      <td>Braund, Mr. Owen Harris</td>
      <td>1</td>
      <td>22.0</td>
      <td>1</td>
      <td>0</td>
      <td>A/5 21171</td>
      <td>7.2500</td>
      <td>NaN</td>
      <td>2</td>
      <td>3</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>Cumings, Mrs. John Bradley (Florence Briggs Th...</td>
      <td>0</td>
      <td>38.0</td>
      <td>1</td>
      <td>0</td>
      <td>PC 17599</td>
      <td>71.2833</td>
      <td>C85</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>1</td>
      <td>3</td>
      <td>Heikkinen, Miss. Laina</td>
      <td>0</td>
      <td>26.0</td>
      <td>0</td>
      <td>0</td>
      <td>STON/O2. 3101282</td>
      <td>7.9250</td>
      <td>NaN</td>
      <td>2</td>
      <td>3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>1</td>
      <td>1</td>
      <td>Futrelle, Mrs. Jacques Heath (Lily May Peel)</td>
      <td>0</td>
      <td>35.0</td>
      <td>1</td>
      <td>0</td>
      <td>113803</td>
      <td>53.1000</td>
      <td>C123</td>
      <td>2</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>0</td>
      <td>3</td>
      <td>Allen, Mr. William Henry</td>
      <td>1</td>
      <td>35.0</td>
      <td>0</td>
      <td>0</td>
      <td>373450</td>
      <td>8.0500</td>
      <td>NaN</td>
      <td>2</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>



## Derive Family Sizes

Another feature we can derive from the Titanic dataset is the family size of the passengers. We can derive the family size by adding the `SibSp` and `Parch` variables. The `SibSp` variable shows the number of siblings and spouses the passenger had on board. The `Parch` variable shows the number of parents and children the passenger had on board. By adding these two variables, we can derive the family size of the passenger. In addition, from the family size, we can derive whether the passenger was alone on board or not. Let's derive these features now:


```python
titanic_df['FamilySize'] = titanic_df['SibSp'] + titanic_df['Parch'] + 1
titanic_df['IsAlone'] = 0
titanic_df.loc[titanic_df['FamilySize'] == 1, 'IsAlone'] = 1
```


```python
titanic_df.groupby('IsAlone')['Survived'].mean().plot(kind='bar', title='Survival Rate by IsAlone')
```




    <Axes: title={'center': 'Survival Rate by IsAlone'}, xlabel='IsAlone'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_41_1.png)
    


We can see that passengers traveling alone had a lower survival rate than passengers traveling with family. 


```python
titanic_df.groupby('FamilySize')['Survived'].mean().plot(kind='bar', title='Survival Rate by Family Size')
```




    <Axes: title={'center': 'Survival Rate by Family Size'}, xlabel='FamilySize'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_43_1.png)
    


In addition, we can see that the family size of the passengers had an effect on their survival rate. Passengers traveling with 1 to 4, or 7 family members had a higher survival rate than passengers traveling alone or with more than 3 family members. 

## Using the length of the names as a feature

We can use the length of the names of the passengers as a feature. The length of the name may indicate the social status of the passenger. For example, passengers with longer names may have a higher social status. Let's create this feature:


```python
titanic_df["Name_length"] = titanic_df["Name"].apply(len)
```

If we plot the length of the Name grouped by social class, we see that the first class passengers have longer names than the second and third class passengers. Also second class passengers have longer names than third class passengers. This means that the length of the name may be a good feature to use in the model.


```python
titanic_df.groupby('Pclass')['Name_length'].mean().plot(kind='bar', title='Average Name Length by Class')
```




    <Axes: title={'center': 'Average Name Length by Class'}, xlabel='Pclass'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_47_1.png)
    


We can check if the length of the name is correlated with the survival rate:


```python
titanic_df.groupby('Survived')['Name_length'].mean().plot(kind='bar', title='Average Name Length by Survival')
```




    <Axes: title={'center': 'Average Name Length by Survival'}, xlabel='Survived'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_49_1.png)
    


The plot shows indeed that people with longer names have a higher survival rate. 

## Drop the processed columns

We can now drop the columns that we have used to derive the new features. We can also drop the columns that we do not need anymore. 


```python
titanic_df = titanic_df.drop(columns=['PassengerId', 'Name', 'Ticket', 'Cabin','Survived'])
```

## Performance after feature engineering

Now we have added several features and encoded the categorical columns, we can evaluate the performance of the model with feature engineering. Below, we compare the base model performance with the performance after feature engineering.


```python
rf_model = RandomForestClassifier(n_estimators=300, random_state=1)

X_train, X_test, y_train, y_test = train_test_split(titanic_df, y_true, test_size=0.2, random_state=42)

rf_model.fit(X_train, y_train)

y_pred = rf_model.predict(X_test)

print("--- Base Model (Without feature engineering) ---")
print(classification_report(y_test_base, y_pred_base))
print("--- Model with feature engineering ---")
print(classification_report(y_test, y_pred))
```

    --- Base Model (Without feature engineering) ---
                  precision    recall  f1-score   support
    
               0       0.82      0.84      0.83       105
               1       0.76      0.74      0.75        74
    
        accuracy                           0.80       179
       macro avg       0.79      0.79      0.79       179
    weighted avg       0.80      0.80      0.80       179
    
    --- Model with feature engineering ---
                  precision    recall  f1-score   support
    
               0       0.87      0.86      0.86       105
               1       0.80      0.81      0.81        74
    
        accuracy                           0.84       179
       macro avg       0.83      0.83      0.83       179
    weighted avg       0.84      0.84      0.84       179
    


The classification report shows that by using feature engineering, we were able to improve the performance of the model. The precision, recall, and F1-score of the model have all improved. This shows that feature engineering can be a powerful tool to improve the performance of the model.

This also shows if we look at the feature importances returned by the Random Forest model:


```python
feature_importances = pd.DataFrame({"feature_name": rf_model.feature_names_in_, "feature_importances": rf_model.feature_importances_})

feature_importances.sort_values(by="feature_importances", ascending=True).plot(x="feature_name", y="feature_importances", kind="barh",  title="Feature Importances", figsize=(10, 5))

```




    <Axes: title={'center': 'Feature Importances'}, ylabel='feature_name'>




    
![png](06a%20-%20Titanic%20Dataset_Feature%20Engineering_files/06a%20-%20Titanic%20Dataset_Feature%20Engineering_56_1.png)
    


We see here that among the six most important features, two are derived features, namely, the `Deck` and `Name_length` features. This shows that feature engineering can be a powerful tool to improve the performance of the model.

# Conclusion

In this notebook, we saw how to use feature engineering to improve the performance of the model. We saw that by using domain knowledge, we can derive new features from the existing data that can help the model to learn patterns that it would not be able to learn otherwise. We saw that by using feature engineering, we were able to improve the performance of the model on the Titanic dataset. We saw that the precision, recall, and F1-score of the model have all improved. This shows that feature engineering can be a powerful tool to improve the performance of the model.

You now have several tools to improve the performance of the model on the Titanic dataset:

- you can use more complex models, such as Multi-layer Perceptrons, Random Forests or eXtreme Gradient Boosting.
- you can use hyperparameter optimization to tune the model's parameters.
- you can use feature engineering to derive new features from the existing data.
