# Supervised Challenge

Now you should have a solid understanding of the basic concepts of supervised learning. In this challenge, you will be working with a dataset that is a bit more complex than the ones you have seen so far. This is a real dataset from the CBS website. Your assignment consists of two parts, a classification task as well as a regression task. You can use any of the models and techniques you have learned so far. 

We will first import the relevant data and then describe the individual challenges.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```

## Read reference data: description of columns in data

The CBS dataset comes with an extra file describing the columns in the dataset. Let's read this file first.


```python
import pandas as pd
 
kwb_description = pd.read_csv(os.path.join(data_directory, "kwb-2018-toelichting.csv"), delimiter=';', names=["column_name", "description", "comments"], index_col=0)
kwb_description.drop(columns=["comments"], inplace=True)
kwb_description.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
  </tbody>
</table>
</div>



We can see that for every column in the dataset, there is a description of the column. This will help us understand the dataset better. So if for example we want to know what the column `a_pau` means, we can look it up in this file using the following code:


```python
kwb_description.loc['a_pau']  # use these descriptions to try and determine the parameter being asked.
```




    description    Personenauto's totaal
    Name: a_pau, dtype: object



After reading the metadata, we can read the dataset itself. The dataset is stored in a CSV file, so we can use the `read_csv` function from the `pandas` library to read the dataset. We will also print the first few rows of the dataset to get an idea of what the dataset looks like.


```python
kwb_data = pd.read_csv(os.path.join(data_directory, "kwb-2018-full.csv"), delimiter=';', encoding='latin1')
kwb_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>g_afs_kv</th>
      <th>g_afs_sc</th>
      <th>g_3km_sc</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>pst_mvp</th>
      <th>pst_dekp</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>NL00</td>
      <td>0</td>
      <td>Nederland</td>
      <td>Nederland</td>
      <td>Land</td>
      <td>NL00</td>
      <td>.</td>
      <td>17181084</td>
      <td>8527041</td>
      <td>8654043</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>4154338</td>
      <td>3367109</td>
      <td>787228</td>
      <td>.</td>
      <td>.</td>
      <td>2</td>
      <td>1978</td>
    </tr>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>.</td>
      <td>.</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>2</th>
      <td>WK000300</td>
      <td>300</td>
      <td>Wijk 00</td>
      <td>Appingedam</td>
      <td>Wijk</td>
      <td>WK000300</td>
      <td>1</td>
      <td>11800</td>
      <td>5750</td>
      <td>6050</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>.</td>
      <td>.</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>3</th>
      <td>BU00030000</td>
      <td>30000</td>
      <td>Appingedam-Centrum</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030000</td>
      <td>1</td>
      <td>2355</td>
      <td>1120</td>
      <td>1235</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>90</td>
      <td>84</td>
      <td>5</td>
      <td>9901</td>
      <td>1</td>
      <td>3</td>
      <td>1195</td>
    </tr>
    <tr>
      <th>4</th>
      <td>BU00030001</td>
      <td>30001</td>
      <td>Appingedam-West</td>
      <td>Appingedam</td>
      <td>Buurt</td>
      <td>BU00030001</td>
      <td>1</td>
      <td>3030</td>
      <td>1505</td>
      <td>1525</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>163</td>
      <td>158</td>
      <td>5</td>
      <td>9903</td>
      <td>5</td>
      <td>4</td>
      <td>896</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 109 columns</p>
</div>



## Selecting the data we want to use

The dataset contains several types of regions, but we are only interested in the municipalities. We first have to find the rows that correspond to municipalities. Let's look at the metadata to find the column containing the region type.


```python
kwb_description
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>description</th>
    </tr>
    <tr>
      <th>column_name</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>gwb_code_10</th>
      <td>gwb code 10</td>
    </tr>
    <tr>
      <th>gwb_code_8</th>
      <td>gwb code 8</td>
    </tr>
    <tr>
      <th>regio</th>
      <td>Regioaanduiding</td>
    </tr>
    <tr>
      <th>gm_naam</th>
      <td>Gemeentenaam</td>
    </tr>
    <tr>
      <th>recs</th>
      <td>Soort regio</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>a_wat_ha</th>
      <td>Oppervlakte water</td>
    </tr>
    <tr>
      <th>pst_mvp</th>
      <td>Meest voorkomende postcode</td>
    </tr>
    <tr>
      <th>pst_dekp</th>
      <td>Dekkingspercentage</td>
    </tr>
    <tr>
      <th>ste_mvs</th>
      <td>Mate van stedelijkheid</td>
    </tr>
    <tr>
      <th>ste_oad</th>
      <td>Omgevingsadressendichtheid</td>
    </tr>
  </tbody>
</table>
<p>109 rows × 1 columns</p>
</div>



We can see here that the `recs` column contains the region type:


```python
kwb_description.loc['recs']
```




    description    Soort regio
    Name: recs, dtype: object



To see what values the `recs` column can have, we can use the `unique` function from the `pandas` library. This function returns all unique values in a column.


```python
kwb_data['recs'].unique()
```




    array(['Land', 'Gemeente', 'Wijk', 'Buurt'], dtype=object)




```python
selected_region_type = 'Gemeente'
```

We can filter the dataset to only include the municipalities by selecting the rows where the `recs` column is equal to `Gemeente`. 


```python
data_for_selected_region_type = kwb_data[kwb_data.recs == selected_region_type]
data_for_selected_region_type.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gwb_code_10</th>
      <th>gwb_code_8</th>
      <th>regio</th>
      <th>gm_naam</th>
      <th>recs</th>
      <th>gwb_code</th>
      <th>ind_wbi</th>
      <th>a_inw</th>
      <th>a_man</th>
      <th>a_vrouw</th>
      <th>...</th>
      <th>g_afs_kv</th>
      <th>g_afs_sc</th>
      <th>g_3km_sc</th>
      <th>a_opp_ha</th>
      <th>a_lan_ha</th>
      <th>a_wat_ha</th>
      <th>pst_mvp</th>
      <th>pst_dekp</th>
      <th>ste_mvs</th>
      <th>ste_oad</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>GM0003</td>
      <td>3</td>
      <td>Appingedam</td>
      <td>Appingedam</td>
      <td>Gemeente</td>
      <td>GM0003</td>
      <td>.</td>
      <td>11801</td>
      <td>5751</td>
      <td>6050</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>2458</td>
      <td>2378</td>
      <td>80</td>
      <td>.</td>
      <td>.</td>
      <td>3</td>
      <td>1051</td>
    </tr>
    <tr>
      <th>9</th>
      <td>GM0005</td>
      <td>5</td>
      <td>Bedum</td>
      <td>Bedum</td>
      <td>Gemeente</td>
      <td>GM0005</td>
      <td>.</td>
      <td>10475</td>
      <td>5235</td>
      <td>5240</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>4496</td>
      <td>4454</td>
      <td>41</td>
      <td>.</td>
      <td>.</td>
      <td>4</td>
      <td>685</td>
    </tr>
    <tr>
      <th>19</th>
      <td>GM0009</td>
      <td>9</td>
      <td>Ten Boer</td>
      <td>Ten Boer</td>
      <td>Gemeente</td>
      <td>GM0009</td>
      <td>.</td>
      <td>7292</td>
      <td>3616</td>
      <td>3676</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>4573</td>
      <td>4531</td>
      <td>42</td>
      <td>.</td>
      <td>.</td>
      <td>5</td>
      <td>372</td>
    </tr>
    <tr>
      <th>34</th>
      <td>GM0010</td>
      <td>10</td>
      <td>Delfzijl</td>
      <td>Delfzijl</td>
      <td>Gemeente</td>
      <td>GM0010</td>
      <td>.</td>
      <td>24864</td>
      <td>12475</td>
      <td>12389</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>22750</td>
      <td>13307</td>
      <td>9443</td>
      <td>.</td>
      <td>.</td>
      <td>4</td>
      <td>677</td>
    </tr>
    <tr>
      <th>64</th>
      <td>GM0014</td>
      <td>14</td>
      <td>Groningen</td>
      <td>Groningen</td>
      <td>Gemeente</td>
      <td>GM0014</td>
      <td>.</td>
      <td>202810</td>
      <td>101299</td>
      <td>101511</td>
      <td>...</td>
      <td>.</td>
      <td>.</td>
      <td>.</td>
      <td>10150</td>
      <td>9492</td>
      <td>658</td>
      <td>.</td>
      <td>.</td>
      <td>1</td>
      <td>3456</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 109 columns</p>
</div>



Finally, we can also check how many municipalities are in the dataset.


```python
len(data_for_selected_region_type)
```




    380



The filtered dataset contains 380 municipalities. The data is from 2018, and indeed that number is correct. 

-----------------

# Classification challenge

Learn a model that predicts `ste_mvs`, the degree of urbanisation.



```python
#begin solution
```


## Get the Labels

Next, it is time to get our features and target variable. The target variable is the degree of urbanization, which is stored in the `ste_mvs` column. The features can be the other columns in the dataset. We can retrieve the target variable by using the following code:


```python
y_true = data_for_selected_region_type[['ste_mvs']]
```

Now the variable `y_true` contains the target variable. In this case, the target variable is the degree of urbanization. CBS classifies the degree of urbanization into five classes. Based on the environmental address density, each neighborhood, district, or municipality has been assigned an urbanization class. The following classification has been used:

Value | Description | Address density
--- | --- | ---
1 | very strongly urbanized | >= 2,500 addresses per km²
2 | strongly urbanized | 1,500 - 2,500 addresses per km²
3 | moderately urbanized | 1,000 - 1,500 addresses per km²
4 | weakly urbanized | 500 - 1,000 addresses per km²
5 | non-urbanized | < 500 addresses per km²

Let's see how many municipalities are in each class.


```python
target_name = ['ste_mvs']
target_categories = ['very strongly urbanized', 'strongly urbanized', 'moderately urbanized', 'weakly urbanized', 'non-urbanized']
y_true[target_name].value_counts().sort_index()
```




    ste_mvs
    1           19
    2           74
    3           78
    4          135
    5           74
    Name: count, dtype: int64





## Selecting some features

The dataset contains a lot of characteristics per municipality. Not all of these characteristics are useful for predicting the degree of urbanization. We can select a subset of the features that we think are most relevant. For example, we can select the area of the municipality in hectares and the number of households as features:


```python
feature_names = ['a_hh', 'a_opp_ha'] 
X = data_for_selected_region_type[feature_names]
X.head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a_hh</th>
      <th>a_opp_ha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>5540</td>
      <td>2458</td>
    </tr>
    <tr>
      <th>9</th>
      <td>4391</td>
      <td>4496</td>
    </tr>
    <tr>
      <th>19</th>
      <td>2980</td>
      <td>4573</td>
    </tr>
    <tr>
      <th>34</th>
      <td>11617</td>
      <td>22750</td>
    </tr>
    <tr>
      <th>64</th>
      <td>122541</td>
      <td>10150</td>
    </tr>
  </tbody>
</table>
</div>



## Split the Data into training and testing data

Now that we have our features and target variable, we can split the data into training and testing data. We will use 80% of the data for training and 20% for testing. We can use the `train_test_split` function from the `sklearn` library to split the data.


```python
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y_true, random_state=42)
```

## Train the Model

Once we have split the data we can train a RandomForest model. We can use the `RandomForestClassifier` class from the `sklearn` library to create a RandomForest model. We can then use the `fit` method to train the model on the training data.


```python
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report

rf_base_model = RandomForestClassifier(n_estimators=300, random_state=42)
rf_base_model.fit(X_train, y_train)

y_pred = rf_base_model.predict(X_test)

print(f"Your model scores: {classification_report(y_test, y_pred)}")
```

    Your model scores:               precision    recall  f1-score   support
    
               1       0.50      0.33      0.40         3
               2       0.71      0.71      0.71        21
               3       0.59      0.59      0.59        22
               4       0.63      0.73      0.68        30
               5       0.80      0.63      0.71        19
    
        accuracy                           0.66        95
       macro avg       0.65      0.60      0.62        95
    weighted avg       0.67      0.66      0.66        95
    


    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/sklearn/base.py:1473: DataConversionWarning: A column-vector y was passed when a 1d array was expected. Please change the shape of y to (n_samples,), for example using ravel().
      return fit_method(estimator, *args, **kwargs)


## Design features that allows you to classify the degree of urbanisation as best as you can!

Try using different features, compute new features or use different classification algorithm.


```python
# a_opp_ha is totale oppervlakte in hele hectaren.
# een km2 is 100 hectaren
# data: inwoners, huishoudens
# 1: zeer sterk stedelijk >= 2 500 adressen per km²
# 2: sterk stedelijk 1 500 - 2 500 adressen per km²
# 3: matig stedelijk 1 000 - 1 500 adressen per km²
# 4: weinig stedelijk 500 - 1 000 adressen per km²
# 5: niet stedelijk < 500 adressen per km²

data_for_selected_region_type['a_hh_p_km2'] = data_for_selected_region_type['a_hh'] / (data_for_selected_region_type['a_opp_ha']/100)
data_for_selected_region_type[['a_hh_p_km2','a_hh','a_opp_ha']].head(5)
```

    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_4272/3336679801.py:10: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame.
    Try using .loc[row_indexer,col_indexer] = value instead
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      data_for_selected_region_type['a_hh_p_km2'] = data_for_selected_region_type['a_hh'] / (data_for_selected_region_type['a_opp_ha']/100)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a_hh_p_km2</th>
      <th>a_hh</th>
      <th>a_opp_ha</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>225.386493</td>
      <td>5540</td>
      <td>2458</td>
    </tr>
    <tr>
      <th>9</th>
      <td>97.664591</td>
      <td>4391</td>
      <td>4496</td>
    </tr>
    <tr>
      <th>19</th>
      <td>65.165099</td>
      <td>2980</td>
      <td>4573</td>
    </tr>
    <tr>
      <th>34</th>
      <td>51.063736</td>
      <td>11617</td>
      <td>22750</td>
    </tr>
    <tr>
      <th>64</th>
      <td>1207.300493</td>
      <td>122541</td>
      <td>10150</td>
    </tr>
  </tbody>
</table>
</div>




```python
X = data_for_selected_region_type[['a_hh_p_km2','a_hh','a_opp_ha']]
X_train, X_test, y_train, y_test = train_test_split(X, y_true, random_state=42)
```


```python
# all in one cell
rf_model = RandomForestClassifier(n_estimators=300, random_state=42)
rf_model.fit(X_train, y_train)

y_pred = rf_model.predict(X_test)

print(f"Your model scores: {classification_report(y_test, y_pred)}")

```

    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/sklearn/base.py:1473: DataConversionWarning: A column-vector y was passed when a 1d array was expected. Please change the shape of y to (n_samples,), for example using ravel().
      return fit_method(estimator, *args, **kwargs)


    Your model scores:               precision    recall  f1-score   support
    
               1       1.00      0.33      0.50         3
               2       0.74      0.81      0.77        21
               3       0.61      0.64      0.62        22
               4       0.64      0.60      0.62        30
               5       0.65      0.68      0.67        19
    
        accuracy                           0.66        95
       macro avg       0.73      0.61      0.64        95
    weighted avg       0.67      0.66      0.66        95
    



```python
#end solution
```

-----------------

# Regression challenge

Learn a model that predicts `a_pau`, the number of cars. Beware, this is a real number now, not a class. This is a regression problem!


The city of The Hague and Heerlen expects that the number of inhabitants will increase with 10% in the next 5 years.
What will be the impact on the number of cars in the city? This is important information when planning public infrastructure.




```python
kwb_description.loc['a_pau']  
```




    description    Personenauto's totaal
    Name: a_pau, dtype: object




```python
#begin solution
```

## Get the Labels

First, we get the target variable. The target variable is the number of cars, which is stored in the `a_pau` column. We can retrieve the target variable by using the following code:


```python
y_reg_true = kwb_data[['a_pau']]
```

## Selecting some features


```python
# should we use kwb full instead??

kwb_data.columns
```




    Index(['gwb_code_10', 'gwb_code_8', 'regio', 'gm_naam', 'recs', 'gwb_code',
           'ind_wbi', 'a_inw', 'a_man', 'a_vrouw',
           ...
           'g_afs_kv', 'g_afs_sc', 'g_3km_sc', 'a_opp_ha', 'a_lan_ha', 'a_wat_ha',
           'pst_mvp', 'pst_dekp', 'ste_mvs', 'ste_oad'],
          dtype='object', length=109)




```python
import numpy as np

kwb_data = kwb_data.replace(".", np.nan)
```


```python
import seaborn as sns

pearson_corr = kwb_data.corr(method='pearson', numeric_only=True)
sns.heatmap(pearson_corr)
```




    <Axes: >




    
![png](07%20-%20Challenge%20Supervised_files/07%20-%20Challenge%20Supervised_45_1.png)
    



```python
pearson_corr['a_pau'].sort_values(ascending=False).plot()
```




    <Axes: >




    
![png](07%20-%20Challenge%20Supervised_files/07%20-%20Challenge%20Supervised_46_1.png)
    



```python
selected_columns = pearson_corr['a_pau'].sort_values(ascending=False).index[:30]
```


```python
X_reg = kwb_data[selected_columns] #['a_hh','a_opp_ha', 'g_woz']]
X_reg
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a_pau</th>
      <th>a_bst_b</th>
      <th>a_gehuwd</th>
      <th>a_45_64</th>
      <th>a_m2w</th>
      <th>a_hh_m_k</th>
      <th>a_65_oo</th>
      <th>a_hh_z_k</th>
      <th>a_verwed</th>
      <th>a_00_14</th>
      <th>...</th>
      <th>a_1p_hh</th>
      <th>a_lan_ha</th>
      <th>a_w_all</th>
      <th>a_opp_ha</th>
      <th>a_ov_nw</th>
      <th>a_nw_all</th>
      <th>a_tur</th>
      <th>a_antaru</th>
      <th>a_wat_ha</th>
      <th>a_marok</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>8373245</td>
      <td>6649495</td>
      <td>6710175</td>
      <td>4839917</td>
      <td>661640</td>
      <td>2594888</td>
      <td>3239116</td>
      <td>2265409</td>
      <td>858676</td>
      <td>2762624</td>
      <td>...</td>
      <td>2997617</td>
      <td>3367109</td>
      <td>1729016</td>
      <td>4154338</td>
      <td>933870</td>
      <td>2242843</td>
      <td>404459</td>
      <td>156294</td>
      <td>787228</td>
      <td>396539</td>
    </tr>
    <tr>
      <th>1</th>
      <td>5440</td>
      <td>4650</td>
      <td>4924</td>
      <td>3562</td>
      <td>550</td>
      <td>1700</td>
      <td>2825</td>
      <td>1775</td>
      <td>829</td>
      <td>1773</td>
      <td>...</td>
      <td>2065</td>
      <td>2378</td>
      <td>869</td>
      <td>2458</td>
      <td>326</td>
      <td>814</td>
      <td>257</td>
      <td>100</td>
      <td>80</td>
      <td>48</td>
    </tr>
    <tr>
      <th>2</th>
      <td>5435</td>
      <td>4645</td>
      <td>4925</td>
      <td>3560</td>
      <td>550</td>
      <td>1700</td>
      <td>2825</td>
      <td>1775</td>
      <td>830</td>
      <td>1770</td>
      <td>...</td>
      <td>2065</td>
      <td>2378</td>
      <td>870</td>
      <td>2458</td>
      <td>325</td>
      <td>815</td>
      <td>255</td>
      <td>100</td>
      <td>80</td>
      <td>45</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1075</td>
      <td>925</td>
      <td>850</td>
      <td>715</td>
      <td>95</td>
      <td>240</td>
      <td>685</td>
      <td>370</td>
      <td>255</td>
      <td>230</td>
      <td>...</td>
      <td>690</td>
      <td>84</td>
      <td>140</td>
      <td>90</td>
      <td>55</td>
      <td>90</td>
      <td>10</td>
      <td>15</td>
      <td>5</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1545</td>
      <td>1305</td>
      <td>1405</td>
      <td>1020</td>
      <td>155</td>
      <td>470</td>
      <td>605</td>
      <td>495</td>
      <td>120</td>
      <td>495</td>
      <td>...</td>
      <td>350</td>
      <td>158</td>
      <td>160</td>
      <td>163</td>
      <td>45</td>
      <td>110</td>
      <td>35</td>
      <td>20</td>
      <td>5</td>
      <td>5</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>16767</th>
      <td>25</td>
      <td>20</td>
      <td>75</td>
      <td>65</td>
      <td>0</td>
      <td>40</td>
      <td>15</td>
      <td>10</td>
      <td>5</td>
      <td>40</td>
      <td>...</td>
      <td>10</td>
      <td>81</td>
      <td>0</td>
      <td>81</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16768</th>
      <td>290</td>
      <td>240</td>
      <td>225</td>
      <td>155</td>
      <td>30</td>
      <td>80</td>
      <td>100</td>
      <td>60</td>
      <td>35</td>
      <td>85</td>
      <td>...</td>
      <td>35</td>
      <td>693</td>
      <td>15</td>
      <td>695</td>
      <td>5</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16769</th>
      <td>570</td>
      <td>450</td>
      <td>350</td>
      <td>235</td>
      <td>50</td>
      <td>145</td>
      <td>95</td>
      <td>105</td>
      <td>20</td>
      <td>150</td>
      <td>...</td>
      <td>100</td>
      <td>403</td>
      <td>45</td>
      <td>413</td>
      <td>10</td>
      <td>15</td>
      <td>5</td>
      <td>0</td>
      <td>9</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16770</th>
      <td>480</td>
      <td>365</td>
      <td>415</td>
      <td>305</td>
      <td>65</td>
      <td>130</td>
      <td>165</td>
      <td>125</td>
      <td>40</td>
      <td>80</td>
      <td>...</td>
      <td>55</td>
      <td>848</td>
      <td>40</td>
      <td>866</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>18</td>
      <td>0</td>
    </tr>
    <tr>
      <th>16771</th>
      <td>400</td>
      <td>340</td>
      <td>370</td>
      <td>225</td>
      <td>60</td>
      <td>125</td>
      <td>155</td>
      <td>105</td>
      <td>25</td>
      <td>125</td>
      <td>...</td>
      <td>30</td>
      <td>787</td>
      <td>25</td>
      <td>790</td>
      <td>5</td>
      <td>5</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>16772 rows × 30 columns</p>
</div>




```python
import numpy as np

X_reg.replace(".", np.nan, inplace=True)
```

    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_4272/2603550806.py:3: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      X_reg.replace(".", np.nan, inplace=True)



```python
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer

X_reg = SimpleImputer(strategy='mean').fit_transform(X_reg)
X_reg = StandardScaler().fit_transform(X_reg) 
```

## Split the Data into training and testing data

After selecting the features and the label, we can split the data into training and testing data. We will use 80% of the data for training and 20% for testing. We can use the `train_test_split` function from the `sklearn` library to split the data.


```python
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_reg, y_reg_true, random_state=42)
X_train.shape, X_test.shape, y_train.shape, y_test.shape
```




    ((12579, 30), (4193, 30), (12579, 1), (4193, 1))



## Train a Baseline Model


```python
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import root_mean_squared_error

rf_reg_model = RandomForestRegressor(n_estimators=300, random_state=42)

rf_reg_model.fit(X_train, y_train.values.ravel())

y_reg_pred = rf_reg_model.predict(X_test)
print(root_mean_squared_error(y_test, y_reg_pred))
```

    126133.94261047641



```python
#end solution
```
