# Markov Chain

This notebook describes the markov chain.
A Markov chain is a simplistic text generator basing itself on a distribution of words in language.

First we load data to be used as input for our generator.
After that we setup the code for the generator.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
from collections import defaultdict, Counter
from typing import Iterable
import random

MarkovChain = dict[str, Counter]


def markov_chain(text: Iterable[str], start_token:str='< START >') -> MarkovChain:
    """
    Creates a Markov Chain from a sequence of strings.
    :param text: An iterable of string
    :param start_token: Token with unique contents, easy to filter.
    :return:
    """
    # we use a counter dictionary to make the code more readable
    # the defaultdict is a type of dictionary (therefore the interface is valid)
    chain = defaultdict(Counter)
    # we first ensure that a value is supplied as the current word.
    current_token = start_token
    # for each following token in the given text
    for next_token in text:
        # Add the next word to the chain
        chain[current_token].update([next_token])
        # from the current perspective we can now set the current token to the next
        current_token = next_token

    # It is possible for the start token to not be in the data if there is no text. (Empty generator)
    if start_token in chain:
      # the start token was never part of the data and should therefore be removed.
      del chain[start_token]
    else:
      # In case of an empty generator raise an error that the chain could not be built
      raise ValueError("Variable text empty!")
    # return the chain
    return chain


def merge_chains(chain_a: MarkovChain, chain_b: MarkovChain) -> MarkovChain:
    """
    Can merge multiple chains.
    Be aware, this function is expensive to execute for large chains and ought to be avoided.
    :param chain_a:
    :param chain_b:
    :return: New MarkovChain from the previous ones
    """
    return {k : chain_a.get(k, Counter()) + chain_b.get(k, Counter())
            for k in chain_a.keys() | chain_b.keys()}


def generate_text(chain: MarkovChain, start_word:str, length=100) -> str:
    """
    Generates text based on the supplied markov chain
    :param chain: the chain to use
    :param start_word: word to start the sequence with
    :param length: How many words need to be generated
    :return: text generated from the chain
    """
    # The starting term is the word we use as start
    current_word = start_word
    # if it is not as is in the chain give a default answer
    if start_word not in chain:
        # You can do some fancy search here, but for now we give the universal answer.
        return "42"
    # all generated text is kept in a list of items
    generated_text = []
    # for as long as we need to generate tokens
    for i in range(length):
        # add the current word
        generated_text.append(current_word)
        # predict the next word based on the list of possibilities from the chain
        next_word = random.choices(list(chain[current_word].keys()),
                                   weights=list(chain[current_word].values()))[0]
        # the predicted word becomes the new current word
        current_word = next_word
    # Merge the texts using a
    return ' '.join(generated_text)


def size_of_chain(chain:MarkovChain) -> dict[str,int]:
    """
    This function computes the size of the chain in multiple dimensions.
    Each dimension is given its own string and the size is expressed in the int.
    :param chain: The markov model on which the sizes need to be computed
    :return: dictionary describing the size.
    """
    return dict(
        keys_total = len(chain.keys()),
        relations_total = sum(len(counter) for counter in iter(chain.values())),
        relations_crunched = sum(sum(counter.values())
          for counter in iter(chain.values()))
    )


```

De code hierboven toont hoe een markov chain gemaakt kan worden en gebruikt.
Om een chain te maken moeten we een Iterable[str] aanbieden. Het voordeel hiervan is dat we zeer grote files als input kunnen geven. Ook wordt het mogelijk dat we meerdere bestanden aan elkaar in 1 keer doorgeven. Zo kunnen we sneller een grotere training uitvoeren op meer data.

De code om een chain te maken uit meerdere files is als volgt:


```python
from os import path
from typing import Iterable


def tokens_from_file(file: str) -> Iterable[str]:
    """
    Accepts a file and yields all tokens contained within
    :param file: The filepath of the data
    :return:
    """
    with open(file, 'r', encoding='utf8') as f_in:
        tokens = (token for line in f_in for token in line.split())
        for token in tokens:
            yield token


def files_2_markov(files: Iterable[str]) -> MarkovChain:
    """
    Converts an arbitrary amount of files into a single MarkovChain
    :param files: Files readable as standard utf8 text
    :return: The markov chain of all supplied texts combined
    """
    # ensure we only use existing files
    usable_files = filter(path.isfile, files)
    # define how we generate all tokens for the chain proces
    tokens = (token
              for file in usable_files
              for token in tokens_from_file(file))
    # convert the tokens into a single markov chain
    return markov_chain(tokens)
```

Naast de code zijn er ook enkele data bestanden geleverd (zie folder Data). Hier staan enkele werken afkomstig uit het Gutenberg project (opensource boeken in txt formaat).
Laten we nu een simpele keten trainen op Dracula en eens wat text genereren:


```python
dracula_chain = files_2_markov([r"Dracula.txt"])
generate_text(dracula_chain, start_word="You")
```




    'You have ready to get into the ship, and must be thanked him, and I am encompassed about something, so glad look is not accept so long as I believe that this bad enough at once fell asleep twice and I could see that now had suffered enough for this one man, could have written words I find it should set. Retired worn out; so deal with me, speaking if unselfish. In the time is ground he woke me. Now let us we must go, for a second, and the spiders; so calm and of that when Geordie comes over'




```python
size_of_chain(dracula_chain)
```




    {'keys_total': 18915, 'relations_total': 87183, 'relations_crunched': 164058}





Met deze techniek kun je ook zien hoe waarschijnlijk de vervolgtermen zijn.


```python
dracula_chain['You']
```




    Counter({'cannot': 1,
             'will,': 2,
             'come': 1,
             'shall,': 1,
             'must': 22,
             'are': 21,
             'return': 2,
             'have': 10,
             'and': 3,
             'will': 12,
             'can,': 1,
             'work': 1,
             'long': 1,
             'know': 6,
             'deal': 1,
             'tell': 3,
             'were': 2,
             'can': 9,
             'shall': 8,
             'attend': 1,
             'keep': 1,
             'want': 1,
             'go': 1,
             'forget': 1,
             'knew': 1,
             'may': 6,
             'do': 3,
             'think': 2,
             'had': 4,
             'can’t': 1,
             'others': 1,
             'used': 1,
             'claim': 1,
             'don’t': 2,
             'might': 1,
             'would': 1,
             'follow': 1,
             'shudder;': 1,
             'pay': 1,
             'provide': 1,
             'provide,': 1,
             'comply': 1,
             'agree': 1})



## Opdracht
De generator is nu erg beperkt, we willen er betere zinnen uithalen.
Hiervoor zijn 2 opties:
 - N-Grammen
 - Meer data

Test beide opties, welke geeft het beste resultaat?
Kijk ondertussen ook naar de grootte van de chain, hoe beïnvloed ieder van de opties de grootte van je chain?


```python

```
