# Preprocessing data

Before we can start building machine learning models, we need to preprocess the data. Preprocessing reshapes the data into a format that can be use to train machine learning models. Some preprocessing steps are generic across different types of machine learning models, but some steps are specific for certain types of machine learning models. We will go over the most common preprocessing steps in this notebook.

We start by loading the data:


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

house_prices_df = pd.read_csv(os.path.join(data_directory, "house-prices/train.csv"), sep= ",", index_col = "Id")
```

As said before, the house prices dataset is part of a Kaggle competition. In this competition, the goal is to train a machine learning model using the training set in `train.csv` and then using that model to predict the price of the houses in the test dataset `test.csv`. The team that makes the best predictions for the test dataset is the winner of the competition. In the code above, we load both the training and test dataset.

We can inspect the first few rows of the training dataframe using the `head()` function.


```python
house_prices_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>MSZoning</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>Street</th>
      <th>Alley</th>
      <th>LotShape</th>
      <th>LandContour</th>
      <th>Utilities</th>
      <th>LotConfig</th>
      <th>...</th>
      <th>PoolArea</th>
      <th>PoolQC</th>
      <th>Fence</th>
      <th>MiscFeature</th>
      <th>MiscVal</th>
      <th>MoSold</th>
      <th>YrSold</th>
      <th>SaleType</th>
      <th>SaleCondition</th>
      <th>SalePrice</th>
    </tr>
    <tr>
      <th>Id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>60</td>
      <td>RL</td>
      <td>65.0</td>
      <td>8450</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>2</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>208500</td>
    </tr>
    <tr>
      <th>2</th>
      <td>20</td>
      <td>RL</td>
      <td>80.0</td>
      <td>9600</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>5</td>
      <td>2007</td>
      <td>WD</td>
      <td>Normal</td>
      <td>181500</td>
    </tr>
    <tr>
      <th>3</th>
      <td>60</td>
      <td>RL</td>
      <td>68.0</td>
      <td>11250</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>9</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>223500</td>
    </tr>
    <tr>
      <th>4</th>
      <td>70</td>
      <td>RL</td>
      <td>60.0</td>
      <td>9550</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Corner</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>2</td>
      <td>2006</td>
      <td>WD</td>
      <td>Abnorml</td>
      <td>140000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>60</td>
      <td>RL</td>
      <td>84.0</td>
      <td>14260</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>12</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>250000</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 80 columns</p>
</div>



## Missing values

In the previous notebook, we saw that the dataset contains missing values. We repeated the code below to show the number of missing values per column.


```python
columns_indices_missing_values = house_prices_df.isna().any()
columns_with_missing_values = house_prices_df.columns[columns_indices_missing_values].tolist()
columns_with_missing_values

number_of_missing_values = { column_name: house_prices_df[column_name].isna().sum() for column_name in columns_with_missing_values }
number_of_missing_values

import seaborn as sns
import matplotlib.pyplot as plt
plt.figure(figsize=(12, 6))
sns.barplot(x=list(number_of_missing_values.keys()), y=list(number_of_missing_values.values()))
plt.xticks(rotation=90)
plt.tight_layout()

```


    
![png](02a%20-%20Preprocessing%20Data%20for%20Regression_files/02a%20-%20Preprocessing%20Data%20for%20Regression_8_0.png)
    


The barplot above shows that the columns with the most missing values are the 'Alley', 'MasVnrType', 'FireplaceQu', 'PoolQC', 'Fence', and 'MiscFeature' columns. Missing values can be a problem for machine learning models, so we need to deal with them. There are several ways to deal with missing values, such as:

- Removing the rows with missing values
- Removing the columns with missing values
- Imputing the missing values
- Using a machine learning model that can handle missing values

In this notebook, we will use the second option and remove the columns with missing values. We will remove the columns with more than 50% missing values. We will use the `dropna()` function to remove the columns with missing values.


```python
house = house_prices_df.drop(['Alley','PoolQC','Fence','MiscFeature'],axis=1)
house.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>MSZoning</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>Street</th>
      <th>LotShape</th>
      <th>LandContour</th>
      <th>Utilities</th>
      <th>LotConfig</th>
      <th>LandSlope</th>
      <th>...</th>
      <th>EnclosedPorch</th>
      <th>3SsnPorch</th>
      <th>ScreenPorch</th>
      <th>PoolArea</th>
      <th>MiscVal</th>
      <th>MoSold</th>
      <th>YrSold</th>
      <th>SaleType</th>
      <th>SaleCondition</th>
      <th>SalePrice</th>
    </tr>
    <tr>
      <th>Id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>60</td>
      <td>RL</td>
      <td>65.0</td>
      <td>8450</td>
      <td>Pave</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>Gtl</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>208500</td>
    </tr>
    <tr>
      <th>2</th>
      <td>20</td>
      <td>RL</td>
      <td>80.0</td>
      <td>9600</td>
      <td>Pave</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>Gtl</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>5</td>
      <td>2007</td>
      <td>WD</td>
      <td>Normal</td>
      <td>181500</td>
    </tr>
    <tr>
      <th>3</th>
      <td>60</td>
      <td>RL</td>
      <td>68.0</td>
      <td>11250</td>
      <td>Pave</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>Gtl</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>9</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>223500</td>
    </tr>
    <tr>
      <th>4</th>
      <td>70</td>
      <td>RL</td>
      <td>60.0</td>
      <td>9550</td>
      <td>Pave</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Corner</td>
      <td>Gtl</td>
      <td>...</td>
      <td>272</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>2006</td>
      <td>WD</td>
      <td>Abnorml</td>
      <td>140000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>60</td>
      <td>RL</td>
      <td>84.0</td>
      <td>14260</td>
      <td>Pave</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>Gtl</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>12</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>250000</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 76 columns</p>
</div>




```python
columns_indices_missing_values = house.isna().any()
columns_with_missing_values = house.columns[columns_indices_missing_values].tolist()
columns_with_missing_values
```




    ['LotFrontage',
     'MasVnrType',
     'MasVnrArea',
     'BsmtQual',
     'BsmtCond',
     'BsmtExposure',
     'BsmtFinType1',
     'BsmtFinType2',
     'Electrical',
     'FireplaceQu',
     'GarageType',
     'GarageYrBlt',
     'GarageFinish',
     'GarageQual',
     'GarageCond']



# Retrieving the column names by type of variable

The columns in the dataset can be divided into different types of variables, such as numerical variables, variables which have a numerical, often continuous value,  and categorical variables, variables with a limited number of values that represent a category. In the house prices dataset, numerical variables have a numerical data type, whereas categorical columns have the type `object`, which in this case means they contain string values. The columns often need to be preprocessed in a different way. We will retrieve the column names for each type of variable.


```python
numerical_columns = house.select_dtypes(include=['int64', 'float64']).columns.tolist()
categorical_columns = house.select_dtypes(include=['object']).columns.tolist()
```

### Numerical variables


```python
numerical_columns = list(set(numerical_columns) - set(["SalePrice"]))
numerical_columns
```




    ['YearBuilt',
     'TotalBsmtSF',
     'LotArea',
     'GrLivArea',
     'LotFrontage',
     'LowQualFinSF',
     'MiscVal',
     '3SsnPorch',
     'GarageYrBlt',
     'WoodDeckSF',
     'BsmtFinSF2',
     'OpenPorchSF',
     'EnclosedPorch',
     'YearRemodAdd',
     'BsmtUnfSF',
     'BsmtFullBath',
     'OverallQual',
     'GarageCars',
     'YrSold',
     '2ndFlrSF',
     'MSSubClass',
     'HalfBath',
     'PoolArea',
     'TotRmsAbvGrd',
     'GarageArea',
     'BedroomAbvGr',
     '1stFlrSF',
     'MoSold',
     'Fireplaces',
     'KitchenAbvGr',
     'OverallCond',
     'ScreenPorch',
     'BsmtHalfBath',
     'FullBath',
     'BsmtFinSF1',
     'MasVnrArea']



### Categorical variables


```python
categorical_columns
```




    ['MSZoning',
     'Street',
     'LotShape',
     'LandContour',
     'Utilities',
     'LotConfig',
     'LandSlope',
     'Neighborhood',
     'Condition1',
     'Condition2',
     'BldgType',
     'HouseStyle',
     'RoofStyle',
     'RoofMatl',
     'Exterior1st',
     'Exterior2nd',
     'MasVnrType',
     'ExterQual',
     'ExterCond',
     'Foundation',
     'BsmtQual',
     'BsmtCond',
     'BsmtExposure',
     'BsmtFinType1',
     'BsmtFinType2',
     'Heating',
     'HeatingQC',
     'CentralAir',
     'Electrical',
     'KitchenQual',
     'Functional',
     'FireplaceQu',
     'GarageType',
     'GarageFinish',
     'GarageQual',
     'GarageCond',
     'PavedDrive',
     'SaleType',
     'SaleCondition']



## Scaling values

For some machine learning models, it is important to scale the values of the numerical variables. Scaling the values of the numerical variables can help the machine learning model to converge faster and can improve the performance of the model. There are several ways to scale the values of the numerical variables, such as:

- Min-max scaling, which scales the values to a fixed range, usually between 0 and 1
- Standard scaling, which scales the values so that they have a mean of 0 and a standard deviation of 1
- Robust scaling, which scales the values so that they are robust to outliers

In this notebook, we will use the `StandardScaler` from the `sklearn.preprocessing` module to scale the values of the numerical variables. 


```python
from sklearn.preprocessing import StandardScaler

house[numerical_columns] = StandardScaler().fit_transform(house[numerical_columns])
```

## Impute missing values

We have seen that several columns have missing values:


```python
numerical_columns_with_missing_values = list(set(columns_with_missing_values).intersection(set(numerical_columns)))
numerical_columns_with_missing_values
```




    ['LotFrontage', 'GarageYrBlt', 'MasVnrArea']



For the machine learning model to work, we need to impute the missing values. There are several ways to impute missing values, such as, imputing the missing values with the mean, median, or mode of the column.  Here we will use the `SimpleImputer` from the `sklearn.impute` module to impute the missing values with the median of the column.  


```python
from sklearn.impute import SimpleImputer

house[numerical_columns_with_missing_values] = SimpleImputer(strategy='median').fit_transform(house[numerical_columns_with_missing_values])
house[numerical_columns_with_missing_values].isnull().sum().to_frame()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>LotFrontage</th>
      <td>0</td>
    </tr>
    <tr>
      <th>GarageYrBlt</th>
      <td>0</td>
    </tr>
    <tr>
      <th>MasVnrArea</th>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>



We can see that none of the numerical columns in the table above have missing values.

## Encoding categorical variables

Machine learning models can only work with numerical data, so we need to encode the categorical variables. There are several ways to encode categorical variables, such as:

- Label encoding, which assigns a unique integer to each category
- One-hot encoding, which creates a binary column for each category, a categorical column with `n` categories will be transformed into `n` binary columns

In this notebook, we will use one-hot encoding to encode the categorical variables. We will use the `OneHotEncoder` from the `sklearn.preprocessing` module to encode the categorical variables.


```python
from sklearn.preprocessing import OneHotEncoder

encoder = OneHotEncoder(sparse_output=False, handle_unknown='ignore')
encoder.fit(house[categorical_columns])
encoder
```




<style>#sk-container-id-2 {
  /* Definition of color scheme common for light and dark mode */
  --sklearn-color-text: black;
  --sklearn-color-line: gray;
  /* Definition of color scheme for unfitted estimators */
  --sklearn-color-unfitted-level-0: #fff5e6;
  --sklearn-color-unfitted-level-1: #f6e4d2;
  --sklearn-color-unfitted-level-2: #ffe0b3;
  --sklearn-color-unfitted-level-3: chocolate;
  /* Definition of color scheme for fitted estimators */
  --sklearn-color-fitted-level-0: #f0f8ff;
  --sklearn-color-fitted-level-1: #d4ebff;
  --sklearn-color-fitted-level-2: #b3dbfd;
  --sklearn-color-fitted-level-3: cornflowerblue;

  /* Specific color for light theme */
  --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, white)));
  --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-icon: #696969;

  @media (prefers-color-scheme: dark) {
    /* Redefinition of color scheme for dark theme */
    --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, #111)));
    --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-icon: #878787;
  }
}

#sk-container-id-2 {
  color: var(--sklearn-color-text);
}

#sk-container-id-2 pre {
  padding: 0;
}

#sk-container-id-2 input.sk-hidden--visually {
  border: 0;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}

#sk-container-id-2 div.sk-dashed-wrapped {
  border: 1px dashed var(--sklearn-color-line);
  margin: 0 0.4em 0.5em 0.4em;
  box-sizing: border-box;
  padding-bottom: 0.4em;
  background-color: var(--sklearn-color-background);
}

#sk-container-id-2 div.sk-container {
  /* jupyter's `normalize.less` sets `[hidden] { display: none; }`
     but bootstrap.min.css set `[hidden] { display: none !important; }`
     so we also need the `!important` here to be able to override the
     default hidden behavior on the sphinx rendered scikit-learn.org.
     See: https://github.com/scikit-learn/scikit-learn/issues/21755 */
  display: inline-block !important;
  position: relative;
}

#sk-container-id-2 div.sk-text-repr-fallback {
  display: none;
}

div.sk-parallel-item,
div.sk-serial,
div.sk-item {
  /* draw centered vertical line to link estimators */
  background-image: linear-gradient(var(--sklearn-color-text-on-default-background), var(--sklearn-color-text-on-default-background));
  background-size: 2px 100%;
  background-repeat: no-repeat;
  background-position: center center;
}

/* Parallel-specific style estimator block */

#sk-container-id-2 div.sk-parallel-item::after {
  content: "";
  width: 100%;
  border-bottom: 2px solid var(--sklearn-color-text-on-default-background);
  flex-grow: 1;
}

#sk-container-id-2 div.sk-parallel {
  display: flex;
  align-items: stretch;
  justify-content: center;
  background-color: var(--sklearn-color-background);
  position: relative;
}

#sk-container-id-2 div.sk-parallel-item {
  display: flex;
  flex-direction: column;
}

#sk-container-id-2 div.sk-parallel-item:first-child::after {
  align-self: flex-end;
  width: 50%;
}

#sk-container-id-2 div.sk-parallel-item:last-child::after {
  align-self: flex-start;
  width: 50%;
}

#sk-container-id-2 div.sk-parallel-item:only-child::after {
  width: 0;
}

/* Serial-specific style estimator block */

#sk-container-id-2 div.sk-serial {
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--sklearn-color-background);
  padding-right: 1em;
  padding-left: 1em;
}


/* Toggleable style: style used for estimator/Pipeline/ColumnTransformer box that is
clickable and can be expanded/collapsed.
- Pipeline and ColumnTransformer use this feature and define the default style
- Estimators will overwrite some part of the style using the `sk-estimator` class
*/

/* Pipeline and ColumnTransformer style (default) */

#sk-container-id-2 div.sk-toggleable {
  /* Default theme specific background. It is overwritten whether we have a
  specific estimator or a Pipeline/ColumnTransformer */
  background-color: var(--sklearn-color-background);
}

/* Toggleable label */
#sk-container-id-2 label.sk-toggleable__label {
  cursor: pointer;
  display: block;
  width: 100%;
  margin-bottom: 0;
  padding: 0.5em;
  box-sizing: border-box;
  text-align: center;
}

#sk-container-id-2 label.sk-toggleable__label-arrow:before {
  /* Arrow on the left of the label */
  content: "▸";
  float: left;
  margin-right: 0.25em;
  color: var(--sklearn-color-icon);
}

#sk-container-id-2 label.sk-toggleable__label-arrow:hover:before {
  color: var(--sklearn-color-text);
}

/* Toggleable content - dropdown */

#sk-container-id-2 div.sk-toggleable__content {
  max-height: 0;
  max-width: 0;
  overflow: hidden;
  text-align: left;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-2 div.sk-toggleable__content.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-2 div.sk-toggleable__content pre {
  margin: 0.2em;
  border-radius: 0.25em;
  color: var(--sklearn-color-text);
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-2 div.sk-toggleable__content.fitted pre {
  /* unfitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-2 input.sk-toggleable__control:checked~div.sk-toggleable__content {
  /* Expand drop-down */
  max-height: 200px;
  max-width: 100%;
  overflow: auto;
}

#sk-container-id-2 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {
  content: "▾";
}

/* Pipeline/ColumnTransformer-specific style */

#sk-container-id-2 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-2 div.sk-label.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator-specific style */

/* Colorize estimator box */
#sk-container-id-2 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-2 div.sk-estimator.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

#sk-container-id-2 div.sk-label label.sk-toggleable__label,
#sk-container-id-2 div.sk-label label {
  /* The background is the default theme color */
  color: var(--sklearn-color-text-on-default-background);
}

/* On hover, darken the color of the background */
#sk-container-id-2 div.sk-label:hover label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

/* Label box, darken color on hover, fitted */
#sk-container-id-2 div.sk-label.fitted:hover label.sk-toggleable__label.fitted {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator label */

#sk-container-id-2 div.sk-label label {
  font-family: monospace;
  font-weight: bold;
  display: inline-block;
  line-height: 1.2em;
}

#sk-container-id-2 div.sk-label-container {
  text-align: center;
}

/* Estimator-specific */
#sk-container-id-2 div.sk-estimator {
  font-family: monospace;
  border: 1px dotted var(--sklearn-color-border-box);
  border-radius: 0.25em;
  box-sizing: border-box;
  margin-bottom: 0.5em;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-2 div.sk-estimator.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

/* on hover */
#sk-container-id-2 div.sk-estimator:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-2 div.sk-estimator.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Specification for estimator info (e.g. "i" and "?") */

/* Common style for "i" and "?" */

.sk-estimator-doc-link,
a:link.sk-estimator-doc-link,
a:visited.sk-estimator-doc-link {
  float: right;
  font-size: smaller;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1em;
  height: 1em;
  width: 1em;
  text-decoration: none !important;
  margin-left: 1ex;
  /* unfitted */
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
  color: var(--sklearn-color-unfitted-level-1);
}

.sk-estimator-doc-link.fitted,
a:link.sk-estimator-doc-link.fitted,
a:visited.sk-estimator-doc-link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
div.sk-estimator:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover,
div.sk-label-container:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

div.sk-estimator.fitted:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover,
div.sk-label-container:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

/* Span, style for the box shown on hovering the info icon */
.sk-estimator-doc-link span {
  display: none;
  z-index: 9999;
  position: relative;
  font-weight: normal;
  right: .2ex;
  padding: .5ex;
  margin: .5ex;
  width: min-content;
  min-width: 20ex;
  max-width: 50ex;
  color: var(--sklearn-color-text);
  box-shadow: 2pt 2pt 4pt #999;
  /* unfitted */
  background: var(--sklearn-color-unfitted-level-0);
  border: .5pt solid var(--sklearn-color-unfitted-level-3);
}

.sk-estimator-doc-link.fitted span {
  /* fitted */
  background: var(--sklearn-color-fitted-level-0);
  border: var(--sklearn-color-fitted-level-3);
}

.sk-estimator-doc-link:hover span {
  display: block;
}

/* "?"-specific style due to the `<a>` HTML tag */

#sk-container-id-2 a.estimator_doc_link {
  float: right;
  font-size: 1rem;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1rem;
  height: 1rem;
  width: 1rem;
  text-decoration: none;
  /* unfitted */
  color: var(--sklearn-color-unfitted-level-1);
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
}

#sk-container-id-2 a.estimator_doc_link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
#sk-container-id-2 a.estimator_doc_link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

#sk-container-id-2 a.estimator_doc_link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
}
</style><div id="sk-container-id-2" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>OneHotEncoder(handle_unknown=&#x27;ignore&#x27;, sparse_output=False)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator fitted sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-2" type="checkbox" checked><label for="sk-estimator-id-2" class="sk-toggleable__label fitted sk-toggleable__label-arrow fitted">&nbsp;&nbsp;OneHotEncoder<a class="sk-estimator-doc-link fitted" rel="noreferrer" target="_blank" href="https://scikit-learn.org/1.5/modules/generated/sklearn.preprocessing.OneHotEncoder.html">?<span>Documentation for OneHotEncoder</span></a><span class="sk-estimator-doc-link fitted">i<span>Fitted</span></span></label><div class="sk-toggleable__content fitted"><pre>OneHotEncoder(handle_unknown=&#x27;ignore&#x27;, sparse_output=False)</pre></div> </div></div></div></div>



The OneHotEncoder from the `sklearn.preprocessing` module creates new columns for each unique value in a categorical column. The new columns contain binary values, where 1 indicates that the value is present and 0 indicates that the value is not present. Each column gets a new column name that consists of the original column name and the unique value. We retrieve these new column names here:


```python
encoded_categorical_columns = list(encoder.get_feature_names_out(categorical_columns))
encoded_categorical_columns
```




    ['MSZoning_C (all)',
     'MSZoning_FV',
     'MSZoning_RH',
     'MSZoning_RL',
     'MSZoning_RM',
     'Street_Grvl',
     'Street_Pave',
     'LotShape_IR1',
     'LotShape_IR2',
     'LotShape_IR3',
     'LotShape_Reg',
     'LandContour_Bnk',
     'LandContour_HLS',
     'LandContour_Low',
     'LandContour_Lvl',
     'Utilities_AllPub',
     'Utilities_NoSeWa',
     'LotConfig_Corner',
     'LotConfig_CulDSac',
     'LotConfig_FR2',
     'LotConfig_FR3',
     'LotConfig_Inside',
     'LandSlope_Gtl',
     'LandSlope_Mod',
     'LandSlope_Sev',
     'Neighborhood_Blmngtn',
     'Neighborhood_Blueste',
     'Neighborhood_BrDale',
     'Neighborhood_BrkSide',
     'Neighborhood_ClearCr',
     'Neighborhood_CollgCr',
     'Neighborhood_Crawfor',
     'Neighborhood_Edwards',
     'Neighborhood_Gilbert',
     'Neighborhood_IDOTRR',
     'Neighborhood_MeadowV',
     'Neighborhood_Mitchel',
     'Neighborhood_NAmes',
     'Neighborhood_NPkVill',
     'Neighborhood_NWAmes',
     'Neighborhood_NoRidge',
     'Neighborhood_NridgHt',
     'Neighborhood_OldTown',
     'Neighborhood_SWISU',
     'Neighborhood_Sawyer',
     'Neighborhood_SawyerW',
     'Neighborhood_Somerst',
     'Neighborhood_StoneBr',
     'Neighborhood_Timber',
     'Neighborhood_Veenker',
     'Condition1_Artery',
     'Condition1_Feedr',
     'Condition1_Norm',
     'Condition1_PosA',
     'Condition1_PosN',
     'Condition1_RRAe',
     'Condition1_RRAn',
     'Condition1_RRNe',
     'Condition1_RRNn',
     'Condition2_Artery',
     'Condition2_Feedr',
     'Condition2_Norm',
     'Condition2_PosA',
     'Condition2_PosN',
     'Condition2_RRAe',
     'Condition2_RRAn',
     'Condition2_RRNn',
     'BldgType_1Fam',
     'BldgType_2fmCon',
     'BldgType_Duplex',
     'BldgType_Twnhs',
     'BldgType_TwnhsE',
     'HouseStyle_1.5Fin',
     'HouseStyle_1.5Unf',
     'HouseStyle_1Story',
     'HouseStyle_2.5Fin',
     'HouseStyle_2.5Unf',
     'HouseStyle_2Story',
     'HouseStyle_SFoyer',
     'HouseStyle_SLvl',
     'RoofStyle_Flat',
     'RoofStyle_Gable',
     'RoofStyle_Gambrel',
     'RoofStyle_Hip',
     'RoofStyle_Mansard',
     'RoofStyle_Shed',
     'RoofMatl_ClyTile',
     'RoofMatl_CompShg',
     'RoofMatl_Membran',
     'RoofMatl_Metal',
     'RoofMatl_Roll',
     'RoofMatl_Tar&Grv',
     'RoofMatl_WdShake',
     'RoofMatl_WdShngl',
     'Exterior1st_AsbShng',
     'Exterior1st_AsphShn',
     'Exterior1st_BrkComm',
     'Exterior1st_BrkFace',
     'Exterior1st_CBlock',
     'Exterior1st_CemntBd',
     'Exterior1st_HdBoard',
     'Exterior1st_ImStucc',
     'Exterior1st_MetalSd',
     'Exterior1st_Plywood',
     'Exterior1st_Stone',
     'Exterior1st_Stucco',
     'Exterior1st_VinylSd',
     'Exterior1st_Wd Sdng',
     'Exterior1st_WdShing',
     'Exterior2nd_AsbShng',
     'Exterior2nd_AsphShn',
     'Exterior2nd_Brk Cmn',
     'Exterior2nd_BrkFace',
     'Exterior2nd_CBlock',
     'Exterior2nd_CmentBd',
     'Exterior2nd_HdBoard',
     'Exterior2nd_ImStucc',
     'Exterior2nd_MetalSd',
     'Exterior2nd_Other',
     'Exterior2nd_Plywood',
     'Exterior2nd_Stone',
     'Exterior2nd_Stucco',
     'Exterior2nd_VinylSd',
     'Exterior2nd_Wd Sdng',
     'Exterior2nd_Wd Shng',
     'MasVnrType_BrkCmn',
     'MasVnrType_BrkFace',
     'MasVnrType_Stone',
     'MasVnrType_nan',
     'ExterQual_Ex',
     'ExterQual_Fa',
     'ExterQual_Gd',
     'ExterQual_TA',
     'ExterCond_Ex',
     'ExterCond_Fa',
     'ExterCond_Gd',
     'ExterCond_Po',
     'ExterCond_TA',
     'Foundation_BrkTil',
     'Foundation_CBlock',
     'Foundation_PConc',
     'Foundation_Slab',
     'Foundation_Stone',
     'Foundation_Wood',
     'BsmtQual_Ex',
     'BsmtQual_Fa',
     'BsmtQual_Gd',
     'BsmtQual_TA',
     'BsmtQual_nan',
     'BsmtCond_Fa',
     'BsmtCond_Gd',
     'BsmtCond_Po',
     'BsmtCond_TA',
     'BsmtCond_nan',
     'BsmtExposure_Av',
     'BsmtExposure_Gd',
     'BsmtExposure_Mn',
     'BsmtExposure_No',
     'BsmtExposure_nan',
     'BsmtFinType1_ALQ',
     'BsmtFinType1_BLQ',
     'BsmtFinType1_GLQ',
     'BsmtFinType1_LwQ',
     'BsmtFinType1_Rec',
     'BsmtFinType1_Unf',
     'BsmtFinType1_nan',
     'BsmtFinType2_ALQ',
     'BsmtFinType2_BLQ',
     'BsmtFinType2_GLQ',
     'BsmtFinType2_LwQ',
     'BsmtFinType2_Rec',
     'BsmtFinType2_Unf',
     'BsmtFinType2_nan',
     'Heating_Floor',
     'Heating_GasA',
     'Heating_GasW',
     'Heating_Grav',
     'Heating_OthW',
     'Heating_Wall',
     'HeatingQC_Ex',
     'HeatingQC_Fa',
     'HeatingQC_Gd',
     'HeatingQC_Po',
     'HeatingQC_TA',
     'CentralAir_N',
     'CentralAir_Y',
     'Electrical_FuseA',
     'Electrical_FuseF',
     'Electrical_FuseP',
     'Electrical_Mix',
     'Electrical_SBrkr',
     'Electrical_nan',
     'KitchenQual_Ex',
     'KitchenQual_Fa',
     'KitchenQual_Gd',
     'KitchenQual_TA',
     'Functional_Maj1',
     'Functional_Maj2',
     'Functional_Min1',
     'Functional_Min2',
     'Functional_Mod',
     'Functional_Sev',
     'Functional_Typ',
     'FireplaceQu_Ex',
     'FireplaceQu_Fa',
     'FireplaceQu_Gd',
     'FireplaceQu_Po',
     'FireplaceQu_TA',
     'FireplaceQu_nan',
     'GarageType_2Types',
     'GarageType_Attchd',
     'GarageType_Basment',
     'GarageType_BuiltIn',
     'GarageType_CarPort',
     'GarageType_Detchd',
     'GarageType_nan',
     'GarageFinish_Fin',
     'GarageFinish_RFn',
     'GarageFinish_Unf',
     'GarageFinish_nan',
     'GarageQual_Ex',
     'GarageQual_Fa',
     'GarageQual_Gd',
     'GarageQual_Po',
     'GarageQual_TA',
     'GarageQual_nan',
     'GarageCond_Ex',
     'GarageCond_Fa',
     'GarageCond_Gd',
     'GarageCond_Po',
     'GarageCond_TA',
     'GarageCond_nan',
     'PavedDrive_N',
     'PavedDrive_P',
     'PavedDrive_Y',
     'SaleType_COD',
     'SaleType_CWD',
     'SaleType_Con',
     'SaleType_ConLD',
     'SaleType_ConLI',
     'SaleType_ConLw',
     'SaleType_New',
     'SaleType_Oth',
     'SaleType_WD',
     'SaleCondition_Abnorml',
     'SaleCondition_AdjLand',
     'SaleCondition_Alloca',
     'SaleCondition_Family',
     'SaleCondition_Normal',
     'SaleCondition_Partial']



After encoding the categorical columns, we can add them to the dataframe:


```python
house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
```

    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])
    /var/folders/5s/1qqrrxd175v_vzyy65mh_y0m0000gn/T/ipykernel_13001/1655041720.py:1: PerformanceWarning: DataFrame is highly fragmented.  This is usually the result of calling `frame.insert` many times, which has poor performance.  Consider joining all columns at once using pd.concat(axis=1) instead. To get a de-fragmented frame, use `newframe = frame.copy()`
      house[encoded_categorical_columns] = encoder.transform(house[categorical_columns])


The `OneHotEncoder` encodes the categorical columns and creates a new column for each category in the categorical columns. We have added the new encoded columns to the dataframe and need to remove the original categorical columns. We can do that using the `drop()` function:


```python
house.drop(categorical_columns, axis=1, inplace=True)
house.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>OverallQual</th>
      <th>OverallCond</th>
      <th>YearBuilt</th>
      <th>YearRemodAdd</th>
      <th>MasVnrArea</th>
      <th>BsmtFinSF1</th>
      <th>BsmtFinSF2</th>
      <th>...</th>
      <th>SaleType_ConLw</th>
      <th>SaleType_New</th>
      <th>SaleType_Oth</th>
      <th>SaleType_WD</th>
      <th>SaleCondition_Abnorml</th>
      <th>SaleCondition_AdjLand</th>
      <th>SaleCondition_Alloca</th>
      <th>SaleCondition_Family</th>
      <th>SaleCondition_Normal</th>
      <th>SaleCondition_Partial</th>
    </tr>
    <tr>
      <th>Id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>0.073375</td>
      <td>-0.208034</td>
      <td>-0.207142</td>
      <td>0.651479</td>
      <td>-0.517200</td>
      <td>1.050994</td>
      <td>0.878668</td>
      <td>0.510015</td>
      <td>0.575425</td>
      <td>-0.288653</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.872563</td>
      <td>0.409895</td>
      <td>-0.091886</td>
      <td>-0.071836</td>
      <td>2.179628</td>
      <td>0.156734</td>
      <td>-0.429577</td>
      <td>-0.572835</td>
      <td>1.171992</td>
      <td>-0.288653</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.073375</td>
      <td>-0.084449</td>
      <td>0.073480</td>
      <td>0.651479</td>
      <td>-0.517200</td>
      <td>0.984752</td>
      <td>0.830215</td>
      <td>0.322174</td>
      <td>0.092907</td>
      <td>-0.288653</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.309859</td>
      <td>-0.414011</td>
      <td>-0.096897</td>
      <td>0.651479</td>
      <td>-0.517200</td>
      <td>-1.863632</td>
      <td>-0.720298</td>
      <td>-0.572835</td>
      <td>-0.499274</td>
      <td>-0.288653</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>0.073375</td>
      <td>0.574676</td>
      <td>0.375148</td>
      <td>1.374795</td>
      <td>-0.517200</td>
      <td>0.951632</td>
      <td>0.733308</td>
      <td>1.360826</td>
      <td>0.463568</td>
      <td>-0.288653</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 287 columns</p>
</div>



# Saving the preprocessed data to disk

After carrying out the preprocessing, we will save the preprocessed data to disk into a new file. We will save the preprocessed data to a new file called `train_preprocessed.csv`. We will use the `to_csv()` function to save the preprocessed data to disk as a CSV file.


```python
house.to_csv(os.path.join(data_directory, "house-prices/train_preprocessed.csv"), index = False)
```
