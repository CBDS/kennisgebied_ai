# Regression datasets

https://www.kaggle.com/code/heyrobin/house-price-prediction-beginner-s-notebook
https://www.kaggle.com/competitions/house-prices-advanced-regression-techniques

with visual and text data:

https://github.com/emanhamed/Houses-dataset
https://www.kaggle.com/datasets/ted8080/house-prices-and-images-socal

# Classification datasets

https://www.kaggle.com/competitions/titanic

# Loading and exploring data 

At the beginning of each of your projects, you will start with loading and exploring your data. Sometimes, your data will come in one file, but more often you will have to combine multiple files. 
In addition, often you will have to clean and preprocess your data. We will take about this in more detail in the following notebooks. In this notebook, we will focus on tabular data. 

## Tabular data

Tabular data is data that is structured in a table. It is a collection of rows and columns. Each row represents an observation, and each column represents a variable for that observation. 
In this course, we will use several tabular datasets. One example is a dataset with house prices. Each row represents the data of one house, and each column represents a variable of that house. 
Each column represents a different aspect of the house, such as the number of bedrooms, the number of bathrooms, the size of the house, the location, etc. Another example, is the Titanic dataset. 
Each row represents a passenger on the Titanic, and each column represents a variable of that passenger, such as the passenger's class, the passenger's age, the passenger's sex, etc. 

## Loading data

In Python, tabular data is often loaded using the `pandas` library. `pandas` is a powerful library that provides a so-called DataFrame object for data manipulation and analysis. 
In the next sections, we will learn how to load data from a CSV file into a `pandas` DataFrame and use `pandas` to explore the data. We will start with the house prices dataset. 
The house prices dataset is a dataset from a [Kaggle data science competition](https://www.kaggle.com/competitions/house-prices-advanced-regression-techniques). It contains
the characteristics of a number of houses together with the sale prices of these houses.



```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

house_prices_df = pd.read_csv(os.path.join(data_directory,"house-prices/train.csv"), sep= ",", index_col= "Id")
house_prices_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>MSZoning</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>Street</th>
      <th>Alley</th>
      <th>LotShape</th>
      <th>LandContour</th>
      <th>Utilities</th>
      <th>LotConfig</th>
      <th>...</th>
      <th>PoolArea</th>
      <th>PoolQC</th>
      <th>Fence</th>
      <th>MiscFeature</th>
      <th>MiscVal</th>
      <th>MoSold</th>
      <th>YrSold</th>
      <th>SaleType</th>
      <th>SaleCondition</th>
      <th>SalePrice</th>
    </tr>
    <tr>
      <th>Id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>60</td>
      <td>RL</td>
      <td>65.0</td>
      <td>8450</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>2</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>208500</td>
    </tr>
    <tr>
      <th>2</th>
      <td>20</td>
      <td>RL</td>
      <td>80.0</td>
      <td>9600</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>5</td>
      <td>2007</td>
      <td>WD</td>
      <td>Normal</td>
      <td>181500</td>
    </tr>
    <tr>
      <th>3</th>
      <td>60</td>
      <td>RL</td>
      <td>68.0</td>
      <td>11250</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>9</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>223500</td>
    </tr>
    <tr>
      <th>4</th>
      <td>70</td>
      <td>RL</td>
      <td>60.0</td>
      <td>9550</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Corner</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>2</td>
      <td>2006</td>
      <td>WD</td>
      <td>Abnorml</td>
      <td>140000</td>
    </tr>
    <tr>
      <th>5</th>
      <td>60</td>
      <td>RL</td>
      <td>84.0</td>
      <td>14260</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>IR1</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>12</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>250000</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 80 columns</p>
</div>



The above code loads the house prices dataset from a CSV file into a `pandas` DataFrame. It specifies the separator between columns (in this case a comma) and sets the 'Id' column as the index column.
The index column is not a feature of the house, but it is a unique identifier for each row. We set it as the index column to make it easier to access the data. `pandas` provides a lot of `read_` functions,
each for different file formats. To see the full list of supported file formats, see the documentation provide [here](https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html).

After we have read the data into a DataFrame, we can start exploring the data. For example, we can print the first five rows of the DataFrame using the `head()` method. We can also print the last five rows of the DataFrame using the `tail()` method.
By providing a number n to the `head()` and `tail()` methods, we can print the first and last n rows of the DataFrame, respectively. For instance:


```python
house_prices_df.tail(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>MSZoning</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>Street</th>
      <th>Alley</th>
      <th>LotShape</th>
      <th>LandContour</th>
      <th>Utilities</th>
      <th>LotConfig</th>
      <th>...</th>
      <th>PoolArea</th>
      <th>PoolQC</th>
      <th>Fence</th>
      <th>MiscFeature</th>
      <th>MiscVal</th>
      <th>MoSold</th>
      <th>YrSold</th>
      <th>SaleType</th>
      <th>SaleCondition</th>
      <th>SalePrice</th>
    </tr>
    <tr>
      <th>Id</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1451</th>
      <td>90</td>
      <td>RL</td>
      <td>60.0</td>
      <td>9000</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>FR2</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>9</td>
      <td>2009</td>
      <td>WD</td>
      <td>Normal</td>
      <td>136000</td>
    </tr>
    <tr>
      <th>1452</th>
      <td>20</td>
      <td>RL</td>
      <td>78.0</td>
      <td>9262</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>5</td>
      <td>2009</td>
      <td>New</td>
      <td>Partial</td>
      <td>287090</td>
    </tr>
    <tr>
      <th>1453</th>
      <td>180</td>
      <td>RM</td>
      <td>35.0</td>
      <td>3675</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>5</td>
      <td>2006</td>
      <td>WD</td>
      <td>Normal</td>
      <td>145000</td>
    </tr>
    <tr>
      <th>1454</th>
      <td>20</td>
      <td>RL</td>
      <td>90.0</td>
      <td>17217</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>7</td>
      <td>2006</td>
      <td>WD</td>
      <td>Abnorml</td>
      <td>84500</td>
    </tr>
    <tr>
      <th>1455</th>
      <td>20</td>
      <td>FV</td>
      <td>62.0</td>
      <td>7500</td>
      <td>Pave</td>
      <td>Pave</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>10</td>
      <td>2009</td>
      <td>WD</td>
      <td>Normal</td>
      <td>185000</td>
    </tr>
    <tr>
      <th>1456</th>
      <td>60</td>
      <td>RL</td>
      <td>62.0</td>
      <td>7917</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>8</td>
      <td>2007</td>
      <td>WD</td>
      <td>Normal</td>
      <td>175000</td>
    </tr>
    <tr>
      <th>1457</th>
      <td>20</td>
      <td>RL</td>
      <td>85.0</td>
      <td>13175</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>MnPrv</td>
      <td>NaN</td>
      <td>0</td>
      <td>2</td>
      <td>2010</td>
      <td>WD</td>
      <td>Normal</td>
      <td>210000</td>
    </tr>
    <tr>
      <th>1458</th>
      <td>70</td>
      <td>RL</td>
      <td>66.0</td>
      <td>9042</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>GdPrv</td>
      <td>Shed</td>
      <td>2500</td>
      <td>5</td>
      <td>2010</td>
      <td>WD</td>
      <td>Normal</td>
      <td>266500</td>
    </tr>
    <tr>
      <th>1459</th>
      <td>20</td>
      <td>RL</td>
      <td>68.0</td>
      <td>9717</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>4</td>
      <td>2010</td>
      <td>WD</td>
      <td>Normal</td>
      <td>142125</td>
    </tr>
    <tr>
      <th>1460</th>
      <td>20</td>
      <td>RL</td>
      <td>75.0</td>
      <td>9937</td>
      <td>Pave</td>
      <td>NaN</td>
      <td>Reg</td>
      <td>Lvl</td>
      <td>AllPub</td>
      <td>Inside</td>
      <td>...</td>
      <td>0</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>0</td>
      <td>6</td>
      <td>2008</td>
      <td>WD</td>
      <td>Normal</td>
      <td>147500</td>
    </tr>
  </tbody>
</table>
<p>10 rows × 80 columns</p>
</div>



This prints the last 10 rows of the DataFrame. 

## Accessing column and row data


To access a column of the DataFrame, we can use the column name as a key to access the column. For instance, to access the 'LotFrontage' column, we can use the following code:


```python
house_prices_df['LotFrontage']
```




    Id
    1       65.0
    2       80.0
    3       68.0
    4       60.0
    5       84.0
            ... 
    1456    62.0
    1457    85.0
    1458    66.0
    1459    68.0
    1460    75.0
    Name: LotFrontage, Length: 1460, dtype: float64



To access a row of the DataFrame, we can use the index of the row. For instance, to access the row with index 5, we can use the following code:


```python
house_prices_df.loc[5]
```




    MSSubClass           60
    MSZoning             RL
    LotFrontage        84.0
    LotArea           14260
    Street             Pave
                      ...  
    MoSold               12
    YrSold             2008
    SaleType             WD
    SaleCondition    Normal
    SalePrice        250000
    Name: 5, Length: 80, dtype: object



This is the last row in the cell above that printed the `head()` of the DataFrame. To access the last row in the DataFrame, we can both use a positive index and a negative index. For instance:


```python
house_prices_df.loc[1460]
```




    MSSubClass           20
    MSZoning             RL
    LotFrontage        75.0
    LotArea            9937
    Street             Pave
                      ...  
    MoSold                6
    YrSold             2008
    SaleType             WD
    SaleCondition    Normal
    SalePrice        147500
    Name: 1460, Length: 80, dtype: object



This prints the last row in the DataFrame. We can also access the last row using a negative index, however we need to use the `iloc` accessor in this case. `iloc` accesses by using a integer index unrelated to the index column.


```python
house_prices_df.iloc[-1]
```




    MSSubClass           20
    MSZoning             RL
    LotFrontage        75.0
    LotArea            9937
    Street             Pave
                      ...  
    MoSold                6
    YrSold             2008
    SaleType             WD
    SaleCondition    Normal
    SalePrice        147500
    Name: 1460, Length: 80, dtype: object



## Exploring the data

Pandas also provides ways to explore the data in the dataframe. We can use the `describe()` method to get a summary of the numericaldata in the DataFrame. This method provides a summary of the central tendency, dispersion and shape of a dataset's distribution, excluding NaN values.


```python
house_prices_df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MSSubClass</th>
      <th>LotFrontage</th>
      <th>LotArea</th>
      <th>OverallQual</th>
      <th>OverallCond</th>
      <th>YearBuilt</th>
      <th>YearRemodAdd</th>
      <th>MasVnrArea</th>
      <th>BsmtFinSF1</th>
      <th>BsmtFinSF2</th>
      <th>...</th>
      <th>WoodDeckSF</th>
      <th>OpenPorchSF</th>
      <th>EnclosedPorch</th>
      <th>3SsnPorch</th>
      <th>ScreenPorch</th>
      <th>PoolArea</th>
      <th>MiscVal</th>
      <th>MoSold</th>
      <th>YrSold</th>
      <th>SalePrice</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>1460.000000</td>
      <td>1201.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1452.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>...</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
      <td>1460.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>56.897260</td>
      <td>70.049958</td>
      <td>10516.828082</td>
      <td>6.099315</td>
      <td>5.575342</td>
      <td>1971.267808</td>
      <td>1984.865753</td>
      <td>103.685262</td>
      <td>443.639726</td>
      <td>46.549315</td>
      <td>...</td>
      <td>94.244521</td>
      <td>46.660274</td>
      <td>21.954110</td>
      <td>3.409589</td>
      <td>15.060959</td>
      <td>2.758904</td>
      <td>43.489041</td>
      <td>6.321918</td>
      <td>2007.815753</td>
      <td>180921.195890</td>
    </tr>
    <tr>
      <th>std</th>
      <td>42.300571</td>
      <td>24.284752</td>
      <td>9981.264932</td>
      <td>1.382997</td>
      <td>1.112799</td>
      <td>30.202904</td>
      <td>20.645407</td>
      <td>181.066207</td>
      <td>456.098091</td>
      <td>161.319273</td>
      <td>...</td>
      <td>125.338794</td>
      <td>66.256028</td>
      <td>61.119149</td>
      <td>29.317331</td>
      <td>55.757415</td>
      <td>40.177307</td>
      <td>496.123024</td>
      <td>2.703626</td>
      <td>1.328095</td>
      <td>79442.502883</td>
    </tr>
    <tr>
      <th>min</th>
      <td>20.000000</td>
      <td>21.000000</td>
      <td>1300.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1872.000000</td>
      <td>1950.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>2006.000000</td>
      <td>34900.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>20.000000</td>
      <td>59.000000</td>
      <td>7553.500000</td>
      <td>5.000000</td>
      <td>5.000000</td>
      <td>1954.000000</td>
      <td>1967.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>2007.000000</td>
      <td>129975.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>50.000000</td>
      <td>69.000000</td>
      <td>9478.500000</td>
      <td>6.000000</td>
      <td>5.000000</td>
      <td>1973.000000</td>
      <td>1994.000000</td>
      <td>0.000000</td>
      <td>383.500000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>0.000000</td>
      <td>25.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>6.000000</td>
      <td>2008.000000</td>
      <td>163000.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>70.000000</td>
      <td>80.000000</td>
      <td>11601.500000</td>
      <td>7.000000</td>
      <td>6.000000</td>
      <td>2000.000000</td>
      <td>2004.000000</td>
      <td>166.000000</td>
      <td>712.250000</td>
      <td>0.000000</td>
      <td>...</td>
      <td>168.000000</td>
      <td>68.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>8.000000</td>
      <td>2009.000000</td>
      <td>214000.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>190.000000</td>
      <td>313.000000</td>
      <td>215245.000000</td>
      <td>10.000000</td>
      <td>9.000000</td>
      <td>2010.000000</td>
      <td>2010.000000</td>
      <td>1600.000000</td>
      <td>5644.000000</td>
      <td>1474.000000</td>
      <td>...</td>
      <td>857.000000</td>
      <td>547.000000</td>
      <td>552.000000</td>
      <td>508.000000</td>
      <td>480.000000</td>
      <td>738.000000</td>
      <td>15500.000000</td>
      <td>12.000000</td>
      <td>2010.000000</td>
      <td>755000.000000</td>
    </tr>
  </tbody>
</table>
<p>8 rows × 37 columns</p>
</div>



Another way to explore the data is by using the `info()` method. This method provides a summary of the data types in the DataFrame.


```python
house_prices_df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    Index: 1460 entries, 1 to 1460
    Data columns (total 80 columns):
     #   Column         Non-Null Count  Dtype  
    ---  ------         --------------  -----  
     0   MSSubClass     1460 non-null   int64  
     1   MSZoning       1460 non-null   object 
     2   LotFrontage    1201 non-null   float64
     3   LotArea        1460 non-null   int64  
     4   Street         1460 non-null   object 
     5   Alley          91 non-null     object 
     6   LotShape       1460 non-null   object 
     7   LandContour    1460 non-null   object 
     8   Utilities      1460 non-null   object 
     9   LotConfig      1460 non-null   object 
     10  LandSlope      1460 non-null   object 
     11  Neighborhood   1460 non-null   object 
     12  Condition1     1460 non-null   object 
     13  Condition2     1460 non-null   object 
     14  BldgType       1460 non-null   object 
     15  HouseStyle     1460 non-null   object 
     16  OverallQual    1460 non-null   int64  
     17  OverallCond    1460 non-null   int64  
     18  YearBuilt      1460 non-null   int64  
     19  YearRemodAdd   1460 non-null   int64  
     20  RoofStyle      1460 non-null   object 
     21  RoofMatl       1460 non-null   object 
     22  Exterior1st    1460 non-null   object 
     23  Exterior2nd    1460 non-null   object 
     24  MasVnrType     588 non-null    object 
     25  MasVnrArea     1452 non-null   float64
     26  ExterQual      1460 non-null   object 
     27  ExterCond      1460 non-null   object 
     28  Foundation     1460 non-null   object 
     29  BsmtQual       1423 non-null   object 
     30  BsmtCond       1423 non-null   object 
     31  BsmtExposure   1422 non-null   object 
     32  BsmtFinType1   1423 non-null   object 
     33  BsmtFinSF1     1460 non-null   int64  
     34  BsmtFinType2   1422 non-null   object 
     35  BsmtFinSF2     1460 non-null   int64  
     36  BsmtUnfSF      1460 non-null   int64  
     37  TotalBsmtSF    1460 non-null   int64  
     38  Heating        1460 non-null   object 
     39  HeatingQC      1460 non-null   object 
     40  CentralAir     1460 non-null   object 
     41  Electrical     1459 non-null   object 
     42  1stFlrSF       1460 non-null   int64  
     43  2ndFlrSF       1460 non-null   int64  
     44  LowQualFinSF   1460 non-null   int64  
     45  GrLivArea      1460 non-null   int64  
     46  BsmtFullBath   1460 non-null   int64  
     47  BsmtHalfBath   1460 non-null   int64  
     48  FullBath       1460 non-null   int64  
     49  HalfBath       1460 non-null   int64  
     50  BedroomAbvGr   1460 non-null   int64  
     51  KitchenAbvGr   1460 non-null   int64  
     52  KitchenQual    1460 non-null   object 
     53  TotRmsAbvGrd   1460 non-null   int64  
     54  Functional     1460 non-null   object 
     55  Fireplaces     1460 non-null   int64  
     56  FireplaceQu    770 non-null    object 
     57  GarageType     1379 non-null   object 
     58  GarageYrBlt    1379 non-null   float64
     59  GarageFinish   1379 non-null   object 
     60  GarageCars     1460 non-null   int64  
     61  GarageArea     1460 non-null   int64  
     62  GarageQual     1379 non-null   object 
     63  GarageCond     1379 non-null   object 
     64  PavedDrive     1460 non-null   object 
     65  WoodDeckSF     1460 non-null   int64  
     66  OpenPorchSF    1460 non-null   int64  
     67  EnclosedPorch  1460 non-null   int64  
     68  3SsnPorch      1460 non-null   int64  
     69  ScreenPorch    1460 non-null   int64  
     70  PoolArea       1460 non-null   int64  
     71  PoolQC         7 non-null      object 
     72  Fence          281 non-null    object 
     73  MiscFeature    54 non-null     object 
     74  MiscVal        1460 non-null   int64  
     75  MoSold         1460 non-null   int64  
     76  YrSold         1460 non-null   int64  
     77  SaleType       1460 non-null   object 
     78  SaleCondition  1460 non-null   object 
     79  SalePrice      1460 non-null   int64  
    dtypes: float64(3), int64(34), object(43)
    memory usage: 956.2+ KB


The `info()` method above shows that the columns in the dataframe are of different data types. For example, the 'MSSubClass' column is of type `int64`, the 'Street' column is of type `object` (which is `pandas` way of saying that the column is of type string), and the 'SalePrice' column is of type `int64`. We also see how many non-null values there are in each column. This shows that the `Alley` column only has 91 non-null values. Since the dataset has 1460 rows, this means that a lot of the rows have no value. To see whether there are more missing values in the dataset, we can use the following code:


```python
columns_indices_missing_values = house_prices_df.isna().any()
columns_with_missing_values = house_prices_df.columns[columns_indices_missing_values].tolist()
columns_with_missing_values
```




    ['LotFrontage',
     'Alley',
     'MasVnrType',
     'MasVnrArea',
     'BsmtQual',
     'BsmtCond',
     'BsmtExposure',
     'BsmtFinType1',
     'BsmtFinType2',
     'Electrical',
     'FireplaceQu',
     'GarageType',
     'GarageYrBlt',
     'GarageFinish',
     'GarageQual',
     'GarageCond',
     'PoolQC',
     'Fence',
     'MiscFeature']



To show how many missing values there are in a specific column, we can use the following code:


```python
number_of_missing_values = { column_name: house_prices_df[column_name].isna().sum() for column_name in columns_with_missing_values }
number_of_missing_values

```




    {'LotFrontage': 259,
     'Alley': 1369,
     'MasVnrType': 872,
     'MasVnrArea': 8,
     'BsmtQual': 37,
     'BsmtCond': 37,
     'BsmtExposure': 38,
     'BsmtFinType1': 37,
     'BsmtFinType2': 38,
     'Electrical': 1,
     'FireplaceQu': 690,
     'GarageType': 81,
     'GarageYrBlt': 81,
     'GarageFinish': 81,
     'GarageQual': 81,
     'GarageCond': 81,
     'PoolQC': 1453,
     'Fence': 1179,
     'MiscFeature': 1406}



We can also show the missing values as a barplot:


```python
import seaborn as sns
import matplotlib.pyplot as plt
plt.figure(figsize=(12, 6))
sns.barplot(x=list(number_of_missing_values.keys()), y=list(number_of_missing_values.values()))
plt.xticks(rotation=90)
plt.tight_layout()
```


    
![png](01%20-%20Load%20and%20Explore%20Data_files/01%20-%20Load%20and%20Explore%20Data_24_0.png)
    


The barplot above shows that the columns with the most missing values are the 'Alley', 'MasVnrType', 'FireplaceQu', 'PoolQC', 'Fence', and 'MiscFeature' columns. We will have to deal with these missing values later in the course. 

## Column types

As we have seen above, the columns in the dataframe have different data types. Some column contain numerical features, such as the 'LotFrontage' column or the 'LotArea' column. Other columns contain categorical features, such as the 'Street' column or the 'Alley' column. Different column types need different types of analysis. For example, numerical columns can be analyzed using summary statistics, while categorical columns can be analyzed using frequency counts. Also, the column types differ in the way how they need to be handled when building machine learning models. Let's separate the column types:


```python
numerical_columns = house_prices_df.select_dtypes(include=['int64', 'float64']).columns.tolist()
numerical_columns
```




    ['MSSubClass',
     'LotFrontage',
     'LotArea',
     'OverallQual',
     'OverallCond',
     'YearBuilt',
     'YearRemodAdd',
     'MasVnrArea',
     'BsmtFinSF1',
     'BsmtFinSF2',
     'BsmtUnfSF',
     'TotalBsmtSF',
     '1stFlrSF',
     '2ndFlrSF',
     'LowQualFinSF',
     'GrLivArea',
     'BsmtFullBath',
     'BsmtHalfBath',
     'FullBath',
     'HalfBath',
     'BedroomAbvGr',
     'KitchenAbvGr',
     'TotRmsAbvGrd',
     'Fireplaces',
     'GarageYrBlt',
     'GarageCars',
     'GarageArea',
     'WoodDeckSF',
     'OpenPorchSF',
     'EnclosedPorch',
     '3SsnPorch',
     'ScreenPorch',
     'PoolArea',
     'MiscVal',
     'MoSold',
     'YrSold',
     'SalePrice']




```python
categorical_columns = house_prices_df.select_dtypes(include=['object']).columns.tolist()
categorical_columns

```




    ['MSZoning',
     'Street',
     'Alley',
     'LotShape',
     'LandContour',
     'Utilities',
     'LotConfig',
     'LandSlope',
     'Neighborhood',
     'Condition1',
     'Condition2',
     'BldgType',
     'HouseStyle',
     'RoofStyle',
     'RoofMatl',
     'Exterior1st',
     'Exterior2nd',
     'MasVnrType',
     'ExterQual',
     'ExterCond',
     'Foundation',
     'BsmtQual',
     'BsmtCond',
     'BsmtExposure',
     'BsmtFinType1',
     'BsmtFinType2',
     'Heating',
     'HeatingQC',
     'CentralAir',
     'Electrical',
     'KitchenQual',
     'Functional',
     'FireplaceQu',
     'GarageType',
     'GarageFinish',
     'GarageQual',
     'GarageCond',
     'PavedDrive',
     'PoolQC',
     'Fence',
     'MiscFeature',
     'SaleType',
     'SaleCondition']



Check whether all columns were accounted for:


```python
len(numerical_columns) + len(categorical_columns) == house_prices_df.shape[1]
```




    True



## Exploring numerical columns

When we look at numerical columns, we are often interested in the distribution of the data. To plot the distribution of one column, we can use a Python package called `seaborn`. Below, we plot the distribution of the 'SalePrice' column.
We both plot a histogram of the data as well as a kernel density estimate (KDE) plot. 


```python


sns.displot(house_prices_df['SalePrice'], bins=100, kde=True)
```




    <seaborn.axisgrid.FacetGrid at 0x16d8f2ad0>




    
![png](01%20-%20Load%20and%20Explore%20Data_files/01%20-%20Load%20and%20Explore%20Data_31_1.png)
    


To plot the distribution of multiple numerical columns, we can use the pandas `hist()` method. This method creates a histogram for each numerical column. We can also set the number of bins, the size, and the font size of the labels.


```python
house_prices_df[numerical_columns].hist(figsize=(15, 15), bins=50, xlabelsize=8, ylabelsize=8);

```


    
![png](01%20-%20Load%20and%20Explore%20Data_files/01%20-%20Load%20and%20Explore%20Data_33_0.png)
    


## Exploring categorical columns

To explore categorical columns, we can use the `value_counts()` method. This method provides a frequency count for each category in the column. For example, to get the frequency count for the 'Street' column, we can use the following code:


```python
house_prices_df['Street'].value_counts()
```




    Street
    Pave    1454
    Grvl       6
    Name: count, dtype: int64




To plot the frequency count for a categorical column, we can use a bar plot. We can create such a bar plot using the `seaborn` package.



```python
sns.barplot(x='Street', y='count', data=house_prices_df['Street'].value_counts().reset_index())
```




    <Axes: xlabel='Street', ylabel='count'>




    
![png](01%20-%20Load%20and%20Explore%20Data_files/01%20-%20Load%20and%20Explore%20Data_37_1.png)
    


# Summary 

In this notebook, we have learned:

- how to load data from a CSV file into a `pandas` DataFrame 
- how to show the first five rows using `head()` and the last five rows using `tail()`
- how to access the column data, using `df['column_name']`, and row data using `loc` and `iloc`
- separate the column types in numerical and categorical columns 
- how to plot the distribution of numerical columns. 

In the next notebook, we will learn how to clean and preprocess the data.
