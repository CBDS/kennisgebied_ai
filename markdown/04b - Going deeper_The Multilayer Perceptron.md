# Going deeper: The Multilayer Perceptron

In the previous notebook, we saw why a single layer perceptron is not enough to solve certain problems. In specific, the XOR problem could not be solved. Generalizing, this means that for some types of problems, simple models like a Perceptron or Logistic Regression, will not be enough. We will need more complex models. In this notebook, we will focus on one of these more complex models: the multilayer perceptron.

A multilayer perceptron was created to address the limitations of the single layer perceptron. The idea is to stack multiple layers of neurons, each layer connected to the next. This way, the model can learn more complex patterns. Multilayer perceptrons form the basis of deep learning, a subfield of machine learning that focuses on models with many layers. Most modern deep learning models, like for example ChatGPT or Google Gemini, originate from ideas developed with multilayer perceptrons. 

In this notebook, you will first train a multilayer perceptron using `scikit-learn` on the Titanic dataset. After training the model, you will optimize its hyperparameters using a `RandomizedSearchCV`.

Let's start again by preparing our data.


```python
import os

try:
    import google.colab
    IN_COLAB = True
except:
    IN_COLAB = False

if IN_COLAB:
    os.environ['DATA_DIRECTORY'] = '/content'
else:
    os.environ['DATA_DIRECTORY'] = '../../data'

data_directory = os.getenv('DATA_DIRECTORY')
```


```bash
%%bash
if [ ! -f "$DATA_DIRECTORY/course-data.zip" ]; then
    wget "https://gitlab.com/api/v4/projects/52375047/jobs/artifacts/master/raw/data/course-data.zip?job=build_data_zip" -O "$DATA_DIRECTORY/course-data.zip"
    unzip "$DATA_DIRECTORY/course-data.zip" -d "$DATA_DIRECTORY"
fi
```


```python
import pandas as pd 

titanic_df = pd.read_csv(os.path.join(data_directory, 'titanic/train_preprocessed.csv'))
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Fare</th>
      <th>Survived</th>
      <th>Pclass_1</th>
      <th>Pclass_2</th>
      <th>Pclass_3</th>
      <th>SibSp_0</th>
      <th>SibSp_1</th>
      <th>SibSp_2</th>
      <th>SibSp_3</th>
      <th>...</th>
      <th>Parch_4</th>
      <th>Parch_5</th>
      <th>Parch_6</th>
      <th>Sex_female</th>
      <th>Sex_male</th>
      <th>Embarked_C</th>
      <th>Embarked_Q</th>
      <th>Embarked_S</th>
      <th>Cabin_False</th>
      <th>Cabin_True</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.565736</td>
      <td>-0.502445</td>
      <td>0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.663861</td>
      <td>0.786845</td>
      <td>1</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.258337</td>
      <td>-0.488854</td>
      <td>1</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.433312</td>
      <td>0.420730</td>
      <td>1</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.433312</td>
      <td>-0.486337</td>
      <td>0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 27 columns</p>
</div>



# Get the Labels 

After the loading the data, we first get the labels of the data. The labels are the target values that we want to predict. In this case, the labels show whether the passenger survived or not. We store the labels in a variable called `y_true`. This variable contains the "ground truth", or the true value, values that we want to predict. By comparing the predicted values of the model to the true values, we can evaluate the performance of the model.


```python
y_true = titanic_df['Survived']
y_true
```




    0      0
    1      1
    2      1
    3      1
    4      0
          ..
    886    0
    887    1
    888    0
    889    1
    890    0
    Name: Survived, Length: 891, dtype: int64



## Remove the Label Column

The second step is to remove the label column from the training data. The label column is the column that we want to predict and therefore cannot be used as a feature. If we do not remove the label column, the model will be able to perfectly predict the label, it could be as easy as returning the label column value. However, in this case, the model will not be able to generalize to new data. 


```python
titanic_df = titanic_df.drop('Survived', axis=1)
titanic_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Age</th>
      <th>Fare</th>
      <th>Pclass_1</th>
      <th>Pclass_2</th>
      <th>Pclass_3</th>
      <th>SibSp_0</th>
      <th>SibSp_1</th>
      <th>SibSp_2</th>
      <th>SibSp_3</th>
      <th>SibSp_4</th>
      <th>...</th>
      <th>Parch_4</th>
      <th>Parch_5</th>
      <th>Parch_6</th>
      <th>Sex_female</th>
      <th>Sex_male</th>
      <th>Embarked_C</th>
      <th>Embarked_Q</th>
      <th>Embarked_S</th>
      <th>Cabin_False</th>
      <th>Cabin_True</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-0.565736</td>
      <td>-0.502445</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.663861</td>
      <td>0.786845</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-0.258337</td>
      <td>-0.488854</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.433312</td>
      <td>0.420730</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.433312</td>
      <td>-0.486337</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>...</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>1.0</td>
      <td>0.0</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 26 columns</p>
</div>



## Split the Data into training and testing data

To evaluate the performance of the model, we split the data into a training set and a testing set. The training set is used to train the model and the testing set is used to evaluate the performance of the model. To split the data, we use the `train_test_split` function from the `sklearn.model_selection` module. We use 80% of the data for training and 20% of the data for testing. We do this by setting the `test_size` parameter to 0.2. We can also set the `random_state` parameter to a fixed value to ensure that the data is split in the same way every time we run the code.


```python
from sklearn.model_selection import train_test_split

train_df, test_df, y_train, y_test = train_test_split(titanic_df, y_true, test_size=0.2, random_state=42)
```


```python
train_df.shape, test_df.shape, y_train.shape, y_test.shape
```




    ((712, 26), (179, 26), (712,), (179,))




```python
#begin solution
```

## Train a Multilayer Perceptron Model

Before we start optimizing the hyperparameters, we first train the model with the default hyperparameters. We use the `LogisticRegression` class from the `sklearn.linear_model` module to create a logistic regression model. We then call the `fit` method on the model to train it on the training data. Finally, we use the `predict` method to make predictions on the test data and evaluate the performance of the model.


```python
from sklearn.neural_network import MLPClassifier

model = MLPClassifier(hidden_layer_sizes= (100, 100, 100), random_state=42, max_iter=1000)
model.fit(train_df, y_train)
```




<style>#sk-container-id-1 {
  /* Definition of color scheme common for light and dark mode */
  --sklearn-color-text: black;
  --sklearn-color-line: gray;
  /* Definition of color scheme for unfitted estimators */
  --sklearn-color-unfitted-level-0: #fff5e6;
  --sklearn-color-unfitted-level-1: #f6e4d2;
  --sklearn-color-unfitted-level-2: #ffe0b3;
  --sklearn-color-unfitted-level-3: chocolate;
  /* Definition of color scheme for fitted estimators */
  --sklearn-color-fitted-level-0: #f0f8ff;
  --sklearn-color-fitted-level-1: #d4ebff;
  --sklearn-color-fitted-level-2: #b3dbfd;
  --sklearn-color-fitted-level-3: cornflowerblue;

  /* Specific color for light theme */
  --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, white)));
  --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, black)));
  --sklearn-color-icon: #696969;

  @media (prefers-color-scheme: dark) {
    /* Redefinition of color scheme for dark theme */
    --sklearn-color-text-on-default-background: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-background: var(--sg-background-color, var(--theme-background, var(--jp-layout-color0, #111)));
    --sklearn-color-border-box: var(--sg-text-color, var(--theme-code-foreground, var(--jp-content-font-color1, white)));
    --sklearn-color-icon: #878787;
  }
}

#sk-container-id-1 {
  color: var(--sklearn-color-text);
}

#sk-container-id-1 pre {
  padding: 0;
}

#sk-container-id-1 input.sk-hidden--visually {
  border: 0;
  clip: rect(1px 1px 1px 1px);
  clip: rect(1px, 1px, 1px, 1px);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  width: 1px;
}

#sk-container-id-1 div.sk-dashed-wrapped {
  border: 1px dashed var(--sklearn-color-line);
  margin: 0 0.4em 0.5em 0.4em;
  box-sizing: border-box;
  padding-bottom: 0.4em;
  background-color: var(--sklearn-color-background);
}

#sk-container-id-1 div.sk-container {
  /* jupyter's `normalize.less` sets `[hidden] { display: none; }`
     but bootstrap.min.css set `[hidden] { display: none !important; }`
     so we also need the `!important` here to be able to override the
     default hidden behavior on the sphinx rendered scikit-learn.org.
     See: https://github.com/scikit-learn/scikit-learn/issues/21755 */
  display: inline-block !important;
  position: relative;
}

#sk-container-id-1 div.sk-text-repr-fallback {
  display: none;
}

div.sk-parallel-item,
div.sk-serial,
div.sk-item {
  /* draw centered vertical line to link estimators */
  background-image: linear-gradient(var(--sklearn-color-text-on-default-background), var(--sklearn-color-text-on-default-background));
  background-size: 2px 100%;
  background-repeat: no-repeat;
  background-position: center center;
}

/* Parallel-specific style estimator block */

#sk-container-id-1 div.sk-parallel-item::after {
  content: "";
  width: 100%;
  border-bottom: 2px solid var(--sklearn-color-text-on-default-background);
  flex-grow: 1;
}

#sk-container-id-1 div.sk-parallel {
  display: flex;
  align-items: stretch;
  justify-content: center;
  background-color: var(--sklearn-color-background);
  position: relative;
}

#sk-container-id-1 div.sk-parallel-item {
  display: flex;
  flex-direction: column;
}

#sk-container-id-1 div.sk-parallel-item:first-child::after {
  align-self: flex-end;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:last-child::after {
  align-self: flex-start;
  width: 50%;
}

#sk-container-id-1 div.sk-parallel-item:only-child::after {
  width: 0;
}

/* Serial-specific style estimator block */

#sk-container-id-1 div.sk-serial {
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: var(--sklearn-color-background);
  padding-right: 1em;
  padding-left: 1em;
}


/* Toggleable style: style used for estimator/Pipeline/ColumnTransformer box that is
clickable and can be expanded/collapsed.
- Pipeline and ColumnTransformer use this feature and define the default style
- Estimators will overwrite some part of the style using the `sk-estimator` class
*/

/* Pipeline and ColumnTransformer style (default) */

#sk-container-id-1 div.sk-toggleable {
  /* Default theme specific background. It is overwritten whether we have a
  specific estimator or a Pipeline/ColumnTransformer */
  background-color: var(--sklearn-color-background);
}

/* Toggleable label */
#sk-container-id-1 label.sk-toggleable__label {
  cursor: pointer;
  display: block;
  width: 100%;
  margin-bottom: 0;
  padding: 0.5em;
  box-sizing: border-box;
  text-align: center;
}

#sk-container-id-1 label.sk-toggleable__label-arrow:before {
  /* Arrow on the left of the label */
  content: "▸";
  float: left;
  margin-right: 0.25em;
  color: var(--sklearn-color-icon);
}

#sk-container-id-1 label.sk-toggleable__label-arrow:hover:before {
  color: var(--sklearn-color-text);
}

/* Toggleable content - dropdown */

#sk-container-id-1 div.sk-toggleable__content {
  max-height: 0;
  max-width: 0;
  overflow: hidden;
  text-align: left;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content pre {
  margin: 0.2em;
  border-radius: 0.25em;
  color: var(--sklearn-color-text);
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-toggleable__content.fitted pre {
  /* unfitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

#sk-container-id-1 input.sk-toggleable__control:checked~div.sk-toggleable__content {
  /* Expand drop-down */
  max-height: 200px;
  max-width: 100%;
  overflow: auto;
}

#sk-container-id-1 input.sk-toggleable__control:checked~label.sk-toggleable__label-arrow:before {
  content: "▾";
}

/* Pipeline/ColumnTransformer-specific style */

#sk-container-id-1 div.sk-label input.sk-toggleable__control:checked~label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-label.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator-specific style */

/* Colorize estimator box */
#sk-container-id-1 div.sk-estimator input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted input.sk-toggleable__control:checked~label.sk-toggleable__label {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

#sk-container-id-1 div.sk-label label.sk-toggleable__label,
#sk-container-id-1 div.sk-label label {
  /* The background is the default theme color */
  color: var(--sklearn-color-text-on-default-background);
}

/* On hover, darken the color of the background */
#sk-container-id-1 div.sk-label:hover label.sk-toggleable__label {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-unfitted-level-2);
}

/* Label box, darken color on hover, fitted */
#sk-container-id-1 div.sk-label.fitted:hover label.sk-toggleable__label.fitted {
  color: var(--sklearn-color-text);
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Estimator label */

#sk-container-id-1 div.sk-label label {
  font-family: monospace;
  font-weight: bold;
  display: inline-block;
  line-height: 1.2em;
}

#sk-container-id-1 div.sk-label-container {
  text-align: center;
}

/* Estimator-specific */
#sk-container-id-1 div.sk-estimator {
  font-family: monospace;
  border: 1px dotted var(--sklearn-color-border-box);
  border-radius: 0.25em;
  box-sizing: border-box;
  margin-bottom: 0.5em;
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-0);
}

#sk-container-id-1 div.sk-estimator.fitted {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-0);
}

/* on hover */
#sk-container-id-1 div.sk-estimator:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-2);
}

#sk-container-id-1 div.sk-estimator.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-2);
}

/* Specification for estimator info (e.g. "i" and "?") */

/* Common style for "i" and "?" */

.sk-estimator-doc-link,
a:link.sk-estimator-doc-link,
a:visited.sk-estimator-doc-link {
  float: right;
  font-size: smaller;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1em;
  height: 1em;
  width: 1em;
  text-decoration: none !important;
  margin-left: 1ex;
  /* unfitted */
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
  color: var(--sklearn-color-unfitted-level-1);
}

.sk-estimator-doc-link.fitted,
a:link.sk-estimator-doc-link.fitted,
a:visited.sk-estimator-doc-link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
div.sk-estimator:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover,
div.sk-label-container:hover .sk-estimator-doc-link:hover,
.sk-estimator-doc-link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

div.sk-estimator.fitted:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover,
div.sk-label-container:hover .sk-estimator-doc-link.fitted:hover,
.sk-estimator-doc-link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

/* Span, style for the box shown on hovering the info icon */
.sk-estimator-doc-link span {
  display: none;
  z-index: 9999;
  position: relative;
  font-weight: normal;
  right: .2ex;
  padding: .5ex;
  margin: .5ex;
  width: min-content;
  min-width: 20ex;
  max-width: 50ex;
  color: var(--sklearn-color-text);
  box-shadow: 2pt 2pt 4pt #999;
  /* unfitted */
  background: var(--sklearn-color-unfitted-level-0);
  border: .5pt solid var(--sklearn-color-unfitted-level-3);
}

.sk-estimator-doc-link.fitted span {
  /* fitted */
  background: var(--sklearn-color-fitted-level-0);
  border: var(--sklearn-color-fitted-level-3);
}

.sk-estimator-doc-link:hover span {
  display: block;
}

/* "?"-specific style due to the `<a>` HTML tag */

#sk-container-id-1 a.estimator_doc_link {
  float: right;
  font-size: 1rem;
  line-height: 1em;
  font-family: monospace;
  background-color: var(--sklearn-color-background);
  border-radius: 1rem;
  height: 1rem;
  width: 1rem;
  text-decoration: none;
  /* unfitted */
  color: var(--sklearn-color-unfitted-level-1);
  border: var(--sklearn-color-unfitted-level-1) 1pt solid;
}

#sk-container-id-1 a.estimator_doc_link.fitted {
  /* fitted */
  border: var(--sklearn-color-fitted-level-1) 1pt solid;
  color: var(--sklearn-color-fitted-level-1);
}

/* On hover */
#sk-container-id-1 a.estimator_doc_link:hover {
  /* unfitted */
  background-color: var(--sklearn-color-unfitted-level-3);
  color: var(--sklearn-color-background);
  text-decoration: none;
}

#sk-container-id-1 a.estimator_doc_link.fitted:hover {
  /* fitted */
  background-color: var(--sklearn-color-fitted-level-3);
}
</style><div id="sk-container-id-1" class="sk-top-container"><div class="sk-text-repr-fallback"><pre>MLPClassifier(hidden_layer_sizes=(100, 100, 100), max_iter=1000,
              random_state=42)</pre><b>In a Jupyter environment, please rerun this cell to show the HTML representation or trust the notebook. <br />On GitHub, the HTML representation is unable to render, please try loading this page with nbviewer.org.</b></div><div class="sk-container" hidden><div class="sk-item"><div class="sk-estimator fitted sk-toggleable"><input class="sk-toggleable__control sk-hidden--visually" id="sk-estimator-id-1" type="checkbox" checked><label for="sk-estimator-id-1" class="sk-toggleable__label fitted sk-toggleable__label-arrow fitted">&nbsp;&nbsp;MLPClassifier<a class="sk-estimator-doc-link fitted" rel="noreferrer" target="_blank" href="https://scikit-learn.org/1.5/modules/generated/sklearn.neural_network.MLPClassifier.html">?<span>Documentation for MLPClassifier</span></a><span class="sk-estimator-doc-link fitted">i<span>Fitted</span></span></label><div class="sk-toggleable__content fitted"><pre>MLPClassifier(hidden_layer_sizes=(100, 100, 100), max_iter=1000,
              random_state=42)</pre></div> </div></div></div></div>



## Evaluate the Model

To evaluate the model, we will use the `classification_report` function from the `sklearn.metrics` module. As we saw, this function calculates various metrics that are useful for evaluating the performance of a classification model. We will use the `classification_report` now to evaluate the initial performance of the model, without having fine-tuned the hyperparameters.


```python
from sklearn.metrics import classification_report

y_pred = model.predict(test_df)

print(classification_report(y_test, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.81      0.90      0.85       105
               1       0.83      0.70      0.76        74
    
        accuracy                           0.82       179
       macro avg       0.82      0.80      0.80       179
    weighted avg       0.82      0.82      0.81       179
    



```python
#end solution
```

## Optimizing results further using RandomSearch

We have seen that a `MLP` with several layers can outperform the `LogisticRegression` model. To see whether we can optimize the results further, now you will use a randomized search to optimize the hyperparameters of the `MLP`. You will use the `RandomizedSearchCV` class from the `sklearn.model_selection` module to perform the randomized search. First, you should define a grid of hyperparameters to search over and then use the `RandomizedSearchCV` class to find the best hyperparameters. The documentation of the `MLPClassifier` can be found [here](https://scikit-learn.org/dev/modules/generated/sklearn.neural_network.MLPClassifier.html). This documentation gives the hyperparameters and possible values that can be used for the search.  Try to define these below. After the hyperparameter search, we can evaluate the `MLP` model with the best hyperparameters. 


```python
#begin solution
```


```python
from sklearn.model_selection import RandomizedSearchCV
import scipy.stats as stats
import warnings

warnings.filterwarnings('ignore')

param_dist = {"hidden_layer_sizes": [(100,), (100, 100), (100, 100, 100)],
              "activation": ["logistic", "tanh", "relu"],
              "alpha": stats.loguniform(0.001, 1),
              "batch_size": stats.randint(1, 100),
              "learning_rate": ["constant", "invscaling", "adaptive"],
              "learning_rate_init": stats.loguniform(0.001, 1), 
              "max_iter": stats.randint(100, 1000)
              }

random_search_model = MLPClassifier(random_state=42)
random_search = RandomizedSearchCV(random_search_model,
                                   param_distributions=param_dist, 
                                   cv=5, 
                                   n_iter=20,
                                   random_state=42)


random_search.fit(train_df, y_train)
print(random_search.best_params_)
```

    /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages/sklearn/neural_network/_multilayer_perceptron.py:690: ConvergenceWarning: Stochastic Optimizer: Maximum iterations (121) reached and the optimization hasn't converged yet.
      warnings.warn(
    /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages/sklearn/neural_network/_multilayer_perceptron.py:690: ConvergenceWarning: Stochastic Optimizer: Maximum iterations (121) reached and the optimization hasn't converged yet.
      warnings.warn(
    /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages/sklearn/neural_network/_multilayer_perceptron.py:690: ConvergenceWarning: Stochastic Optimizer: Maximum iterations (121) reached and the optimization hasn't converged yet.
      warnings.warn(
    /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages/sklearn/neural_network/_multilayer_perceptron.py:690: ConvergenceWarning: Stochastic Optimizer: Maximum iterations (121) reached and the optimization hasn't converged yet.
      warnings.warn(


    {'activation': 'logistic', 'alpha': 0.001378323745500718, 'batch_size': 51, 'hidden_layer_sizes': (100, 100, 100), 'learning_rate': 'constant', 'learning_rate_init': 0.0224645516805326, 'max_iter': 373}


Let's display the results of the `RandomizedSearchCV` ordered by the `rank_test_score` in ascending order, i.e the best model will be the first one:


```python
# Utility function to report best scores
def report(results) -> pd.DataFrame:
    return pd.DataFrame(results).sort_values(by='rank_test_score')

report(random_search.cv_results_)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mean_fit_time</th>
      <th>std_fit_time</th>
      <th>mean_score_time</th>
      <th>std_score_time</th>
      <th>param_activation</th>
      <th>param_alpha</th>
      <th>param_batch_size</th>
      <th>param_hidden_layer_sizes</th>
      <th>param_learning_rate</th>
      <th>param_learning_rate_init</th>
      <th>param_max_iter</th>
      <th>params</th>
      <th>split0_test_score</th>
      <th>split1_test_score</th>
      <th>split2_test_score</th>
      <th>split3_test_score</th>
      <th>split4_test_score</th>
      <th>mean_test_score</th>
      <th>std_test_score</th>
      <th>rank_test_score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>1.383837</td>
      <td>0.731598</td>
      <td>0.001176</td>
      <td>0.000312</td>
      <td>logistic</td>
      <td>0.001378</td>
      <td>51</td>
      <td>(100, 100, 100)</td>
      <td>constant</td>
      <td>0.022465</td>
      <td>373</td>
      <td>{'activation': 'logistic', 'alpha': 0.00137832...</td>
      <td>0.853147</td>
      <td>0.846154</td>
      <td>0.845070</td>
      <td>0.739437</td>
      <td>0.823944</td>
      <td>0.821550</td>
      <td>0.042203</td>
      <td>1</td>
    </tr>
    <tr>
      <th>18</th>
      <td>1.059275</td>
      <td>0.161742</td>
      <td>0.002005</td>
      <td>0.001472</td>
      <td>tanh</td>
      <td>0.030296</td>
      <td>9</td>
      <td>(100, 100, 100)</td>
      <td>adaptive</td>
      <td>0.002151</td>
      <td>991</td>
      <td>{'activation': 'tanh', 'alpha': 0.030296104428...</td>
      <td>0.832168</td>
      <td>0.825175</td>
      <td>0.816901</td>
      <td>0.795775</td>
      <td>0.816901</td>
      <td>0.817384</td>
      <td>0.012220</td>
      <td>2</td>
    </tr>
    <tr>
      <th>10</th>
      <td>0.183286</td>
      <td>0.045650</td>
      <td>0.001136</td>
      <td>0.000354</td>
      <td>relu</td>
      <td>0.582938</td>
      <td>72</td>
      <td>(100, 100)</td>
      <td>adaptive</td>
      <td>0.036518</td>
      <td>801</td>
      <td>{'activation': 'relu', 'alpha': 0.582938454299...</td>
      <td>0.832168</td>
      <td>0.839161</td>
      <td>0.802817</td>
      <td>0.760563</td>
      <td>0.838028</td>
      <td>0.814547</td>
      <td>0.030065</td>
      <td>3</td>
    </tr>
    <tr>
      <th>13</th>
      <td>0.352146</td>
      <td>0.099077</td>
      <td>0.000993</td>
      <td>0.000032</td>
      <td>logistic</td>
      <td>0.003946</td>
      <td>88</td>
      <td>(100, 100, 100)</td>
      <td>adaptive</td>
      <td>0.003949</td>
      <td>491</td>
      <td>{'activation': 'logistic', 'alpha': 0.00394590...</td>
      <td>0.811189</td>
      <td>0.846154</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.823944</td>
      <td>0.810342</td>
      <td>0.028094</td>
      <td>4</td>
    </tr>
    <tr>
      <th>7</th>
      <td>0.174311</td>
      <td>0.034847</td>
      <td>0.002935</td>
      <td>0.003457</td>
      <td>relu</td>
      <td>0.112148</td>
      <td>44</td>
      <td>(100,)</td>
      <td>adaptive</td>
      <td>0.003312</td>
      <td>180</td>
      <td>{'activation': 'relu', 'alpha': 0.112147747841...</td>
      <td>0.811189</td>
      <td>0.825175</td>
      <td>0.802817</td>
      <td>0.795775</td>
      <td>0.809859</td>
      <td>0.808963</td>
      <td>0.009801</td>
      <td>5</td>
    </tr>
    <tr>
      <th>11</th>
      <td>0.462121</td>
      <td>0.070880</td>
      <td>0.001548</td>
      <td>0.000789</td>
      <td>logistic</td>
      <td>0.014657</td>
      <td>82</td>
      <td>(100, 100, 100)</td>
      <td>constant</td>
      <td>0.057578</td>
      <td>316</td>
      <td>{'activation': 'logistic', 'alpha': 0.01465655...</td>
      <td>0.790210</td>
      <td>0.818182</td>
      <td>0.866197</td>
      <td>0.788732</td>
      <td>0.774648</td>
      <td>0.807594</td>
      <td>0.032526</td>
      <td>6</td>
    </tr>
    <tr>
      <th>12</th>
      <td>0.256344</td>
      <td>0.054968</td>
      <td>0.001076</td>
      <td>0.000454</td>
      <td>logistic</td>
      <td>0.002647</td>
      <td>15</td>
      <td>(100,)</td>
      <td>constant</td>
      <td>0.001114</td>
      <td>620</td>
      <td>{'activation': 'logistic', 'alpha': 0.00264711...</td>
      <td>0.811189</td>
      <td>0.825175</td>
      <td>0.809859</td>
      <td>0.767606</td>
      <td>0.823944</td>
      <td>0.807554</td>
      <td>0.020945</td>
      <td>7</td>
    </tr>
    <tr>
      <th>15</th>
      <td>0.487919</td>
      <td>0.103331</td>
      <td>0.002321</td>
      <td>0.001240</td>
      <td>relu</td>
      <td>0.354908</td>
      <td>12</td>
      <td>(100, 100)</td>
      <td>constant</td>
      <td>0.001551</td>
      <td>506</td>
      <td>{'activation': 'relu', 'alpha': 0.354907954609...</td>
      <td>0.818182</td>
      <td>0.825175</td>
      <td>0.823944</td>
      <td>0.760563</td>
      <td>0.795775</td>
      <td>0.804728</td>
      <td>0.024491</td>
      <td>8</td>
    </tr>
    <tr>
      <th>2</th>
      <td>0.175925</td>
      <td>0.027044</td>
      <td>0.001418</td>
      <td>0.000492</td>
      <td>tanh</td>
      <td>0.004335</td>
      <td>64</td>
      <td>(100,)</td>
      <td>constant</td>
      <td>0.008179</td>
      <td>121</td>
      <td>{'activation': 'tanh', 'alpha': 0.004335281794...</td>
      <td>0.804196</td>
      <td>0.818182</td>
      <td>0.802817</td>
      <td>0.774648</td>
      <td>0.809859</td>
      <td>0.801940</td>
      <td>0.014678</td>
      <td>9</td>
    </tr>
    <tr>
      <th>16</th>
      <td>0.202151</td>
      <td>0.036667</td>
      <td>0.001558</td>
      <td>0.000597</td>
      <td>tanh</td>
      <td>0.009453</td>
      <td>99</td>
      <td>(100, 100)</td>
      <td>adaptive</td>
      <td>0.048276</td>
      <td>838</td>
      <td>{'activation': 'tanh', 'alpha': 0.009452571391...</td>
      <td>0.804196</td>
      <td>0.818182</td>
      <td>0.830986</td>
      <td>0.739437</td>
      <td>0.816901</td>
      <td>0.801940</td>
      <td>0.032382</td>
      <td>9</td>
    </tr>
    <tr>
      <th>19</th>
      <td>0.408444</td>
      <td>0.249645</td>
      <td>0.001749</td>
      <td>0.001226</td>
      <td>logistic</td>
      <td>0.004029</td>
      <td>96</td>
      <td>(100, 100, 100)</td>
      <td>constant</td>
      <td>0.008771</td>
      <td>195</td>
      <td>{'activation': 'logistic', 'alpha': 0.00402863...</td>
      <td>0.811189</td>
      <td>0.839161</td>
      <td>0.809859</td>
      <td>0.760563</td>
      <td>0.774648</td>
      <td>0.799084</td>
      <td>0.028106</td>
      <td>11</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.477488</td>
      <td>0.243106</td>
      <td>0.001512</td>
      <td>0.001013</td>
      <td>logistic</td>
      <td>0.019762</td>
      <td>49</td>
      <td>(100, 100, 100)</td>
      <td>adaptive</td>
      <td>0.015834</td>
      <td>575</td>
      <td>{'activation': 'logistic', 'alpha': 0.01976218...</td>
      <td>0.776224</td>
      <td>0.818182</td>
      <td>0.823944</td>
      <td>0.774648</td>
      <td>0.788732</td>
      <td>0.796346</td>
      <td>0.020843</td>
      <td>12</td>
    </tr>
    <tr>
      <th>4</th>
      <td>0.435755</td>
      <td>0.129903</td>
      <td>0.001075</td>
      <td>0.000446</td>
      <td>relu</td>
      <td>0.023346</td>
      <td>47</td>
      <td>(100, 100)</td>
      <td>adaptive</td>
      <td>0.014041</td>
      <td>343</td>
      <td>{'activation': 'relu', 'alpha': 0.023345864076...</td>
      <td>0.790210</td>
      <td>0.818182</td>
      <td>0.788732</td>
      <td>0.760563</td>
      <td>0.823944</td>
      <td>0.796326</td>
      <td>0.022867</td>
      <td>13</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.554353</td>
      <td>0.092895</td>
      <td>0.000993</td>
      <td>0.000320</td>
      <td>relu</td>
      <td>0.001995</td>
      <td>75</td>
      <td>(100,)</td>
      <td>adaptive</td>
      <td>0.001153</td>
      <td>869</td>
      <td>{'activation': 'relu', 'alpha': 0.001994916615...</td>
      <td>0.797203</td>
      <td>0.797203</td>
      <td>0.788732</td>
      <td>0.753521</td>
      <td>0.830986</td>
      <td>0.793529</td>
      <td>0.024713</td>
      <td>14</td>
    </tr>
    <tr>
      <th>8</th>
      <td>1.369865</td>
      <td>0.422551</td>
      <td>0.001508</td>
      <td>0.001143</td>
      <td>tanh</td>
      <td>0.097178</td>
      <td>2</td>
      <td>(100, 100)</td>
      <td>invscaling</td>
      <td>0.004206</td>
      <td>871</td>
      <td>{'activation': 'tanh', 'alpha': 0.097177753050...</td>
      <td>0.797203</td>
      <td>0.797203</td>
      <td>0.746479</td>
      <td>0.781690</td>
      <td>0.830986</td>
      <td>0.790712</td>
      <td>0.027365</td>
      <td>15</td>
    </tr>
    <tr>
      <th>0</th>
      <td>0.423980</td>
      <td>0.093149</td>
      <td>0.001491</td>
      <td>0.000477</td>
      <td>relu</td>
      <td>0.245261</td>
      <td>15</td>
      <td>(100, 100, 100)</td>
      <td>constant</td>
      <td>0.061738</td>
      <td>221</td>
      <td>{'activation': 'relu', 'alpha': 0.245261263113...</td>
      <td>0.790210</td>
      <td>0.825175</td>
      <td>0.781690</td>
      <td>0.767606</td>
      <td>0.781690</td>
      <td>0.789274</td>
      <td>0.019361</td>
      <td>16</td>
    </tr>
    <tr>
      <th>9</th>
      <td>0.277335</td>
      <td>0.052397</td>
      <td>0.001090</td>
      <td>0.000165</td>
      <td>tanh</td>
      <td>0.001241</td>
      <td>18</td>
      <td>(100, 100)</td>
      <td>invscaling</td>
      <td>0.015327</td>
      <td>369</td>
      <td>{'activation': 'tanh', 'alpha': 0.001241480428...</td>
      <td>0.776224</td>
      <td>0.804196</td>
      <td>0.760563</td>
      <td>0.760563</td>
      <td>0.774648</td>
      <td>0.775239</td>
      <td>0.015941</td>
      <td>17</td>
    </tr>
    <tr>
      <th>14</th>
      <td>0.471379</td>
      <td>0.198399</td>
      <td>0.001022</td>
      <td>0.000309</td>
      <td>relu</td>
      <td>0.234707</td>
      <td>5</td>
      <td>(100, 100)</td>
      <td>adaptive</td>
      <td>0.089791</td>
      <td>127</td>
      <td>{'activation': 'relu', 'alpha': 0.234707313030...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.767606</td>
      <td>0.774648</td>
      <td>0.619718</td>
      <td>0.681345</td>
      <td>0.073346</td>
      <td>18</td>
    </tr>
    <tr>
      <th>6</th>
      <td>0.391357</td>
      <td>0.108227</td>
      <td>0.001503</td>
      <td>0.001053</td>
      <td>logistic</td>
      <td>0.788671</td>
      <td>9</td>
      <td>(100, 100)</td>
      <td>constant</td>
      <td>0.004928</td>
      <td>191</td>
      <td>{'activation': 'logistic', 'alpha': 0.78867141...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>19</td>
    </tr>
    <tr>
      <th>17</th>
      <td>0.345910</td>
      <td>0.053180</td>
      <td>0.001346</td>
      <td>0.000855</td>
      <td>logistic</td>
      <td>0.822501</td>
      <td>3</td>
      <td>(100,)</td>
      <td>constant</td>
      <td>0.048287</td>
      <td>866</td>
      <td>{'activation': 'logistic', 'alpha': 0.82250071...</td>
      <td>0.622378</td>
      <td>0.622378</td>
      <td>0.626761</td>
      <td>0.626761</td>
      <td>0.619718</td>
      <td>0.623599</td>
      <td>0.002758</td>
      <td>19</td>
    </tr>
  </tbody>
</table>
</div>



We see that the hyperparameter search does not improve the model performance.


```python
y_pred = random_search.predict(test_df)
print(classification_report(y_test, y_pred))
```

                  precision    recall  f1-score   support
    
               0       0.83      0.84      0.83       105
               1       0.77      0.76      0.76        74
    
        accuracy                           0.80       179
       macro avg       0.80      0.80      0.80       179
    weighted avg       0.80      0.80      0.80       179
    



```python
#end solution
```

# Conclusion

In this notebook, we have seen how to train a multilayer perceptron model to predict whether a passenger survived the Titanic disaster. We have seen that a multilayer perceptron model can outperform a logistic regression model. We have also seen how to optimize the hyperparameters of the model using a randomized search. We have seen that the hyperparameter search does not improve the model performance.
