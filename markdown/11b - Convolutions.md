# Convolutions and Image Processing

In the next notebook, we will present some deep-learning network that will greatly improve the image classification from the previous notebook. These so-called convolutional networks shook the world in the early 2010s when they improved the classification performance on the famous **ImageNet** dataset with a large margin. Convolutional neural networks are neural networks that have convolutional layers. A convolutional layer contains several convolutional kernels. To understand how convolutional neural networks work, it is important to understand what convolutional kernels (and convolutions) are.

Convolutions are a mathematical operation that takes two inputs and produces a third output. In the context of image processing, the two inputs are the image and a filter (also called a kernel), and the output is a new image. The filter is a small matrix that slides over the image, and at each position, it multiplies the pixel values of the image by the values of the filter, and then sums the result. This operation is repeated for every pixel in the image, and the result is a new image. You can see how this works in the image below:

![Demonstration of a convolutional kernel](./convolutional_kernel.gif)

In this notebook, you will explore how convolutions/convolutional kernels work and how they can be used for image processing. We will start loading the data.


```python
!pip install datasets 
```

    Requirement already satisfied: datasets in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (2.14.6)
    Requirement already satisfied: numpy>=1.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (1.26.1)
    Requirement already satisfied: pyarrow>=8.0.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (14.0.0)
    Requirement already satisfied: dill<0.3.8,>=0.3.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.3.7)
    Requirement already satisfied: pandas in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (2.1.2)
    Requirement already satisfied: requests>=2.19.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (2.31.0)
    Requirement already satisfied: tqdm>=4.62.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (4.66.1)
    Requirement already satisfied: xxhash in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (3.4.1)
    Requirement already satisfied: multiprocess in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.70.15)
    Requirement already satisfied: fsspec<=2023.10.0,>=2023.1.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from fsspec[http]<=2023.10.0,>=2023.1.0->datasets) (2023.10.0)
    Requirement already satisfied: aiohttp in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (3.8.6)
    Requirement already satisfied: huggingface-hub<1.0.0,>=0.14.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (0.28.1)
    Requirement already satisfied: packaging in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (23.2)
    Requirement already satisfied: pyyaml>=5.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from datasets) (6.0.1)
    Requirement already satisfied: attrs>=17.3.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (23.1.0)
    Requirement already satisfied: charset-normalizer<4.0,>=2.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (3.3.2)
    Requirement already satisfied: multidict<7.0,>=4.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (6.0.4)
    Requirement already satisfied: async-timeout<5.0,>=4.0.0a3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (4.0.3)
    Requirement already satisfied: yarl<2.0,>=1.0 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.9.2)
    Requirement already satisfied: frozenlist>=1.1.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.4.0)
    Requirement already satisfied: aiosignal>=1.1.2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from aiohttp->datasets) (1.3.1)
    Requirement already satisfied: filelock in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub<1.0.0,>=0.14.0->datasets) (3.13.1)
    Requirement already satisfied: typing-extensions>=3.7.4.3 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from huggingface-hub<1.0.0,>=0.14.0->datasets) (4.8.0)
    Requirement already satisfied: idna<4,>=2.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (3.4)
    Requirement already satisfied: urllib3<3,>=1.21.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (2.0.7)
    Requirement already satisfied: certifi>=2017.4.17 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from requests>=2.19.0->datasets) (2023.7.22)
    Requirement already satisfied: python-dateutil>=2.8.2 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2.8.2)
    Requirement already satisfied: pytz>=2020.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2023.3.post1)
    Requirement already satisfied: tzdata>=2022.1 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from pandas->datasets) (2023.3)
    Requirement already satisfied: six>=1.5 in /Users/timdejong/.pyenv/versions/3.11.4/envs/ai_course/lib/python3.11/site-packages (from python-dateutil>=2.8.2->pandas->datasets) (1.16.0)



```python
from datasets import load_dataset

dataset = load_dataset("cats_vs_dogs", split="train")
```

    /Users/timdejong/.pyenv/versions/ai_course/lib/python3.11/site-packages/datasets/table.py:1421: FutureWarning: promote has been superseded by mode='default'.
      table = cls._concat_blocks(blocks, axis=0)


## Create a sample of images

The cats and dogs dataset is quite large. If we would use the entire dataset, the processing time would be too long. Therefore, we will create a sample of the dataset. We will also use the images that have a width of at least 300 pixels. From this selection, we create a sample of 500 images.


```python
from itertools import islice
import numpy as np

def memory_efficient_sampler(dataset, sample_size, width_threshold=300, batch_size=100):
    """
    Yields indices of selected wide images in memory-efficient batches
    """
    selected = 0
    
    # Create shuffled indices
    all_indices = np.random.permutation(len(dataset))
    print("all indices", all_indices)
    
    # Process dataset in batches
    for i in range(0, len(all_indices), batch_size):
        batch_indices = all_indices[i:min(i + batch_size, len(all_indices))]
        batch = dataset.select(batch_indices)
        
        # Find wide images in current batch
        selected_batch_indices = [idx for idx, img in enumerate(batch['image']) 
                        if img.size[0] > width_threshold ]
        wide_indices = [batch_indices[idx] for idx in selected_batch_indices]

        # Randomly select from current batch
        batch_selection = np.random.choice(
            wide_indices, 
            size=min(len(wide_indices), sample_size - selected),
            replace=False
        ).tolist()
        
        # Update tracking
        selected += len(batch_selection)
        
        # Yield selected indices
        yield from batch_selection
        
        if selected >= sample_size:
            break
```


```python
from random import sample
import numpy as np

# Use the generator to get indices
selected_indices = list(memory_efficient_sampler(dataset, sample_size=500))
dataset = dataset.select(selected_indices)

X = dataset['image']
y_true = dataset['labels']
```

    all indices [ 7503  4541 13285 ... 21448 10248 22324]


## Some image examples

Before we extract the features and train a machine learning algorithm, let's first take a look at some examples of the images in the dataset. Every time you run the cell below, a random image of our sample will be shown.


```python
import matplotlib.pyplot as plt
import random

image = X[random.randint(0, len(X)-1)]
plt.imshow(image)
```




    <matplotlib.image.AxesImage at 0x324279f10>




    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_7_1.png)
    


## Convolutional Kernels

Convolutional kernels are well known constructs in computer vision that are used for a variety of purposes.  A convolutional kernel consists of a matrix that is *convolved* with the image; the kernel is used to calculate a weighted sum of an area of an image at a certain position (x, y). When applied to all the pixels in the image they can be used for example for blurring, sharpening, and edge detection. Below you will explore several well-known convolutional kernels. 

We will start with the **Sobel** kernels to bring out the edges in some of the cats and dogs images.

### Sobel Kernels

**Sobel kernels** are used for edge detection. They are used to calculate the gradient of the image intensity at each pixel. The Sobel kernels are defined as follows:

$$
\begin{bmatrix}
-1 & 0 & 1 \\
-2 & 0 & 2 \\
-1 & 0 & 1
\end{bmatrix}

\begin{bmatrix}
-1 & -2 & -1 \\
0 & 0 & 0 \\
1 & 2 & 1
\end{bmatrix}

$$

The first kernel is used to calculate the gradient in the x-direction, and the second kernel is used to calculate the gradient in the y-direction. The gradient in the x-direction is used to detect vertical edges, and the gradient in the y-direction is used to detect horizontal edges. In the code below, we will apply the Sobel kernels to some of the images in the dataset.


```python
from scipy.signal import convolve2d

sobel_x = np.array([[-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1]
                   ])

sobel_y = np.array([[-1, -2, -1],
                    [0, 0, 0],
                    [1, 2, 1]
                   ])

number_of_images = 5
_, ax = plt.subplots(number_of_images, 4, figsize=(10, 10))

random_images = sample(X, number_of_images)
for i, image in enumerate(random_images):
    ax[i, 0].imshow(image)
    gray_level_image = np.array(image).mean(axis=2)

    ax[i, 1].imshow(gray_level_image,cmap="gray")
    ax[i, 2].imshow(convolve2d(gray_level_image, sobel_x), cmap="gray")
    ax[i, 3].imshow(convolve2d(gray_level_image, sobel_y), cmap="gray")
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_9_0.png)
    


### Sharpening Kernel

The **Sharpening kernel** helps enhancing the edges of an image it makes them appear clearer and more defined. It is defined as follows:

$$
\begin{bmatrix}
0 & -1 & 0 \\
-1 & 5 & -1 \\
0 & -1 & 0
\end{bmatrix}
$$

Below you can try to apply the Sharpening kernel to some of the images in the dataset. Hint: use the code for the Sobel kernels as a starting point.


```python
def apply_kernel(images, number_of_images, kernel):
    _, ax = plt.subplots(number_of_images, 3, figsize=(10, 10))
    random_images = sample(images, number_of_images)
    for i, image in enumerate(random_images):
        ax[i, 0].imshow(image)
        gray_level_image = np.array(image).mean(axis=2)

        ax[i, 1].imshow(gray_level_image,cmap="gray")
        convolved_image = convolve2d(gray_level_image, kernel)
        convolved_image = (convolved_image - convolved_image.min()) / (convolved_image.max() - convolved_image.min())
        ax[i, 2].imshow(convolved_image, cmap="gray")
```


```python
sharpening_kernel = np.array([[0, -1, 0],
                            [-1, 5, -1],
                            [0, -1, 0]
                           ])

apply_kernel(X, 5, sharpening_kernel)
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_12_0.png)
    


### Gaussian Blur Kernel

The **Gaussian blur kernel** is used to blur the image. Blurring is an operation that is used to reduce the noise in the image. 
It can be defined as follows:

$$
\begin{bmatrix}
1 & 2 & 1 \\
2 & 4 & 2 \\
1 & 2 & 1
\end{bmatrix}
$$

but there are also alternative and larger kernels that can be used for blurring. Like this one:

$$
\begin{bmatrix}
1 & 4 & 6 & 4 & 1 \\
4 & 16 & 24 & 16 & 4 \\
6 & 24 & 36 & 24 & 6 \\
4 & 16 & 24 & 16 & 4 \\
1 & 4 & 6 & 4 & 1
\end{bmatrix}
$$

Below you can try to apply the Gaussian blur kernel to some of the images in the dataset. 


```python
gaussian_kernel = np.array([[1, 4, 6, 4, 1],
                             [4, 16, 24, 16, 4],
                             [6, 24, 36, 24, 6],
                             [4, 16, 24, 16, 4],
                             [1, 4, 6, 4, 1]])

number_of_images = 5
apply_kernel(X, number_of_images, gaussian_kernel)
                            
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_14_0.png)
    


### Emboss Kernel

The **Emboss kernel** is used to give a 3D effect to the image. It is defined as follows:

$$
\begin{bmatrix}
-2 & -1 & 0 \\
-1 & 1 & 1 \\
0 & 1 & 2
\end{bmatrix}
$$

Below you can try to apply the Emboss kernel to some of the images in the dataset.


```python
emboss_kernel = np.array([[-2, -1, 0],
                            [-1, 1, 1],
                            [0, 1, 2]])

number_of_images = 5
apply_kernel(X, number_of_images, emboss_kernel)
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_16_0.png)
    


## Many more kernels

There are many more examples of convolutional kernels that can be used for image processing. For example there are still the following types of kernels:
- **Outline kernel** is used to outline the image. It is defined as follows:
  $$ \begin{bmatrix} -1 & -1 & -1 \\ -1 & 8 & -1 \\ -1 & -1 & -1 \end{bmatrix} $$
- **Laplace kernel** is used for edge detection. It is defined as follows:
  $$ \begin{bmatrix} 0 & 1 & 0 \\ 1 & -4 & 1 \\ 0 & 1 & 0 \end{bmatrix} $$  
- **Prewitt kernel** is used for edge detection. It is defined as follows:
  $$ \begin{bmatrix} 1 & 1 & 1 \\ 0 & 0 & 0 \\ -1 & -1 & -1 \end{bmatrix} $$ 
  
  or 

  $$ \begin{bmatrix} 1 & 0 & -1 \\ 1 & 0 & -1 \\ 1 & 0 & -1 \end{bmatrix} $$
- **Motion blur kernel** is used to simulate motion blur. It is defined as follows:
  $$ \begin{bmatrix} 1/5 & 0 & 0 & 0 & 0 \\ 0 & 1/5 & 0 & 0 & 0 \\ 0 & 0 & 1/5 & 0 & 0 \\ 0 & 0 & 0 & 1/5 & 0 \\ 0 & 0 & 0 & 0 & 1/5 \end{bmatrix} $$

And many more can be found online... Below try to apply some of these kernels to some of the images in the dataset. This will give you an idea of how these kernels work and what they can be used for.


```python
outline_kernel = np.array([[-1, -1, -1],
                            [-1, 8, -1],
                            [-1, -1, -1]])

number_of_images = 5
apply_kernel(X, number_of_images, outline_kernel)
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_18_0.png)
    



```python
motion_blur = np.identity(5) / 5
number_of_images = 5
apply_kernel(X, number_of_images, motion_blur)
```


    
![png](11b%20-%20Convolutions_files/11b%20-%20Convolutions_19_0.png)
    


# Conclusion

In this notebook, you have learned about convolutional kernels and how they can be used for image processing. You have seen how the Sobel kernels can be used for edge detection, the sharpening kernel can be used to enhance the edges of an image, the Gaussian blur kernel can be used to blur the image, and the emboss kernel can be used to give a 3D effect to the image. You have also seen that there are many more convolutional kernels that can be used for image processing. These kernels have been used extensively in image processing software. They are also the building blocks for convolutional neural networks, which are used for image classification, object detection, and many other tasks in computer vision. We will look at convolutional neural networks in the next notebook.

For more information on convolutional kernels, we recommend the following resources:
- [Kernel operations](https://en.wikipedia.org/wiki/Kernel_(image_processing))
- [Types of convolutional kernels](https://www.geeksforgeeks.org/types-of-convolution-kernels/)
- [Image kernels: explained visually](https://setosa.io/ev/image-kernels/)
