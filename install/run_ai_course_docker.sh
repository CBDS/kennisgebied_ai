#!/bin/bash
docker run -it --rm -v $(realpath ../notebooks/):/tf/notebooks -v $(realpath ../data):/tf/data --runtime=nvidia -p 8888:8888 cbs_academy/ai_course_jupyter:latest

